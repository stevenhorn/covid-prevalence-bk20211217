0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1885.50    52.22
p_loo       62.78        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      554   87.4%
 (0.5, 0.7]   (ok)         61    9.6%
   (0.7, 1]   (bad)        16    2.5%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.173  0.020   0.150    0.215  ...   107.0      99.0      58.0   0.99
pu        0.707  0.007   0.700    0.721  ...    88.0      53.0      35.0   1.01
mu        0.126  0.016   0.097    0.152  ...    25.0      26.0      93.0   1.03
mus       0.272  0.047   0.199    0.374  ...   125.0     112.0      60.0   1.01
gamma     0.411  0.064   0.293    0.517  ...   123.0     138.0      74.0   1.03
Is_begin  0.768  0.719   0.002    2.162  ...    90.0      59.0      47.0   1.06
Ia_begin  0.957  0.921   0.008    2.852  ...    80.0      65.0      60.0   1.07
E_begin   0.484  0.554   0.007    1.374  ...   102.0      68.0      79.0   1.00

[8 rows x 11 columns]