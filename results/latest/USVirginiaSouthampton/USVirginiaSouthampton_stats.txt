0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -1812.87    59.56
p_loo       70.76        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      591   93.5%
 (0.5, 0.7]   (ok)         29    4.6%
   (0.7, 1]   (bad)         9    1.4%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.182  0.025   0.151    0.231  ...   151.0     102.0     100.0   1.02
pu        0.709  0.007   0.700    0.726  ...    54.0      32.0      38.0   1.02
mu        0.148  0.019   0.124    0.186  ...    12.0      16.0      57.0   1.11
mus       0.279  0.035   0.205    0.331  ...    22.0      21.0      60.0   1.09
gamma     0.504  0.070   0.395    0.646  ...    54.0      57.0      87.0   1.04
Is_begin  0.542  0.581   0.002    1.624  ...    54.0      45.0      59.0   1.03
Ia_begin  0.729  0.829   0.010    2.061  ...    93.0     120.0      87.0   1.01
E_begin   0.319  0.339   0.000    0.920  ...   110.0      75.0      20.0   0.99

[8 rows x 11 columns]