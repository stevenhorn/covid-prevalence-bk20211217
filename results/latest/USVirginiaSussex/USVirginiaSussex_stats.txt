0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -1561.76    46.34
p_loo       54.71        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      580   91.8%
 (0.5, 0.7]   (ok)         40    6.3%
   (0.7, 1]   (bad)        10    1.6%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.180  0.021   0.153    0.220  ...    66.0      60.0      59.0   1.01
pu        0.711  0.010   0.700    0.731  ...   152.0      62.0      40.0   1.03
mu        0.185  0.025   0.138    0.231  ...     7.0       7.0      24.0   1.25
mus       0.271  0.035   0.191    0.329  ...   120.0     111.0      65.0   1.02
gamma     0.418  0.076   0.257    0.529  ...   152.0     152.0      65.0   1.08
Is_begin  0.560  0.512   0.005    1.654  ...    85.0      69.0      93.0   1.01
Ia_begin  1.212  1.502   0.014    3.626  ...    60.0      68.0      57.0   1.07
E_begin   0.460  0.594   0.001    1.649  ...    77.0      88.0      83.0   0.99

[8 rows x 11 columns]