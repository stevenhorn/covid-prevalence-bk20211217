0 Divergences 
Passed validation 
2021-12-09 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 635 log-likelihood matrix

         Estimate       SE
elpd_loo -2714.78    40.05
p_loo       29.44        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      589   92.8%
 (0.5, 0.7]   (ok)         36    5.7%
   (0.7, 1]   (bad)        10    1.6%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.160  0.010   0.150    0.180  ...    20.0       9.0      39.0   1.19
pu        0.703  0.003   0.700    0.709  ...   126.0      73.0      60.0   1.00
mu        0.111  0.014   0.089    0.132  ...    18.0      17.0      54.0   1.11
mus       0.163  0.023   0.130    0.216  ...    53.0      58.0      26.0   1.05
gamma     0.236  0.039   0.177    0.313  ...    23.0      41.0      37.0   1.05
Is_begin  0.696  0.709   0.006    2.249  ...    46.0      18.0      46.0   1.09
Ia_begin  0.618  0.542   0.022    1.731  ...   152.0     152.0      93.0   1.00
E_begin   0.339  0.360   0.012    1.110  ...    13.0      17.0      25.0   1.10

[8 rows x 11 columns]