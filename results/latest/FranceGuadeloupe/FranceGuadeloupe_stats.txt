0 Divergences 
Failed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 633 log-likelihood matrix

         Estimate       SE
elpd_loo -3653.13    70.68
p_loo       20.86        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      616   97.3%
 (0.5, 0.7]   (ok)         14    2.2%
   (0.7, 1]   (bad)         3    0.5%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.190  0.003   0.186    0.197  ...     4.0       4.0      27.0   1.69
pu        0.704  0.000   0.703    0.705  ...     3.0       3.0      37.0   2.50
mu        0.249  0.005   0.243    0.254  ...     3.0       3.0      22.0   2.77
mus       0.241  0.021   0.219    0.262  ...     3.0       3.0      16.0   2.79
gamma     0.244  0.006   0.237    0.251  ...     3.0       3.0      18.0   2.00
Is_begin  0.669  0.106   0.525    0.831  ...     3.0       3.0      18.0   3.02
Ia_begin  1.591  0.408   1.107    2.080  ...     3.0       3.0      24.0   1.95
E_begin   3.120  0.724   2.301    4.006  ...     3.0       4.0      22.0   1.84

[8 rows x 11 columns]