0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1892.57    53.30
p_loo       46.28        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      596   94.0%
 (0.5, 0.7]   (ok)         27    4.3%
   (0.7, 1]   (bad)         9    1.4%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.158  0.008   0.150    0.178  ...   152.0     152.0      96.0   1.00
pu        0.703  0.002   0.700    0.707  ...   131.0     114.0      93.0   1.06
mu        0.137  0.016   0.108    0.163  ...    56.0      56.0      81.0   1.02
mus       0.225  0.036   0.163    0.291  ...    82.0      90.0      46.0   0.99
gamma     0.300  0.036   0.236    0.354  ...   102.0      94.0      59.0   1.02
Is_begin  0.825  0.992   0.001    2.969  ...    92.0      22.0      15.0   1.10
Ia_begin  1.235  1.043   0.082    2.929  ...   109.0     124.0      88.0   1.00
E_begin   0.563  0.631   0.015    1.641  ...    85.0      72.0      59.0   1.03

[8 rows x 11 columns]