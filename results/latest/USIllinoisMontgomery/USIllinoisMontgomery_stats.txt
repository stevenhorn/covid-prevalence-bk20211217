0 Divergences 
Failed validation 
2021-12-09 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 635 log-likelihood matrix

         Estimate       SE
elpd_loo -1934.26    50.76
p_loo       43.36        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      592   93.2%
 (0.5, 0.7]   (ok)         36    5.7%
   (0.7, 1]   (bad)         6    0.9%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.163  0.013   0.150    0.188  ...   117.0      95.0      24.0   1.00
pu        0.704  0.003   0.700    0.709  ...    99.0      77.0      58.0   1.02
mu        0.097  0.012   0.077    0.117  ...    23.0      22.0      35.0   1.07
mus       0.178  0.028   0.133    0.226  ...    82.0      97.0      45.0   1.07
gamma     0.270  0.049   0.201    0.380  ...    62.0     104.0      60.0   1.02
Is_begin  0.513  0.501   0.021    1.618  ...    14.0      40.0      59.0   1.06
Ia_begin  0.936  0.891   0.025    2.869  ...    95.0     100.0      87.0   1.03
E_begin   0.392  0.339   0.002    1.027  ...    89.0      62.0      57.0   1.03

[8 rows x 11 columns]