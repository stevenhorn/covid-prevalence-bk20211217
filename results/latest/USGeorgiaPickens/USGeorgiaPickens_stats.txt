0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1952.52    43.10
p_loo       40.30        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      599   94.5%
 (0.5, 0.7]   (ok)         28    4.4%
   (0.7, 1]   (bad)         7    1.1%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.165  0.014   0.150    0.186  ...    91.0      88.0      49.0   1.00
pu        0.705  0.004   0.700    0.713  ...   152.0     152.0      60.0   1.01
mu        0.110  0.020   0.078    0.147  ...     9.0       8.0      20.0   1.20
mus       0.177  0.027   0.123    0.220  ...    13.0      15.0      59.0   1.10
gamma     0.242  0.041   0.176    0.318  ...    71.0      71.0      60.0   1.01
Is_begin  0.808  0.710   0.030    2.221  ...   152.0     142.0     100.0   1.00
Ia_begin  0.541  0.436   0.015    1.379  ...    70.0      50.0      52.0   0.99
E_begin   0.410  0.420   0.004    1.093  ...   113.0     105.0      84.0   1.00

[8 rows x 11 columns]