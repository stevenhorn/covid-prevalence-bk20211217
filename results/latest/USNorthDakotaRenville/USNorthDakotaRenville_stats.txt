0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo  -795.53    36.88
p_loo       51.82        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      584   92.1%
 (0.5, 0.7]   (ok)         34    5.4%
   (0.7, 1]   (bad)        12    1.9%
   (1, Inf)   (very bad)    4    0.6%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.232  0.047   0.162    0.325  ...    81.0     103.0      38.0   0.99
pu        0.730  0.015   0.705    0.756  ...    90.0      92.0      60.0   1.00
mu        0.169  0.029   0.126    0.235  ...    14.0      16.0      49.0   1.14
mus       0.276  0.045   0.204    0.366  ...   152.0     152.0      59.0   1.01
gamma     0.368  0.051   0.281    0.455  ...   152.0     139.0      88.0   1.06
Is_begin  0.624  0.703   0.012    2.253  ...    56.0      97.0      60.0   1.04
Ia_begin  0.793  1.014   0.000    2.884  ...   103.0     105.0      55.0   1.00
E_begin   0.329  0.540   0.000    1.116  ...    96.0      43.0      38.0   1.03

[8 rows x 11 columns]