0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -3932.57    50.52
p_loo       40.29        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      581   91.9%
 (0.5, 0.7]   (ok)         42    6.6%
   (0.7, 1]   (bad)         8    1.3%
   (1, Inf)   (very bad)    1    0.2%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.173   0.016   0.151    0.205  ...   131.0     144.0      96.0   1.02
pu         0.707   0.006   0.700    0.718  ...   152.0     124.0      59.0   1.02
mu         0.102   0.010   0.079    0.116  ...    41.0      41.0      19.0   1.02
mus        0.171   0.027   0.125    0.221  ...    91.0      88.0      80.0   1.01
gamma      0.314   0.051   0.221    0.394  ...    61.0      59.0      60.0   0.98
Is_begin  27.467  18.715   0.582   61.442  ...    50.0      39.0      43.0   1.03
Ia_begin  62.970  32.126  14.267  113.866  ...    67.0      60.0      59.0   1.00
E_begin   43.527  27.420   2.168   90.238  ...    68.0      64.0      46.0   1.02

[8 rows x 11 columns]