0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo  -133.27    61.20
p_loo       65.03        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      587   92.6%
 (0.5, 0.7]   (ok)         32    5.0%
   (0.7, 1]   (bad)        13    2.1%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.259  0.055   0.153    0.340  ...   137.0     152.0      39.0   1.00
pu        0.805  0.024   0.754    0.839  ...    67.0      64.0      57.0   1.03
mu        0.146  0.029   0.097    0.189  ...    65.0      64.0      60.0   0.99
mus       0.263  0.034   0.210    0.325  ...    33.0      33.0      59.0   1.02
gamma     0.280  0.039   0.223    0.358  ...    96.0      63.0      60.0   1.07
Is_begin  0.642  0.699   0.001    1.869  ...    45.0      24.0      15.0   1.05
Ia_begin  1.241  1.265   0.001    3.922  ...    48.0      65.0      40.0   0.99
E_begin   0.361  0.356   0.005    1.100  ...    63.0      56.0      39.0   1.00

[8 rows x 11 columns]