0 Divergences 
Failed validation 
2021-12-09 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 635 log-likelihood matrix

         Estimate       SE
elpd_loo -2714.19    57.28
p_loo      101.53        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      591   93.1%
 (0.5, 0.7]   (ok)         31    4.9%
   (0.7, 1]   (bad)        11    1.7%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.157  0.006   0.150    0.168  ...     8.0       6.0      14.0   1.30
pu        0.702  0.002   0.700    0.707  ...    62.0      22.0      54.0   1.09
mu        0.121  0.045   0.061    0.183  ...     3.0       3.0      22.0   2.29
mus       0.207  0.048   0.141    0.287  ...     5.0       5.0      22.0   1.49
gamma     0.202  0.137   0.051    0.386  ...     3.0       3.0      24.0   2.24
Is_begin  0.647  0.731   0.010    2.363  ...    64.0      51.0      57.0   1.00
Ia_begin  0.604  0.936   0.009    2.131  ...    31.0       9.0      21.0   1.22
E_begin   0.396  0.519   0.010    1.240  ...    32.0      48.0      22.0   1.06

[8 rows x 11 columns]