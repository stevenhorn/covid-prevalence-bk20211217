0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 633 log-likelihood matrix

         Estimate       SE
elpd_loo   320.41   129.07
p_loo      142.18        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      538   85.0%
 (0.5, 0.7]   (ok)         25    3.9%
   (0.7, 1]   (bad)        54    8.5%
   (1, Inf)   (very bad)   16    2.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.163  0.014   0.150    0.191  ...    16.0      19.0      57.0   1.09
pu        0.705  0.008   0.700    0.717  ...    86.0      62.0      42.0   1.04
mu        0.237  0.045   0.173    0.317  ...    19.0      21.0      60.0   1.09
mus       0.502  0.047   0.400    0.579  ...   134.0     123.0      58.0   0.99
gamma     0.568  0.060   0.472    0.690  ...    77.0      75.0      60.0   1.02
Is_begin  0.882  0.754   0.024    2.478  ...    87.0      72.0      86.0   1.00
Ia_begin  1.163  1.037   0.017    3.330  ...    62.0      40.0      40.0   1.05
E_begin   0.459  0.464   0.029    1.189  ...   100.0      80.0      60.0   1.00

[8 rows x 11 columns]