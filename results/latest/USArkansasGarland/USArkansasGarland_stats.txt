0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2470.23    41.87
p_loo       45.23        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      588   92.7%
 (0.5, 0.7]   (ok)         34    5.4%
   (0.7, 1]   (bad)         9    1.4%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.186  0.025   0.151    0.226  ...    25.0      26.0      59.0   1.06
pu        0.711  0.008   0.700    0.727  ...    64.0      49.0      59.0   1.05
mu        0.113  0.019   0.082    0.147  ...     8.0       7.0      21.0   1.25
mus       0.171  0.032   0.126    0.225  ...    45.0      35.0      58.0   1.03
gamma     0.263  0.041   0.204    0.345  ...    67.0      65.0      91.0   1.02
Is_begin  1.092  0.983   0.046    2.716  ...    62.0      53.0      59.0   0.98
Ia_begin  1.740  1.879   0.017    5.498  ...    34.0      20.0      43.0   1.07
E_begin   1.051  1.175   0.022    3.524  ...    67.0      30.0      25.0   1.07

[8 rows x 11 columns]