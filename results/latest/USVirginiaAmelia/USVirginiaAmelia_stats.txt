0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -1459.17    45.91
p_loo       47.98        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      593   93.8%
 (0.5, 0.7]   (ok)         31    4.9%
   (0.7, 1]   (bad)         6    0.9%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.243  0.059   0.151    0.331  ...    32.0      29.0      22.0   1.06
pu        0.765  0.024   0.729    0.819  ...     8.0       8.0      24.0   1.21
mu        0.124  0.018   0.096    0.165  ...    25.0      28.0      79.0   1.07
mus       0.186  0.036   0.121    0.242  ...    38.0      51.0      84.0   1.04
gamma     0.240  0.042   0.165    0.317  ...    55.0      58.0      57.0   1.02
Is_begin  0.680  0.631   0.025    2.085  ...    67.0      51.0      30.0   0.99
Ia_begin  1.358  1.352   0.039    3.015  ...   108.0     106.0      60.0   1.03
E_begin   0.562  0.903   0.005    1.564  ...    88.0      62.0      35.0   0.98

[8 rows x 11 columns]