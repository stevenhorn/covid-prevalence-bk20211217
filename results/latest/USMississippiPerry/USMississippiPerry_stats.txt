0 Divergences 
Passed validation 
2021-12-09 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 635 log-likelihood matrix

         Estimate       SE
elpd_loo -1553.84    39.74
p_loo       46.58        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      583   91.8%
 (0.5, 0.7]   (ok)         41    6.5%
   (0.7, 1]   (bad)        10    1.6%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.199  0.032   0.151    0.257  ...    17.0      10.0      38.0   1.19
pu        0.716  0.012   0.700    0.740  ...    60.0      54.0      38.0   1.02
mu        0.138  0.019   0.106    0.175  ...    36.0      40.0      96.0   1.06
mus       0.204  0.028   0.146    0.245  ...    94.0      92.0      88.0   1.01
gamma     0.320  0.046   0.247    0.397  ...    10.0      10.0      74.0   1.18
Is_begin  0.764  0.690   0.003    2.096  ...    78.0      55.0      68.0   1.01
Ia_begin  1.662  1.476   0.177    4.996  ...    76.0      69.0      93.0   1.04
E_begin   0.599  0.607   0.007    1.773  ...    94.0      76.0      42.0   1.01

[8 rows x 11 columns]