0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1874.51   116.83
p_loo       49.94        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      608   95.9%
 (0.5, 0.7]   (ok)         17    2.7%
   (0.7, 1]   (bad)         6    0.9%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.157  0.006   0.151    0.168  ...    63.0      94.0      58.0   1.00
pu        0.702  0.002   0.700    0.707  ...    95.0      54.0      58.0   1.02
mu        0.099  0.010   0.083    0.118  ...    39.0      40.0      60.0   1.01
mus       0.324  0.038   0.249    0.382  ...    91.0      96.0      65.0   1.00
gamma     0.803  0.100   0.642    0.968  ...    71.0      72.0      60.0   1.01
Is_begin  0.860  0.788   0.008    2.351  ...    50.0      45.0      60.0   1.02
Ia_begin  1.202  1.146   0.004    3.455  ...    50.0      45.0      59.0   1.03
E_begin   0.510  0.432   0.013    1.257  ...    41.0      25.0      77.0   1.07

[8 rows x 11 columns]