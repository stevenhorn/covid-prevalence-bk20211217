0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -3052.86    45.50
p_loo       46.86        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      601   94.8%
 (0.5, 0.7]   (ok)         26    4.1%
   (0.7, 1]   (bad)         4    0.6%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.165  0.011   0.153    0.188  ...    85.0      80.0      59.0   1.08
pu        0.704  0.004   0.700    0.712  ...   108.0      65.0      38.0   1.02
mu        0.100  0.022   0.069    0.142  ...     6.0       5.0      14.0   1.42
mus       0.169  0.030   0.118    0.221  ...   126.0     119.0      65.0   1.02
gamma     0.250  0.042   0.178    0.325  ...    68.0      76.0      40.0   1.01
Is_begin  1.148  1.024   0.033    3.073  ...    97.0     113.0      96.0   1.03
Ia_begin  0.762  0.544   0.091    1.817  ...    83.0      58.0      66.0   1.04
E_begin   0.871  0.811   0.017    2.354  ...   109.0      81.0      60.0   0.99

[8 rows x 11 columns]