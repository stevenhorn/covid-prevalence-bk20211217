1 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 633 log-likelihood matrix

         Estimate       SE
elpd_loo -2510.56    34.04
p_loo       42.46        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      575   90.8%
 (0.5, 0.7]   (ok)         50    7.9%
   (0.7, 1]   (bad)         8    1.3%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.301  0.048   0.197    0.350  ...    43.0      38.0      40.0   1.05
pu        0.881  0.027   0.836    0.900  ...    57.0      62.0      22.0   0.99
mu        0.175  0.032   0.122    0.224  ...     4.0       4.0      14.0   1.57
mus       0.206  0.043   0.143    0.305  ...    83.0     111.0      58.0   0.99
gamma     0.298  0.045   0.232    0.399  ...    69.0      74.0      60.0   1.04
Is_begin  0.874  0.878   0.005    2.617  ...    94.0      73.0      91.0   1.00
Ia_begin  0.723  0.565   0.050    1.787  ...    24.0       9.0      57.0   1.18
E_begin   0.574  0.610   0.027    1.929  ...    66.0      58.0      87.0   1.00

[8 rows x 11 columns]