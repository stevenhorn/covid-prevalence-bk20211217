0 Divergences 
Passed validation 
2021-12-16 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 643 log-likelihood matrix

         Estimate       SE
elpd_loo -2704.75    62.90
p_loo       54.54        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      578   89.9%
 (0.5, 0.7]   (ok)         52    8.1%
   (0.7, 1]   (bad)        10    1.6%
   (1, Inf)   (very bad)    3    0.5%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.251   0.059   0.155    0.344  ...    13.0      11.0      22.0   1.16
pu         0.747   0.035   0.701    0.811  ...    19.0      14.0      22.0   1.13
mu         0.172   0.021   0.143    0.216  ...    16.0      17.0      39.0   1.11
mus        0.240   0.034   0.188    0.299  ...   100.0     109.0      96.0   1.00
gamma      0.390   0.063   0.283    0.497  ...   150.0     152.0      69.0   0.99
Is_begin   7.771   6.559   0.061   19.742  ...   131.0      76.0      43.0   0.99
Ia_begin  47.032  25.632   6.782   93.052  ...    81.0      75.0      60.0   1.00
E_begin   22.416  18.633   0.213   56.506  ...   133.0      92.0      60.0   1.06

[8 rows x 11 columns]