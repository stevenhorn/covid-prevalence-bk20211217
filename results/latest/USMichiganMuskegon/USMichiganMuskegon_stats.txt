0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2982.55    52.83
p_loo       32.04        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      502   79.2%
 (0.5, 0.7]   (ok)         33    5.2%
   (0.7, 1]   (bad)        51    8.0%
   (1, Inf)   (very bad)   48    7.6%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.165  0.014   0.151    0.180  ...     3.0       3.0      20.0   2.28
pu        0.702  0.000   0.701    0.702  ...     4.0       4.0      42.0   1.68
mu        0.183  0.006   0.176    0.191  ...     3.0       3.0       4.0   2.23
mus       0.228  0.029   0.197    0.270  ...     3.0       3.0      14.0   2.82
gamma     0.299  0.108   0.190    0.417  ...     3.0       3.0      14.0   2.05
Is_begin  0.679  0.049   0.573    0.756  ...    15.0      17.0      14.0   1.72
Ia_begin  2.735  2.276   0.403    5.254  ...     3.0       3.0      20.0   2.10
E_begin   0.385  0.116   0.263    0.545  ...     3.0       3.0       9.0   2.31

[8 rows x 11 columns]