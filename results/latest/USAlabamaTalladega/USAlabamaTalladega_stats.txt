0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2456.27    54.68
p_loo       58.80        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      583   92.0%
 (0.5, 0.7]   (ok)         42    6.6%
   (0.7, 1]   (bad)         6    0.9%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.180  0.023   0.151    0.227  ...    21.0      14.0      45.0   1.12
pu        0.708  0.006   0.700    0.720  ...    70.0      48.0      91.0   1.00
mu        0.114  0.016   0.084    0.142  ...    22.0      22.0      75.0   1.04
mus       0.186  0.038   0.129    0.263  ...   149.0     152.0      87.0   1.11
gamma     0.205  0.034   0.150    0.256  ...    79.0      73.0      61.0   1.02
Is_begin  0.864  0.810   0.027    2.653  ...   142.0     108.0      59.0   1.00
Ia_begin  1.603  1.572   0.038    4.536  ...   119.0      77.0      39.0   1.02
E_begin   0.759  0.870   0.005    2.331  ...   119.0      64.0      76.0   1.03

[8 rows x 11 columns]