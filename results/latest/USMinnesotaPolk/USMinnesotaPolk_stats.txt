0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1915.25    36.73
p_loo       31.46        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      592   93.4%
 (0.5, 0.7]   (ok)         35    5.5%
   (0.7, 1]   (bad)         6    0.9%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.164  0.011   0.150    0.186  ...    72.0      55.0      42.0   1.04
pu        0.705  0.004   0.700    0.712  ...    91.0      66.0      40.0   1.01
mu        0.124  0.016   0.094    0.151  ...    39.0      35.0      57.0   1.03
mus       0.178  0.023   0.141    0.219  ...    56.0      57.0      39.0   1.03
gamma     0.242  0.042   0.172    0.321  ...    93.0      96.0      60.0   0.98
Is_begin  0.539  0.541   0.020    1.427  ...   110.0      81.0      88.0   1.00
Ia_begin  1.099  0.980   0.084    2.434  ...    91.0      61.0      74.0   1.01
E_begin   0.472  0.545   0.006    1.830  ...    64.0      36.0      30.0   1.05

[8 rows x 11 columns]