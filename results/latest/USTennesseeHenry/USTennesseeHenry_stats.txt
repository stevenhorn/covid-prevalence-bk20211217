0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -2163.18    56.93
p_loo       51.66        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      590   93.4%
 (0.5, 0.7]   (ok)         29    4.6%
   (0.7, 1]   (bad)        11    1.7%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.214  0.039   0.160    0.288  ...    39.0      46.0      37.0   0.99
pu        0.721  0.015   0.700    0.749  ...    69.0      56.0      60.0   1.02
mu        0.139  0.022   0.100    0.173  ...    21.0      21.0      60.0   1.04
mus       0.223  0.041   0.167    0.305  ...   129.0     151.0      93.0   1.00
gamma     0.355  0.064   0.246    0.458  ...   107.0     128.0      96.0   1.00
Is_begin  0.667  0.795   0.014    2.717  ...   106.0     113.0      93.0   0.98
Ia_begin  1.177  1.467   0.021    3.392  ...   102.0      85.0      42.0   0.99
E_begin   0.528  0.808   0.007    2.016  ...    99.0      81.0      20.0   1.01

[8 rows x 11 columns]