0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1744.01    37.24
p_loo       36.02        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      598   94.3%
 (0.5, 0.7]   (ok)         28    4.4%
   (0.7, 1]   (bad)         5    0.8%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.165  0.015   0.150    0.199  ...   152.0     143.0      48.0   1.02
pu        0.705  0.004   0.700    0.713  ...    75.0      76.0      59.0   0.98
mu        0.097  0.015   0.070    0.126  ...    21.0      19.0      14.0   1.05
mus       0.151  0.022   0.117    0.191  ...    69.0      72.0      60.0   1.03
gamma     0.240  0.034   0.176    0.307  ...    59.0      68.0      60.0   1.01
Is_begin  0.672  0.562   0.001    1.601  ...   102.0      91.0      60.0   1.05
Ia_begin  1.348  1.614   0.034    3.968  ...   106.0     112.0      85.0   1.00
E_begin   0.625  0.665   0.002    2.025  ...   126.0     140.0      28.0   0.99

[8 rows x 11 columns]