0 Divergences 
Failed validation 
2021-12-09 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 635 log-likelihood matrix

         Estimate       SE
elpd_loo -1143.65    50.30
p_loo       55.81        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      593   93.4%
 (0.5, 0.7]   (ok)         31    4.9%
   (0.7, 1]   (bad)         9    1.4%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.178  0.023   0.151    0.212  ...   110.0      83.0      60.0   1.01
pu        0.708  0.007   0.700    0.720  ...    84.0      39.0      15.0   1.00
mu        0.124  0.022   0.090    0.159  ...    36.0      42.0      39.0   1.05
mus       0.228  0.031   0.184    0.286  ...   152.0     152.0      69.0   0.99
gamma     0.337  0.053   0.238    0.430  ...    91.0      98.0      76.0   1.04
Is_begin  0.317  0.338   0.019    0.744  ...    94.0     152.0      61.0   1.02
Ia_begin  0.455  0.553   0.002    1.380  ...    91.0      91.0      59.0   1.02
E_begin   0.231  0.316   0.004    1.068  ...    94.0     113.0      57.0   1.02

[8 rows x 11 columns]