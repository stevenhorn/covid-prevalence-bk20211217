0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1953.60    49.61
p_loo       47.85        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      599   94.5%
 (0.5, 0.7]   (ok)         29    4.6%
   (0.7, 1]   (bad)         4    0.6%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.165  0.015   0.151    0.193  ...   106.0      69.0      93.0   0.98
pu        0.704  0.004   0.700    0.713  ...   134.0      91.0      93.0   1.02
mu        0.106  0.018   0.076    0.132  ...     6.0       6.0      38.0   1.30
mus       0.172  0.025   0.134    0.225  ...    51.0      52.0      80.0   1.00
gamma     0.272  0.044   0.189    0.337  ...    19.0      19.0      18.0   1.07
Is_begin  0.601  0.619   0.016    1.858  ...    49.0      57.0      74.0   1.06
Ia_begin  0.991  1.243   0.002    3.516  ...   108.0      65.0      60.0   1.01
E_begin   0.386  0.537   0.012    1.193  ...   104.0      94.0      43.0   1.02

[8 rows x 11 columns]