0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -3455.65    47.26
p_loo       47.36        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      580   91.5%
 (0.5, 0.7]   (ok)         42    6.6%
   (0.7, 1]   (bad)        11    1.7%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.172  0.021   0.150    0.218  ...    46.0      32.0      22.0   1.07
pu        0.707  0.007   0.700    0.723  ...    51.0      46.0      43.0   1.05
mu        0.128  0.016   0.104    0.151  ...     7.0       8.0      74.0   1.21
mus       0.184  0.028   0.136    0.239  ...   115.0     130.0      60.0   0.99
gamma     0.359  0.045   0.288    0.456  ...   152.0     152.0      55.0   1.00
Is_begin  1.445  1.395   0.002    4.040  ...   108.0      36.0      38.0   1.05
Ia_begin  3.357  2.281   0.566    7.823  ...    91.0     104.0      60.0   1.05
E_begin   1.701  1.820   0.011    5.102  ...    75.0      47.0      60.0   1.04

[8 rows x 11 columns]