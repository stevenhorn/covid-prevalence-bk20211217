0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1622.32    36.94
p_loo       33.79        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      598   94.3%
 (0.5, 0.7]   (ok)         33    5.2%
   (0.7, 1]   (bad)         3    0.5%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.196  0.035   0.153    0.256  ...    21.0      30.0      57.0   1.08
pu        0.711  0.009   0.701    0.727  ...    54.0      19.0      40.0   1.09
mu        0.155  0.024   0.122    0.205  ...     4.0       4.0      23.0   1.57
mus       0.214  0.024   0.165    0.254  ...    27.0      27.0      56.0   1.06
gamma     0.248  0.038   0.183    0.321  ...    11.0      11.0      21.0   1.14
Is_begin  1.087  0.810   0.040    2.798  ...    70.0      67.0      49.0   1.02
Ia_begin  1.502  1.548   0.047    4.760  ...    76.0      54.0      60.0   1.05
E_begin   0.874  0.800   0.035    2.526  ...    41.0      35.0      58.0   1.03

[8 rows x 11 columns]