0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2720.06    66.76
p_loo       56.38        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      593   93.5%
 (0.5, 0.7]   (ok)         36    5.7%
   (0.7, 1]   (bad)         4    0.6%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.180  0.028   0.150    0.231  ...    24.0      36.0      22.0   1.05
pu        0.710  0.008   0.700    0.727  ...   152.0     123.0      59.0   1.00
mu        0.121  0.016   0.092    0.148  ...     8.0       7.0      54.0   1.23
mus       0.265  0.035   0.214    0.344  ...    82.0      87.0      56.0   1.00
gamma     0.445  0.060   0.349    0.536  ...   125.0     137.0      66.0   0.99
Is_begin  2.875  1.959   0.046    6.454  ...   152.0     141.0      60.0   0.99
Ia_begin  5.521  4.025   0.038   13.391  ...   110.0      95.0      59.0   1.00
E_begin   3.213  2.793   0.114    8.827  ...    32.0      34.0      55.0   1.07

[8 rows x 11 columns]