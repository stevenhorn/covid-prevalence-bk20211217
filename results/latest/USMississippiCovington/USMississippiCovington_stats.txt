0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1887.31    36.99
p_loo       36.42        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      590   93.1%
 (0.5, 0.7]   (ok)         36    5.7%
   (0.7, 1]   (bad)         7    1.1%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.160  0.010   0.150    0.175  ...    64.0      93.0      60.0   1.11
pu        0.704  0.005   0.700    0.714  ...   131.0     152.0      20.0   1.07
mu        0.088  0.013   0.068    0.114  ...    33.0      33.0      93.0   1.06
mus       0.151  0.029   0.103    0.207  ...   107.0     120.0      56.0   1.00
gamma     0.233  0.038   0.169    0.301  ...    86.0      89.0      59.0   1.00
Is_begin  0.426  0.391   0.010    1.220  ...   104.0      92.0     100.0   1.09
Ia_begin  0.898  1.037   0.003    2.970  ...    89.0      77.0      57.0   0.98
E_begin   0.424  0.345   0.009    1.127  ...    76.0      77.0      51.0   0.98

[8 rows x 11 columns]