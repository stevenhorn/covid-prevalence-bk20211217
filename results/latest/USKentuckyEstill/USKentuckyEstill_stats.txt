0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1749.47    49.33
p_loo       43.98        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      600   94.6%
 (0.5, 0.7]   (ok)         29    4.6%
   (0.7, 1]   (bad)         3    0.5%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.167  0.015   0.150    0.199  ...    62.0      54.0      38.0   1.10
pu        0.707  0.005   0.700    0.713  ...   102.0      81.0      59.0   1.02
mu        0.103  0.019   0.075    0.140  ...    29.0      28.0      81.0   1.01
mus       0.162  0.026   0.119    0.214  ...   134.0     140.0      91.0   1.00
gamma     0.246  0.042   0.181    0.315  ...    87.0      83.0      91.0   1.00
Is_begin  0.177  0.297   0.002    0.656  ...    36.0      21.0      60.0   1.08
Ia_begin  0.249  0.432   0.001    0.824  ...    48.0      33.0      49.0   1.04
E_begin   0.136  0.207   0.000    0.493  ...    53.0      20.0      58.0   1.08

[8 rows x 11 columns]