0 Divergences 
Failed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -1689.02    71.16
p_loo       48.94        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      601   95.1%
 (0.5, 0.7]   (ok)         22    3.5%
   (0.7, 1]   (bad)         8    1.3%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.169  0.018   0.150    0.210  ...    96.0      75.0      30.0   1.06
pu        0.706  0.005   0.700    0.715  ...   140.0     129.0      60.0   1.01
mu        0.086  0.014   0.060    0.106  ...    56.0      50.0      54.0   0.99
mus       0.208  0.040   0.136    0.269  ...    43.0      41.0      49.0   1.02
gamma     0.270  0.066   0.150    0.361  ...    65.0      57.0      59.0   1.06
Is_begin  0.297  0.329   0.001    0.926  ...    66.0      36.0      58.0   1.04
Ia_begin  0.552  0.746   0.009    1.984  ...    93.0      71.0      60.0   1.01
E_begin   0.234  0.259   0.003    0.713  ...    61.0      54.0      60.0   0.99

[8 rows x 11 columns]