0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2220.89    37.35
p_loo       39.12        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      575   90.7%
 (0.5, 0.7]   (ok)         50    7.9%
   (0.7, 1]   (bad)         9    1.4%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.185  0.023   0.152    0.223  ...    68.0      93.0      56.0   1.02
pu        0.711  0.008   0.700    0.726  ...   152.0     152.0      91.0   1.09
mu        0.109  0.017   0.084    0.141  ...    38.0      42.0      59.0   1.02
mus       0.159  0.025   0.123    0.214  ...    33.0      32.0      84.0   1.05
gamma     0.231  0.038   0.160    0.291  ...    14.0      13.0      22.0   1.12
Is_begin  0.936  0.870   0.054    2.660  ...   116.0     152.0      93.0   0.99
Ia_begin  1.710  1.785   0.004    4.587  ...   105.0      61.0      80.0   1.02
E_begin   0.834  0.868   0.004    2.687  ...    76.0      35.0      66.0   1.04

[8 rows x 11 columns]