0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -3044.17    37.33
p_loo       33.27        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      596   94.0%
 (0.5, 0.7]   (ok)         28    4.4%
   (0.7, 1]   (bad)         9    1.4%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.158  0.007   0.150    0.171  ...   104.0      76.0      60.0   1.00
pu        0.703  0.002   0.700    0.707  ...   120.0      93.0      97.0   1.01
mu        0.085  0.012   0.065    0.106  ...    15.0      18.0      39.0   1.09
mus       0.169  0.028   0.124    0.228  ...    78.0      43.0      65.0   1.05
gamma     0.236  0.038   0.172    0.293  ...   131.0     123.0     100.0   0.99
Is_begin  1.291  0.765   0.106    2.617  ...    83.0      76.0      34.0   1.04
Ia_begin  2.123  1.927   0.022    5.087  ...    29.0      27.0      59.0   1.03
E_begin   1.153  1.225   0.042    3.500  ...    85.0      72.0      93.0   1.01

[8 rows x 11 columns]