0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2696.20    71.23
p_loo       58.66        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      596   94.0%
 (0.5, 0.7]   (ok)         28    4.4%
   (0.7, 1]   (bad)         7    1.1%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.158  0.007   0.150    0.176  ...    82.0      51.0      60.0   1.03
pu        0.703  0.003   0.700    0.708  ...    43.0      34.0      56.0   1.04
mu        0.081  0.009   0.067    0.097  ...    15.0      16.0      39.0   1.09
mus       0.266  0.036   0.209    0.339  ...    49.0      49.0      60.0   1.02
gamma     0.358  0.054   0.248    0.448  ...    74.0      70.0      49.0   1.00
Is_begin  0.966  0.882   0.033    2.878  ...    85.0      79.0      99.0   1.00
Ia_begin  1.492  1.416   0.062    4.823  ...   122.0     118.0      59.0   1.00
E_begin   0.658  0.676   0.002    2.003  ...    64.0      31.0      59.0   1.04

[8 rows x 11 columns]