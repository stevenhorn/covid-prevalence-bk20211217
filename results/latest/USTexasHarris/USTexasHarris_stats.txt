11 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -4869.42    61.96
p_loo       58.44        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      579   91.6%
 (0.5, 0.7]   (ok)         43    6.8%
   (0.7, 1]   (bad)         9    1.4%
   (1, Inf)   (very bad)    1    0.2%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.221   0.051   0.151    0.310  ...    58.0      24.0      24.0   1.08
pu         0.733   0.023   0.703    0.776  ...    15.0      12.0      15.0   1.10
mu         0.115   0.013   0.092    0.137  ...    32.0      32.0      53.0   1.03
mus        0.235   0.031   0.179    0.284  ...    74.0      77.0      93.0   1.01
gamma      0.376   0.061   0.280    0.503  ...   103.0     106.0      93.0   0.99
Is_begin  10.946   8.059   0.252   28.640  ...    76.0      95.0      60.0   1.05
Ia_begin  19.422  12.234   2.339   40.769  ...    92.0      80.0      60.0   1.01
E_begin   15.966  11.977   0.938   37.832  ...    90.0      80.0      42.0   0.99

[8 rows x 11 columns]