14 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2926.96    39.03
p_loo       58.25        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      597   94.2%
 (0.5, 0.7]   (ok)         33    5.2%
   (0.7, 1]   (bad)         2    0.3%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.167  0.019   0.150    0.207  ...    19.0      19.0      15.0   1.12
pu        0.703  0.003   0.700    0.710  ...    31.0      22.0      41.0   1.40
mu        0.163  0.037   0.111    0.223  ...     3.0       3.0      34.0   2.11
mus       0.192  0.028   0.152    0.244  ...     4.0       4.0      44.0   1.59
gamma     0.208  0.078   0.124    0.329  ...     3.0       3.0      56.0   1.96
Is_begin  1.091  0.926   0.076    3.012  ...    12.0       8.0      24.0   1.24
Ia_begin  0.474  0.421   0.028    1.405  ...    13.0       8.0      36.0   1.25
E_begin   0.379  0.578   0.004    1.494  ...    25.0       8.0      15.0   1.26

[8 rows x 11 columns]