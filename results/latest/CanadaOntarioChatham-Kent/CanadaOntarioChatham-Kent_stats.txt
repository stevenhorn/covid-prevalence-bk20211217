0 Divergences 
Passed validation 
2021-12-16 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 643 log-likelihood matrix

         Estimate       SE
elpd_loo -1893.45    36.07
p_loo       25.19        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      624   97.0%
 (0.5, 0.7]   (ok)         14    2.2%
   (0.7, 1]   (bad)         4    0.6%
   (1, Inf)   (very bad)    1    0.2%

            mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.233  0.023   0.194    0.267  ...     3.0       3.0      15.0   2.28
pu         0.818  0.008   0.808    0.828  ...     3.0       4.0      59.0   1.85
mu         0.368  0.001   0.365    0.370  ...     4.0       4.0      20.0   1.52
mus        0.267  0.012   0.248    0.286  ...     3.0       3.0      14.0   2.17
gamma      0.317  0.016   0.293    0.343  ...     3.0       3.0      15.0   1.99
Is_begin   3.041  1.024   1.772    4.728  ...     4.0       4.0      18.0   1.67
Ia_begin  10.215  4.613   4.698   17.023  ...     3.0       3.0      38.0   2.18
E_begin    2.786  1.503   0.816    5.262  ...     3.0       3.0      22.0   1.98

[8 rows x 11 columns]