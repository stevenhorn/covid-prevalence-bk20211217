0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -3639.25    33.32
p_loo       34.93        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      560   88.3%
 (0.5, 0.7]   (ok)         56    8.8%
   (0.7, 1]   (bad)        16    2.5%
   (1, Inf)   (very bad)    2    0.3%

             mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa          0.176   0.019   0.151    0.215  ...    82.0      69.0      96.0   1.02
pu          0.710   0.009   0.700    0.725  ...    57.0      54.0      33.0   1.05
mu          0.131   0.017   0.099    0.160  ...     5.0       5.0      53.0   1.41
mus         0.170   0.021   0.137    0.210  ...   114.0     121.0      61.0   1.04
gamma       0.329   0.039   0.275    0.406  ...    66.0      65.0      59.0   1.02
Is_begin   37.825  20.397   4.395   77.046  ...    22.0      65.0      37.0   1.04
Ia_begin   85.638  38.443  15.137  148.287  ...    33.0      23.0      17.0   1.11
E_begin   174.987  69.027  56.572  315.748  ...    67.0      57.0      60.0   1.01

[8 rows x 11 columns]