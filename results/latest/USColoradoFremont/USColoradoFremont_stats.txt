0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2296.94    49.26
p_loo       46.48        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      599   94.5%
 (0.5, 0.7]   (ok)         30    4.7%
   (0.7, 1]   (bad)         4    0.6%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.160  0.010   0.150    0.176  ...    79.0      58.0      86.0   1.00
pu        0.703  0.003   0.700    0.710  ...    43.0      28.0      19.0   1.07
mu        0.112  0.021   0.074    0.142  ...     5.0       6.0      38.0   1.34
mus       0.173  0.025   0.129    0.216  ...    33.0      33.0     100.0   1.05
gamma     0.232  0.042   0.167    0.295  ...    16.0      18.0      38.0   1.16
Is_begin  0.639  0.627   0.002    1.600  ...    89.0      66.0      34.0   1.00
Ia_begin  1.368  1.223   0.032    3.774  ...    62.0      43.0      58.0   0.98
E_begin   0.587  0.629   0.002    1.581  ...    72.0      67.0      60.0   1.02

[8 rows x 11 columns]