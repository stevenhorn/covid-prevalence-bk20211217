0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -3048.81    42.32
p_loo       43.65        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      582   91.8%
 (0.5, 0.7]   (ok)         47    7.4%
   (0.7, 1]   (bad)         4    0.6%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.170  0.015   0.150    0.198  ...   137.0     144.0      93.0   1.01
pu        0.705  0.005   0.700    0.715  ...    97.0      73.0      49.0   1.03
mu        0.094  0.015   0.072    0.123  ...    26.0      24.0      36.0   1.10
mus       0.158  0.029   0.108    0.215  ...   126.0     130.0      83.0   1.02
gamma     0.218  0.033   0.155    0.277  ...   118.0     127.0      88.0   0.99
Is_begin  1.518  1.532   0.042    4.702  ...   152.0     152.0     100.0   1.00
Ia_begin  3.919  3.193   0.043    9.213  ...   151.0     122.0      60.0   1.00
E_begin   1.583  1.698   0.006    5.435  ...    87.0      48.0      59.0   1.04

[8 rows x 11 columns]