0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1466.09    91.09
p_loo       67.08        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      594   93.7%
 (0.5, 0.7]   (ok)         31    4.9%
   (0.7, 1]   (bad)         7    1.1%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.173  0.021   0.150    0.206  ...   124.0      84.0      57.0   1.02
pu        0.707  0.007   0.700    0.717  ...    94.0      66.0      60.0   0.98
mu        0.091  0.013   0.068    0.112  ...    45.0      47.0      83.0   1.01
mus       0.262  0.036   0.191    0.322  ...    78.0      69.0      69.0   1.03
gamma     0.603  0.089   0.451    0.754  ...    96.0      90.0      56.0   1.04
Is_begin  0.349  0.419   0.002    1.180  ...    85.0      76.0      60.0   0.99
Ia_begin  0.505  0.599   0.014    1.675  ...    81.0      67.0      87.0   1.00
E_begin   0.213  0.277   0.001    0.764  ...    97.0      82.0      74.0   0.98

[8 rows x 11 columns]