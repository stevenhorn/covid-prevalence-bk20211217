0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1751.67    48.08
p_loo       38.81        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      590   93.1%
 (0.5, 0.7]   (ok)         34    5.4%
   (0.7, 1]   (bad)         7    1.1%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.165  0.015   0.150    0.194  ...    87.0      69.0      48.0   1.02
pu        0.705  0.004   0.700    0.713  ...    79.0      71.0      58.0   1.00
mu        0.111  0.020   0.077    0.152  ...     5.0       5.0      14.0   1.44
mus       0.175  0.035   0.113    0.250  ...    43.0      45.0      60.0   1.02
gamma     0.246  0.033   0.183    0.302  ...    48.0      48.0      59.0   1.00
Is_begin  0.981  0.722   0.050    2.353  ...    97.0      79.0      57.0   1.03
Ia_begin  1.474  1.228   0.008    3.734  ...    48.0      45.0      60.0   1.04
E_begin   0.631  0.711   0.005    1.357  ...    60.0      56.0      60.0   1.03

[8 rows x 11 columns]