0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 633 log-likelihood matrix

         Estimate       SE
elpd_loo -1633.23    36.16
p_loo       39.26        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      590   93.2%
 (0.5, 0.7]   (ok)         36    5.7%
   (0.7, 1]   (bad)         7    1.1%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.189  0.026   0.151    0.235  ...    89.0      71.0      39.0   1.02
pu        0.714  0.010   0.700    0.735  ...    22.0      20.0      43.0   1.10
mu        0.143  0.017   0.111    0.172  ...    10.0      11.0      48.0   1.15
mus       0.227  0.031   0.165    0.269  ...    61.0      53.0      59.0   1.00
gamma     0.382  0.059   0.285    0.513  ...    80.0      88.0      39.0   1.02
Is_begin  1.787  1.275   0.059    3.904  ...    53.0      39.0      14.0   1.01
Ia_begin  5.087  3.106   0.083   10.403  ...    26.0      22.0      22.0   1.07
E_begin   3.634  3.444   0.067   11.415  ...    45.0      64.0      60.0   1.05

[8 rows x 11 columns]