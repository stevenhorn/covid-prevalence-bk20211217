0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 633 log-likelihood matrix

         Estimate       SE
elpd_loo -1214.88    53.96
p_loo       71.87        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      584   92.3%
 (0.5, 0.7]   (ok)         40    6.3%
   (0.7, 1]   (bad)         5    0.8%
   (1, Inf)   (very bad)    4    0.6%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.271  0.052   0.169    0.340  ...    55.0      57.0      33.0   0.99
pu        0.873  0.035   0.795    0.899  ...    28.0      40.0      14.0   1.03
mu        0.152  0.017   0.124    0.176  ...    30.0      21.0      59.0   1.08
mus       0.379  0.052   0.287    0.458  ...    59.0     109.0      40.0   1.02
gamma     0.688  0.103   0.501    0.826  ...   132.0     152.0      81.0   1.01
Is_begin  1.692  1.059   0.004    3.582  ...    24.0      20.0      33.0   1.12
Ia_begin  4.347  2.835   0.116    8.847  ...    65.0      54.0      31.0   1.03
E_begin   3.302  3.059   0.108    8.073  ...    50.0      29.0      60.0   1.06

[8 rows x 11 columns]