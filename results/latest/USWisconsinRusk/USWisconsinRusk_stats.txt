0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -1585.93    43.28
p_loo       45.58        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      596   94.3%
 (0.5, 0.7]   (ok)         25    4.0%
   (0.7, 1]   (bad)         8    1.3%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.177  0.020   0.153    0.222  ...   152.0     152.0      72.0   1.07
pu        0.708  0.007   0.700    0.725  ...   152.0      87.0      40.0   1.00
mu        0.116  0.018   0.088    0.150  ...    40.0      37.0      55.0   1.01
mus       0.195  0.035   0.133    0.262  ...   128.0     152.0      46.0   1.01
gamma     0.263  0.043   0.189    0.337  ...    83.0     124.0      46.0   1.00
Is_begin  0.522  0.398   0.010    1.169  ...    89.0      73.0      36.0   0.99
Ia_begin  1.090  1.210   0.004    3.345  ...    73.0      78.0      60.0   1.03
E_begin   0.520  0.623   0.002    1.856  ...    95.0      51.0      35.0   1.04

[8 rows x 11 columns]