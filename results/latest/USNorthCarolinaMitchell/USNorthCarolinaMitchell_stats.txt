0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1686.64    39.50
p_loo       49.79        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      579   91.3%
 (0.5, 0.7]   (ok)         43    6.8%
   (0.7, 1]   (bad)         8    1.3%
   (1, Inf)   (very bad)    4    0.6%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.193  0.038   0.153    0.273  ...    58.0      39.0      91.0   1.05
pu        0.714  0.013   0.700    0.742  ...    88.0      74.0      26.0   1.02
mu        0.131  0.020   0.091    0.172  ...    12.0      18.0      15.0   1.08
mus       0.214  0.042   0.145    0.282  ...    27.0      26.0      39.0   1.05
gamma     0.289  0.061   0.186    0.401  ...    47.0      44.0      60.0   1.03
Is_begin  0.615  0.634   0.002    2.061  ...    36.0      19.0      14.0   1.07
Ia_begin  1.129  1.164   0.002    3.336  ...    53.0      29.0      57.0   1.06
E_begin   0.531  0.528   0.013    1.742  ...    26.0      25.0      49.0   1.06

[8 rows x 11 columns]