0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2136.16    51.74
p_loo       45.39        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      587   92.6%
 (0.5, 0.7]   (ok)         38    6.0%
   (0.7, 1]   (bad)         8    1.3%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.169  0.017   0.150    0.194  ...    81.0     100.0      46.0   1.07
pu        0.706  0.005   0.700    0.716  ...    39.0      40.0      40.0   0.99
mu        0.119  0.015   0.091    0.148  ...    40.0      42.0      91.0   1.02
mus       0.194  0.029   0.143    0.240  ...    66.0      77.0      40.0   0.99
gamma     0.307  0.049   0.224    0.385  ...    96.0      97.0      96.0   0.99
Is_begin  1.147  0.983   0.028    2.990  ...    80.0      40.0      49.0   1.03
Ia_begin  2.196  1.687   0.087    5.291  ...    67.0      56.0      19.0   1.05
E_begin   1.288  1.352   0.013    3.889  ...    74.0      58.0      39.0   1.00

[8 rows x 11 columns]