2 Divergences 
Failed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -1652.68   104.57
p_loo       86.71        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      595   94.1%
 (0.5, 0.7]   (ok)         31    4.9%
   (0.7, 1]   (bad)         4    0.6%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.166  0.015   0.150    0.188  ...   103.0      83.0      20.0   1.00
pu        0.705  0.004   0.700    0.713  ...    86.0      67.0      40.0   0.99
mu        0.139  0.017   0.104    0.174  ...    33.0      40.0      25.0   1.05
mus       0.309  0.040   0.242    0.388  ...    24.0      30.0      61.0   1.07
gamma     0.589  0.077   0.443    0.721  ...    14.0      14.0      59.0   1.12
Is_begin  0.482  0.687   0.001    1.785  ...    63.0      34.0      53.0   1.05
Ia_begin  0.679  1.068   0.007    1.954  ...    46.0      42.0      60.0   0.99
E_begin   0.213  0.325   0.001    0.610  ...    75.0      29.0      57.0   1.04

[8 rows x 11 columns]