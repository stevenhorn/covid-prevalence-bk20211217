0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -2061.64    35.23
p_loo       40.69        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      603   95.4%
 (0.5, 0.7]   (ok)         23    3.6%
   (0.7, 1]   (bad)         6    0.9%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.213  0.039   0.150    0.271  ...    71.0      70.0      40.0   1.03
pu        0.721  0.017   0.701    0.750  ...    25.0      30.0      40.0   1.09
mu        0.139  0.023   0.104    0.179  ...    16.0      17.0      69.0   1.09
mus       0.186  0.035   0.140    0.268  ...   109.0      86.0      40.0   1.04
gamma     0.268  0.039   0.215    0.351  ...    54.0      55.0      43.0   1.00
Is_begin  0.709  0.649   0.012    1.840  ...    61.0     107.0      74.0   1.02
Ia_begin  1.321  1.949   0.023    5.924  ...    53.0     152.0      66.0   1.07
E_begin   0.561  0.844   0.018    1.820  ...    98.0     120.0      83.0   1.00

[8 rows x 11 columns]