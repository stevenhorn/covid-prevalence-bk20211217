0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1539.46    79.98
p_loo       58.21        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      589   92.9%
 (0.5, 0.7]   (ok)         35    5.5%
   (0.7, 1]   (bad)         5    0.8%
   (1, Inf)   (very bad)    5    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.161  0.009   0.150    0.176  ...    97.0      63.0      84.0   1.04
pu        0.703  0.003   0.700    0.710  ...   152.0      76.0     100.0   1.01
mu        0.097  0.017   0.068    0.126  ...    67.0      61.0      59.0   1.03
mus       0.265  0.041   0.193    0.330  ...    58.0      56.0      59.0   1.01
gamma     0.340  0.052   0.242    0.433  ...    68.0      69.0      32.0   1.07
Is_begin  0.133  0.206   0.001    0.455  ...    66.0      59.0      36.0   1.00
Ia_begin  0.146  0.196   0.000    0.407  ...    93.0      49.0      60.0   1.03
E_begin   0.087  0.134   0.001    0.255  ...    92.0      54.0      62.0   1.01

[8 rows x 11 columns]