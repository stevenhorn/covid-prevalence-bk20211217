0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -2262.57    61.73
p_loo       57.39        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      588   93.0%
 (0.5, 0.7]   (ok)         38    6.0%
   (0.7, 1]   (bad)         5    0.8%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.169  0.017   0.151    0.205  ...    61.0      59.0      95.0   1.02
pu        0.707  0.007   0.700    0.722  ...    17.0      16.0      40.0   1.11
mu        0.093  0.012   0.072    0.115  ...    36.0      31.0      31.0   1.03
mus       0.210  0.030   0.154    0.257  ...    61.0      65.0      59.0   1.03
gamma     0.357  0.050   0.278    0.456  ...   152.0     152.0      81.0   1.01
Is_begin  0.513  0.590   0.016    1.569  ...    81.0      74.0      59.0   1.02
Ia_begin  0.696  0.813   0.005    2.130  ...    86.0      54.0      96.0   1.03
E_begin   0.282  0.337   0.001    0.793  ...    53.0      39.0      22.0   1.04

[8 rows x 11 columns]