0 Divergences 
Passed validation 
2021-12-09 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 635 log-likelihood matrix

         Estimate       SE
elpd_loo -2047.90    44.01
p_loo       34.56        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      591   93.1%
 (0.5, 0.7]   (ok)         40    6.3%
   (0.7, 1]   (bad)         3    0.5%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.157  0.010   0.151    0.189  ...     6.0       3.0      22.0   2.25
pu        0.702  0.002   0.700    0.706  ...     3.0       3.0      22.0   2.29
mu        0.154  0.034   0.111    0.198  ...     3.0       3.0      17.0   2.31
mus       0.182  0.017   0.138    0.201  ...     9.0      14.0      24.0   1.32
gamma     0.344  0.028   0.291    0.386  ...     8.0       7.0      15.0   1.24
Is_begin  1.020  0.633   0.414    2.406  ...     4.0       4.0      14.0   1.70
Ia_begin  0.313  0.184   0.081    0.629  ...     3.0       4.0      24.0   1.78
E_begin   0.647  0.514   0.083    2.030  ...     4.0       4.0      14.0   1.68

[8 rows x 11 columns]