0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2004.28    46.69
p_loo       42.30        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      587   92.6%
 (0.5, 0.7]   (ok)         36    5.7%
   (0.7, 1]   (bad)        10    1.6%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.243  0.057   0.167    0.347  ...   152.0     152.0      95.0   1.03
pu        0.748  0.026   0.706    0.791  ...    17.0      18.0      55.0   1.11
mu        0.137  0.024   0.095    0.187  ...    17.0      17.0      15.0   1.10
mus       0.181  0.032   0.122    0.239  ...   126.0     128.0      67.0   1.01
gamma     0.227  0.032   0.174    0.282  ...    87.0      89.0      91.0   1.00
Is_begin  1.471  1.079   0.054    3.549  ...   152.0     152.0      91.0   1.01
Ia_begin  3.670  2.726   0.108    9.749  ...    75.0     112.0      23.0   1.01
E_begin   2.169  2.046   0.132    5.534  ...    94.0      74.0     100.0   1.03

[8 rows x 11 columns]