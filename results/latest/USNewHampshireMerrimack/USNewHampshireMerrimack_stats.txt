0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2612.51    53.32
p_loo       44.66        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      596   94.0%
 (0.5, 0.7]   (ok)         29    4.6%
   (0.7, 1]   (bad)         8    1.3%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.230  0.048   0.154    0.304  ...    69.0      78.0      60.0   0.99
pu        0.721  0.017   0.700    0.755  ...    35.0      25.0      38.0   1.01
mu        0.136  0.021   0.104    0.167  ...    27.0      21.0      40.0   1.10
mus       0.167  0.026   0.129    0.219  ...    58.0      60.0      88.0   1.04
gamma     0.223  0.040   0.160    0.305  ...    82.0      74.0      60.0   1.01
Is_begin  0.707  0.557   0.005    1.643  ...   122.0      72.0      40.0   1.02
Ia_begin  1.136  1.095   0.001    2.813  ...    32.0      13.0      14.0   1.12
E_begin   0.636  0.608   0.007    1.722  ...    76.0      62.0      57.0   0.99

[8 rows x 11 columns]