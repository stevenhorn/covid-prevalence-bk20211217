0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1725.95    36.37
p_loo       44.88        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      581   91.6%
 (0.5, 0.7]   (ok)         44    6.9%
   (0.7, 1]   (bad)         9    1.4%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.188  0.024   0.152    0.226  ...   107.0     152.0     100.0   0.99
pu        0.712  0.008   0.700    0.724  ...   113.0      95.0      58.0   0.98
mu        0.157  0.017   0.128    0.189  ...   113.0     104.0      43.0   1.03
mus       0.217  0.028   0.164    0.271  ...   107.0     148.0     100.0   0.99
gamma     0.287  0.043   0.201    0.362  ...    92.0     105.0      25.0   1.00
Is_begin  0.556  0.452   0.013    1.304  ...    94.0      72.0      40.0   1.04
Ia_begin  1.226  0.922   0.015    2.922  ...   152.0      88.0      48.0   1.01
E_begin   0.585  0.556   0.007    1.685  ...   131.0      90.0      74.0   0.99

[8 rows x 11 columns]