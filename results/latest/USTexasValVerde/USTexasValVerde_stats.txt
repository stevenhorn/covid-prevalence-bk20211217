0 Divergences 
Failed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -2901.36    90.49
p_loo       40.97        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      589   93.2%
 (0.5, 0.7]   (ok)         21    3.3%
   (0.7, 1]   (bad)        11    1.7%
   (1, Inf)   (very bad)   11    1.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.165  0.007   0.157    0.174  ...     3.0       3.0      15.0   2.37
pu        0.703  0.001   0.702    0.704  ...     4.0       5.0      22.0   1.49
mu        0.133  0.012   0.117    0.151  ...     3.0       3.0      22.0   1.98
mus       0.200  0.027   0.169    0.233  ...     3.0       3.0      15.0   2.07
gamma     0.368  0.041   0.308    0.420  ...     3.0       3.0      23.0   1.97
Is_begin  0.528  0.105   0.410    0.711  ...     3.0       3.0      15.0   1.95
Ia_begin  1.031  0.549   0.355    1.629  ...     3.0       3.0      22.0   3.00
E_begin   0.272  0.272   0.002    0.595  ...     3.0       3.0      33.0   2.09

[8 rows x 11 columns]