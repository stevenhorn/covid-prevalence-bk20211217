0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1858.77    37.33
p_loo       38.74        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      597   94.2%
 (0.5, 0.7]   (ok)         32    5.0%
   (0.7, 1]   (bad)         4    0.6%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.195  0.032   0.151    0.256  ...   118.0     105.0      60.0   1.01
pu        0.715  0.011   0.701    0.737  ...   134.0     142.0      42.0   1.00
mu        0.100  0.013   0.080    0.124  ...    13.0      12.0      40.0   1.16
mus       0.156  0.025   0.121    0.200  ...   115.0     116.0      52.0   1.01
gamma     0.299  0.048   0.239    0.404  ...    57.0      45.0      15.0   1.04
Is_begin  0.615  0.515   0.007    1.618  ...   106.0      90.0      59.0   1.01
Ia_begin  1.027  1.457   0.005    3.675  ...    36.0      32.0      45.0   1.03
E_begin   0.557  0.684   0.016    2.105  ...    79.0      50.0     100.0   1.05

[8 rows x 11 columns]