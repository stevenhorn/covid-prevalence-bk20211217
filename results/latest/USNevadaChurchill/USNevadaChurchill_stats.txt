0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2089.34    50.39
p_loo       50.80        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      593   93.5%
 (0.5, 0.7]   (ok)         30    4.7%
   (0.7, 1]   (bad)         8    1.3%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.173  0.020   0.150    0.208  ...    88.0      65.0      40.0   1.01
pu        0.706  0.006   0.700    0.717  ...    87.0      48.0      22.0   1.03
mu        0.115  0.017   0.084    0.147  ...    63.0      63.0      38.0   1.02
mus       0.197  0.040   0.130    0.268  ...    28.0      66.0      33.0   1.15
gamma     0.250  0.043   0.180    0.338  ...   110.0      96.0      41.0   1.05
Is_begin  0.357  0.368   0.001    1.070  ...    69.0      73.0      49.0   1.02
Ia_begin  0.623  0.796   0.018    2.394  ...    54.0      26.0      40.0   1.02
E_begin   0.259  0.284   0.006    0.828  ...    59.0      41.0      43.0   1.00

[8 rows x 11 columns]