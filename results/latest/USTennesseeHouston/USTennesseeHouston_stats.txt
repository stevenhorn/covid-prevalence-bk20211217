0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -1555.25    43.28
p_loo       42.79        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      600   94.9%
 (0.5, 0.7]   (ok)         21    3.3%
   (0.7, 1]   (bad)         9    1.4%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.161  0.009   0.151    0.180  ...   152.0      70.0      34.0   1.01
pu        0.704  0.004   0.700    0.713  ...    88.0      68.0      60.0   1.01
mu        0.118  0.020   0.083    0.151  ...     5.0       6.0      24.0   1.37
mus       0.203  0.028   0.154    0.258  ...    54.0      53.0      60.0   1.01
gamma     0.282  0.050   0.195    0.371  ...    44.0      44.0      60.0   1.01
Is_begin  0.555  0.630   0.002    1.729  ...    44.0      70.0      46.0   1.01
Ia_begin  1.072  0.885   0.002    2.885  ...    77.0      87.0      60.0   1.01
E_begin   0.496  0.527   0.002    1.449  ...    55.0      28.0      43.0   1.06

[8 rows x 11 columns]