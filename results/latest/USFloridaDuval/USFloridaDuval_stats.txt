0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -4255.08    46.11
p_loo       59.85        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      610   96.2%
 (0.5, 0.7]   (ok)         16    2.5%
   (0.7, 1]   (bad)         5    0.8%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.166  0.017   0.150    0.204  ...   110.0     106.0      60.0   1.01
pu        0.705  0.005   0.700    0.716  ...   152.0      98.0      57.0   1.01
mu        0.114  0.023   0.078    0.155  ...     8.0       9.0      59.0   1.18
mus       0.175  0.026   0.121    0.220  ...    20.0      18.0      28.0   1.09
gamma     0.212  0.049   0.122    0.282  ...     4.0       5.0      38.0   1.47
Is_begin  1.349  1.622   0.005    4.265  ...    43.0      21.0      56.0   1.04
Ia_begin  3.317  4.021   0.004   12.909  ...    69.0      47.0      17.0   1.03
E_begin   1.501  2.167   0.013    6.219  ...    45.0      37.0      39.0   1.03

[8 rows x 11 columns]