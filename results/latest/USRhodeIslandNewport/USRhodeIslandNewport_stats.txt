0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2676.71    47.42
p_loo       41.71        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      596   94.0%
 (0.5, 0.7]   (ok)         30    4.7%
   (0.7, 1]   (bad)         6    0.9%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.157  0.010   0.150    0.173  ...   126.0      69.0      40.0   0.99
pu        0.703  0.003   0.700    0.709  ...   109.0      63.0      22.0   0.99
mu        0.093  0.012   0.074    0.111  ...    34.0      37.0      54.0   0.98
mus       0.140  0.021   0.106    0.184  ...    61.0      62.0      88.0   1.01
gamma     0.216  0.046   0.140    0.288  ...    71.0      57.0      59.0   1.03
Is_begin  0.990  1.042   0.014    2.701  ...    70.0      48.0      81.0   1.00
Ia_begin  1.427  1.506   0.013    5.143  ...    66.0      20.0      14.0   1.06
E_begin   0.717  0.683   0.001    2.150  ...    27.0      19.0      14.0   1.02

[8 rows x 11 columns]