0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1728.71    33.32
p_loo       40.69        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      581   91.6%
 (0.5, 0.7]   (ok)         44    6.9%
   (0.7, 1]   (bad)         8    1.3%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.245  0.055   0.160    0.337  ...    87.0      76.0      43.0   1.10
pu        0.780  0.028   0.726    0.826  ...    50.0      49.0      43.0   1.00
mu        0.129  0.023   0.097    0.179  ...    19.0      20.0      46.0   1.10
mus       0.186  0.030   0.149    0.257  ...    80.0      91.0      68.0   1.03
gamma     0.239  0.040   0.173    0.308  ...   114.0     108.0      62.0   1.01
Is_begin  0.768  0.746   0.004    2.567  ...   137.0      87.0      33.0   1.00
Ia_begin  1.521  1.530   0.010    4.553  ...    74.0      37.0      60.0   1.06
E_begin   0.891  1.201   0.004    2.715  ...   115.0      50.0      56.0   1.02

[8 rows x 11 columns]