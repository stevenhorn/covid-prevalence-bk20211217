0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -1641.29    31.10
p_loo       37.52        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      587   92.9%
 (0.5, 0.7]   (ok)         39    6.2%
   (0.7, 1]   (bad)         4    0.6%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.248  0.048   0.171    0.339  ...    55.0      50.0      31.0   1.03
pu        0.759  0.024   0.721    0.800  ...    54.0      57.0      32.0   1.04
mu        0.142  0.022   0.114    0.203  ...    41.0      36.0      40.0   1.02
mus       0.175  0.031   0.123    0.229  ...    98.0      96.0      58.0   0.99
gamma     0.204  0.040   0.150    0.286  ...   106.0     100.0      67.0   0.99
Is_begin  0.587  0.706   0.005    1.907  ...   124.0      69.0      60.0   0.99
Ia_begin  1.780  1.870   0.040    4.936  ...    30.0      22.0      91.0   1.07
E_begin   0.676  0.821   0.002    2.178  ...    41.0      21.0      40.0   1.09

[8 rows x 11 columns]