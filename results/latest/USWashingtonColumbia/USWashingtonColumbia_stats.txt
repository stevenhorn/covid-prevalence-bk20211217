0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo  -851.80    50.37
p_loo       61.73        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      582   92.1%
 (0.5, 0.7]   (ok)         37    5.9%
   (0.7, 1]   (bad)        11    1.7%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.228  0.050   0.152    0.322  ...    66.0      74.0      33.0   1.02
pu        0.724  0.017   0.700    0.754  ...   108.0     121.0      60.0   1.03
mu        0.155  0.025   0.113    0.213  ...    57.0      57.0      67.0   1.03
mus       0.285  0.042   0.227    0.373  ...   109.0     106.0      27.0   1.04
gamma     0.340  0.052   0.239    0.417  ...   141.0     123.0      76.0   1.11
Is_begin  0.451  0.412   0.002    1.164  ...    65.0      57.0      26.0   1.03
Ia_begin  0.736  0.890   0.010    2.031  ...    75.0      69.0      86.0   1.01
E_begin   0.355  0.468   0.002    1.170  ...    40.0      25.0      59.0   1.08

[8 rows x 11 columns]