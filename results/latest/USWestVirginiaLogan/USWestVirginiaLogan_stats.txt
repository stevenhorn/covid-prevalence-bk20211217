0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -1897.81    44.30
p_loo       48.88        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      590   93.4%
 (0.5, 0.7]   (ok)         27    4.3%
   (0.7, 1]   (bad)        13    2.1%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.167  0.017   0.150    0.203  ...   121.0      79.0      57.0   1.03
pu        0.705  0.005   0.700    0.718  ...    87.0     101.0      38.0   1.10
mu        0.116  0.020   0.084    0.155  ...    40.0      31.0      54.0   1.08
mus       0.208  0.032   0.156    0.259  ...   146.0     152.0      80.0   1.06
gamma     0.308  0.052   0.199    0.382  ...   100.0     102.0      60.0   1.02
Is_begin  0.489  0.617   0.000    1.828  ...   110.0      80.0     100.0   1.02
Ia_begin  0.882  1.160   0.004    2.729  ...    68.0      57.0      74.0   1.04
E_begin   0.346  0.449   0.000    1.199  ...    71.0      42.0      40.0   1.03

[8 rows x 11 columns]