0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 633 log-likelihood matrix

         Estimate       SE
elpd_loo -5705.04    32.49
p_loo       29.48        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      578   91.3%
 (0.5, 0.7]   (ok)         45    7.1%
   (0.7, 1]   (bad)         9    1.4%
   (1, Inf)   (very bad)    1    0.2%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.243   0.059   0.150    0.340  ...    99.0      96.0      60.0   1.01
pu         0.821   0.049   0.706    0.874  ...    15.0      21.0      57.0   1.25
mu         0.096   0.015   0.074    0.126  ...     7.0       7.0      31.0   1.33
mus        0.139   0.025   0.099    0.189  ...    22.0      17.0      69.0   1.11
gamma      0.221   0.035   0.162    0.284  ...   140.0     135.0      93.0   0.98
Is_begin  29.201  25.590   0.332   77.106  ...    87.0     152.0      73.0   1.01
Ia_begin  84.290  69.380   2.594  200.461  ...    65.0      44.0      43.0   1.03
E_begin   57.428  43.191   4.847  145.628  ...    88.0      94.0      48.0   1.03

[8 rows x 11 columns]