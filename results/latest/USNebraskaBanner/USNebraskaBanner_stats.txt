0 Divergences 
Failed validation 
2021-12-09 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 635 log-likelihood matrix

         Estimate       SE
elpd_loo  -310.05    94.76
p_loo       88.51        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      580   91.3%
 (0.5, 0.7]   (ok)         43    6.8%
   (0.7, 1]   (bad)        10    1.6%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.242  0.057   0.161    0.340  ...    61.0      77.0      40.0   1.01
pu        0.737  0.023   0.701    0.786  ...    10.0      10.0      23.0   1.18
mu        0.126  0.024   0.090    0.179  ...    43.0      57.0      59.0   1.05
mus       0.246  0.041   0.173    0.332  ...    54.0      54.0      57.0   1.02
gamma     0.262  0.045   0.195    0.361  ...    53.0      57.0      60.0   1.02
Is_begin  0.252  0.287   0.000    0.733  ...    32.0      17.0      22.0   1.12
Ia_begin  0.343  0.424   0.000    1.219  ...    20.0      17.0      22.0   1.12
E_begin   0.197  0.275   0.000    0.789  ...    47.0      28.0      42.0   1.08

[8 rows x 11 columns]