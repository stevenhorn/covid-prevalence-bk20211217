0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2340.35    44.28
p_loo       24.90        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      612   96.5%
 (0.5, 0.7]   (ok)         21    3.3%
   (0.7, 1]   (bad)         1    0.2%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.152  0.001   0.150    0.154  ...     3.0       3.0      15.0   2.01
pu        0.706  0.001   0.705    0.708  ...     4.0       4.0      14.0   1.66
mu        0.141  0.016   0.118    0.168  ...     3.0       3.0      33.0   2.64
mus       0.161  0.014   0.142    0.184  ...     3.0       4.0      32.0   1.78
gamma     0.282  0.045   0.229    0.368  ...     3.0       3.0      18.0   2.20
Is_begin  0.328  0.063   0.245    0.452  ...     4.0       4.0      14.0   1.58
Ia_begin  4.182  1.778   1.901    7.119  ...     3.0       3.0      15.0   2.35
E_begin   0.980  0.283   0.632    1.527  ...     4.0       4.0      14.0   1.74

[8 rows x 11 columns]