0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1847.90    49.49
p_loo       51.41        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      595   93.8%
 (0.5, 0.7]   (ok)         29    4.6%
   (0.7, 1]   (bad)         7    1.1%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.209  0.034   0.153    0.270  ...    86.0      98.0      88.0   1.02
pu        0.716  0.013   0.700    0.744  ...    75.0      62.0      26.0   1.02
mu        0.119  0.014   0.096    0.147  ...    38.0      36.0      32.0   1.04
mus       0.181  0.029   0.125    0.237  ...    21.0      41.0      27.0   1.05
gamma     0.268  0.045   0.204    0.363  ...    14.0      15.0      33.0   1.14
Is_begin  0.702  0.639   0.009    1.670  ...   106.0      86.0      60.0   1.01
Ia_begin  1.175  1.157   0.005    3.070  ...    70.0      62.0      59.0   1.01
E_begin   0.577  0.729   0.008    1.631  ...    66.0      70.0      65.0   1.03

[8 rows x 11 columns]