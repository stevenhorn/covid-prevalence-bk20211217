0 Divergences 
Failed validation 
2021-12-09 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 635 log-likelihood matrix

         Estimate       SE
elpd_loo  -989.09    92.19
p_loo       69.07        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      600   94.5%
 (0.5, 0.7]   (ok)         26    4.1%
   (0.7, 1]   (bad)         5    0.8%
   (1, Inf)   (very bad)    4    0.6%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.214  0.044   0.150    0.292  ...   108.0     116.0      59.0   1.00
pu        0.717  0.013   0.700    0.743  ...   114.0      49.0      22.0   1.03
mu        0.148  0.023   0.109    0.191  ...    69.0      72.0      96.0   1.00
mus       0.278  0.035   0.208    0.339  ...    68.0      69.0      75.0   1.03
gamma     0.514  0.073   0.411    0.653  ...    41.0      48.0      59.0   1.04
Is_begin  0.577  0.556   0.008    1.610  ...   118.0     137.0      96.0   1.03
Ia_begin  0.748  0.677   0.023    2.021  ...   115.0     115.0      60.0   1.00
E_begin   0.375  0.351   0.001    0.887  ...    50.0      33.0      93.0   1.05

[8 rows x 11 columns]