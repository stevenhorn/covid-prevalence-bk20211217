0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -1862.62    47.99
p_loo       43.56        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      587   92.9%
 (0.5, 0.7]   (ok)         37    5.9%
   (0.7, 1]   (bad)         6    0.9%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.178  0.024   0.151    0.226  ...    27.0      39.0      21.0   1.06
pu        0.709  0.008   0.700    0.724  ...    71.0      46.0      22.0   1.02
mu        0.124  0.018   0.095    0.152  ...    13.0      13.0      57.0   1.14
mus       0.184  0.029   0.136    0.234  ...    85.0      65.0      60.0   1.01
gamma     0.325  0.050   0.237    0.426  ...    39.0      40.0      60.0   1.05
Is_begin  0.534  0.563   0.009    1.564  ...   133.0      70.0      40.0   1.04
Ia_begin  0.905  0.906   0.008    2.991  ...    56.0      20.0      57.0   1.09
E_begin   0.434  0.487   0.009    1.402  ...    68.0      68.0      60.0   1.03

[8 rows x 11 columns]