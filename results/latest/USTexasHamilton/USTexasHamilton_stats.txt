0 Divergences 
Failed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -1538.58    77.27
p_loo       65.14        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      601   95.1%
 (0.5, 0.7]   (ok)         22    3.5%
   (0.7, 1]   (bad)         5    0.8%
   (1, Inf)   (very bad)    4    0.6%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.186  0.029   0.150    0.242  ...   152.0      98.0      18.0   1.02
pu        0.710  0.007   0.701    0.726  ...    73.0      76.0      93.0   1.00
mu        0.142  0.022   0.112    0.191  ...    45.0      47.0      76.0   1.03
mus       0.309  0.041   0.251    0.399  ...    65.0      65.0      91.0   1.02
gamma     0.434  0.070   0.304    0.563  ...    77.0      72.0      59.0   1.04
Is_begin  0.733  0.711   0.020    2.038  ...   152.0     152.0      95.0   0.99
Ia_begin  1.149  1.451   0.022    3.918  ...    87.0      95.0      84.0   1.02
E_begin   0.499  0.678   0.002    1.362  ...    73.0     107.0      49.0   1.00

[8 rows x 11 columns]