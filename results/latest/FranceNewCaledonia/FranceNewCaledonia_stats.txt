0 Divergences 
Failed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 633 log-likelihood matrix

         Estimate       SE
elpd_loo -1923.80    85.87
p_loo       59.69        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      617   97.5%
 (0.5, 0.7]   (ok)         10    1.6%
   (0.7, 1]   (bad)         1    0.2%
   (1, Inf)   (very bad)    5    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.319  0.019   0.291    0.348  ...    64.0      60.0      57.0   1.03
pu        0.895  0.003   0.889    0.900  ...    65.0      52.0      32.0   1.01
mu        0.065  0.009   0.048    0.079  ...    75.0      58.0      83.0   1.04
mus       0.047  0.001   0.044    0.048  ...    80.0      72.0      42.0   1.02
gamma     0.702  0.091   0.565    0.912  ...    98.0      98.0      54.0   1.03
Is_begin  0.625  0.481   0.010    1.474  ...   110.0      92.0      47.0   1.00
Ia_begin  0.660  0.548   0.004    1.712  ...   101.0      74.0      69.0   1.04
E_begin   0.340  0.324   0.007    0.966  ...    72.0      50.0      54.0   1.03

[8 rows x 11 columns]