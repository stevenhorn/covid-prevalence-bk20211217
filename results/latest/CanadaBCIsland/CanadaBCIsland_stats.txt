15 Divergences 
Passed validation 
2021-12-16 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 643 log-likelihood matrix

         Estimate       SE
elpd_loo -2537.28    54.55
p_loo       84.45        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      601   93.5%
 (0.5, 0.7]   (ok)         36    5.6%
   (0.7, 1]   (bad)         5    0.8%
   (1, Inf)   (very bad)    1    0.2%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.293   0.035   0.234    0.348  ...     8.0       9.0      56.0   1.19
pu         0.889   0.009   0.873    0.900  ...    21.0      17.0      41.0   1.12
mu         0.265   0.078   0.158    0.353  ...     3.0       3.0      18.0   2.35
mus        0.273   0.097   0.139    0.408  ...     3.0       4.0      55.0   1.79
gamma      0.264   0.042   0.199    0.348  ...     5.0       5.0      24.0   1.38
Is_begin   4.819   3.941   0.306   11.776  ...    44.0      32.0      55.0   1.03
Ia_begin  26.268  22.837   0.247   79.547  ...     7.0       7.0      59.0   1.30
E_begin   12.107  14.983   0.217   38.331  ...    11.0      13.0      59.0   1.11

[8 rows x 11 columns]