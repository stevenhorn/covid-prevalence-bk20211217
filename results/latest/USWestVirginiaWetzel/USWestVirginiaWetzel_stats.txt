0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -1569.24    34.72
p_loo       40.76        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      577   91.3%
 (0.5, 0.7]   (ok)         43    6.8%
   (0.7, 1]   (bad)        11    1.7%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.167  0.017   0.150    0.198  ...   152.0     152.0      40.0   0.99
pu        0.704  0.004   0.700    0.714  ...   133.0      96.0      54.0   1.01
mu        0.121  0.016   0.094    0.152  ...    38.0      38.0      44.0   1.03
mus       0.190  0.032   0.128    0.247  ...    70.0      73.0      59.0   1.04
gamma     0.279  0.039   0.195    0.336  ...   105.0     129.0      59.0   1.05
Is_begin  0.541  0.566   0.012    1.654  ...    99.0     152.0      72.0   1.03
Ia_begin  1.071  1.130   0.009    3.203  ...    96.0     152.0      95.0   1.03
E_begin   0.394  0.455   0.006    1.027  ...   105.0     152.0      55.0   1.03

[8 rows x 11 columns]