0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -3616.86    36.76
p_loo       35.47        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      580   91.5%
 (0.5, 0.7]   (ok)         46    7.3%
   (0.7, 1]   (bad)         6    0.9%
   (1, Inf)   (very bad)    2    0.3%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.198   0.046   0.150    0.284  ...    38.0      60.0      16.0   1.04
pu         0.713   0.012   0.700    0.734  ...    49.0      24.0      44.0   1.06
mu         0.139   0.021   0.100    0.170  ...    10.0      10.0      77.0   1.23
mus        0.175   0.023   0.141    0.214  ...   152.0     152.0      84.0   1.03
gamma      0.304   0.048   0.232    0.387  ...    94.0     123.0      60.0   1.06
Is_begin   7.687   4.198   0.179   15.596  ...    44.0      34.0      47.0   1.03
Ia_begin  21.161   7.513   9.559   34.988  ...   129.0     122.0      91.0   1.00
E_begin   35.133  16.121   5.200   61.995  ...   105.0     102.0      51.0   0.99

[8 rows x 11 columns]