0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -2354.04    49.90
p_loo       50.11        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      589   93.2%
 (0.5, 0.7]   (ok)         38    6.0%
   (0.7, 1]   (bad)         2    0.3%
   (1, Inf)   (very bad)    3    0.5%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.234   0.050   0.151    0.317  ...   128.0     115.0      16.0   1.00
pu         0.746   0.029   0.702    0.793  ...    99.0      98.0      86.0   1.01
mu         0.176   0.028   0.131    0.238  ...    20.0      20.0      59.0   1.09
mus        0.233   0.038   0.169    0.307  ...    77.0      83.0      44.0   0.99
gamma      0.375   0.051   0.272    0.474  ...    91.0      89.0      59.0   1.03
Is_begin   6.877   3.392   1.070   12.363  ...   101.0      93.0      93.0   0.99
Ia_begin  14.853   8.567   0.202   28.668  ...    71.0      62.0      39.0   1.06
E_begin   16.567  10.566   0.232   36.578  ...    54.0      31.0      38.0   1.04

[8 rows x 11 columns]