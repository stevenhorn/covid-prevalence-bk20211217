0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2018.84    38.35
p_loo       38.32        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      585   92.3%
 (0.5, 0.7]   (ok)         38    6.0%
   (0.7, 1]   (bad)         9    1.4%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.166  0.015   0.150    0.194  ...    92.0      89.0      88.0   1.05
pu        0.704  0.004   0.700    0.712  ...    64.0      43.0      80.0   1.02
mu        0.116  0.014   0.090    0.140  ...    15.0      16.0      54.0   1.09
mus       0.167  0.031   0.120    0.230  ...    37.0      84.0      24.0   1.09
gamma     0.224  0.036   0.168    0.289  ...   130.0     109.0      60.0   1.01
Is_begin  0.451  0.419   0.006    1.287  ...    63.0      36.0      40.0   1.02
Ia_begin  0.836  1.205   0.011    2.922  ...    28.0      60.0      60.0   1.12
E_begin   0.380  0.599   0.000    1.213  ...    69.0      42.0      59.0   1.06

[8 rows x 11 columns]