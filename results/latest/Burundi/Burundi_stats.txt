0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 633 log-likelihood matrix

         Estimate       SE
elpd_loo -2915.77    61.55
p_loo       66.57        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      604   95.4%
 (0.5, 0.7]   (ok)         14    2.2%
   (0.7, 1]   (bad)        10    1.6%
   (1, Inf)   (very bad)    5    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.290  0.049   0.194    0.349  ...    81.0      84.0      23.0   1.10
pu        0.887  0.014   0.857    0.900  ...    74.0     112.0      60.0   1.00
mu        0.194  0.028   0.152    0.244  ...     8.0       8.0      40.0   1.20
mus       0.215  0.035   0.152    0.280  ...   109.0     107.0      83.0   1.05
gamma     0.306  0.045   0.242    0.381  ...    75.0      78.0      57.0   1.00
Is_begin  0.937  0.882   0.013    2.898  ...   121.0      99.0      91.0   0.98
Ia_begin  2.249  2.148   0.041    6.383  ...    89.0      83.0      60.0   1.01
E_begin   1.276  1.193   0.047    3.677  ...    67.0      81.0      58.0   1.02

[8 rows x 11 columns]