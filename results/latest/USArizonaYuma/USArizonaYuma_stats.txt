0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -3008.80    51.56
p_loo       37.89        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      588   92.7%
 (0.5, 0.7]   (ok)         36    5.7%
   (0.7, 1]   (bad)         9    1.4%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.156  0.006   0.150    0.169  ...   152.0     111.0      31.0   1.00
pu        0.702  0.002   0.700    0.706  ...   148.0      99.0      48.0   0.99
mu        0.075  0.008   0.062    0.089  ...    48.0      52.0      59.0   0.99
mus       0.127  0.016   0.091    0.151  ...    88.0      94.0      88.0   1.01
gamma     0.307  0.055   0.205    0.402  ...    72.0      95.0      84.0   1.00
Is_begin  0.429  0.402   0.004    1.139  ...   111.0      58.0      83.0   1.02
Ia_begin  0.693  0.639   0.010    2.044  ...    70.0      54.0      60.0   1.03
E_begin   0.311  0.384   0.006    0.905  ...   102.0      64.0      60.0   0.99

[8 rows x 11 columns]