3 Divergences 
Failed validation 
2021-12-09 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 635 log-likelihood matrix

         Estimate       SE
elpd_loo -3832.03    44.13
p_loo       38.33        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      590   92.9%
 (0.5, 0.7]   (ok)         34    5.4%
   (0.7, 1]   (bad)        10    1.6%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.174  0.022   0.150    0.211  ...   152.0     152.0      65.0   0.99
pu        0.708  0.007   0.700    0.720  ...   140.0     100.0      57.0   1.01
mu        0.103  0.019   0.069    0.136  ...     9.0       9.0      59.0   1.23
mus       0.163  0.023   0.128    0.211  ...    89.0     102.0      97.0   1.00
gamma     0.194  0.033   0.142    0.254  ...    90.0      98.0      91.0   1.05
Is_begin  0.491  0.468   0.012    1.478  ...   124.0      85.0      38.0   1.00
Ia_begin  0.935  0.724   0.029    2.361  ...    54.0      64.0      88.0   1.01
E_begin   0.389  0.438   0.001    1.475  ...    65.0      52.0      56.0   1.04

[8 rows x 11 columns]