0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1299.10    47.80
p_loo       47.48        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      605   95.4%
 (0.5, 0.7]   (ok)         23    3.6%
   (0.7, 1]   (bad)         4    0.6%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.262  0.044   0.186    0.344  ...   129.0     133.0      88.0   1.00
pu        0.825  0.014   0.800    0.847  ...   152.0     152.0      59.0   1.03
mu        0.116  0.020   0.088    0.151  ...    14.0      16.0      60.0   1.13
mus       0.168  0.033   0.104    0.218  ...   113.0     121.0      60.0   1.02
gamma     0.229  0.039   0.149    0.276  ...    83.0      80.0      59.0   1.00
Is_begin  0.839  0.773   0.011    2.016  ...   104.0      95.0      57.0   1.00
Ia_begin  1.610  1.426   0.035    4.539  ...    91.0     114.0      74.0   1.01
E_begin   0.781  0.815   0.008    2.145  ...    30.0      19.0      95.0   1.08

[8 rows x 11 columns]