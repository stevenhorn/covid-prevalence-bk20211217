2 Divergences 
Passed validation 
2021-12-09 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 635 log-likelihood matrix

         Estimate       SE
elpd_loo -3594.47    37.91
p_loo       56.52        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      587   92.4%
 (0.5, 0.7]   (ok)         29    4.6%
   (0.7, 1]   (bad)        16    2.5%
   (1, Inf)   (very bad)    3    0.5%

             mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa          0.174   0.023   0.150    0.223  ...    19.0      56.0      18.0   1.20
pu          0.707   0.007   0.700    0.719  ...    20.0      66.0      74.0   1.16
mu          0.100   0.014   0.072    0.119  ...     7.0       9.0      14.0   1.19
mus         0.197   0.028   0.152    0.246  ...    39.0      41.0      39.0   1.07
gamma       0.429   0.069   0.322    0.552  ...   152.0     152.0      66.0   1.04
Is_begin   44.447  22.412   3.781   79.165  ...    50.0      53.0      60.0   1.02
Ia_begin   97.267  60.157   5.018  205.117  ...    20.0      15.0      20.0   1.11
E_begin   103.515  67.047  12.584  231.608  ...    95.0      89.0      99.0   1.00

[8 rows x 11 columns]