0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1392.05    48.58
p_loo       54.00        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      581   91.6%
 (0.5, 0.7]   (ok)         41    6.5%
   (0.7, 1]   (bad)        10    1.6%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.245  0.048   0.159    0.319  ...    40.0      43.0      83.0   1.02
pu        0.753  0.033   0.702    0.804  ...     9.0       9.0      31.0   1.17
mu        0.127  0.021   0.087    0.163  ...     8.0       8.0      39.0   1.21
mus       0.216  0.042   0.160    0.312  ...   103.0     152.0      40.0   1.08
gamma     0.279  0.059   0.181    0.394  ...    73.0     101.0      60.0   1.14
Is_begin  0.868  0.752   0.002    2.005  ...    76.0      55.0     100.0   1.04
Ia_begin  1.369  1.600   0.019    5.433  ...   139.0     113.0      88.0   0.99
E_begin   0.768  0.757   0.002    2.288  ...   122.0      83.0      60.0   1.00

[8 rows x 11 columns]