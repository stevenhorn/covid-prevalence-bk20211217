0 Divergences 
Failed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -1348.13    78.35
p_loo       60.90        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      607   96.0%
 (0.5, 0.7]   (ok)         20    3.2%
   (0.7, 1]   (bad)         4    0.6%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.184  0.029   0.150    0.244  ...    76.0      66.0      40.0   1.04
pu        0.711  0.007   0.701    0.724  ...   138.0     130.0      37.0   0.99
mu        0.130  0.021   0.099    0.167  ...    72.0      96.0      60.0   1.06
mus       0.245  0.040   0.187    0.328  ...    66.0      61.0      43.0   1.03
gamma     0.317  0.057   0.226    0.406  ...    53.0      69.0      60.0   1.02
Is_begin  0.742  0.880   0.014    2.559  ...    72.0      72.0      40.0   1.07
Ia_begin  1.227  1.314   0.023    3.757  ...    92.0     100.0      65.0   1.02
E_begin   0.557  0.708   0.008    1.895  ...    42.0     107.0      49.0   1.01

[8 rows x 11 columns]