0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2441.25    37.35
p_loo       38.13        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      598   94.3%
 (0.5, 0.7]   (ok)         29    4.6%
   (0.7, 1]   (bad)         6    0.9%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.188  0.032   0.150    0.250  ...    63.0      45.0      18.0   1.09
pu        0.713  0.011   0.701    0.732  ...    52.0      57.0      60.0   1.00
mu        0.116  0.018   0.087    0.151  ...     7.0       7.0      42.0   1.27
mus       0.165  0.024   0.116    0.202  ...    88.0      91.0      60.0   1.10
gamma     0.225  0.041   0.161    0.287  ...    61.0      60.0      18.0   1.02
Is_begin  0.959  0.787   0.001    2.610  ...    97.0      93.0      72.0   0.98
Ia_begin  1.697  1.616   0.085    5.327  ...    61.0      34.0      91.0   1.02
E_begin   0.784  0.901   0.019    2.660  ...    51.0      34.0      23.0   1.06

[8 rows x 11 columns]