0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo  -680.56    92.00
p_loo      102.90        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      580   91.5%
 (0.5, 0.7]   (ok)         34    5.4%
   (0.7, 1]   (bad)        16    2.5%
   (1, Inf)   (very bad)    4    0.6%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.177  0.024   0.150    0.220  ...    62.0      45.0      14.0   1.05
pu        0.709  0.007   0.700    0.721  ...    27.0      24.0      49.0   1.07
mu        0.210  0.042   0.125    0.268  ...    18.0      21.0      17.0   1.07
mus       0.375  0.075   0.256    0.516  ...     8.0       8.0      38.0   1.22
gamma     0.448  0.088   0.298    0.615  ...     6.0       6.0      39.0   1.34
Is_begin  0.539  0.773   0.006    2.102  ...    76.0      36.0      59.0   1.03
Ia_begin  0.749  1.275   0.000    2.104  ...    73.0      29.0      22.0   1.02
E_begin   0.364  0.411   0.009    1.256  ...    44.0      23.0      36.0   1.04

[8 rows x 11 columns]