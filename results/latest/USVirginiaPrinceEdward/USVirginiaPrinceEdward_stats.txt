0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -1795.40    33.68
p_loo       44.80        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      580   91.8%
 (0.5, 0.7]   (ok)         40    6.3%
   (0.7, 1]   (bad)        10    1.6%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.243  0.051   0.157    0.339  ...    70.0      71.0      56.0   1.03
pu        0.740  0.023   0.700    0.779  ...    40.0      40.0      58.0   1.05
mu        0.144  0.021   0.113    0.189  ...    39.0      41.0      60.0   1.03
mus       0.189  0.029   0.142    0.244  ...    73.0      72.0      99.0   1.00
gamma     0.261  0.045   0.185    0.342  ...   152.0     152.0      43.0   0.99
Is_begin  0.608  0.660   0.000    1.904  ...   107.0     121.0      57.0   1.00
Ia_begin  1.121  1.237   0.007    3.055  ...   111.0     152.0      74.0   1.00
E_begin   0.512  0.681   0.012    2.184  ...    93.0      37.0      58.0   1.04

[8 rows x 11 columns]