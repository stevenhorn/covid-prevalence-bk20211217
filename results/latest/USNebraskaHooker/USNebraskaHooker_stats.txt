0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo  -461.45    61.91
p_loo       75.08        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      557   87.9%
 (0.5, 0.7]   (ok)         61    9.6%
   (0.7, 1]   (bad)        10    1.6%
   (1, Inf)   (very bad)    6    0.9%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.253  0.053   0.165    0.333  ...    78.0      83.0      59.0   1.07
pu        0.837  0.019   0.808    0.872  ...    70.0      72.0      61.0   1.05
mu        0.147  0.033   0.093    0.198  ...    64.0      62.0      59.0   1.00
mus       0.270  0.035   0.216    0.342  ...    81.0      83.0      86.0   1.02
gamma     0.374  0.058   0.290    0.481  ...    95.0      99.0      59.0   1.05
Is_begin  0.677  0.724   0.008    2.170  ...    66.0      66.0      81.0   1.01
Ia_begin  1.059  1.229   0.003    4.000  ...    87.0      90.0      87.0   1.02
E_begin   0.455  0.716   0.002    1.076  ...    98.0      57.0     100.0   1.01

[8 rows x 11 columns]