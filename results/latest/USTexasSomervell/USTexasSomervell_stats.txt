0 Divergences 
Failed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -1636.31    72.27
p_loo       60.39        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      604   95.6%
 (0.5, 0.7]   (ok)         22    3.5%
   (0.7, 1]   (bad)         3    0.5%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.168  0.015   0.150    0.194  ...   152.0     152.0      38.0   1.08
pu        0.707  0.006   0.700    0.717  ...   134.0     103.0      60.0   1.05
mu        0.105  0.016   0.071    0.128  ...    82.0      83.0     100.0   1.03
mus       0.201  0.039   0.139    0.262  ...    53.0      63.0      49.0   1.01
gamma     0.292  0.055   0.200    0.403  ...    77.0      68.0      59.0   1.00
Is_begin  0.192  0.229   0.000    0.592  ...    63.0      56.0      18.0   1.02
Ia_begin  0.270  0.342   0.001    0.947  ...    69.0      86.0      33.0   0.99
E_begin   0.154  0.269   0.002    0.344  ...    71.0      65.0      60.0   1.00

[8 rows x 11 columns]