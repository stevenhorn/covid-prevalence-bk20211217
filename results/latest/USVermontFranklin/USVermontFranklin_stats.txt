0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -1772.40    62.68
p_loo       58.99        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      586   92.7%
 (0.5, 0.7]   (ok)         40    6.3%
   (0.7, 1]   (bad)         4    0.6%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.253  0.056   0.169    0.349  ...   120.0     110.0      42.0   1.01
pu        0.797  0.023   0.756    0.836  ...    89.0     102.0      74.0   1.08
mu        0.140  0.017   0.110    0.169  ...    29.0      22.0      53.0   1.06
mus       0.211  0.039   0.143    0.275  ...   152.0     152.0      60.0   1.01
gamma     0.337  0.054   0.232    0.406  ...   104.0      88.0      32.0   1.03
Is_begin  1.316  1.128   0.057    3.257  ...   101.0      85.0      59.0   1.00
Ia_begin  0.754  0.474   0.056    1.588  ...   123.0     124.0      97.0   1.00
E_begin   1.226  1.241   0.006    4.633  ...   146.0     148.0      88.0   1.00

[8 rows x 11 columns]