0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2886.12    41.05
p_loo       33.74        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      572   90.2%
 (0.5, 0.7]   (ok)         52    8.2%
   (0.7, 1]   (bad)         9    1.4%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.159  0.009   0.150    0.175  ...    96.0     128.0      55.0   1.04
pu        0.703  0.002   0.700    0.707  ...   120.0      92.0      59.0   1.00
mu        0.070  0.010   0.056    0.091  ...    12.0      14.0      59.0   1.13
mus       0.134  0.024   0.092    0.176  ...   152.0     152.0      88.0   1.01
gamma     0.241  0.037   0.180    0.302  ...   112.0     152.0      86.0   1.01
Is_begin  0.746  0.799   0.013    2.648  ...   105.0      72.0      93.0   1.02
Ia_begin  1.489  1.452   0.032    3.684  ...    84.0      51.0      40.0   1.04
E_begin   0.512  0.497   0.001    1.459  ...    65.0      54.0      58.0   1.01

[8 rows x 11 columns]