0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -3788.14    49.94
p_loo       59.40        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      563   88.8%
 (0.5, 0.7]   (ok)         55    8.7%
   (0.7, 1]   (bad)        12    1.9%
   (1, Inf)   (very bad)    4    0.6%

             mean       sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa          0.232    0.044   0.161    0.311  ...    78.0      61.0      38.0   1.03
pu          0.731    0.028   0.701    0.787  ...    27.0      30.0      60.0   1.09
mu          0.157    0.020   0.124    0.200  ...    25.0      28.0      31.0   1.08
mus         0.225    0.034   0.163    0.292  ...    33.0      36.0      73.0   1.06
gamma       0.326    0.048   0.244    0.410  ...    65.0      60.0      53.0   1.03
Is_begin   55.394   32.403   3.424  116.297  ...    29.0      20.0      46.0   1.10
Ia_begin  163.732   53.697  73.232  253.065  ...   109.0      98.0      97.0   1.05
E_begin   224.887  107.834  15.359  398.823  ...    68.0      65.0      40.0   1.03

[8 rows x 11 columns]