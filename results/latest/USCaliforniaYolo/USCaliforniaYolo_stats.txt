0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2629.33    36.34
p_loo       38.82        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      585   92.3%
 (0.5, 0.7]   (ok)         41    6.5%
   (0.7, 1]   (bad)         7    1.1%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.251  0.052   0.167    0.345  ...    78.0      66.0      39.0   1.02
pu        0.773  0.029   0.722    0.828  ...    12.0      14.0      30.0   1.09
mu        0.133  0.021   0.094    0.169  ...    29.0      30.0      77.0   1.01
mus       0.175  0.034   0.120    0.229  ...   113.0     113.0      59.0   1.01
gamma     0.251  0.036   0.194    0.317  ...   132.0     133.0     100.0   1.05
Is_begin  0.728  0.525   0.045    1.818  ...   148.0     105.0      60.0   0.98
Ia_begin  1.376  1.108   0.075    2.911  ...    42.0      37.0      45.0   1.04
E_begin   0.879  0.923   0.009    2.841  ...    70.0      40.0      48.0   1.03

[8 rows x 11 columns]