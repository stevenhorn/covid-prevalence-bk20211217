0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2209.27    43.78
p_loo       37.97        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      601   94.8%
 (0.5, 0.7]   (ok)         27    4.3%
   (0.7, 1]   (bad)         5    0.8%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.161  0.010   0.150    0.184  ...   107.0     112.0      87.0   1.00
pu        0.704  0.004   0.700    0.715  ...   152.0     152.0      49.0   1.01
mu        0.090  0.015   0.062    0.111  ...    36.0      32.0      46.0   1.04
mus       0.161  0.030   0.104    0.212  ...   144.0     152.0      60.0   1.00
gamma     0.256  0.043   0.187    0.336  ...    82.0      94.0      59.0   0.98
Is_begin  0.430  0.411   0.012    1.094  ...    72.0      79.0      68.0   1.00
Ia_begin  0.676  0.900   0.020    1.782  ...    72.0      93.0      88.0   1.00
E_begin   0.295  0.347   0.003    0.817  ...    93.0      86.0      59.0   0.99

[8 rows x 11 columns]