0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -1994.46    50.77
p_loo       47.24        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      601   95.1%
 (0.5, 0.7]   (ok)         23    3.6%
   (0.7, 1]   (bad)         6    0.9%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.172  0.015   0.150    0.198  ...    97.0      74.0      22.0   1.02
pu        0.707  0.007   0.700    0.720  ...    58.0      81.0      32.0   1.04
mu        0.103  0.013   0.071    0.120  ...    42.0      39.0      96.0   1.03
mus       0.202  0.036   0.150    0.279  ...    82.0     115.0      81.0   1.05
gamma     0.288  0.047   0.209    0.380  ...   119.0     109.0      56.0   1.01
Is_begin  0.582  0.712   0.012    1.888  ...    99.0      75.0      56.0   1.01
Ia_begin  0.904  0.847   0.006    2.084  ...   110.0     107.0      56.0   1.01
E_begin   0.374  0.364   0.005    1.033  ...    55.0      49.0      45.0   1.03

[8 rows x 11 columns]