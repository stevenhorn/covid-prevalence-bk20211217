0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2806.53    80.80
p_loo       87.96        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      587   92.6%
 (0.5, 0.7]   (ok)         39    6.2%
   (0.7, 1]   (bad)         4    0.6%
   (1, Inf)   (very bad)    4    0.6%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.224  0.057   0.155    0.333  ...    90.0     140.0      74.0   1.08
pu        0.724  0.020   0.700    0.770  ...    65.0      53.0      60.0   0.99
mu        0.117  0.012   0.094    0.138  ...    10.0      10.0      38.0   1.18
mus       0.353  0.058   0.265    0.465  ...    11.0       9.0      21.0   1.18
gamma     0.589  0.102   0.418    0.756  ...     8.0       8.0      85.0   1.23
Is_begin  1.047  0.840   0.027    2.631  ...   108.0      88.0      60.0   0.99
Ia_begin  1.353  1.342   0.002    4.008  ...    98.0      65.0      88.0   1.01
E_begin   0.610  0.581   0.002    1.705  ...    62.0      68.0      40.0   1.09

[8 rows x 11 columns]