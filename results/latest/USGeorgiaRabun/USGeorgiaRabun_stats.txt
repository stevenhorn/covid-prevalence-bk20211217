0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1613.86    34.26
p_loo       39.87        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      602   95.0%
 (0.5, 0.7]   (ok)         24    3.8%
   (0.7, 1]   (bad)         7    1.1%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.209  0.044   0.152    0.289  ...    71.0     125.0      59.0   1.04
pu        0.722  0.015   0.701    0.745  ...   146.0     114.0      80.0   1.01
mu        0.132  0.018   0.100    0.162  ...    40.0      41.0      59.0   1.01
mus       0.188  0.029   0.134    0.239  ...   152.0     152.0      66.0   0.98
gamma     0.240  0.043   0.159    0.301  ...   112.0     119.0      32.0   1.01
Is_begin  0.570  0.530   0.013    1.674  ...   148.0     126.0      88.0   1.03
Ia_begin  1.163  1.211   0.004    3.149  ...    93.0     152.0      60.0   1.00
E_begin   0.579  0.785   0.005    1.895  ...   115.0     117.0      59.0   1.00

[8 rows x 11 columns]