0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1577.10    36.55
p_loo       39.48        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      582   91.8%
 (0.5, 0.7]   (ok)         42    6.6%
   (0.7, 1]   (bad)        10    1.6%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.240  0.053   0.153    0.326  ...    63.0      64.0      93.0   1.00
pu        0.799  0.023   0.748    0.834  ...    15.0      16.0      49.0   1.11
mu        0.161  0.030   0.120    0.229  ...    22.0      23.0      56.0   1.14
mus       0.192  0.038   0.143    0.266  ...    12.0      13.0      59.0   1.14
gamma     0.241  0.043   0.166    0.330  ...   110.0      94.0      69.0   1.03
Is_begin  0.712  0.719   0.010    1.976  ...   107.0      92.0      75.0   0.99
Ia_begin  1.623  1.446   0.083    4.450  ...    94.0     152.0      91.0   1.06
E_begin   0.834  0.917   0.018    2.668  ...   114.0      82.0      59.0   1.01

[8 rows x 11 columns]