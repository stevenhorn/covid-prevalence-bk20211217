0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -3119.71    65.64
p_loo       59.80        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      600   94.9%
 (0.5, 0.7]   (ok)         27    4.3%
   (0.7, 1]   (bad)         4    0.6%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.160  0.011   0.150    0.179  ...    85.0      60.0      30.0   0.99
pu        0.704  0.003   0.700    0.709  ...    70.0      99.0      60.0   1.01
mu        0.099  0.013   0.077    0.120  ...    11.0      11.0      46.0   1.13
mus       0.160  0.030   0.107    0.222  ...    28.0      30.0      54.0   1.09
gamma     0.266  0.041   0.201    0.334  ...    43.0      29.0      96.0   1.06
Is_begin  0.678  0.703   0.009    1.781  ...    70.0      42.0      32.0   0.99
Ia_begin  0.461  0.477   0.002    1.347  ...    66.0      52.0      60.0   1.01
E_begin   0.397  0.580   0.001    1.538  ...   104.0      63.0      35.0   0.99

[8 rows x 11 columns]