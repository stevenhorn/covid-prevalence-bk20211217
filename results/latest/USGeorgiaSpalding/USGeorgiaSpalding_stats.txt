0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2434.45    61.77
p_loo       59.49        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      584   92.1%
 (0.5, 0.7]   (ok)         39    6.2%
   (0.7, 1]   (bad)         9    1.4%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.173  0.019   0.150    0.208  ...   110.0      91.0      60.0   1.04
pu        0.706  0.006   0.700    0.718  ...   135.0     133.0      99.0   0.98
mu        0.119  0.019   0.092    0.154  ...    10.0      11.0      30.0   1.14
mus       0.229  0.051   0.162    0.338  ...    37.0      38.0      38.0   1.02
gamma     0.339  0.048   0.257    0.443  ...    99.0      83.0      88.0   1.02
Is_begin  0.975  0.898   0.024    2.569  ...   119.0     109.0      74.0   1.04
Ia_begin  0.652  0.454   0.012    1.427  ...   125.0      82.0      58.0   1.03
E_begin   0.613  0.677   0.062    1.553  ...    81.0     103.0      49.0   1.00

[8 rows x 11 columns]