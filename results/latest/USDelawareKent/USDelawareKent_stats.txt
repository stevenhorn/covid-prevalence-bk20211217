0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2735.00    58.33
p_loo       63.10        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      585   92.3%
 (0.5, 0.7]   (ok)         35    5.5%
   (0.7, 1]   (bad)        11    1.7%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.161  0.009   0.151    0.181  ...   152.0     152.0      95.0   1.02
pu        0.703  0.003   0.700    0.709  ...   152.0     128.0      99.0   1.01
mu        0.098  0.011   0.074    0.118  ...    22.0      24.0      60.0   1.06
mus       0.180  0.028   0.134    0.235  ...   114.0     126.0      99.0   1.03
gamma     0.323  0.062   0.211    0.420  ...   152.0     152.0      97.0   1.01
Is_begin  0.712  0.707   0.002    2.213  ...   152.0      59.0      43.0   1.02
Ia_begin  1.614  1.156   0.116    4.345  ...    78.0      67.0      60.0   1.04
E_begin   1.271  1.248   0.020    4.091  ...    92.0      70.0      59.0   1.00

[8 rows x 11 columns]