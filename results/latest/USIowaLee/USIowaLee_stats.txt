0 Divergences 
Passed validation 
2021-12-09 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 635 log-likelihood matrix

         Estimate       SE
elpd_loo -2383.85    62.99
p_loo       62.83        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      600   94.5%
 (0.5, 0.7]   (ok)         25    3.9%
   (0.7, 1]   (bad)         7    1.1%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.159  0.008   0.150    0.174  ...   138.0     152.0      91.0   1.07
pu        0.703  0.003   0.700    0.707  ...   152.0     152.0      39.0   0.99
mu        0.079  0.013   0.055    0.103  ...    39.0      39.0      58.0   1.03
mus       0.175  0.029   0.127    0.219  ...   152.0     152.0      47.0   0.99
gamma     0.248  0.044   0.172    0.317  ...   130.0     123.0      59.0   0.99
Is_begin  0.553  0.600   0.004    1.509  ...    95.0      75.0      43.0   1.02
Ia_begin  0.799  0.989   0.006    2.741  ...   133.0      88.0      27.0   1.00
E_begin   0.305  0.364   0.001    1.165  ...   104.0      78.0      59.0   1.03

[8 rows x 11 columns]