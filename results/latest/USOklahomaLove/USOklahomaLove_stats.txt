0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1770.16    43.11
p_loo       40.66        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      602   95.0%
 (0.5, 0.7]   (ok)         21    3.3%
   (0.7, 1]   (bad)         8    1.3%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.173  0.017   0.151    0.204  ...   118.0     117.0      75.0   0.99
pu        0.706  0.005   0.700    0.717  ...   135.0      75.0      40.0   1.02
mu        0.115  0.015   0.086    0.144  ...    52.0      55.0      56.0   1.06
mus       0.189  0.033   0.141    0.264  ...    82.0     113.0      38.0   1.02
gamma     0.268  0.039   0.206    0.334  ...    24.0      25.0      56.0   1.06
Is_begin  0.591  0.559   0.002    1.826  ...   112.0      76.0      60.0   1.03
Ia_begin  0.918  0.932   0.039    2.404  ...    24.0      55.0      24.0   1.04
E_begin   0.382  0.411   0.008    1.152  ...    61.0      52.0      59.0   0.99

[8 rows x 11 columns]