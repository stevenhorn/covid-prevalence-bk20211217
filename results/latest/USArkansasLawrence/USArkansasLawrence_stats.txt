0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1705.59    59.71
p_loo       56.09        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      578   91.2%
 (0.5, 0.7]   (ok)         46    7.3%
   (0.7, 1]   (bad)         9    1.4%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.159  0.008   0.150    0.175  ...   152.0     152.0      77.0   0.99
pu        0.703  0.003   0.700    0.709  ...    82.0      89.0      48.0   1.02
mu        0.088  0.015   0.062    0.114  ...    21.0      21.0      59.0   1.06
mus       0.202  0.037   0.143    0.277  ...    83.0      99.0     100.0   1.03
gamma     0.261  0.044   0.199    0.374  ...   139.0     152.0      37.0   1.00
Is_begin  0.648  0.599   0.005    2.014  ...   119.0      99.0      40.0   1.00
Ia_begin  0.922  0.970   0.026    2.767  ...   112.0     105.0      87.0   1.03
E_begin   0.524  0.556   0.002    1.699  ...   108.0     102.0      58.0   0.99

[8 rows x 11 columns]