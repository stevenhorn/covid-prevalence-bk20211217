0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1933.35    41.47
p_loo       49.07        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      576   90.9%
 (0.5, 0.7]   (ok)         52    8.2%
   (0.7, 1]   (bad)         4    0.6%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.182  0.026   0.151    0.235  ...    58.0      40.0      53.0   1.01
pu        0.712  0.012   0.700    0.734  ...    25.0      21.0      61.0   1.07
mu        0.128  0.022   0.093    0.175  ...    61.0      59.0      40.0   1.03
mus       0.185  0.032   0.138    0.238  ...    87.0     124.0      84.0   1.00
gamma     0.295  0.053   0.214    0.400  ...   148.0     149.0      97.0   1.01
Is_begin  0.447  0.484   0.001    1.176  ...    75.0      72.0      81.0   1.03
Ia_begin  1.003  1.303   0.002    3.757  ...   113.0     125.0      88.0   1.00
E_begin   0.442  0.580   0.000    1.421  ...    98.0     102.0      59.0   1.00

[8 rows x 11 columns]