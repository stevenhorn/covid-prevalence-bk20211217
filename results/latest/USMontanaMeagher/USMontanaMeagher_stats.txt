0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo  -775.15    55.23
p_loo       68.13        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      562   88.6%
 (0.5, 0.7]   (ok)         57    9.0%
   (0.7, 1]   (bad)        13    2.1%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.220  0.053   0.151    0.306  ...   152.0     152.0      96.0   1.01
pu        0.733  0.021   0.704    0.774  ...    64.0      55.0      48.0   1.03
mu        0.165  0.027   0.129    0.224  ...    77.0      86.0      93.0   1.01
mus       0.264  0.041   0.201    0.341  ...   150.0     152.0      93.0   1.00
gamma     0.316  0.051   0.241    0.411  ...   118.0     148.0      40.0   1.01
Is_begin  0.758  0.694   0.067    2.198  ...   107.0     152.0      96.0   1.03
Ia_begin  0.902  1.057   0.042    2.739  ...    93.0     152.0      61.0   1.06
E_begin   0.405  0.470   0.012    0.987  ...    97.0      94.0      88.0   0.98

[8 rows x 11 columns]