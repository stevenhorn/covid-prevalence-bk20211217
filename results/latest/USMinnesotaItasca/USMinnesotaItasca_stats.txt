0 Divergences 
Passed validation 
2021-12-09 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 635 log-likelihood matrix

         Estimate       SE
elpd_loo -2093.83    38.09
p_loo       32.18        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      591   93.1%
 (0.5, 0.7]   (ok)         41    6.5%
   (0.7, 1]   (bad)         3    0.5%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.174  0.021   0.150    0.207  ...    86.0     107.0      73.0   1.01
pu        0.707  0.006   0.700    0.719  ...    94.0      65.0      43.0   1.00
mu        0.139  0.019   0.103    0.174  ...    18.0      14.0      54.0   1.10
mus       0.180  0.033   0.130    0.228  ...   152.0     107.0      65.0   1.11
gamma     0.245  0.038   0.183    0.316  ...    71.0      69.0      60.0   1.02
Is_begin  0.653  0.724   0.019    1.893  ...   107.0     115.0      96.0   1.01
Ia_begin  1.213  1.230   0.106    4.054  ...    61.0      79.0      88.0   1.05
E_begin   0.448  0.420   0.000    1.209  ...    76.0      63.0      43.0   1.05

[8 rows x 11 columns]