0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1950.84    39.13
p_loo       40.73        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      595   93.8%
 (0.5, 0.7]   (ok)         31    4.9%
   (0.7, 1]   (bad)         7    1.1%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.230  0.044   0.154    0.302  ...    73.0      75.0      60.0   1.02
pu        0.722  0.015   0.700    0.750  ...    68.0      61.0      26.0   1.02
mu        0.115  0.020   0.090    0.154  ...    34.0      26.0      57.0   1.06
mus       0.163  0.031   0.107    0.226  ...    82.0     106.0      59.0   1.05
gamma     0.195  0.033   0.148    0.257  ...    49.0      48.0      59.0   1.00
Is_begin  0.637  0.497   0.023    1.638  ...    77.0     145.0      96.0   1.02
Ia_begin  1.201  1.126   0.033    3.394  ...    99.0      46.0      80.0   1.05
E_begin   0.571  0.606   0.011    1.884  ...   109.0      42.0      40.0   1.05

[8 rows x 11 columns]