0 Divergences 
Passed validation 
2021-12-09 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 635 log-likelihood matrix

         Estimate       SE
elpd_loo -1739.79    49.62
p_loo       47.24        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      584   92.0%
 (0.5, 0.7]   (ok)         41    6.5%
   (0.7, 1]   (bad)         9    1.4%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.202  0.034   0.152    0.269  ...   152.0     136.0      70.0   1.00
pu        0.715  0.011   0.701    0.735  ...    51.0      43.0      60.0   1.05
mu        0.135  0.019   0.098    0.165  ...    60.0      58.0      46.0   1.00
mus       0.196  0.031   0.154    0.261  ...    92.0     121.0      99.0   1.01
gamma     0.261  0.048   0.175    0.331  ...   152.0     152.0      56.0   1.02
Is_begin  0.698  0.628   0.015    1.921  ...    97.0      77.0      93.0   1.02
Ia_begin  1.451  1.408   0.091    4.657  ...    68.0      33.0      56.0   1.06
E_begin   0.585  0.495   0.009    1.519  ...    72.0      53.0      58.0   1.01

[8 rows x 11 columns]