0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -2720.33    51.77
p_loo       48.10        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      587   92.9%
 (0.5, 0.7]   (ok)         37    5.9%
   (0.7, 1]   (bad)         7    1.1%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.189  0.031   0.150    0.252  ...   123.0     102.0      93.0   0.99
pu        0.714  0.009   0.700    0.727  ...   103.0      92.0      79.0   1.01
mu        0.120  0.016   0.092    0.144  ...    42.0      50.0      31.0   1.06
mus       0.170  0.030   0.118    0.218  ...   108.0     133.0      18.0   1.02
gamma     0.264  0.043   0.202    0.360  ...   101.0     104.0      60.0   1.02
Is_begin  1.039  0.852   0.024    2.723  ...   138.0      85.0      29.0   1.02
Ia_begin  0.750  0.553   0.028    1.815  ...   146.0     109.0      96.0   1.03
E_begin   0.561  0.579   0.010    1.828  ...    94.0     100.0      84.0   1.01

[8 rows x 11 columns]