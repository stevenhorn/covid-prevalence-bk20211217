0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2963.89    67.74
p_loo       45.14        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      581   91.6%
 (0.5, 0.7]   (ok)         47    7.4%
   (0.7, 1]   (bad)         4    0.6%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.168  0.016   0.150    0.197  ...   152.0     119.0      97.0   1.00
pu        0.707  0.005   0.700    0.718  ...    24.0      38.0     100.0   1.06
mu        0.093  0.012   0.067    0.113  ...    13.0      14.0      60.0   1.12
mus       0.197  0.030   0.139    0.248  ...   143.0     152.0      91.0   1.05
gamma     0.268  0.048   0.195    0.359  ...    65.0      66.0      59.0   1.03
Is_begin  0.803  0.848   0.019    2.222  ...    97.0      66.0      53.0   1.00
Ia_begin  1.610  2.021   0.003    5.894  ...   103.0      80.0      38.0   1.02
E_begin   0.616  0.721   0.001    1.614  ...    76.0      53.0      38.0   1.03

[8 rows x 11 columns]