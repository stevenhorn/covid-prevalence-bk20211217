0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -1380.53    71.54
p_loo       78.31        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      578   91.5%
 (0.5, 0.7]   (ok)         41    6.5%
   (0.7, 1]   (bad)         8    1.3%
   (1, Inf)   (very bad)    5    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.231  0.051   0.153    0.316  ...   114.0     126.0      93.0   0.99
pu        0.728  0.021   0.700    0.773  ...    98.0      86.0      60.0   1.00
mu        0.150  0.025   0.105    0.193  ...    15.0      16.0      53.0   1.13
mus       0.246  0.032   0.193    0.295  ...    69.0      75.0      59.0   1.01
gamma     0.340  0.062   0.237    0.437  ...    44.0      56.0      40.0   0.99
Is_begin  0.553  0.575   0.003    1.581  ...   119.0      64.0      34.0   1.00
Ia_begin  0.713  0.779   0.006    2.199  ...    91.0      92.0      49.0   1.01
E_begin   0.370  0.486   0.012    1.274  ...    83.0     152.0      81.0   1.00

[8 rows x 11 columns]