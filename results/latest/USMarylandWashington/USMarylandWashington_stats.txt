0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2438.54    40.01
p_loo       44.00        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      575   90.7%
 (0.5, 0.7]   (ok)         47    7.4%
   (0.7, 1]   (bad)        10    1.6%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.173  0.020   0.150    0.207  ...    71.0      57.0      31.0   1.03
pu        0.708  0.006   0.700    0.719  ...    41.0      35.0      91.0   1.01
mu        0.108  0.015   0.086    0.139  ...    31.0      31.0      99.0   0.99
mus       0.156  0.024   0.121    0.205  ...    90.0      85.0     100.0   1.05
gamma     0.248  0.038   0.185    0.318  ...    56.0      57.0      40.0   1.01
Is_begin  0.950  0.983   0.019    2.855  ...    89.0      57.0      40.0   1.01
Ia_begin  2.351  1.691   0.261    5.933  ...    72.0      64.0      58.0   1.01
E_begin   1.313  1.110   0.009    3.296  ...    92.0      81.0      60.0   0.98

[8 rows x 11 columns]