0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1222.94    54.44
p_loo       56.99        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      593   93.5%
 (0.5, 0.7]   (ok)         29    4.6%
   (0.7, 1]   (bad)         9    1.4%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.241  0.060   0.151    0.345  ...     9.0       9.0      15.0   1.19
pu        0.759  0.033   0.702    0.812  ...    13.0      13.0      40.0   1.15
mu        0.149  0.024   0.103    0.188  ...    42.0      42.0      59.0   1.00
mus       0.220  0.038   0.157    0.289  ...    59.0      58.0     102.0   1.01
gamma     0.277  0.056   0.195    0.385  ...    72.0      70.0      60.0   1.01
Is_begin  0.538  0.650   0.001    1.984  ...    34.0      21.0      15.0   1.08
Ia_begin  0.801  1.000   0.006    2.641  ...     9.0       8.0      25.0   1.26
E_begin   0.371  0.476   0.007    1.338  ...    22.0      11.0      59.0   1.15

[8 rows x 11 columns]