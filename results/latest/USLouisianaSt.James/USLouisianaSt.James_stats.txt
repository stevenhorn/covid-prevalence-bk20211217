0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1959.32    60.33
p_loo       56.66        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      601   94.8%
 (0.5, 0.7]   (ok)         27    4.3%
   (0.7, 1]   (bad)         4    0.6%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.171  0.019   0.150    0.210  ...   152.0     152.0      19.0   1.06
pu        0.705  0.004   0.700    0.711  ...   133.0     102.0      93.0   1.04
mu        0.125  0.019   0.092    0.166  ...    11.0      11.0      67.0   1.15
mus       0.215  0.040   0.125    0.287  ...   152.0     152.0      40.0   1.08
gamma     0.349  0.048   0.253    0.424  ...    66.0      70.0      40.0   1.01
Is_begin  1.734  1.451   0.027    3.974  ...    87.0      66.0      58.0   1.03
Ia_begin  5.571  3.399   0.083   11.933  ...    86.0      77.0      40.0   0.98
E_begin   4.661  3.745   0.154   11.920  ...   129.0     152.0     100.0   0.99

[8 rows x 11 columns]