0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -2219.66    58.93
p_loo       68.32        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      590   93.4%
 (0.5, 0.7]   (ok)         29    4.6%
   (0.7, 1]   (bad)         9    1.4%
   (1, Inf)   (very bad)    4    0.6%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.159  0.008   0.150    0.175  ...   102.0     152.0      81.0   1.01
pu        0.703  0.004   0.700    0.711  ...   152.0     152.0      75.0   1.00
mu        0.135  0.019   0.108    0.180  ...    84.0      76.0      86.0   1.02
mus       0.269  0.034   0.201    0.318  ...   152.0     152.0      97.0   1.05
gamma     0.429  0.060   0.328    0.535  ...   152.0     152.0      93.0   0.99
Is_begin  0.732  0.772   0.010    2.088  ...   112.0     120.0     100.0   1.00
Ia_begin  1.095  1.184   0.026    2.958  ...   124.0     100.0      60.0   1.00
E_begin   0.431  0.427   0.004    1.097  ...    84.0      62.0      60.0   0.98

[8 rows x 11 columns]