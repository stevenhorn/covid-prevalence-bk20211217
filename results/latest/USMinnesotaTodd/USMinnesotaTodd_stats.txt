0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1877.97    42.64
p_loo       40.53        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      601   94.8%
 (0.5, 0.7]   (ok)         25    3.9%
   (0.7, 1]   (bad)         8    1.3%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.161  0.009   0.150    0.177  ...   121.0      84.0      60.0   1.00
pu        0.704  0.004   0.700    0.711  ...   152.0     152.0      40.0   1.00
mu        0.122  0.012   0.099    0.141  ...    37.0      34.0      35.0   1.01
mus       0.214  0.029   0.174    0.280  ...   102.0     126.0      60.0   0.99
gamma     0.299  0.045   0.237    0.406  ...    76.0      73.0      93.0   0.99
Is_begin  0.205  0.230   0.006    0.880  ...    88.0      65.0      65.0   1.01
Ia_begin  0.279  0.329   0.000    0.978  ...    61.0      69.0      60.0   1.04
E_begin   0.129  0.176   0.000    0.457  ...    56.0      55.0      40.0   1.03

[8 rows x 11 columns]