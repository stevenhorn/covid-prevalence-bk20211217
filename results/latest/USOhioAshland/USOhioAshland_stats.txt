0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1907.78    59.71
p_loo       42.78        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      587   92.6%
 (0.5, 0.7]   (ok)         39    6.2%
   (0.7, 1]   (bad)         7    1.1%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.183  0.025   0.151    0.226  ...   130.0     152.0      88.0   1.00
pu        0.710  0.008   0.701    0.724  ...   152.0     139.0      40.0   1.01
mu        0.110  0.015   0.084    0.140  ...    29.0      30.0      40.0   1.04
mus       0.191  0.034   0.132    0.251  ...   152.0     151.0      83.0   1.03
gamma     0.244  0.040   0.172    0.319  ...   147.0     152.0      93.0   1.02
Is_begin  0.609  0.688   0.001    2.095  ...    90.0      63.0      43.0   1.03
Ia_begin  1.224  1.218   0.058    3.947  ...   103.0      85.0      59.0   0.99
E_begin   0.454  0.528   0.003    1.349  ...   120.0     152.0      83.0   1.01

[8 rows x 11 columns]