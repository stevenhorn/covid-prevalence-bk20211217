0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1965.53   116.29
p_loo       61.02        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      601   94.8%
 (0.5, 0.7]   (ok)         25    3.9%
   (0.7, 1]   (bad)         6    0.9%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.157  0.007   0.151    0.171  ...    45.0      29.0      59.0   1.06
pu        0.702  0.002   0.700    0.704  ...   129.0      67.0      31.0   1.01
mu        0.097  0.009   0.084    0.117  ...    48.0      52.0      84.0   1.04
mus       0.328  0.036   0.258    0.401  ...   107.0     152.0      40.0   1.01
gamma     0.807  0.088   0.624    0.951  ...    82.0      88.0      81.0   0.99
Is_begin  0.588  0.589   0.008    1.826  ...    97.0      65.0      40.0   1.04
Ia_begin  0.648  0.837   0.003    2.136  ...    51.0      64.0      60.0   0.98
E_begin   0.321  0.449   0.005    0.953  ...    68.0      51.0      68.0   1.01

[8 rows x 11 columns]