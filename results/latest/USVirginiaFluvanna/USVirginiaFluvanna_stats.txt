0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -1877.24    56.85
p_loo       71.34        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      587   92.9%
 (0.5, 0.7]   (ok)         32    5.1%
   (0.7, 1]   (bad)         9    1.4%
   (1, Inf)   (very bad)    4    0.6%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.188  0.034   0.151    0.254  ...   152.0     152.0      60.0   1.00
pu        0.712  0.011   0.700    0.733  ...    83.0      99.0      59.0   1.01
mu        0.184  0.021   0.152    0.218  ...    43.0      45.0      39.0   1.03
mus       0.340  0.051   0.251    0.444  ...    36.0      67.0      43.0   1.08
gamma     0.601  0.080   0.454    0.728  ...   152.0     152.0      65.0   1.02
Is_begin  0.935  0.797   0.012    2.786  ...   140.0     136.0      99.0   1.00
Ia_begin  1.444  1.577   0.003    4.922  ...   104.0      77.0      60.0   1.01
E_begin   0.837  1.002   0.001    3.188  ...   115.0      86.0      60.0   1.00

[8 rows x 11 columns]