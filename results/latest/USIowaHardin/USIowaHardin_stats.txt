0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1943.90    51.83
p_loo       49.36        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      596   94.0%
 (0.5, 0.7]   (ok)         27    4.3%
   (0.7, 1]   (bad)         6    0.9%
   (1, Inf)   (very bad)    5    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.160  0.009   0.150    0.179  ...    70.0      96.0      59.0   1.06
pu        0.703  0.002   0.700    0.708  ...   152.0     107.0      70.0   1.00
mu        0.090  0.013   0.063    0.109  ...    20.0      18.0      38.0   1.12
mus       0.166  0.024   0.126    0.217  ...   152.0     152.0      86.0   0.99
gamma     0.237  0.043   0.172    0.321  ...   152.0     152.0      43.0   1.09
Is_begin  0.440  0.593   0.000    1.658  ...    72.0      32.0      40.0   1.01
Ia_begin  0.821  0.707   0.026    1.948  ...    58.0      59.0      60.0   1.02
E_begin   0.318  0.354   0.006    0.887  ...    65.0      59.0      83.0   1.02

[8 rows x 11 columns]