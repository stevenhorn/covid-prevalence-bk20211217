0 Divergences 
Failed validation 
2021-12-09 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 635 log-likelihood matrix

         Estimate       SE
elpd_loo -2529.77    73.56
p_loo       56.55        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      593   93.4%
 (0.5, 0.7]   (ok)         39    6.1%
   (0.7, 1]   (bad)         2    0.3%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.162  0.013   0.150    0.187  ...   123.0      77.0      49.0   1.01
pu        0.704  0.004   0.700    0.710  ...   152.0      83.0      22.0   1.01
mu        0.078  0.010   0.061    0.099  ...    11.0      12.0      30.0   1.16
mus       0.169  0.032   0.125    0.234  ...    63.0      58.0      38.0   1.01
gamma     0.351  0.058   0.248    0.457  ...   102.0      99.0      72.0   1.02
Is_begin  0.544  0.519   0.023    1.389  ...   117.0      72.0      40.0   1.03
Ia_begin  0.819  0.881   0.007    2.385  ...    24.0      16.0      93.0   1.11
E_begin   0.306  0.287   0.008    0.961  ...    59.0      27.0      59.0   1.07

[8 rows x 11 columns]