0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1517.08    41.91
p_loo       54.21        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      589   92.9%
 (0.5, 0.7]   (ok)         32    5.0%
   (0.7, 1]   (bad)         8    1.3%
   (1, Inf)   (very bad)    5    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.237  0.048   0.159    0.319  ...    27.0      29.0      58.0   1.06
pu        0.727  0.018   0.700    0.758  ...    73.0      61.0      48.0   1.05
mu        0.146  0.022   0.107    0.189  ...    48.0      53.0      61.0   1.01
mus       0.198  0.037   0.146    0.283  ...   152.0     152.0      49.0   0.98
gamma     0.241  0.047   0.159    0.315  ...   103.0     107.0      96.0   1.00
Is_begin  0.625  0.571   0.020    1.774  ...    91.0     121.0      86.0   0.99
Ia_begin  1.168  1.304   0.019    4.052  ...    75.0     117.0      40.0   1.00
E_begin   0.447  0.498   0.020    1.410  ...   111.0     101.0      93.0   0.98

[8 rows x 11 columns]