0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -3447.79    36.16
p_loo       42.65        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      583   92.0%
 (0.5, 0.7]   (ok)         41    6.5%
   (0.7, 1]   (bad)         9    1.4%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.159  0.009   0.150    0.172  ...   102.0      24.0      15.0   1.07
pu        0.703  0.003   0.700    0.710  ...   152.0      99.0      58.0   1.00
mu        0.106  0.015   0.078    0.134  ...    27.0      27.0      58.0   1.04
mus       0.194  0.033   0.145    0.263  ...    79.0      75.0      53.0   1.00
gamma     0.283  0.043   0.213    0.354  ...    42.0      35.0      60.0   1.05
Is_begin  2.084  1.241   0.320    3.918  ...   145.0     132.0      60.0   1.00
Ia_begin  6.638  3.423   1.180   12.294  ...    93.0      84.0      31.0   1.01
E_begin   6.123  5.252   0.010   14.973  ...    84.0      61.0      43.0   1.01

[8 rows x 11 columns]