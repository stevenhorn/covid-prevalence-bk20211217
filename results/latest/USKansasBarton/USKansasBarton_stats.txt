0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2137.43    39.49
p_loo       38.50        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      610   96.2%
 (0.5, 0.7]   (ok)         18    2.8%
   (0.7, 1]   (bad)         5    0.8%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.159  0.007   0.150    0.174  ...   152.0     152.0      48.0   0.99
pu        0.703  0.003   0.700    0.709  ...   152.0     152.0      95.0   1.07
mu        0.088  0.014   0.061    0.110  ...    30.0      33.0      74.0   1.04
mus       0.156  0.031   0.096    0.202  ...   152.0     152.0      65.0   1.02
gamma     0.200  0.038   0.142    0.282  ...   120.0     113.0      56.0   1.03
Is_begin  0.606  0.592   0.001    1.901  ...    53.0      73.0      56.0   1.09
Ia_begin  0.905  0.946   0.001    2.987  ...    56.0      46.0      40.0   1.02
E_begin   0.441  0.457   0.007    1.252  ...    99.0     123.0      80.0   0.99

[8 rows x 11 columns]