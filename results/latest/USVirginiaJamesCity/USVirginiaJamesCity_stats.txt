2 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -2151.19    32.39
p_loo       38.67        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      581   91.9%
 (0.5, 0.7]   (ok)         41    6.5%
   (0.7, 1]   (bad)         8    1.3%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.242  0.052   0.167    0.336  ...    51.0      51.0      55.0   1.02
pu        0.750  0.025   0.714    0.792  ...    37.0      34.0      30.0   1.05
mu        0.161  0.021   0.134    0.204  ...    45.0      42.0      40.0   1.05
mus       0.213  0.034   0.156    0.278  ...    85.0      83.0      38.0   1.05
gamma     0.287  0.038   0.220    0.377  ...    84.0      85.0      88.0   0.99
Is_begin  2.773  1.728   0.061    5.599  ...    38.0      35.0      43.0   1.05
Ia_begin  5.868  3.484   0.540   12.804  ...    76.0      61.0      14.0   1.00
E_begin   4.688  3.365   0.085   11.505  ...    31.0      26.0      30.0   1.07

[8 rows x 11 columns]