0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1331.11    44.49
p_loo       40.44        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      600   94.6%
 (0.5, 0.7]   (ok)         28    4.4%
   (0.7, 1]   (bad)         5    0.8%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.161  0.010   0.151    0.178  ...   120.0     103.0      69.0   1.03
pu        0.703  0.003   0.700    0.710  ...    96.0      51.0      57.0   1.03
mu        0.110  0.017   0.074    0.141  ...    68.0      74.0      44.0   1.00
mus       0.196  0.030   0.140    0.245  ...    59.0      58.0      58.0   1.00
gamma     0.266  0.051   0.182    0.361  ...    72.0      91.0      59.0   1.05
Is_begin  0.639  0.685   0.012    2.143  ...   131.0      92.0      52.0   1.02
Ia_begin  1.176  1.114   0.013    3.467  ...   104.0      88.0      58.0   1.01
E_begin   0.466  0.444   0.005    1.380  ...    58.0      48.0      47.0   1.04

[8 rows x 11 columns]