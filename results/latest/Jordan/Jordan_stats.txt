0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 633 log-likelihood matrix

         Estimate       SE
elpd_loo -4287.22    63.94
p_loo       38.74        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      574   90.7%
 (0.5, 0.7]   (ok)         51    8.1%
   (0.7, 1]   (bad)         6    0.9%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.261  0.058   0.167    0.349  ...    65.0      61.0      57.0   1.00
pu        0.751  0.025   0.706    0.785  ...    61.0      59.0      43.0   1.03
mu        0.149  0.021   0.104    0.184  ...    24.0      24.0      25.0   1.06
mus       0.197  0.028   0.153    0.247  ...   122.0     123.0      83.0   1.00
gamma     0.258  0.039   0.197    0.332  ...    58.0      69.0      48.0   1.00
Is_begin  4.873  4.685   0.086   12.899  ...    72.0      43.0      40.0   1.01
Ia_begin  8.511  9.193   0.108   25.000  ...    94.0      74.0      60.0   1.02
E_begin   3.201  3.558   0.084   11.293  ...    90.0      63.0      51.0   1.00

[8 rows x 11 columns]