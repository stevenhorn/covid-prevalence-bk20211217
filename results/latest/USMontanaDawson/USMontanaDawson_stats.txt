0 Divergences 
Failed validation 
2021-12-09 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 635 log-likelihood matrix

         Estimate       SE
elpd_loo -1511.61    38.94
p_loo       31.23        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      600   94.5%
 (0.5, 0.7]   (ok)         31    4.9%
   (0.7, 1]   (bad)         4    0.6%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.158  0.008   0.150    0.172  ...   152.0     152.0      83.0   1.05
pu        0.703  0.002   0.700    0.707  ...   115.0      85.0      40.0   0.99
mu        0.088  0.015   0.060    0.110  ...    69.0      75.0      54.0   0.99
mus       0.137  0.027   0.099    0.187  ...    71.0      81.0      93.0   1.03
gamma     0.180  0.031   0.121    0.229  ...    91.0      90.0      60.0   1.02
Is_begin  0.170  0.225   0.002    0.615  ...    49.0      38.0      59.0   1.06
Ia_begin  0.330  0.515   0.002    1.111  ...    78.0      40.0      88.0   1.00
E_begin   0.137  0.199   0.002    0.572  ...    62.0      60.0      60.0   1.05

[8 rows x 11 columns]