0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1067.83   107.32
p_loo       71.21        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      561   88.5%
 (0.5, 0.7]   (ok)         62    9.8%
   (0.7, 1]   (bad)         9    1.4%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.166  0.016   0.150    0.194  ...   129.0      85.0      60.0   0.99
pu        0.706  0.005   0.700    0.716  ...    96.0      70.0      43.0   1.00
mu        0.126  0.019   0.099    0.169  ...    64.0      63.0      56.0   0.99
mus       0.314  0.045   0.244    0.390  ...    77.0      77.0      53.0   1.00
gamma     0.695  0.089   0.538    0.848  ...    45.0      54.0      84.0   1.04
Is_begin  0.310  0.330   0.007    0.866  ...    38.0      40.0      62.0   1.04
Ia_begin  0.388  0.521   0.001    1.628  ...   113.0      86.0      59.0   1.00
E_begin   0.190  0.205   0.003    0.658  ...   116.0      64.0      57.0   0.99

[8 rows x 11 columns]