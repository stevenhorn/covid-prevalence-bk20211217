0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -1383.73    65.48
p_loo       68.49        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      600   94.9%
 (0.5, 0.7]   (ok)         21    3.3%
   (0.7, 1]   (bad)         7    1.1%
   (1, Inf)   (very bad)    4    0.6%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.175  0.021   0.150    0.215  ...    54.0      39.0      61.0   1.04
pu        0.707  0.005   0.701    0.717  ...    72.0      52.0      60.0   1.03
mu        0.138  0.026   0.091    0.173  ...     7.0       8.0      60.0   1.21
mus       0.212  0.029   0.162    0.262  ...    60.0      60.0      80.0   1.01
gamma     0.305  0.056   0.219    0.420  ...    66.0      67.0      59.0   1.04
Is_begin  0.513  0.579   0.001    1.652  ...   109.0      67.0     100.0   1.03
Ia_begin  0.910  1.003   0.007    3.075  ...    67.0      59.0      59.0   1.02
E_begin   0.410  0.518   0.001    1.453  ...    98.0      53.0      22.0   1.02

[8 rows x 11 columns]