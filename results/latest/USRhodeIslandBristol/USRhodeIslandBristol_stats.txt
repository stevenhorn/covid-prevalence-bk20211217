0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2506.91    52.63
p_loo       46.24        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      604   95.3%
 (0.5, 0.7]   (ok)         23    3.6%
   (0.7, 1]   (bad)         5    0.8%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.157  0.008   0.150    0.168  ...    65.0     115.0      60.0   1.04
pu        0.702  0.002   0.700    0.706  ...   152.0      90.0      17.0   1.03
mu        0.090  0.014   0.072    0.117  ...    20.0      16.0      67.0   1.09
mus       0.148  0.029   0.108    0.209  ...    80.0     150.0      93.0   1.00
gamma     0.205  0.038   0.145    0.265  ...    40.0      51.0      39.0   1.02
Is_begin  0.558  0.377   0.027    1.262  ...   122.0      95.0      47.0   1.00
Ia_begin  1.196  0.953   0.026    2.842  ...    53.0      32.0      56.0   1.06
E_begin   0.657  0.703   0.012    1.693  ...    78.0      71.0      53.0   1.03

[8 rows x 11 columns]