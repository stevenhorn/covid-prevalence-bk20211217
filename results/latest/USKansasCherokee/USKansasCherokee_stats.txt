0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2074.66    35.88
p_loo       32.71        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      591   93.2%
 (0.5, 0.7]   (ok)         32    5.0%
   (0.7, 1]   (bad)        10    1.6%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.157  0.007   0.150    0.169  ...   152.0     152.0      81.0   1.01
pu        0.702  0.002   0.700    0.708  ...   152.0     147.0      60.0   1.00
mu        0.092  0.014   0.067    0.118  ...    26.0      25.0      51.0   1.08
mus       0.149  0.025   0.110    0.204  ...   132.0     146.0      69.0   1.03
gamma     0.207  0.041   0.145    0.280  ...   127.0     134.0      83.0   1.02
Is_begin  0.699  0.585   0.010    1.771  ...   105.0     141.0      83.0   1.00
Ia_begin  1.285  1.460   0.035    4.321  ...   148.0     152.0      99.0   1.01
E_begin   0.533  0.673   0.005    1.653  ...    71.0     134.0      59.0   0.99

[8 rows x 11 columns]