0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2621.15    49.62
p_loo      125.43        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      602   95.0%
 (0.5, 0.7]   (ok)         21    3.3%
   (0.7, 1]   (bad)         7    1.1%
   (1, Inf)   (very bad)    4    0.6%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.173  0.020   0.150    0.210  ...    34.0      31.0      42.0   1.01
pu        0.707  0.005   0.700    0.717  ...    30.0      16.0      43.0   1.11
mu        0.128  0.051   0.067    0.232  ...     3.0       4.0      15.0   1.76
mus       0.178  0.043   0.129    0.291  ...     5.0       6.0      19.0   1.34
gamma     0.229  0.072   0.095    0.344  ...     4.0       5.0      25.0   1.41
Is_begin  0.706  0.603   0.026    2.039  ...   113.0      75.0      97.0   1.00
Ia_begin  1.390  0.954   0.196    3.041  ...    72.0      65.0      59.0   1.01
E_begin   1.118  1.004   0.009    3.074  ...    55.0      21.0      40.0   1.08

[8 rows x 11 columns]