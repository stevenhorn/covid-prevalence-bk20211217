0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1618.63    59.60
p_loo       53.42        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      601   94.8%
 (0.5, 0.7]   (ok)         27    4.3%
   (0.7, 1]   (bad)         5    0.8%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.164  0.013   0.150    0.194  ...    59.0      44.0      24.0   1.00
pu        0.704  0.004   0.700    0.714  ...    94.0      72.0      74.0   1.07
mu        0.156  0.032   0.106    0.202  ...     4.0       5.0      16.0   1.46
mus       0.180  0.032   0.132    0.244  ...    14.0      15.0      17.0   1.09
gamma     0.197  0.035   0.145    0.261  ...    37.0      46.0      95.0   1.08
Is_begin  0.406  0.471   0.014    1.455  ...    75.0      67.0     100.0   1.02
Ia_begin  0.757  0.873   0.026    2.519  ...    15.0      15.0      48.0   1.13
E_begin   0.278  0.483   0.005    0.740  ...    20.0      18.0      40.0   1.13

[8 rows x 11 columns]