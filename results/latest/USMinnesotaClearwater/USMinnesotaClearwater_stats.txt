0 Divergences 
Passed validation 
2021-12-09 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 635 log-likelihood matrix

         Estimate       SE
elpd_loo -1340.27    41.39
p_loo       47.41        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      593   93.4%
 (0.5, 0.7]   (ok)         30    4.7%
   (0.7, 1]   (bad)         9    1.4%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.169  0.015   0.151    0.193  ...   141.0     125.0      40.0   1.01
pu        0.704  0.004   0.700    0.711  ...    56.0      45.0      58.0   1.07
mu        0.122  0.016   0.093    0.142  ...    76.0     102.0      96.0   1.03
mus       0.214  0.031   0.160    0.263  ...    81.0     122.0      68.0   1.01
gamma     0.320  0.054   0.238    0.426  ...    79.0     105.0      38.0   1.02
Is_begin  0.452  0.487   0.009    1.576  ...    86.0      85.0      57.0   1.02
Ia_begin  0.754  0.797   0.012    1.974  ...    69.0      52.0      38.0   1.04
E_begin   0.334  0.418   0.000    0.924  ...    85.0      33.0      56.0   1.06

[8 rows x 11 columns]