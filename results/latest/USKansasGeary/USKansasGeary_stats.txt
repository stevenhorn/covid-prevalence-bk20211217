0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2402.24    65.18
p_loo       71.63        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      603   95.1%
 (0.5, 0.7]   (ok)         24    3.8%
   (0.7, 1]   (bad)         3    0.5%
   (1, Inf)   (very bad)    4    0.6%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.168  0.020   0.150    0.210  ...    53.0      36.0      15.0   1.02
pu        0.707  0.007   0.700    0.720  ...   138.0     110.0      60.0   1.01
mu        0.116  0.019   0.079    0.141  ...    20.0      20.0      47.0   1.09
mus       0.196  0.035   0.146    0.277  ...   119.0     109.0      75.0   1.00
gamma     0.246  0.048   0.168    0.331  ...    79.0      81.0      30.0   1.12
Is_begin  0.653  0.802   0.005    2.429  ...    99.0      67.0      66.0   1.01
Ia_begin  1.210  1.290   0.004    3.651  ...    73.0      81.0      40.0   1.00
E_begin   0.424  0.431   0.003    1.255  ...   132.0      84.0      55.0   1.00

[8 rows x 11 columns]