0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2102.14    40.02
p_loo       34.73        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      598   94.3%
 (0.5, 0.7]   (ok)         29    4.6%
   (0.7, 1]   (bad)         6    0.9%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.180  0.028   0.150    0.226  ...    84.0     109.0      86.0   1.00
pu        0.709  0.007   0.700    0.722  ...    95.0      98.0      86.0   1.00
mu        0.117  0.022   0.070    0.148  ...    14.0      14.0      38.0   1.11
mus       0.196  0.035   0.145    0.265  ...   103.0     122.0      96.0   1.05
gamma     0.228  0.035   0.180    0.314  ...    28.0      28.0      48.0   1.09
Is_begin  0.674  0.672   0.045    2.054  ...    97.0      99.0      87.0   1.01
Ia_begin  1.240  1.490   0.017    3.312  ...   100.0     129.0     100.0   0.98
E_begin   0.446  0.465   0.001    1.337  ...    98.0      79.0      59.0   1.00

[8 rows x 11 columns]