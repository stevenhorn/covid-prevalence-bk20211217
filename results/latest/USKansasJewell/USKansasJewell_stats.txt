0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1194.63    48.52
p_loo       53.53        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      585   92.3%
 (0.5, 0.7]   (ok)         37    5.8%
   (0.7, 1]   (bad)         9    1.4%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.159  0.010   0.150    0.184  ...    86.0     103.0      88.0   0.99
pu        0.703  0.004   0.700    0.710  ...    80.0      63.0      14.0   0.99
mu        0.115  0.018   0.083    0.147  ...    49.0      72.0      91.0   1.03
mus       0.208  0.031   0.158    0.276  ...    90.0     109.0      88.0   1.04
gamma     0.254  0.038   0.185    0.316  ...    60.0      91.0      93.0   1.03
Is_begin  0.553  0.484   0.013    1.394  ...    38.0      30.0      91.0   1.06
Ia_begin  0.876  0.989   0.005    3.601  ...   121.0      69.0      22.0   1.02
E_begin   0.326  0.325   0.003    1.059  ...   115.0     104.0      97.0   1.00

[8 rows x 11 columns]