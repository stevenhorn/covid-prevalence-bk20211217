0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1893.43    76.27
p_loo       54.46        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      604   95.3%
 (0.5, 0.7]   (ok)         20    3.2%
   (0.7, 1]   (bad)         6    0.9%
   (1, Inf)   (very bad)    4    0.6%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.154  0.004   0.150    0.162  ...   152.0     152.0      60.0   0.98
pu        0.702  0.002   0.700    0.705  ...   102.0     152.0      37.0   1.04
mu        0.082  0.013   0.060    0.107  ...    59.0      55.0      60.0   1.02
mus       0.205  0.039   0.141    0.280  ...    73.0      64.0      83.0   1.01
gamma     0.303  0.057   0.220    0.410  ...    99.0     127.0      35.0   1.01
Is_begin  0.490  0.515   0.001    1.483  ...   101.0      60.0      19.0   1.02
Ia_begin  0.830  0.839   0.018    2.392  ...    59.0      41.0      58.0   1.06
E_begin   0.383  0.451   0.002    1.220  ...    98.0      75.0      50.0   1.00

[8 rows x 11 columns]