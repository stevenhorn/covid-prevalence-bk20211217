0 Divergences 
Failed validation 
2021-12-09 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 635 log-likelihood matrix

         Estimate       SE
elpd_loo -2312.53   100.95
p_loo       74.17        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      604   95.1%
 (0.5, 0.7]   (ok)         22    3.5%
   (0.7, 1]   (bad)         5    0.8%
   (1, Inf)   (very bad)    4    0.6%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.156  0.007   0.150    0.169  ...    85.0      44.0      59.0   1.03
pu        0.702  0.002   0.700    0.706  ...    80.0      51.0      79.0   1.00
mu        0.109  0.020   0.075    0.141  ...     4.0       4.0      22.0   1.58
mus       0.309  0.035   0.250    0.379  ...    13.0      19.0      18.0   1.11
gamma     0.696  0.089   0.557    0.878  ...    38.0      40.0      60.0   1.04
Is_begin  0.621  0.719   0.002    2.294  ...   110.0      92.0      83.0   0.99
Ia_begin  0.990  1.345   0.004    3.392  ...    97.0     100.0      38.0   0.99
E_begin   0.446  0.569   0.005    1.518  ...    99.0      33.0      24.0   1.06

[8 rows x 11 columns]