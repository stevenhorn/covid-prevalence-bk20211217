1 Divergences 
Passed validation 
2021-12-09 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 635 log-likelihood matrix

         Estimate       SE
elpd_loo -3070.91    34.64
p_loo       36.86        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      581   91.5%
 (0.5, 0.7]   (ok)         46    7.2%
   (0.7, 1]   (bad)         8    1.3%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.190  0.029   0.151    0.236  ...    76.0      67.0      24.0   1.01
pu        0.714  0.009   0.702    0.731  ...    71.0      78.0      54.0   1.03
mu        0.097  0.014   0.072    0.127  ...    36.0      35.0      51.0   1.02
mus       0.145  0.022   0.109    0.185  ...    93.0     101.0     100.0   1.00
gamma     0.204  0.035   0.130    0.261  ...    78.0      75.0      57.0   1.03
Is_begin  1.274  0.820   0.139    2.557  ...   101.0      91.0     100.0   1.00
Ia_begin  2.305  2.244   0.011    6.101  ...    93.0      22.0      40.0   1.08
E_begin   1.550  1.530   0.084    4.181  ...    21.0      12.0      38.0   1.14

[8 rows x 11 columns]