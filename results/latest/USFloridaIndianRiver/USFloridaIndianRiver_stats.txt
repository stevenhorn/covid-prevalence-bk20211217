0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -3014.46    41.67
p_loo       28.79        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      599   94.5%
 (0.5, 0.7]   (ok)         26    4.1%
   (0.7, 1]   (bad)         8    1.3%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.191  0.029   0.150    0.237  ...    73.0      68.0      47.0   1.01
pu        0.712  0.012   0.700    0.739  ...   120.0      96.0      62.0   1.02
mu        0.144  0.019   0.108    0.175  ...    24.0      26.0      24.0   1.06
mus       0.169  0.025   0.119    0.211  ...    77.0      75.0      59.0   1.01
gamma     0.209  0.032   0.147    0.256  ...    38.0      39.0      56.0   1.02
Is_begin  0.532  0.461   0.000    1.509  ...   120.0      78.0      22.0   1.01
Ia_begin  1.019  0.771   0.052    2.324  ...   114.0      90.0      60.0   0.99
E_begin   0.455  0.434   0.009    1.196  ...    42.0      44.0      55.0   1.04

[8 rows x 11 columns]