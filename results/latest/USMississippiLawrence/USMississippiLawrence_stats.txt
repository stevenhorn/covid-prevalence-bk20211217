0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1563.05    35.77
p_loo       41.17        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      581   91.6%
 (0.5, 0.7]   (ok)         44    6.9%
   (0.7, 1]   (bad)         8    1.3%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.192  0.030   0.150    0.243  ...    99.0      39.0      31.0   1.04
pu        0.713  0.012   0.700    0.737  ...    30.0      35.0      24.0   1.01
mu        0.124  0.019   0.095    0.157  ...    52.0      53.0      60.0   1.05
mus       0.172  0.026   0.125    0.214  ...    92.0      89.0      58.0   0.99
gamma     0.258  0.044   0.187    0.333  ...    44.0      45.0      30.0   1.00
Is_begin  0.716  0.597   0.001    1.657  ...   113.0     100.0      59.0   1.05
Ia_begin  1.350  1.501   0.050    4.385  ...    78.0      64.0      59.0   1.02
E_begin   0.622  0.674   0.021    2.236  ...    87.0      56.0      92.0   1.01

[8 rows x 11 columns]