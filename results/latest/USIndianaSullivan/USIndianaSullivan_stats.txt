0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1656.56    45.10
p_loo       48.12        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      579   91.3%
 (0.5, 0.7]   (ok)         43    6.8%
   (0.7, 1]   (bad)         9    1.4%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.175  0.019   0.151    0.212  ...    79.0      82.0      60.0   0.99
pu        0.707  0.006   0.701    0.719  ...    73.0      71.0      38.0   1.02
mu        0.119  0.013   0.097    0.145  ...    11.0      12.0      54.0   1.16
mus       0.195  0.032   0.148    0.265  ...    73.0      82.0      60.0   1.01
gamma     0.309  0.056   0.211    0.402  ...    65.0      85.0      40.0   1.07
Is_begin  0.585  0.645   0.001    1.912  ...    58.0      29.0      30.0   1.05
Ia_begin  0.805  0.697   0.026    2.216  ...    81.0      71.0      93.0   1.00
E_begin   0.379  0.368   0.012    1.114  ...    75.0      58.0      58.0   1.04

[8 rows x 11 columns]