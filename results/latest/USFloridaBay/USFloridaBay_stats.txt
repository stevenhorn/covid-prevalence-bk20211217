0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -3243.08    45.60
p_loo       35.48        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      605   95.4%
 (0.5, 0.7]   (ok)         23    3.6%
   (0.7, 1]   (bad)         6    0.9%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.162  0.014   0.150    0.195  ...   112.0     125.0      60.0   0.99
pu        0.703  0.003   0.700    0.709  ...   152.0     116.0      74.0   1.03
mu        0.106  0.018   0.077    0.133  ...     6.0       6.0      20.0   1.32
mus       0.161  0.028   0.111    0.205  ...    87.0     102.0      60.0   1.00
gamma     0.279  0.042   0.206    0.344  ...   112.0     107.0      60.0   1.01
Is_begin  0.756  0.730   0.006    2.194  ...   126.0      83.0     100.0   1.00
Ia_begin  1.107  1.198   0.007    3.621  ...    65.0      35.0      43.0   1.02
E_begin   0.429  0.356   0.011    1.081  ...   152.0     152.0      59.0   1.01

[8 rows x 11 columns]