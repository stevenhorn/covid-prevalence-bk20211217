0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1675.61    55.54
p_loo       53.12        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      593   93.5%
 (0.5, 0.7]   (ok)         28    4.4%
   (0.7, 1]   (bad)         8    1.3%
   (1, Inf)   (very bad)    5    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.201  0.036   0.151    0.268  ...    97.0      81.0      59.0   1.00
pu        0.713  0.012   0.700    0.735  ...    58.0      49.0      60.0   1.00
mu        0.135  0.016   0.104    0.168  ...    50.0      50.0      79.0   1.00
mus       0.268  0.045   0.207    0.346  ...    80.0      68.0      65.0   1.01
gamma     0.354  0.056   0.251    0.446  ...    78.0      79.0      56.0   1.01
Is_begin  0.582  0.548   0.009    1.435  ...    46.0      26.0      24.0   1.05
Ia_begin  0.945  0.980   0.030    3.156  ...    87.0      88.0      99.0   1.00
E_begin   0.368  0.379   0.014    1.115  ...    64.0      39.0      59.0   1.05

[8 rows x 11 columns]