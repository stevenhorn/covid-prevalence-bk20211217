0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -3740.50    32.70
p_loo       36.63        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      570   90.2%
 (0.5, 0.7]   (ok)         53    8.4%
   (0.7, 1]   (bad)         8    1.3%
   (1, Inf)   (very bad)    1    0.2%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.244   0.054   0.160    0.340  ...   151.0     152.0      44.0   1.09
pu         0.753   0.032   0.709    0.809  ...     7.0       8.0      87.0   1.21
mu         0.114   0.019   0.083    0.146  ...    10.0      11.0      26.0   1.16
mus        0.159   0.034   0.107    0.225  ...    31.0      24.0      30.0   1.08
gamma      0.246   0.039   0.175    0.310  ...   136.0     139.0      91.0   1.03
Is_begin  12.569   9.872   0.287   30.593  ...    96.0      46.0      40.0   1.02
Ia_begin  31.989  20.401   4.219   72.286  ...    50.0      44.0      36.0   1.05
E_begin   25.554  16.125   4.103   56.738  ...    71.0      76.0      88.0   1.03

[8 rows x 11 columns]