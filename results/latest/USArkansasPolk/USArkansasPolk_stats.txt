0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1533.44    38.68
p_loo       40.57        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      584   92.1%
 (0.5, 0.7]   (ok)         44    6.9%
   (0.7, 1]   (bad)         6    0.9%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.206  0.035   0.151    0.258  ...    45.0      43.0      58.0   1.03
pu        0.716  0.013   0.701    0.743  ...    44.0      44.0      42.0   1.04
mu        0.118  0.020   0.085    0.159  ...    50.0      45.0      40.0   1.01
mus       0.196  0.037   0.137    0.254  ...    41.0      42.0      93.0   1.01
gamma     0.265  0.036   0.195    0.323  ...    27.0      24.0      32.0   1.04
Is_begin  0.725  0.746   0.001    2.034  ...   102.0      71.0      43.0   0.99
Ia_begin  1.114  1.392   0.029    4.302  ...    90.0      79.0      60.0   1.00
E_begin   0.470  0.535   0.004    1.596  ...   114.0      94.0     100.0   1.00

[8 rows x 11 columns]