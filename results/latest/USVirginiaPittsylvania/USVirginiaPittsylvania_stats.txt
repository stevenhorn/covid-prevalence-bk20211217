0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -2258.96    41.26
p_loo       46.32        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      585   92.6%
 (0.5, 0.7]   (ok)         39    6.2%
   (0.7, 1]   (bad)         5    0.8%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.252  0.052   0.156    0.326  ...    43.0      61.0      57.0   1.03
pu        0.745  0.022   0.703    0.777  ...    26.0      28.0      59.0   1.06
mu        0.128  0.016   0.099    0.157  ...    10.0      10.0      59.0   1.16
mus       0.207  0.035   0.152    0.280  ...    15.0      14.0      58.0   1.10
gamma     0.280  0.045   0.208    0.363  ...    24.0      25.0      40.0   1.06
Is_begin  0.892  0.707   0.046    2.097  ...    70.0      63.0      60.0   1.00
Ia_begin  1.256  1.014   0.042    2.820  ...    41.0      43.0      60.0   1.02
E_begin   0.488  0.563   0.005    1.399  ...    63.0      34.0      32.0   1.02

[8 rows x 11 columns]