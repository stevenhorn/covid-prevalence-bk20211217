0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo  -891.87    41.86
p_loo       47.41        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      595   93.8%
 (0.5, 0.7]   (ok)         32    5.0%
   (0.7, 1]   (bad)         5    0.8%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.229  0.048   0.150    0.314  ...    66.0      69.0      43.0   0.99
pu        0.725  0.015   0.700    0.752  ...    58.0      58.0      42.0   1.01
mu        0.133  0.022   0.091    0.171  ...    18.0      18.0      57.0   1.09
mus       0.186  0.033   0.136    0.257  ...    97.0     115.0      60.0   1.06
gamma     0.224  0.038   0.161    0.299  ...    69.0      79.0      59.0   1.04
Is_begin  0.312  0.424   0.003    1.077  ...   103.0     134.0      79.0   1.00
Ia_begin  0.444  0.588   0.009    1.716  ...    58.0      73.0      80.0   0.99
E_begin   0.194  0.269   0.001    0.727  ...    86.0      84.0      55.0   1.00

[8 rows x 11 columns]