8 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 633 log-likelihood matrix

         Estimate       SE
elpd_loo -4180.70    35.64
p_loo       54.32        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      556   87.8%
 (0.5, 0.7]   (ok)         54    8.5%
   (0.7, 1]   (bad)        11    1.7%
   (1, Inf)   (very bad)   12    1.9%

             mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa          0.275   0.052   0.190    0.348  ...    12.0      12.0      57.0   1.13
pu          0.863   0.044   0.763    0.900  ...     7.0       8.0      15.0   1.20
mu          0.145   0.018   0.110    0.174  ...     7.0       8.0      43.0   1.23
mus         0.202   0.035   0.147    0.264  ...    56.0      65.0      59.0   1.04
gamma       0.316   0.051   0.246    0.435  ...    40.0      39.0      40.0   1.06
Is_begin   26.102  12.877   1.955   45.325  ...    36.0      34.0      15.0   1.07
Ia_begin   88.857  21.921  46.827  127.087  ...    20.0      23.0      37.0   1.10
E_begin   227.786  97.098  54.389  378.932  ...    14.0      14.0      48.0   1.13

[8 rows x 11 columns]