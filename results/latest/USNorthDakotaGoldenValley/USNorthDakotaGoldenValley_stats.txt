0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo  -804.21    51.24
p_loo       65.33        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      566   89.3%
 (0.5, 0.7]   (ok)         59    9.3%
   (0.7, 1]   (bad)         6    0.9%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.200  0.038   0.151    0.267  ...   102.0     104.0      69.0   1.01
pu        0.717  0.012   0.700    0.736  ...    78.0      81.0      87.0   1.02
mu        0.208  0.035   0.146    0.265  ...    48.0      50.0      60.0   0.98
mus       0.295  0.038   0.213    0.358  ...    72.0      74.0      93.0   1.01
gamma     0.382  0.060   0.290    0.500  ...   152.0     152.0      65.0   0.99
Is_begin  0.502  0.493   0.012    1.498  ...    72.0      69.0      96.0   1.01
Ia_begin  0.795  0.947   0.004    2.634  ...    71.0      65.0      60.0   1.01
E_begin   0.351  0.396   0.013    1.162  ...    59.0      72.0      59.0   1.01

[8 rows x 11 columns]