0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1844.97    68.80
p_loo       56.88        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      587   92.6%
 (0.5, 0.7]   (ok)         38    6.0%
   (0.7, 1]   (bad)         8    1.3%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.218  0.051   0.152    0.301  ...    38.0      38.0      43.0   1.05
pu        0.726  0.016   0.702    0.756  ...    47.0      48.0      38.0   0.99
mu        0.135  0.020   0.092    0.170  ...    14.0      12.0      55.0   1.13
mus       0.244  0.039   0.184    0.332  ...   112.0     105.0      60.0   1.01
gamma     0.350  0.053   0.268    0.452  ...    51.0      60.0      59.0   1.00
Is_begin  0.865  0.790   0.001    2.217  ...    84.0      24.0      14.0   1.06
Ia_begin  1.556  1.134   0.157    3.609  ...    45.0      44.0      20.0   1.00
E_begin   0.673  0.692   0.042    2.305  ...    99.0      88.0      66.0   1.01

[8 rows x 11 columns]