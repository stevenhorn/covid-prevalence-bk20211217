0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2291.52    36.95
p_loo       35.50        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      598   94.3%
 (0.5, 0.7]   (ok)         31    4.9%
   (0.7, 1]   (bad)         5    0.8%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.158  0.008   0.150    0.176  ...   110.0      86.0      25.0   1.03
pu        0.702  0.002   0.700    0.707  ...    30.0      12.0      22.0   1.13
mu        0.107  0.011   0.087    0.127  ...    14.0      15.0      57.0   1.11
mus       0.178  0.027   0.137    0.223  ...    55.0      62.0      59.0   1.00
gamma     0.281  0.034   0.230    0.345  ...    71.0      86.0      86.0   0.99
Is_begin  0.363  0.374   0.016    1.071  ...    43.0      39.0      58.0   1.06
Ia_begin  0.540  0.641   0.017    2.201  ...    80.0      79.0      96.0   1.01
E_begin   0.279  0.335   0.002    0.909  ...    80.0      43.0      59.0   1.01

[8 rows x 11 columns]