0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2511.45    36.31
p_loo       34.18        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      601   94.8%
 (0.5, 0.7]   (ok)         27    4.3%
   (0.7, 1]   (bad)         4    0.6%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.255  0.050   0.176    0.350  ...   103.0     124.0      54.0   1.02
pu        0.762  0.025   0.710    0.800  ...    64.0      70.0      29.0   1.01
mu        0.139  0.020   0.103    0.171  ...    20.0      20.0      39.0   1.09
mus       0.188  0.033   0.137    0.257  ...    65.0      78.0      56.0   1.03
gamma     0.240  0.043   0.179    0.327  ...    76.0      75.0      67.0   0.99
Is_begin  0.614  0.482   0.022    1.491  ...   124.0      87.0      40.0   0.99
Ia_begin  1.182  1.228   0.004    3.711  ...    89.0      77.0      59.0   0.99
E_begin   0.546  0.731   0.001    1.893  ...    92.0      31.0      33.0   1.05

[8 rows x 11 columns]