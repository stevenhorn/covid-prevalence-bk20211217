0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 633 log-likelihood matrix

         Estimate       SE
elpd_loo -1086.08    53.57
p_loo       60.97        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      593   93.7%
 (0.5, 0.7]   (ok)         30    4.7%
   (0.7, 1]   (bad)         7    1.1%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.250  0.054   0.158    0.336  ...   148.0     152.0      60.0   1.02
pu        0.797  0.046   0.723    0.879  ...     6.0       6.0      15.0   1.28
mu        0.206  0.032   0.151    0.256  ...    34.0      34.0      59.0   1.03
mus       0.274  0.050   0.184    0.362  ...    96.0      83.0      14.0   0.99
gamma     0.341  0.063   0.242    0.467  ...   102.0      95.0      59.0   1.00
Is_begin  0.896  0.862   0.001    2.912  ...    77.0      90.0      86.0   1.00
Ia_begin  0.703  0.525   0.060    1.776  ...   152.0     137.0      41.0   1.02
E_begin   0.691  0.706   0.009    2.145  ...    56.0      64.0      95.0   1.03

[8 rows x 11 columns]