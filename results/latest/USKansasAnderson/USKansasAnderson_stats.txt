0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1525.45    38.43
p_loo       41.03        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      592   93.4%
 (0.5, 0.7]   (ok)         35    5.5%
   (0.7, 1]   (bad)         6    0.9%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.172  0.020   0.151    0.215  ...    44.0      77.0      38.0   1.00
pu        0.706  0.006   0.700    0.716  ...    85.0      37.0      58.0   1.03
mu        0.118  0.018   0.091    0.154  ...    49.0      57.0      57.0   1.04
mus       0.177  0.032   0.123    0.226  ...   127.0     123.0      60.0   1.05
gamma     0.249  0.042   0.166    0.303  ...    72.0      64.0      93.0   1.05
Is_begin  0.536  0.583   0.005    1.594  ...    95.0      67.0      60.0   1.02
Ia_begin  0.922  1.075   0.027    2.817  ...    93.0      68.0      38.0   1.02
E_begin   0.388  0.449   0.000    1.190  ...    88.0      69.0      59.0   0.99

[8 rows x 11 columns]