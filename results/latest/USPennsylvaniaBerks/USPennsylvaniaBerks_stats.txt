0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -3149.23    58.88
p_loo      110.03        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      567   89.4%
 (0.5, 0.7]   (ok)         56    8.8%
   (0.7, 1]   (bad)         8    1.3%
   (1, Inf)   (very bad)    3    0.5%

           mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.161   0.012   0.150    0.184  ...   152.0     152.0     100.0   1.05
pu        0.703   0.003   0.700    0.709  ...    70.0      89.0      61.0   1.01
mu        0.125   0.038   0.072    0.179  ...     3.0       4.0      60.0   1.81
mus       0.261   0.073   0.130    0.372  ...     7.0       7.0      69.0   1.27
gamma     0.280   0.083   0.172    0.401  ...     3.0       4.0      32.0   1.84
Is_begin  1.987   1.168   0.009    4.085  ...    14.0      12.0      14.0   1.15
Ia_begin  5.601   3.672   0.526   12.781  ...     6.0       6.0      51.0   1.38
E_begin   9.132  10.421   0.039   30.867  ...     7.0       5.0      45.0   1.38

[8 rows x 11 columns]