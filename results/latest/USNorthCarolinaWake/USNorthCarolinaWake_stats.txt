0 Divergences 
Passed validation 
2021-12-09 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 635 log-likelihood matrix

         Estimate       SE
elpd_loo -3975.65    36.79
p_loo       40.38        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      573   90.2%
 (0.5, 0.7]   (ok)         52    8.2%
   (0.7, 1]   (bad)         8    1.3%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.215  0.049   0.151    0.307  ...    58.0      61.0      42.0   1.09
pu        0.731  0.017   0.703    0.761  ...    59.0      59.0      79.0   0.98
mu        0.127  0.018   0.088    0.151  ...    13.0      14.0      43.0   1.11
mus       0.157  0.023   0.122    0.201  ...    91.0      91.0      97.0   1.03
gamma     0.246  0.044   0.173    0.337  ...   152.0     152.0      88.0   1.00
Is_begin  1.861  1.999   0.017    5.819  ...    96.0      79.0      56.0   1.02
Ia_begin  3.629  4.041   0.126   10.500  ...    87.0      25.0      59.0   1.07
E_begin   1.463  1.896   0.037    5.135  ...   100.0      66.0      60.0   1.04

[8 rows x 11 columns]