0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2717.93    56.84
p_loo      128.60        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      615   97.0%
 (0.5, 0.7]   (ok)         13    2.1%
   (0.7, 1]   (bad)         5    0.8%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.177  0.023   0.151    0.221  ...     6.0       7.0      53.0   1.29
pu        0.707  0.006   0.700    0.720  ...    11.0      17.0      59.0   1.09
mu        0.116  0.030   0.080    0.162  ...     4.0       5.0      42.0   1.48
mus       0.163  0.032   0.114    0.220  ...    21.0      23.0      49.0   1.08
gamma     0.178  0.055   0.092    0.268  ...     3.0       4.0      53.0   1.67
Is_begin  1.129  0.979   0.017    2.867  ...    26.0      31.0      37.0   1.09
Ia_begin  0.728  0.541   0.005    1.890  ...   104.0      79.0      54.0   0.98
E_begin   0.640  0.675   0.034    2.218  ...    73.0      71.0      60.0   0.99

[8 rows x 11 columns]