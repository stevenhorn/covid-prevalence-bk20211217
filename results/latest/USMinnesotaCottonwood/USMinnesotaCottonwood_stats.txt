0 Divergences 
Failed validation 
2021-12-09 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 635 log-likelihood matrix

         Estimate       SE
elpd_loo -1498.99    35.60
p_loo       37.16        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      590   92.9%
 (0.5, 0.7]   (ok)         34    5.4%
   (0.7, 1]   (bad)        11    1.7%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.172  0.021   0.150    0.213  ...    87.0      65.0      60.0   1.00
pu        0.707  0.006   0.701    0.718  ...    81.0      71.0      97.0   1.01
mu        0.128  0.018   0.098    0.162  ...    48.0      34.0      65.0   1.07
mus       0.192  0.037   0.133    0.262  ...    44.0      65.0      81.0   1.03
gamma     0.259  0.044   0.184    0.338  ...    23.0      29.0      56.0   1.02
Is_begin  0.440  0.585   0.003    1.381  ...    54.0      44.0      60.0   1.03
Ia_begin  0.758  0.931   0.047    2.214  ...    94.0      51.0      42.0   0.98
E_begin   0.350  0.526   0.001    1.308  ...    72.0      31.0      16.0   1.01

[8 rows x 11 columns]