0 Divergences 
Failed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -2818.01    87.77
p_loo       61.16        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      586   92.7%
 (0.5, 0.7]   (ok)         34    5.4%
   (0.7, 1]   (bad)         9    1.4%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.167  0.014   0.150    0.200  ...   152.0     152.0      39.0   1.05
pu        0.706  0.005   0.700    0.714  ...    70.0      68.0      74.0   1.01
mu        0.111  0.016   0.085    0.140  ...    23.0      24.0      38.0   1.10
mus       0.345  0.039   0.281    0.422  ...    67.0      69.0      56.0   1.08
gamma     0.671  0.106   0.530    0.929  ...    64.0      70.0      53.0   1.01
Is_begin  0.728  0.738   0.014    2.218  ...    73.0      79.0      40.0   1.03
Ia_begin  0.980  1.001   0.001    3.018  ...   137.0     107.0     100.0   1.01
E_begin   0.468  0.498   0.007    1.630  ...    64.0      67.0      60.0   1.03

[8 rows x 11 columns]