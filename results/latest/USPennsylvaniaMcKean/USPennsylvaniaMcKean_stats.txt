0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1792.93    62.19
p_loo       52.62        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      576   90.9%
 (0.5, 0.7]   (ok)         46    7.3%
   (0.7, 1]   (bad)        10    1.6%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.206  0.034   0.155    0.260  ...   102.0      84.0      60.0   1.00
pu        0.719  0.012   0.701    0.738  ...    86.0      85.0      93.0   1.01
mu        0.143  0.017   0.116    0.176  ...    16.0      17.0      22.0   1.10
mus       0.240  0.043   0.166    0.302  ...    65.0      65.0      43.0   1.03
gamma     0.317  0.048   0.237    0.410  ...    61.0      56.0      59.0   1.01
Is_begin  0.620  0.659   0.005    2.069  ...    62.0      41.0      40.0   1.04
Ia_begin  1.041  0.996   0.028    2.953  ...    71.0      66.0      96.0   1.02
E_begin   0.470  0.637   0.000    1.375  ...    34.0      11.0      14.0   1.17

[8 rows x 11 columns]