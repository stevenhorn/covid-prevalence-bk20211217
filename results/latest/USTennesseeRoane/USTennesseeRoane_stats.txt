0 Divergences 
Failed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -2385.42    48.70
p_loo       41.78        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      601   95.1%
 (0.5, 0.7]   (ok)         24    3.8%
   (0.7, 1]   (bad)         4    0.6%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.165  0.011   0.150    0.182  ...    92.0      56.0      32.0   1.00
pu        0.705  0.005   0.700    0.715  ...    98.0      80.0      60.0   1.01
mu        0.101  0.014   0.078    0.123  ...    40.0      45.0      42.0   1.02
mus       0.175  0.024   0.138    0.224  ...    75.0      77.0      79.0   1.01
gamma     0.240  0.033   0.186    0.297  ...    66.0      76.0      56.0   1.02
Is_begin  0.731  0.643   0.010    1.941  ...    57.0      45.0      60.0   1.06
Ia_begin  1.407  1.297   0.110    3.949  ...    61.0      61.0      81.0   0.99
E_begin   0.549  0.511   0.011    1.625  ...    59.0      55.0      40.0   1.01

[8 rows x 11 columns]