0 Divergences 
Passed validation 
2021-12-09 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 635 log-likelihood matrix

         Estimate       SE
elpd_loo -2633.88    50.74
p_loo       17.53        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      624   98.3%
 (0.5, 0.7]   (ok)         10    1.6%
   (0.7, 1]   (bad)         1    0.2%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.174  0.008   0.164    0.188  ...     3.0       3.0      15.0   1.89
pu        0.709  0.003   0.704    0.713  ...     3.0       4.0      42.0   1.85
mu        0.292  0.010   0.279    0.305  ...     3.0       3.0      20.0   2.40
mus       0.285  0.004   0.280    0.291  ...     3.0       4.0      20.0   1.88
gamma     0.152  0.007   0.140    0.163  ...     3.0       3.0      32.0   1.96
Is_begin  1.312  1.424   0.027    3.912  ...     3.0       3.0      16.0   2.03
Ia_begin  1.841  0.642   0.955    3.293  ...     3.0       4.0      16.0   1.87
E_begin   1.869  1.314   0.698    4.751  ...     3.0       3.0      14.0   2.47

[8 rows x 11 columns]