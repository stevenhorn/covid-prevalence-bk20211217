0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1695.81    45.70
p_loo       41.97        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      601   94.8%
 (0.5, 0.7]   (ok)         27    4.3%
   (0.7, 1]   (bad)         2    0.3%
   (1, Inf)   (very bad)    4    0.6%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.160  0.010   0.150    0.185  ...   127.0      89.0      40.0   1.03
pu        0.703  0.003   0.700    0.709  ...    73.0      80.0      60.0   1.00
mu        0.108  0.017   0.083    0.144  ...     9.0      12.0      37.0   1.15
mus       0.185  0.029   0.133    0.240  ...    54.0      70.0      59.0   1.06
gamma     0.227  0.037   0.145    0.285  ...    21.0      19.0      93.0   1.09
Is_begin  0.374  0.331   0.005    0.868  ...    39.0      23.0      40.0   1.08
Ia_begin  0.726  0.804   0.001    2.451  ...    56.0      54.0      57.0   0.99
E_begin   0.285  0.288   0.008    0.837  ...    48.0      52.0      60.0   0.99

[8 rows x 11 columns]