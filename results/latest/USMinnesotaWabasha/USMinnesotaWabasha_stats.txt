0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1770.74    43.52
p_loo       38.60        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      587   92.6%
 (0.5, 0.7]   (ok)         36    5.7%
   (0.7, 1]   (bad)         9    1.4%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.171  0.016   0.151    0.206  ...   152.0     139.0      38.0   1.05
pu        0.708  0.007   0.700    0.719  ...   116.0      67.0      29.0   1.00
mu        0.110  0.015   0.085    0.143  ...    30.0      27.0      59.0   1.03
mus       0.170  0.031   0.126    0.231  ...    51.0      32.0      32.0   1.05
gamma     0.201  0.042   0.127    0.273  ...   129.0     109.0      60.0   0.99
Is_begin  0.596  0.666   0.007    1.673  ...   111.0      96.0      96.0   1.02
Ia_begin  1.654  1.805   0.011    5.269  ...    79.0      42.0      38.0   1.06
E_begin   0.554  0.903   0.010    1.622  ...    95.0      71.0     100.0   1.02

[8 rows x 11 columns]