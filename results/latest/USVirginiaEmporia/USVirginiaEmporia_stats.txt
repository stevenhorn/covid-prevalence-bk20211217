0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -1269.98    50.39
p_loo       49.71        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      586   92.7%
 (0.5, 0.7]   (ok)         39    6.2%
   (0.7, 1]   (bad)         6    0.9%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.234  0.050   0.152    0.312  ...   152.0     152.0      78.0   1.04
pu        0.733  0.023   0.700    0.771  ...    40.0      69.0      59.0   1.05
mu        0.131  0.031   0.076    0.175  ...    39.0      39.0      58.0   1.00
mus       0.185  0.038   0.119    0.254  ...    93.0     106.0      58.0   1.03
gamma     0.253  0.046   0.182    0.364  ...    65.0      66.0      38.0   1.08
Is_begin  0.485  0.527   0.002    1.677  ...   110.0      89.0      77.0   1.00
Ia_begin  0.859  0.881   0.028    2.327  ...    62.0      65.0      60.0   1.01
E_begin   0.477  0.608   0.012    1.115  ...    90.0      95.0      59.0   1.01

[8 rows x 11 columns]