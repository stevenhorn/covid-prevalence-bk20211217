0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -1776.40    37.04
p_loo       29.89        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      601   95.1%
 (0.5, 0.7]   (ok)         28    4.4%
   (0.7, 1]   (bad)         3    0.5%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.159  0.010   0.150    0.178  ...    99.0      59.0      38.0   1.02
pu        0.703  0.004   0.700    0.709  ...   152.0     128.0      24.0   0.99
mu        0.080  0.013   0.054    0.099  ...    36.0      36.0     100.0   1.02
mus       0.155  0.028   0.118    0.225  ...    90.0     102.0      48.0   1.02
gamma     0.215  0.045   0.146    0.300  ...    92.0      74.0      96.0   1.01
Is_begin  0.387  0.366   0.018    1.129  ...    72.0      53.0      53.0   1.02
Ia_begin  0.620  0.689   0.011    1.830  ...    73.0      58.0      59.0   0.99
E_begin   0.300  0.293   0.007    0.821  ...    60.0     113.0      43.0   1.08

[8 rows x 11 columns]