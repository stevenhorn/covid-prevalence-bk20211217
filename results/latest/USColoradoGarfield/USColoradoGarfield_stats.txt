0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2217.13    32.35
p_loo       33.69        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      602   95.0%
 (0.5, 0.7]   (ok)         30    4.7%
   (0.7, 1]   (bad)         2    0.3%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.223  0.045   0.159    0.318  ...    63.0      33.0      59.0   1.06
pu        0.724  0.017   0.702    0.754  ...    12.0      12.0      59.0   1.15
mu        0.113  0.018   0.082    0.145  ...     4.0       4.0      74.0   1.58
mus       0.167  0.025   0.123    0.206  ...    80.0      88.0      59.0   0.99
gamma     0.201  0.033   0.153    0.259  ...    91.0      85.0      93.0   0.98
Is_begin  1.276  0.960   0.078    2.996  ...    69.0      68.0      80.0   1.04
Ia_begin  2.422  1.944   0.022    6.180  ...   113.0      77.0      40.0   1.01
E_begin   1.265  1.269   0.026    3.830  ...    70.0      56.0      34.0   0.98

[8 rows x 11 columns]