0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1961.33    44.74
p_loo       44.55        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      591   93.2%
 (0.5, 0.7]   (ok)         33    5.2%
   (0.7, 1]   (bad)         9    1.4%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.157  0.005   0.151    0.169  ...    81.0      25.0      45.0   1.07
pu        0.704  0.003   0.700    0.707  ...    37.0      36.0      60.0   1.06
mu        0.099  0.025   0.067    0.141  ...     3.0       3.0      20.0   2.14
mus       0.203  0.032   0.148    0.260  ...     9.0       8.0      69.0   1.22
gamma     0.360  0.052   0.263    0.450  ...    99.0      95.0      50.0   1.00
Is_begin  0.382  0.400   0.009    1.036  ...    10.0       4.0      22.0   1.54
Ia_begin  0.870  0.785   0.012    2.601  ...     4.0       6.0      16.0   1.36
E_begin   0.245  0.295   0.002    0.808  ...    26.0      19.0      14.0   1.09

[8 rows x 11 columns]