49 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2718.38    38.44
p_loo       33.88        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      606   95.6%
 (0.5, 0.7]   (ok)         19    3.0%
   (0.7, 1]   (bad)         8    1.3%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.165  0.014   0.150    0.190  ...    24.0      15.0      23.0   1.12
pu        0.705  0.004   0.700    0.712  ...    22.0      22.0      43.0   1.09
mu        0.127  0.016   0.106    0.155  ...     9.0      12.0      60.0   1.18
mus       0.165  0.023   0.137    0.218  ...    54.0      36.0      57.0   1.06
gamma     0.264  0.033   0.215    0.328  ...    20.0      27.0      39.0   1.07
Is_begin  0.941  0.814   0.015    2.203  ...    83.0      56.0      36.0   1.02
Ia_begin  1.314  1.341   0.071    3.236  ...    62.0      28.0      22.0   1.07
E_begin   0.729  0.839   0.011    2.904  ...    49.0      30.0      38.0   1.03

[8 rows x 11 columns]