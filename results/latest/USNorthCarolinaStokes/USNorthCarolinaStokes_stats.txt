0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2183.06    38.16
p_loo       42.17        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      593   93.5%
 (0.5, 0.7]   (ok)         29    4.6%
   (0.7, 1]   (bad)        11    1.7%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.179  0.024   0.151    0.229  ...    62.0      79.0      60.0   1.01
pu        0.708  0.007   0.700    0.722  ...   123.0     101.0      58.0   1.04
mu        0.122  0.022   0.081    0.162  ...    21.0      18.0      59.0   1.09
mus       0.166  0.028   0.125    0.219  ...    38.0      79.0      38.0   1.03
gamma     0.225  0.042   0.166    0.316  ...    97.0      96.0      96.0   1.03
Is_begin  0.566  0.547   0.032    1.725  ...    88.0      61.0      36.0   1.01
Ia_begin  1.216  1.290   0.022    3.776  ...    88.0      86.0      53.0   1.02
E_begin   0.470  0.498   0.001    1.529  ...    69.0      63.0      60.0   1.03

[8 rows x 11 columns]