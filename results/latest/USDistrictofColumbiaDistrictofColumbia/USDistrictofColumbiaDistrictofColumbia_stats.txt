0 Divergences 
Passed validation 
2021-12-09 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 635 log-likelihood matrix

         Estimate       SE
elpd_loo -3519.28    33.24
p_loo       37.41        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      599   94.3%
 (0.5, 0.7]   (ok)         27    4.3%
   (0.7, 1]   (bad)         8    1.3%
   (1, Inf)   (very bad)    1    0.2%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.242   0.057   0.150    0.325  ...    62.0      49.0      16.0   1.03
pu         0.751   0.030   0.712    0.809  ...    14.0      18.0      40.0   1.11
mu         0.121   0.019   0.082    0.152  ...    10.0      10.0      88.0   1.17
mus        0.157   0.028   0.107    0.208  ...    57.0      69.0      16.0   1.03
gamma      0.238   0.030   0.192    0.298  ...    79.0      83.0      70.0   0.99
Is_begin  29.368  19.404   4.167   70.143  ...    40.0      37.0      40.0   1.09
Ia_begin  61.887  33.439   7.199  124.373  ...    80.0      74.0      36.0   0.98
E_begin   64.740  43.630   7.658  144.060  ...   126.0      93.0      59.0   0.99

[8 rows x 11 columns]