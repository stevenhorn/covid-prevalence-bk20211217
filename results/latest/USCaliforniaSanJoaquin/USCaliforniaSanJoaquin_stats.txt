0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -3851.10    64.39
p_loo       68.22        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      579   91.3%
 (0.5, 0.7]   (ok)         42    6.6%
   (0.7, 1]   (bad)        10    1.6%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.230  0.046   0.156    0.301  ...    81.0     101.0      83.0   1.00
pu        0.724  0.016   0.700    0.752  ...    14.0      16.0      40.0   1.11
mu        0.145  0.019   0.111    0.177  ...    19.0      17.0      21.0   1.13
mus       0.202  0.031   0.146    0.255  ...    51.0      60.0      38.0   1.00
gamma     0.271  0.045   0.202    0.366  ...    23.0      21.0      49.0   1.10
Is_begin  3.049  3.588   0.021    9.760  ...   126.0      85.0      60.0   1.02
Ia_begin  7.171  5.041   0.145   15.530  ...    38.0      69.0      30.0   1.02
E_begin   3.085  2.956   0.062    9.809  ...   105.0      76.0      83.0   1.01

[8 rows x 11 columns]