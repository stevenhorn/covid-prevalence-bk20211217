0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -1666.59    51.09
p_loo       61.36        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      587   92.9%
 (0.5, 0.7]   (ok)         33    5.2%
   (0.7, 1]   (bad)         8    1.3%
   (1, Inf)   (very bad)    4    0.6%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.165  0.015   0.150    0.194  ...   152.0     103.0      22.0   1.01
pu        0.706  0.005   0.700    0.715  ...   121.0      83.0      19.0   1.08
mu        0.110  0.015   0.083    0.135  ...    44.0      40.0      77.0   1.06
mus       0.339  0.043   0.253    0.408  ...   152.0     152.0      60.0   1.02
gamma     0.525  0.083   0.404    0.701  ...    79.0     110.0      81.0   1.01
Is_begin  0.615  0.615   0.008    1.800  ...   113.0      61.0      25.0   1.03
Ia_begin  0.721  0.890   0.023    1.911  ...    85.0      81.0      65.0   0.99
E_begin   0.360  0.453   0.015    1.057  ...   103.0     122.0      59.0   0.99

[8 rows x 11 columns]