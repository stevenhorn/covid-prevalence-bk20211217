0 Divergences 
Failed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -1085.96    44.23
p_loo       55.94        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      587   92.9%
 (0.5, 0.7]   (ok)         34    5.4%
   (0.7, 1]   (bad)         8    1.3%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.216  0.035   0.160    0.267  ...    73.0      72.0      53.0   1.00
pu        0.718  0.011   0.701    0.736  ...    69.0      64.0      40.0   1.02
mu        0.134  0.030   0.081    0.183  ...     9.0       9.0      72.0   1.18
mus       0.197  0.040   0.127    0.266  ...    94.0      78.0      74.0   1.00
gamma     0.261  0.039   0.198    0.343  ...    97.0      82.0      56.0   1.04
Is_begin  0.454  0.546   0.013    1.485  ...    78.0      78.0      72.0   1.05
Ia_begin  0.867  1.100   0.016    3.351  ...    83.0      59.0      76.0   1.02
E_begin   0.357  0.459   0.000    1.468  ...   102.0     109.0      59.0   1.02

[8 rows x 11 columns]