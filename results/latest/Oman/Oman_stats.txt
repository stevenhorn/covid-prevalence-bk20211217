7 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 633 log-likelihood matrix

         Estimate       SE
elpd_loo -4328.83    46.57
p_loo       42.03        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      586   92.6%
 (0.5, 0.7]   (ok)         40    6.3%
   (0.7, 1]   (bad)         4    0.6%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.261  0.045   0.187    0.344  ...    46.0      43.0      42.0   1.03
pu        0.841  0.017   0.812    0.871  ...    25.0      32.0      15.0   1.06
mu        0.134  0.016   0.106    0.161  ...    36.0      38.0      60.0   1.01
mus       0.170  0.030   0.125    0.222  ...   152.0     152.0      59.0   1.01
gamma     0.242  0.035   0.184    0.313  ...    64.0      64.0      60.0   0.99
Is_begin  2.758  2.948   0.009    8.215  ...    38.0      97.0      59.0   1.10
Ia_begin  5.202  4.924   0.129   14.400  ...   101.0      72.0      59.0   1.04
E_begin   3.624  3.160   0.152   10.300  ...    69.0      55.0      50.0   1.02

[8 rows x 11 columns]