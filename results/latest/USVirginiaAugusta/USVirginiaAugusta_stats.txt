0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -2353.88    44.08
p_loo       44.84        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      588   93.0%
 (0.5, 0.7]   (ok)         37    5.9%
   (0.7, 1]   (bad)         6    0.9%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.180  0.021   0.151    0.224  ...    93.0      80.0      59.0   1.03
pu        0.710  0.009   0.700    0.723  ...    66.0      55.0      40.0   1.04
mu        0.118  0.017   0.086    0.150  ...    26.0      26.0      15.0   1.03
mus       0.169  0.032   0.129    0.233  ...    71.0      87.0      86.0   0.99
gamma     0.276  0.046   0.216    0.385  ...   152.0     152.0      37.0   1.05
Is_begin  0.628  0.686   0.002    2.086  ...    78.0      90.0      24.0   1.05
Ia_begin  1.190  1.230   0.004    4.055  ...    87.0      85.0      93.0   0.99
E_begin   0.500  0.518   0.005    1.371  ...    91.0      77.0      56.0   1.01

[8 rows x 11 columns]