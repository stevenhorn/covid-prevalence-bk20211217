0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -1881.43    64.71
p_loo       62.58        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      599   94.8%
 (0.5, 0.7]   (ok)         24    3.8%
   (0.7, 1]   (bad)         5    0.8%
   (1, Inf)   (very bad)    4    0.6%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.234  0.044   0.160    0.318  ...    43.0      45.0      32.0   1.03
pu        0.726  0.017   0.702    0.755  ...    65.0      57.0      57.0   1.02
mu        0.144  0.019   0.112    0.178  ...     7.0       8.0      59.0   1.23
mus       0.275  0.057   0.186    0.386  ...    33.0      29.0      49.0   1.03
gamma     0.413  0.062   0.289    0.516  ...    11.0      11.0      59.0   1.14
Is_begin  0.759  0.983   0.000    2.844  ...    56.0      24.0      42.0   1.06
Ia_begin  1.126  1.048   0.036    3.018  ...    66.0      57.0      60.0   0.99
E_begin   0.651  0.708   0.006    2.304  ...    50.0      74.0      59.0   1.10

[8 rows x 11 columns]