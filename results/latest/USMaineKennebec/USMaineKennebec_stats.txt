0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2269.78    47.97
p_loo       47.92        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      581   91.6%
 (0.5, 0.7]   (ok)         44    6.9%
   (0.7, 1]   (bad)         7    1.1%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.239  0.050   0.163    0.333  ...   106.0     139.0      23.0   1.03
pu        0.729  0.019   0.700    0.764  ...    69.0      62.0      79.0   0.98
mu        0.147  0.022   0.111    0.183  ...    10.0       9.0      49.0   1.18
mus       0.200  0.032   0.147    0.260  ...    48.0      35.0      60.0   1.06
gamma     0.302  0.053   0.215    0.411  ...    73.0      70.0      60.0   1.02
Is_begin  0.702  0.661   0.011    1.957  ...    80.0      44.0      23.0   1.04
Ia_begin  1.302  1.023   0.031    3.115  ...    55.0      41.0      60.0   0.99
E_begin   1.137  1.078   0.038    3.383  ...    86.0      63.0      34.0   1.00

[8 rows x 11 columns]