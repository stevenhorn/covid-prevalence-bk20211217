0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1824.95    70.19
p_loo       65.20        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      589   92.9%
 (0.5, 0.7]   (ok)         37    5.8%
   (0.7, 1]   (bad)         6    0.9%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.164  0.013   0.150    0.188  ...    79.0      86.0      40.0   1.03
pu        0.704  0.004   0.700    0.716  ...   152.0     104.0      60.0   0.98
mu        0.139  0.016   0.112    0.171  ...    34.0      35.0      57.0   1.03
mus       0.260  0.035   0.194    0.313  ...   127.0     114.0      59.0   1.00
gamma     0.438  0.057   0.358    0.559  ...    92.0      89.0      60.0   1.02
Is_begin  0.605  0.674   0.023    2.022  ...    76.0      97.0      60.0   1.01
Ia_begin  0.812  1.083   0.002    2.985  ...    84.0      90.0     100.0   1.03
E_begin   0.413  0.519   0.002    1.300  ...    95.0     105.0      91.0   1.02

[8 rows x 11 columns]