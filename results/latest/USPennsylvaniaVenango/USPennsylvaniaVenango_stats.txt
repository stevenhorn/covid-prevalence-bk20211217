0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1837.39    48.77
p_loo       53.50        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      580   91.5%
 (0.5, 0.7]   (ok)         40    6.3%
   (0.7, 1]   (bad)        13    2.1%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.183  0.025   0.150    0.227  ...   152.0     138.0      57.0   1.00
pu        0.711  0.008   0.700    0.725  ...    72.0      49.0      59.0   1.00
mu        0.114  0.012   0.093    0.132  ...    26.0      30.0      17.0   1.05
mus       0.181  0.028   0.143    0.233  ...   143.0     144.0      67.0   1.00
gamma     0.262  0.049   0.183    0.354  ...    81.0      72.0      60.0   1.01
Is_begin  0.617  0.629   0.010    1.821  ...   152.0     134.0      91.0   1.01
Ia_begin  1.162  1.278   0.006    3.468  ...    54.0     143.0      49.0   0.99
E_begin   0.542  0.625   0.005    2.110  ...    77.0     152.0     100.0   1.00

[8 rows x 11 columns]