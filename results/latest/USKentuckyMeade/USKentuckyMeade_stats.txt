0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1849.99    45.55
p_loo       41.23        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      587   92.6%
 (0.5, 0.7]   (ok)         39    6.2%
   (0.7, 1]   (bad)         6    0.9%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.230  0.047   0.152    0.308  ...    98.0     101.0      60.0   1.03
pu        0.729  0.020   0.700    0.765  ...   107.0     106.0      93.0   1.00
mu        0.109  0.018   0.079    0.136  ...     8.0       7.0      24.0   1.25
mus       0.178  0.030   0.127    0.234  ...    46.0      49.0      40.0   1.02
gamma     0.220  0.037   0.160    0.288  ...    31.0      55.0      56.0   1.09
Is_begin  0.646  0.669   0.020    1.856  ...   112.0      85.0      92.0   0.99
Ia_begin  1.011  1.285   0.003    3.325  ...    90.0      72.0      58.0   1.00
E_begin   0.608  1.239   0.005    1.351  ...    92.0      93.0      43.0   1.00

[8 rows x 11 columns]