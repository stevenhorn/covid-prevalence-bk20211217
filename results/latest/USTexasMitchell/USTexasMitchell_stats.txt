0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -1333.25    46.12
p_loo       45.07        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      519   82.1%
 (0.5, 0.7]   (ok)         95   15.0%
   (0.7, 1]   (bad)        18    2.8%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.171  0.016   0.150    0.200  ...    83.0      72.0      58.0   1.01
pu        0.707  0.005   0.700    0.716  ...    99.0      93.0      55.0   0.99
mu        0.112  0.019   0.081    0.147  ...    20.0      22.0      58.0   1.09
mus       0.185  0.045   0.107    0.268  ...    73.0     149.0      53.0   1.05
gamma     0.228  0.045   0.144    0.316  ...    58.0      70.0      40.0   0.99
Is_begin  0.504  0.552   0.003    1.864  ...    74.0      77.0      57.0   1.01
Ia_begin  0.863  1.239   0.006    2.415  ...    85.0      87.0      86.0   1.01
E_begin   0.355  0.451   0.040    1.318  ...   106.0     124.0      18.0   1.00

[8 rows x 11 columns]