0 Divergences 
Passed validation 
2021-12-16 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 643 log-likelihood matrix

         Estimate       SE
elpd_loo -2746.00    67.59
p_loo       61.54        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      571   88.8%
 (0.5, 0.7]   (ok)         58    9.0%
   (0.7, 1]   (bad)        10    1.6%
   (1, Inf)   (very bad)    4    0.6%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.254   0.051   0.155    0.332  ...    91.0      82.0      40.0   1.04
pu         0.835   0.042   0.739    0.889  ...    29.0      26.0      44.0   1.03
mu         0.168   0.033   0.125    0.231  ...     7.0       8.0      42.0   1.22
mus        0.215   0.029   0.160    0.264  ...   152.0     152.0      91.0   1.01
gamma      0.315   0.046   0.240    0.396  ...    86.0     102.0      96.0   1.02
Is_begin   7.371   5.476   0.044   16.250  ...    81.0      59.0      38.0   1.05
Ia_begin  47.833  26.007   8.696   95.831  ...    47.0      38.0      36.0   1.02
E_begin   35.540  25.590   3.040   83.809  ...    70.0      69.0      62.0   1.02

[8 rows x 11 columns]