0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2519.62    57.88
p_loo       52.46        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      586   92.4%
 (0.5, 0.7]   (ok)         37    5.8%
   (0.7, 1]   (bad)         9    1.4%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.167  0.017   0.151    0.206  ...    46.0      31.0      60.0   1.07
pu        0.704  0.004   0.700    0.714  ...    65.0      34.0      40.0   1.02
mu        0.091  0.015   0.062    0.118  ...     6.0       7.0      16.0   1.29
mus       0.193  0.033   0.133    0.254  ...    54.0      50.0      31.0   1.03
gamma     0.254  0.052   0.162    0.353  ...    82.0      76.0      19.0   1.01
Is_begin  0.805  0.703   0.008    2.272  ...   102.0      83.0      59.0   1.06
Ia_begin  0.526  0.397   0.004    1.389  ...    83.0      86.0      83.0   1.02
E_begin   0.416  0.526   0.011    1.259  ...    60.0      57.0      42.0   1.00

[8 rows x 11 columns]