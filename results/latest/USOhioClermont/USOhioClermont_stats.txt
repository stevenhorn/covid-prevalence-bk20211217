0 Divergences 
Passed validation 
2021-12-09 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 635 log-likelihood matrix

         Estimate       SE
elpd_loo -2488.73    51.29
p_loo       40.49        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      596   93.9%
 (0.5, 0.7]   (ok)         29    4.6%
   (0.7, 1]   (bad)         8    1.3%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.173  0.017   0.153    0.206  ...    69.0      75.0      91.0   1.07
pu        0.707  0.006   0.700    0.718  ...    72.0      48.0      59.0   1.08
mu        0.099  0.012   0.082    0.120  ...    32.0      29.0      51.0   1.04
mus       0.158  0.027   0.112    0.197  ...    75.0      79.0      81.0   1.02
gamma     0.229  0.040   0.159    0.302  ...   104.0     145.0      37.0   1.01
Is_begin  0.795  0.767   0.036    2.154  ...    69.0      64.0      59.0   0.99
Ia_begin  0.570  0.480   0.035    1.502  ...    80.0      65.0      37.0   1.00
E_begin   0.478  0.544   0.009    1.844  ...    61.0      56.0     100.0   0.98

[8 rows x 11 columns]