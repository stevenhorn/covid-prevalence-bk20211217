0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -3009.97    48.89
p_loo       47.35        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      585   92.3%
 (0.5, 0.7]   (ok)         38    6.0%
   (0.7, 1]   (bad)         8    1.3%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.238  0.054   0.156    0.341  ...    24.0      25.0      38.0   1.05
pu        0.723  0.020   0.700    0.762  ...    32.0      19.0      30.0   1.05
mu        0.129  0.018   0.098    0.161  ...    28.0      25.0      15.0   1.03
mus       0.168  0.028   0.117    0.224  ...   152.0     152.0      85.0   1.03
gamma     0.269  0.051   0.191    0.375  ...    61.0      78.0      76.0   1.04
Is_begin  0.776  0.640   0.017    1.997  ...   125.0     110.0      57.0   0.99
Ia_begin  1.913  1.605   0.121    5.371  ...    98.0      67.0      54.0   1.01
E_begin   0.850  1.013   0.001    2.314  ...    96.0      64.0      59.0   1.01

[8 rows x 11 columns]