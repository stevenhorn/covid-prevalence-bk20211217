0 Divergences 
Passed validation 
2021-12-16 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 643 log-likelihood matrix

         Estimate       SE
elpd_loo -2378.55    42.83
p_loo       46.42        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      584   90.8%
 (0.5, 0.7]   (ok)         44    6.8%
   (0.7, 1]   (bad)        12    1.9%
   (1, Inf)   (very bad)    3    0.5%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.265   0.061   0.161    0.349  ...   133.0     134.0      49.0   1.07
pu         0.827   0.053   0.738    0.900  ...    34.0      35.0      60.0   0.99
mu         0.168   0.024   0.134    0.208  ...    38.0      40.0      76.0   1.03
mus        0.203   0.033   0.147    0.261  ...   152.0     152.0      59.0   1.01
gamma      0.303   0.048   0.236    0.404  ...   152.0     152.0      76.0   1.01
Is_begin   6.823   4.903   0.399   17.448  ...    96.0     134.0      59.0   1.02
Ia_begin  21.849  20.584   1.415   66.905  ...    80.0      74.0      84.0   0.98
E_begin   10.765  11.339   0.339   36.681  ...    39.0      39.0      59.0   1.04

[8 rows x 11 columns]