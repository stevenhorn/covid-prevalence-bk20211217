0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2042.81    44.90
p_loo       46.62        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      603   95.1%
 (0.5, 0.7]   (ok)         19    3.0%
   (0.7, 1]   (bad)        10    1.6%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.158  0.009   0.150    0.174  ...   103.0     106.0      38.0   1.03
pu        0.703  0.002   0.700    0.707  ...   105.0      87.0      91.0   1.04
mu        0.093  0.014   0.069    0.117  ...    28.0      29.0      60.0   1.01
mus       0.154  0.025   0.111    0.200  ...    63.0      63.0      83.0   1.01
gamma     0.223  0.041   0.144    0.285  ...    23.0      20.0      48.0   1.10
Is_begin  0.782  0.617   0.073    1.976  ...    99.0      80.0      56.0   0.99
Ia_begin  1.397  1.493   0.043    5.026  ...    51.0      31.0      95.0   1.08
E_begin   0.758  0.709   0.005    2.035  ...    62.0      52.0      40.0   1.09

[8 rows x 11 columns]