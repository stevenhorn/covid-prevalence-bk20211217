0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2493.27    51.92
p_loo       54.69        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      602   95.0%
 (0.5, 0.7]   (ok)         20    3.2%
   (0.7, 1]   (bad)         9    1.4%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.169  0.018   0.150    0.207  ...   152.0     152.0      56.0   1.00
pu        0.706  0.006   0.700    0.715  ...    57.0      75.0      57.0   1.00
mu        0.096  0.016   0.070    0.126  ...    52.0      37.0      60.0   1.05
mus       0.160  0.026   0.119    0.206  ...   124.0     123.0      83.0   0.99
gamma     0.214  0.043   0.144    0.289  ...   127.0     111.0      59.0   1.04
Is_begin  0.690  0.700   0.002    2.074  ...   152.0     152.0      93.0   1.00
Ia_begin  0.984  1.069   0.010    3.291  ...   120.0     123.0      91.0   0.99
E_begin   0.370  0.353   0.022    1.031  ...    74.0      68.0      59.0   1.00

[8 rows x 11 columns]