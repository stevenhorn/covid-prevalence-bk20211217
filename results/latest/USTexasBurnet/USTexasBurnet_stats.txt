0 Divergences 
Failed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -2464.35    90.93
p_loo       67.31        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      589   93.2%
 (0.5, 0.7]   (ok)         29    4.6%
   (0.7, 1]   (bad)        10    1.6%
   (1, Inf)   (very bad)    4    0.6%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.167  0.020   0.150    0.207  ...    78.0      27.0      17.0   1.04
pu        0.705  0.004   0.700    0.714  ...   152.0     129.0      39.0   1.00
mu        0.111  0.012   0.095    0.135  ...    38.0      37.0      59.0   1.03
mus       0.320  0.039   0.255    0.394  ...    78.0      85.0      95.0   1.00
gamma     0.687  0.086   0.540    0.880  ...   115.0     111.0     100.0   0.98
Is_begin  0.805  0.868   0.001    2.634  ...    60.0      31.0      42.0   1.03
Ia_begin  1.448  1.305   0.032    4.372  ...    71.0      74.0      40.0   1.02
E_begin   0.603  0.483   0.074    1.377  ...    82.0      91.0      95.0   1.02

[8 rows x 11 columns]