0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1097.72    34.39
p_loo       46.76        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      571   90.1%
 (0.5, 0.7]   (ok)         48    7.6%
   (0.7, 1]   (bad)        14    2.2%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.234  0.048   0.162    0.323  ...    57.0      69.0      29.0   1.02
pu        0.738  0.020   0.701    0.770  ...    39.0      40.0      57.0   1.01
mu        0.142  0.024   0.098    0.179  ...     6.0       7.0      43.0   1.30
mus       0.216  0.038   0.147    0.286  ...    14.0      11.0      58.0   1.13
gamma     0.293  0.044   0.227    0.367  ...    73.0      82.0      59.0   1.00
Is_begin  0.753  0.740   0.002    2.032  ...    86.0      63.0      15.0   1.02
Ia_begin  1.106  1.143   0.005    3.848  ...    91.0      31.0      43.0   1.04
E_begin   0.377  0.316   0.005    0.938  ...    53.0      27.0      15.0   1.10

[8 rows x 11 columns]