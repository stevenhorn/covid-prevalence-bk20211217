0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -2397.28    54.25
p_loo       40.76        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      594   94.0%
 (0.5, 0.7]   (ok)         32    5.1%
   (0.7, 1]   (bad)         5    0.8%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.187  0.024   0.154    0.230  ...    81.0      81.0      59.0   1.00
pu        0.711  0.008   0.700    0.724  ...   106.0      84.0      59.0   0.99
mu        0.120  0.016   0.093    0.152  ...    76.0      71.0      93.0   0.99
mus       0.174  0.030   0.134    0.236  ...   115.0     132.0      88.0   1.00
gamma     0.213  0.040   0.139    0.272  ...    70.0      73.0      96.0   1.02
Is_begin  0.711  0.734   0.000    1.967  ...    75.0      53.0      60.0   1.04
Ia_begin  1.205  1.394   0.007    3.040  ...    79.0      60.0      60.0   1.04
E_begin   0.517  0.570   0.014    1.397  ...    85.0      70.0      96.0   1.00

[8 rows x 11 columns]