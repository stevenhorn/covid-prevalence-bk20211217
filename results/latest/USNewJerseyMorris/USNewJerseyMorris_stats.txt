0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2909.53    37.46
p_loo       45.97        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      572   90.2%
 (0.5, 0.7]   (ok)         53    8.4%
   (0.7, 1]   (bad)         7    1.1%
   (1, Inf)   (very bad)    2    0.3%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.191   0.033   0.151    0.250  ...    77.0      72.0      59.0   1.00
pu         0.714   0.012   0.700    0.737  ...    57.0      50.0      35.0   1.06
mu         0.111   0.014   0.089    0.138  ...     7.0       7.0      23.0   1.28
mus        0.191   0.030   0.138    0.248  ...    66.0      71.0      30.0   1.00
gamma      0.323   0.048   0.251    0.413  ...    85.0      90.0      60.0   1.00
Is_begin  18.075  12.742   0.597   38.599  ...    18.0      16.0      16.0   1.09
Ia_begin  53.516  20.305   7.550   83.426  ...    30.0      31.0      22.0   1.02
E_begin   71.588  37.455  17.531  147.176  ...    62.0      53.0      31.0   0.98

[8 rows x 11 columns]