0 Divergences 
Failed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo  -895.78    39.51
p_loo       46.81        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      592   93.7%
 (0.5, 0.7]   (ok)         34    5.4%
   (0.7, 1]   (bad)         5    0.8%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.244  0.054   0.150    0.330  ...    41.0      38.0      55.0   1.02
pu        0.746  0.024   0.706    0.788  ...    45.0      44.0      22.0   0.99
mu        0.128  0.024   0.083    0.168  ...    40.0      42.0      59.0   1.04
mus       0.191  0.032   0.135    0.252  ...    59.0      57.0      56.0   1.03
gamma     0.221  0.040   0.158    0.297  ...   140.0     134.0      46.0   1.01
Is_begin  0.245  0.229   0.010    0.611  ...    78.0      62.0      75.0   0.99
Ia_begin  0.360  0.357   0.002    1.066  ...    53.0      29.0      58.0   1.06
E_begin   0.184  0.200   0.000    0.521  ...    62.0      40.0      58.0   1.02

[8 rows x 11 columns]