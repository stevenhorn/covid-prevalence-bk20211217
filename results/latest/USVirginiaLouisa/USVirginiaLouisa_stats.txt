0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -1766.02    31.04
p_loo       36.02        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      594   94.0%
 (0.5, 0.7]   (ok)         30    4.7%
   (0.7, 1]   (bad)         8    1.3%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.250  0.046   0.160    0.329  ...    52.0      53.0      60.0   0.98
pu        0.748  0.027   0.700    0.792  ...    33.0      29.0      31.0   1.02
mu        0.144  0.027   0.100    0.199  ...    10.0      12.0      59.0   1.13
mus       0.175  0.036   0.105    0.231  ...   137.0     125.0     100.0   0.98
gamma     0.216  0.038   0.147    0.297  ...    63.0      62.0      38.0   1.01
Is_begin  0.934  0.945   0.011    2.504  ...   120.0      83.0      59.0   0.99
Ia_begin  2.035  1.809   0.045    5.857  ...    72.0     110.0      97.0   1.02
E_begin   0.675  0.610   0.011    1.959  ...    65.0      71.0      39.0   1.01

[8 rows x 11 columns]