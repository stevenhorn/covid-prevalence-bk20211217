0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -1843.46    39.77
p_loo       47.68        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      584   92.4%
 (0.5, 0.7]   (ok)         39    6.2%
   (0.7, 1]   (bad)         9    1.4%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.170  0.015   0.150    0.194  ...   113.0      91.0      37.0   0.98
pu        0.705  0.004   0.700    0.714  ...   138.0      83.0      39.0   0.99
mu        0.112  0.014   0.090    0.137  ...    51.0      40.0      39.0   1.05
mus       0.205  0.035   0.129    0.264  ...   134.0     133.0      65.0   1.00
gamma     0.293  0.047   0.222    0.375  ...   116.0      82.0      66.0   1.02
Is_begin  0.672  0.733   0.007    1.936  ...    87.0      74.0      51.0   1.00
Ia_begin  0.645  0.414   0.064    1.459  ...    20.0      20.0      72.0   1.08
E_begin   0.358  0.342   0.030    1.055  ...    43.0      40.0      49.0   1.04

[8 rows x 11 columns]