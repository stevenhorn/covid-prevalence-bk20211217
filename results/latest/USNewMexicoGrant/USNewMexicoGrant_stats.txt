0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1811.25    48.99
p_loo       37.54        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      602   95.0%
 (0.5, 0.7]   (ok)         25    3.9%
   (0.7, 1]   (bad)         4    0.6%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.227  0.042   0.157    0.295  ...    85.0      83.0      76.0   1.01
pu        0.723  0.016   0.701    0.751  ...    89.0      83.0      93.0   0.99
mu        0.125  0.020   0.096    0.168  ...    17.0      14.0      59.0   1.10
mus       0.162  0.037   0.093    0.229  ...    65.0      56.0      49.0   1.10
gamma     0.167  0.029   0.131    0.230  ...    87.0      89.0      60.0   0.99
Is_begin  0.692  0.574   0.034    1.794  ...    61.0      94.0      30.0   1.06
Ia_begin  1.316  1.193   0.008    3.775  ...    92.0      67.0      56.0   1.03
E_begin   0.556  0.584   0.012    1.917  ...    69.0      52.0      73.0   1.04

[8 rows x 11 columns]