0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2755.51    35.61
p_loo       37.58        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      587   92.6%
 (0.5, 0.7]   (ok)         39    6.2%
   (0.7, 1]   (bad)         8    1.3%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.166  0.013   0.150    0.189  ...    86.0      76.0      60.0   1.01
pu        0.706  0.005   0.700    0.714  ...   152.0     152.0      40.0   0.98
mu        0.097  0.013   0.073    0.121  ...    51.0      50.0      86.0   1.02
mus       0.185  0.031   0.139    0.231  ...    75.0      80.0      57.0   1.03
gamma     0.248  0.044   0.178    0.312  ...   152.0     108.0      48.0   1.00
Is_begin  2.100  1.739   0.000    4.958  ...    34.0      15.0      22.0   1.12
Ia_begin  4.927  3.017   0.344   10.776  ...    77.0      61.0      69.0   1.01
E_begin   3.149  2.985   0.010    9.742  ...    84.0      72.0      96.0   1.00

[8 rows x 11 columns]