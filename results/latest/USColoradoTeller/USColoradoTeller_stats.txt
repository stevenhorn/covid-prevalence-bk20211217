0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1718.79    31.27
p_loo       35.79        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      599   94.5%
 (0.5, 0.7]   (ok)         31    4.9%
   (0.7, 1]   (bad)         4    0.6%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.236  0.048   0.150    0.313  ...    20.0      18.0      37.0   1.08
pu        0.737  0.019   0.702    0.766  ...    28.0      26.0      22.0   1.09
mu        0.139  0.021   0.102    0.185  ...    45.0      43.0      58.0   0.99
mus       0.169  0.032   0.115    0.221  ...   116.0     112.0      93.0   1.00
gamma     0.209  0.043   0.155    0.293  ...    49.0      52.0      69.0   1.01
Is_begin  0.999  0.846   0.058    2.538  ...    48.0      37.0      56.0   1.07
Ia_begin  2.093  1.599   0.030    4.905  ...    78.0      74.0      59.0   1.04
E_begin   0.892  0.724   0.019    2.055  ...    81.0      64.0      33.0   1.03

[8 rows x 11 columns]