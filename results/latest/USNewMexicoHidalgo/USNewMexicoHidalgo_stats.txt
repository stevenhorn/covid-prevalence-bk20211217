0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1172.00    51.18
p_loo       62.48        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      586   92.4%
 (0.5, 0.7]   (ok)         39    6.2%
   (0.7, 1]   (bad)         5    0.8%
   (1, Inf)   (very bad)    4    0.6%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.243  0.049   0.150    0.315  ...    65.0      66.0      40.0   1.04
pu        0.744  0.021   0.702    0.775  ...    61.0      64.0      40.0   1.03
mu        0.150  0.025   0.101    0.194  ...    18.0      21.0      59.0   1.08
mus       0.216  0.033   0.161    0.287  ...    39.0      69.0      56.0   1.07
gamma     0.305  0.046   0.229    0.390  ...    69.0      79.0      57.0   1.00
Is_begin  0.459  0.456   0.011    1.245  ...    99.0      87.0      91.0   1.01
Ia_begin  0.742  0.739   0.005    2.368  ...    99.0      83.0      60.0   0.99
E_begin   0.290  0.274   0.003    0.771  ...    63.0      57.0      58.0   0.98

[8 rows x 11 columns]