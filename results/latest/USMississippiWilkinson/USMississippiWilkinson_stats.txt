0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1323.57    40.07
p_loo       43.44        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      589   92.9%
 (0.5, 0.7]   (ok)         38    6.0%
   (0.7, 1]   (bad)         7    1.1%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.251  0.057   0.160    0.344  ...    39.0      36.0      40.0   1.05
pu        0.770  0.023   0.735    0.806  ...    16.0      15.0      48.0   1.13
mu        0.149  0.025   0.113    0.201  ...     5.0       6.0      57.0   1.39
mus       0.197  0.031   0.152    0.269  ...    53.0      53.0      97.0   1.09
gamma     0.280  0.056   0.158    0.378  ...    73.0      70.0      39.0   1.04
Is_begin  1.465  1.036   0.094    2.998  ...    82.0      61.0      72.0   1.03
Ia_begin  3.019  2.365   0.126    7.538  ...    95.0      53.0      74.0   1.03
E_begin   1.818  1.696   0.035    4.403  ...    91.0      99.0      77.0   1.00

[8 rows x 11 columns]