0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1361.56    58.52
p_loo       59.56        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      596   94.0%
 (0.5, 0.7]   (ok)         31    4.9%
   (0.7, 1]   (bad)         5    0.8%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.206  0.042   0.150    0.284  ...    40.0      60.0      40.0   1.03
pu        0.719  0.012   0.703    0.742  ...    50.0      46.0      59.0   1.04
mu        0.148  0.024   0.105    0.196  ...    19.0      19.0      59.0   1.10
mus       0.247  0.040   0.179    0.322  ...    66.0      64.0      88.0   1.03
gamma     0.336  0.051   0.244    0.435  ...   144.0     152.0      99.0   1.00
Is_begin  0.575  0.499   0.027    1.389  ...    79.0      86.0      35.0   1.00
Ia_begin  0.826  0.810   0.045    2.457  ...    83.0      51.0      47.0   1.00
E_begin   0.432  0.449   0.013    1.358  ...    74.0      80.0      18.0   0.99

[8 rows x 11 columns]