0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1133.66    61.12
p_loo       68.16        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      589   92.9%
 (0.5, 0.7]   (ok)         36    5.7%
   (0.7, 1]   (bad)         7    1.1%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.215  0.047   0.151    0.290  ...    78.0      49.0      40.0   1.02
pu        0.725  0.018   0.700    0.755  ...    71.0      68.0      51.0   1.06
mu        0.141  0.029   0.097    0.199  ...    68.0      77.0      93.0   1.01
mus       0.227  0.035   0.161    0.279  ...    99.0     116.0      54.0   0.99
gamma     0.279  0.045   0.200    0.366  ...   100.0      88.0      59.0   1.04
Is_begin  0.453  0.375   0.001    1.269  ...    91.0      61.0      38.0   1.05
Ia_begin  0.673  0.640   0.001    2.023  ...    84.0      78.0      37.0   1.00
E_begin   0.336  0.380   0.000    1.005  ...    54.0      31.0      60.0   1.03

[8 rows x 11 columns]