0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -3154.66    47.96
p_loo       14.30        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      625   98.6%
 (0.5, 0.7]   (ok)          8    1.3%
   (0.7, 1]   (bad)         1    0.2%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.175  0.006   0.168    0.182  ...     3.0       3.0      14.0   2.01
pu        0.703  0.001   0.702    0.705  ...     3.0       3.0      14.0   2.11
mu        0.279  0.002   0.277    0.283  ...     3.0       3.0      27.0   1.99
mus       0.275  0.041   0.233    0.316  ...     3.0       3.0      17.0   2.02
gamma     0.172  0.012   0.159    0.187  ...     3.0       3.0      21.0   2.85
Is_begin  1.607  0.388   1.121    2.116  ...     3.0       3.0      14.0   2.21
Ia_begin  2.973  1.380   1.538    4.649  ...     3.0       3.0      20.0   2.03
E_begin   1.899  0.394   1.389    2.622  ...     3.0       3.0      14.0   2.07

[8 rows x 11 columns]