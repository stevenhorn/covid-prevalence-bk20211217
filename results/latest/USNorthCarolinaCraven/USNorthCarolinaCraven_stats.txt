0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2591.44    36.07
p_loo       34.86        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      605   95.4%
 (0.5, 0.7]   (ok)         25    3.9%
   (0.7, 1]   (bad)         3    0.5%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.226  0.045   0.158    0.316  ...    60.0      59.0      91.0   1.01
pu        0.725  0.016   0.703    0.758  ...    45.0      49.0      51.0   1.03
mu        0.119  0.019   0.090    0.155  ...    12.0      14.0      65.0   1.13
mus       0.154  0.028   0.111    0.205  ...    87.0      88.0      88.0   1.00
gamma     0.211  0.035   0.162    0.279  ...    66.0      68.0      88.0   1.02
Is_begin  0.733  0.692   0.021    1.941  ...    97.0      79.0      60.0   1.09
Ia_begin  1.443  1.344   0.031    4.636  ...    90.0      29.0      80.0   1.07
E_begin   0.590  0.635   0.001    1.710  ...   102.0      25.0      32.0   1.09

[8 rows x 11 columns]