0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1746.54    72.90
p_loo       52.65        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      588   92.7%
 (0.5, 0.7]   (ok)         36    5.7%
   (0.7, 1]   (bad)         9    1.4%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.159  0.008   0.150    0.175  ...    91.0      80.0      59.0   1.02
pu        0.704  0.003   0.700    0.710  ...   113.0      95.0      38.0   1.00
mu        0.132  0.017   0.101    0.161  ...    15.0      13.0      36.0   1.15
mus       0.251  0.040   0.188    0.317  ...    45.0      58.0      77.0   1.05
gamma     0.328  0.048   0.229    0.397  ...    56.0      54.0      59.0   1.01
Is_begin  0.828  0.820   0.045    3.005  ...    52.0      66.0      56.0   1.07
Ia_begin  1.382  1.146   0.027    3.460  ...    66.0      50.0      79.0   1.04
E_begin   0.638  0.658   0.005    1.593  ...    39.0      33.0      40.0   1.04

[8 rows x 11 columns]