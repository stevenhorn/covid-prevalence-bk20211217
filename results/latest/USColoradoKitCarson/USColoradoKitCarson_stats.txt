0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1369.88    56.44
p_loo       49.40        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      596   94.0%
 (0.5, 0.7]   (ok)         31    4.9%
   (0.7, 1]   (bad)         3    0.5%
   (1, Inf)   (very bad)    4    0.6%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.163  0.009   0.151    0.179  ...   152.0     152.0      97.0   1.03
pu        0.704  0.004   0.700    0.712  ...   100.0      92.0      63.0   1.00
mu        0.099  0.017   0.067    0.125  ...    36.0      33.0      38.0   1.05
mus       0.178  0.037   0.116    0.242  ...    88.0      85.0      57.0   1.03
gamma     0.222  0.040   0.148    0.292  ...   152.0     152.0      75.0   1.00
Is_begin  0.602  0.637   0.015    2.099  ...    58.0      17.0      96.0   1.09
Ia_begin  1.095  1.406   0.002    4.000  ...    99.0      56.0      22.0   1.01
E_begin   0.489  0.545   0.010    1.421  ...    71.0      56.0      86.0   1.01

[8 rows x 11 columns]