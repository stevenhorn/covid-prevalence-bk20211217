0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2514.12    35.88
p_loo       34.01        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      580   91.5%
 (0.5, 0.7]   (ok)         42    6.6%
   (0.7, 1]   (bad)        11    1.7%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.175  0.021   0.150    0.216  ...    62.0      44.0      31.0   1.05
pu        0.709  0.008   0.700    0.719  ...    39.0      49.0      59.0   1.04
mu        0.090  0.015   0.069    0.120  ...    11.0      11.0      33.0   1.15
mus       0.149  0.023   0.108    0.191  ...    85.0      83.0      59.0   0.99
gamma     0.256  0.042   0.183    0.334  ...    59.0      73.0      60.0   1.01
Is_begin  0.815  0.662   0.010    1.900  ...    89.0      62.0      32.0   0.99
Ia_begin  0.638  0.571   0.019    1.547  ...   137.0     111.0      60.0   1.00
E_begin   0.546  0.583   0.005    1.961  ...   107.0     115.0      60.0   1.00

[8 rows x 11 columns]