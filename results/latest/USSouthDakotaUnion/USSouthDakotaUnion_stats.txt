0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -1709.15    41.28
p_loo       44.53        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      598   94.6%
 (0.5, 0.7]   (ok)         27    4.3%
   (0.7, 1]   (bad)         5    0.8%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.184  0.034   0.151    0.254  ...    52.0      36.0      58.0   1.05
pu        0.713  0.009   0.702    0.727  ...    19.0      16.0      56.0   1.11
mu        0.109  0.016   0.082    0.137  ...     9.0       9.0      37.0   1.19
mus       0.171  0.027   0.125    0.220  ...   106.0      97.0      37.0   1.01
gamma     0.206  0.037   0.145    0.287  ...    74.0      56.0      25.0   1.00
Is_begin  0.645  0.565   0.066    1.874  ...   122.0      75.0      58.0   1.03
Ia_begin  1.217  1.411   0.014    4.245  ...    77.0      36.0      30.0   1.04
E_begin   0.565  0.638   0.008    1.760  ...    77.0      46.0      60.0   1.02

[8 rows x 11 columns]