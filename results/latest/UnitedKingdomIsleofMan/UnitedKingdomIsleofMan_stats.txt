40 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 633 log-likelihood matrix

         Estimate       SE
elpd_loo -1971.95    97.64
p_loo       73.38        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      510   80.6%
 (0.5, 0.7]   (ok)         66   10.4%
   (0.7, 1]   (bad)        37    5.8%
   (1, Inf)   (very bad)   20    3.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.164  0.012   0.152    0.190  ...    11.0      16.0      53.0   1.49
pu        0.710  0.004   0.702    0.714  ...     5.0       7.0      58.0   1.57
mu        0.132  0.008   0.117    0.144  ...     6.0      10.0      57.0   1.81
mus       0.308  0.063   0.250    0.423  ...     3.0       4.0      54.0   1.92
gamma     0.900  0.061   0.778    1.001  ...    44.0      44.0      72.0   1.15
Is_begin  0.967  0.730   0.132    2.755  ...    13.0      42.0      25.0   1.63
Ia_begin  2.116  1.980   0.074    5.800  ...    10.0       7.0      38.0   1.71
E_begin   0.846  1.090   0.032    3.571  ...     6.0       8.0      68.0   1.74

[8 rows x 11 columns]