0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1820.71    35.99
p_loo       38.19        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      593   93.5%
 (0.5, 0.7]   (ok)         33    5.2%
   (0.7, 1]   (bad)         8    1.3%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.163  0.010   0.150    0.184  ...   152.0     141.0      40.0   1.06
pu        0.703  0.003   0.700    0.708  ...    91.0      70.0      40.0   1.04
mu        0.111  0.015   0.082    0.137  ...     8.0       8.0      15.0   1.20
mus       0.213  0.032   0.168    0.283  ...   102.0     115.0      93.0   1.02
gamma     0.299  0.035   0.237    0.366  ...    75.0      78.0      69.0   1.01
Is_begin  0.619  0.483   0.008    1.389  ...    61.0      93.0      56.0   1.00
Ia_begin  1.191  1.180   0.031    3.375  ...   138.0     152.0      87.0   1.00
E_begin   0.590  0.534   0.038    1.652  ...   122.0      90.0      60.0   0.99

[8 rows x 11 columns]