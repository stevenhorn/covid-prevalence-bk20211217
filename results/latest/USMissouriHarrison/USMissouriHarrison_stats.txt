0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1511.64    77.36
p_loo       55.92        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      607   95.7%
 (0.5, 0.7]   (ok)         21    3.3%
   (0.7, 1]   (bad)         5    0.8%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.159  0.009   0.150    0.172  ...   102.0      47.0      53.0   1.09
pu        0.703  0.003   0.700    0.708  ...   118.0     103.0      72.0   1.02
mu        0.117  0.012   0.093    0.134  ...    22.0      24.0      48.0   1.07
mus       0.281  0.026   0.241    0.326  ...    55.0      52.0      60.0   1.03
gamma     0.535  0.079   0.404    0.665  ...   152.0     152.0      79.0   1.00
Is_begin  0.608  0.633   0.027    1.603  ...    95.0      53.0      48.0   1.03
Ia_begin  1.086  1.091   0.005    3.961  ...    77.0      65.0      39.0   1.04
E_begin   0.392  0.431   0.003    1.344  ...   116.0      68.0      59.0   1.00

[8 rows x 11 columns]