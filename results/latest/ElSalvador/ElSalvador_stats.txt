38 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 633 log-likelihood matrix

         Estimate       SE
elpd_loo -4215.22    43.74
p_loo       40.07        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      604   95.4%
 (0.5, 0.7]   (ok)         20    3.2%
   (0.7, 1]   (bad)         9    1.4%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.273  0.047   0.174    0.339  ...   127.0     139.0      59.0   0.99
pu        0.881  0.019   0.855    0.900  ...    60.0      63.0      59.0   1.05
mu        0.151  0.039   0.084    0.219  ...     3.0       4.0      43.0   1.82
mus       0.151  0.033   0.097    0.219  ...    68.0      62.0      43.0   1.05
gamma     0.153  0.027   0.119    0.207  ...    82.0      82.0      87.0   1.06
Is_begin  0.938  0.852   0.030    2.687  ...    59.0      61.0      22.0   1.03
Ia_begin  2.095  1.582   0.041    4.758  ...    10.0       9.0      54.0   1.19
E_begin   1.216  1.063   0.008    2.991  ...    40.0      29.0      20.0   1.07

[8 rows x 11 columns]