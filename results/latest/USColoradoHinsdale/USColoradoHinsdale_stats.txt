0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo  -349.20    37.30
p_loo       39.97        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      595   93.8%
 (0.5, 0.7]   (ok)         28    4.4%
   (0.7, 1]   (bad)        11    1.7%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.264  0.055   0.169    0.348  ...   125.0     126.0      60.0   1.00
pu        0.845  0.021   0.811    0.884  ...    56.0      55.0      59.0   1.04
mu        0.148  0.032   0.093    0.201  ...    17.0      16.0      59.0   1.11
mus       0.203  0.032   0.156    0.284  ...    44.0      45.0      60.0   1.04
gamma     0.225  0.033   0.159    0.274  ...    83.0      78.0      60.0   1.01
Is_begin  0.790  0.869   0.009    2.408  ...   101.0      89.0      49.0   1.01
Ia_begin  1.730  1.751   0.067    4.951  ...    79.0      71.0      58.0   1.05
E_begin   0.727  0.778   0.004    2.423  ...    64.0      59.0      88.0   1.05

[8 rows x 11 columns]