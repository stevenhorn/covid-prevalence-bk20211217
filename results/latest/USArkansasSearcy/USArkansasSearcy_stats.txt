0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1309.99    48.26
p_loo       57.85        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      586   92.4%
 (0.5, 0.7]   (ok)         38    6.0%
   (0.7, 1]   (bad)         7    1.1%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.168  0.016   0.150    0.202  ...    40.0      23.0      29.0   1.07
pu        0.708  0.006   0.700    0.719  ...    57.0      68.0      40.0   1.03
mu        0.149  0.035   0.098    0.219  ...     4.0       4.0      30.0   1.64
mus       0.222  0.041   0.164    0.312  ...     7.0       6.0      20.0   1.31
gamma     0.289  0.047   0.199    0.356  ...    80.0      95.0      60.0   1.03
Is_begin  0.659  0.732   0.013    2.211  ...    27.0      24.0      14.0   1.07
Ia_begin  1.218  1.151   0.013    3.469  ...    80.0     110.0      79.0   1.02
E_begin   0.451  0.463   0.000    1.372  ...    46.0      52.0      57.0   1.08

[8 rows x 11 columns]