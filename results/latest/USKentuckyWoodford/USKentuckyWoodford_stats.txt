0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1932.08    72.42
p_loo       55.32        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      583   92.0%
 (0.5, 0.7]   (ok)         39    6.2%
   (0.7, 1]   (bad)        10    1.6%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.180  0.022   0.152    0.227  ...    31.0      41.0      51.0   1.05
pu        0.708  0.007   0.700    0.721  ...    72.0      43.0      59.0   1.01
mu        0.120  0.019   0.094    0.158  ...     4.0       4.0      56.0   1.61
mus       0.219  0.030   0.177    0.283  ...    48.0      42.0      58.0   1.06
gamma     0.297  0.045   0.213    0.367  ...   120.0     111.0      84.0   1.09
Is_begin  0.683  0.651   0.004    1.962  ...    51.0      39.0      60.0   1.00
Ia_begin  1.301  1.068   0.009    3.174  ...    39.0      34.0      42.0   1.07
E_begin   0.567  0.576   0.009    1.531  ...    84.0      50.0      42.0   1.03

[8 rows x 11 columns]