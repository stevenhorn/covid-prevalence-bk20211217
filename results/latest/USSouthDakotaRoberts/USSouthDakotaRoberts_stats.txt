0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -1388.36    44.92
p_loo       48.95        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      598   94.6%
 (0.5, 0.7]   (ok)         28    4.4%
   (0.7, 1]   (bad)         4    0.6%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.204  0.041   0.153    0.272  ...    27.0      45.0      41.0   1.08
pu        0.717  0.011   0.702    0.737  ...   148.0     128.0      83.0   1.00
mu        0.131  0.022   0.095    0.167  ...    71.0      71.0      57.0   1.03
mus       0.212  0.033   0.157    0.272  ...    58.0      72.0      65.0   1.03
gamma     0.269  0.044   0.201    0.341  ...    91.0      89.0      81.0   1.02
Is_begin  0.665  0.635   0.006    1.937  ...   130.0     104.0      36.0   1.00
Ia_begin  1.204  1.210   0.005    3.545  ...    43.0      96.0      59.0   1.07
E_begin   0.521  0.636   0.007    1.320  ...    91.0      29.0      55.0   1.04

[8 rows x 11 columns]