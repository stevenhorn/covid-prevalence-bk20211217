0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -2990.43    65.75
p_loo       27.96        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      618   97.8%
 (0.5, 0.7]   (ok)         11    1.7%
   (0.7, 1]   (bad)         3    0.5%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.178  0.003   0.175    0.182  ...     3.0       3.0      27.0   2.48
pu        0.708  0.004   0.704    0.712  ...     3.0       3.0      22.0   2.40
mu        0.194  0.011   0.181    0.206  ...     3.0       3.0      38.0   2.51
mus       0.181  0.002   0.179    0.185  ...     3.0       4.0      19.0   1.72
gamma     0.260  0.007   0.250    0.269  ...     3.0       3.0      14.0   1.94
Is_begin  1.938  0.246   1.645    2.385  ...     3.0       3.0      15.0   1.90
Ia_begin  0.766  0.027   0.716    0.812  ...     6.0       7.0      15.0   1.23
E_begin   0.878  0.424   0.412    1.337  ...     3.0       3.0      21.0   2.64

[8 rows x 11 columns]