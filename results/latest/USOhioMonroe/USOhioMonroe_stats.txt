0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1453.14    49.85
p_loo       56.45        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      585   92.3%
 (0.5, 0.7]   (ok)         42    6.6%
   (0.7, 1]   (bad)         6    0.9%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.172  0.020   0.151    0.208  ...    91.0      80.0      40.0   1.04
pu        0.706  0.005   0.700    0.714  ...   103.0      78.0      54.0   1.03
mu        0.115  0.018   0.083    0.148  ...    61.0      61.0      58.0   1.00
mus       0.224  0.039   0.165    0.298  ...   103.0      98.0      49.0   1.01
gamma     0.314  0.050   0.226    0.392  ...    73.0      73.0      57.0   1.08
Is_begin  0.356  0.465   0.000    1.241  ...    51.0      38.0      59.0   1.05
Ia_begin  0.427  0.440   0.005    1.387  ...    96.0      71.0      59.0   0.98
E_begin   0.191  0.246   0.001    0.695  ...    61.0      56.0      59.0   1.06

[8 rows x 11 columns]