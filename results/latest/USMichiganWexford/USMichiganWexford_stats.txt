36 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1963.93    39.48
p_loo       42.83        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      577   91.0%
 (0.5, 0.7]   (ok)         34    5.4%
   (0.7, 1]   (bad)        22    3.5%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.170  0.018   0.151    0.211  ...    22.0      29.0      36.0   1.44
pu        0.704  0.003   0.700    0.711  ...     9.0       5.0      34.0   1.42
mu        0.136  0.013   0.111    0.154  ...     4.0       5.0      39.0   1.41
mus       0.172  0.023   0.140    0.213  ...     6.0       5.0      25.0   1.43
gamma     0.242  0.029   0.193    0.294  ...    20.0      20.0      47.0   1.25
Is_begin  0.904  0.927   0.143    2.277  ...    14.0       8.0      53.0   1.27
Ia_begin  2.440  1.100   0.711    4.677  ...     8.0       9.0      15.0   1.31
E_begin   0.783  0.762   0.052    2.130  ...     9.0       5.0      70.0   1.37

[8 rows x 11 columns]