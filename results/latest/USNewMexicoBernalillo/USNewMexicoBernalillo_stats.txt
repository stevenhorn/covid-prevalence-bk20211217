1 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -3558.81    36.70
p_loo       34.30        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      583   92.0%
 (0.5, 0.7]   (ok)         44    6.9%
   (0.7, 1]   (bad)         7    1.1%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.205  0.031   0.156    0.255  ...   152.0     106.0      60.0   1.00
pu        0.720  0.014   0.702    0.744  ...   152.0     144.0      93.0   0.99
mu        0.130  0.021   0.105    0.180  ...    29.0      29.0      38.0   1.04
mus       0.172  0.030   0.114    0.223  ...   101.0      89.0      75.0   1.01
gamma     0.259  0.039   0.200    0.345  ...   152.0     152.0      83.0   0.99
Is_begin  4.502  3.019   0.341   10.391  ...    51.0     102.0      40.0   1.07
Ia_begin  9.335  6.464   0.257   22.654  ...    80.0      64.0      18.0   1.01
E_begin   6.146  5.567   0.484   17.037  ...    96.0      57.0      43.0   1.00

[8 rows x 11 columns]