0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2525.22    41.97
p_loo       40.90        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      583   92.0%
 (0.5, 0.7]   (ok)         42    6.6%
   (0.7, 1]   (bad)         8    1.3%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.164  0.013   0.150    0.197  ...   152.0     119.0      59.0   1.02
pu        0.705  0.005   0.700    0.715  ...   152.0     108.0      70.0   1.02
mu        0.104  0.016   0.071    0.134  ...    73.0      67.0      96.0   1.01
mus       0.166  0.027   0.131    0.222  ...   152.0     152.0      60.0   1.03
gamma     0.248  0.049   0.165    0.326  ...   152.0     152.0      88.0   1.04
Is_begin  5.012  4.220   0.112   12.528  ...   130.0     119.0      60.0   1.01
Ia_begin  6.635  6.107   0.041   16.791  ...    95.0     130.0      39.0   1.03
E_begin   3.251  3.022   0.151    7.315  ...   121.0     105.0      59.0   1.00

[8 rows x 11 columns]