0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2017.32    53.13
p_loo       44.89        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      604   95.3%
 (0.5, 0.7]   (ok)         19    3.0%
   (0.7, 1]   (bad)        10    1.6%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.156  0.007   0.150    0.169  ...    72.0      54.0      60.0   1.01
pu        0.702  0.002   0.700    0.707  ...   152.0     111.0      55.0   1.04
mu        0.125  0.017   0.101    0.161  ...    17.0      20.0      65.0   1.07
mus       0.161  0.027   0.114    0.211  ...    66.0      68.0      59.0   1.00
gamma     0.204  0.026   0.166    0.259  ...    46.0      44.0      40.0   1.07
Is_begin  0.629  0.627   0.003    1.828  ...   138.0     141.0      60.0   1.02
Ia_begin  1.012  1.007   0.062    3.299  ...   122.0     124.0      91.0   0.99
E_begin   0.460  0.478   0.005    1.303  ...    73.0      55.0      36.0   1.03

[8 rows x 11 columns]