0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1554.93    36.59
p_loo       37.38        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      600   94.6%
 (0.5, 0.7]   (ok)         23    3.6%
   (0.7, 1]   (bad)        10    1.6%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.225  0.051   0.153    0.308  ...    59.0      54.0      60.0   1.01
pu        0.726  0.019   0.700    0.757  ...    52.0      43.0      30.0   1.00
mu        0.132  0.022   0.092    0.165  ...     5.0       5.0      47.0   1.41
mus       0.183  0.032   0.117    0.231  ...    38.0      68.0      69.0   1.03
gamma     0.215  0.037   0.165    0.293  ...   130.0     144.0      39.0   1.06
Is_begin  0.457  0.518   0.012    1.393  ...    66.0      77.0      46.0   0.99
Ia_begin  0.799  0.713   0.016    2.248  ...   123.0      79.0     100.0   1.00
E_begin   0.366  0.396   0.018    1.272  ...   104.0      84.0      73.0   1.00

[8 rows x 11 columns]