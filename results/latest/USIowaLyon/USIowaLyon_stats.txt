0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1695.57    42.13
p_loo       43.24        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      608   95.9%
 (0.5, 0.7]   (ok)         16    2.5%
   (0.7, 1]   (bad)         9    1.4%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.160  0.009   0.150    0.181  ...   107.0      80.0      40.0   1.06
pu        0.703  0.004   0.700    0.711  ...   126.0      68.0      54.0   1.00
mu        0.082  0.013   0.060    0.108  ...    41.0      50.0      39.0   1.05
mus       0.175  0.034   0.129    0.240  ...    56.0      57.0      88.0   1.01
gamma     0.228  0.043   0.169    0.344  ...    66.0      68.0      37.0   1.02
Is_begin  0.714  0.784   0.001    2.189  ...    69.0     115.0      36.0   1.05
Ia_begin  0.796  0.880   0.035    2.369  ...    59.0      68.0      60.0   1.03
E_begin   0.383  0.439   0.001    1.150  ...    57.0      94.0      39.0   1.00

[8 rows x 11 columns]