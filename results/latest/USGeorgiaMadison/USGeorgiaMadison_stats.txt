0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1909.87    56.79
p_loo       50.10        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      592   93.4%
 (0.5, 0.7]   (ok)         38    6.0%
   (0.7, 1]   (bad)         3    0.5%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.191  0.031   0.152    0.246  ...    47.0     102.0      60.0   1.07
pu        0.712  0.011   0.700    0.732  ...     9.0       7.0      22.0   1.25
mu        0.102  0.014   0.081    0.130  ...    13.0      12.0      60.0   1.16
mus       0.177  0.027   0.127    0.215  ...    20.0      25.0      56.0   1.08
gamma     0.307  0.063   0.211    0.422  ...     5.0       5.0      34.0   1.49
Is_begin  0.562  0.627   0.016    1.619  ...   126.0     101.0      40.0   0.99
Ia_begin  1.043  1.088   0.019    3.076  ...    62.0      92.0      56.0   0.99
E_begin   0.469  0.609   0.003    1.480  ...    59.0      78.0      54.0   1.01

[8 rows x 11 columns]