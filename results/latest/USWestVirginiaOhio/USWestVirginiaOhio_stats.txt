0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -1942.19    41.99
p_loo       45.81        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      564   89.2%
 (0.5, 0.7]   (ok)         60    9.5%
   (0.7, 1]   (bad)         7    1.1%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.179  0.023   0.151    0.220  ...   122.0     100.0      39.0   1.00
pu        0.708  0.006   0.700    0.721  ...    57.0      47.0      33.0   1.07
mu        0.136  0.020   0.105    0.180  ...    37.0      38.0      24.0   1.04
mus       0.187  0.028   0.135    0.227  ...   113.0     117.0      72.0   1.03
gamma     0.278  0.050   0.209    0.373  ...    78.0      74.0      42.0   1.02
Is_begin  0.867  0.816   0.003    2.469  ...    67.0      60.0      58.0   1.02
Ia_begin  1.484  1.489   0.016    4.072  ...    75.0      61.0      96.0   1.03
E_begin   0.801  0.651   0.015    1.856  ...    86.0      82.0      79.0   0.99

[8 rows x 11 columns]