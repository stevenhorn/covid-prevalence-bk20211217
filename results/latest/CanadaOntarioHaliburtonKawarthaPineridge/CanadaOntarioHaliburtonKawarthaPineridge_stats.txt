0 Divergences 
Passed validation 
2021-12-16 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 643 log-likelihood matrix

         Estimate       SE
elpd_loo -1808.95    44.97
p_loo       58.98        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      538   83.7%
 (0.5, 0.7]   (ok)         35    5.4%
   (0.7, 1]   (bad)        43    6.7%
   (1, Inf)   (very bad)   27    4.2%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.288   0.048   0.193    0.348  ...    12.0      22.0      25.0   1.14
pu         0.887   0.010   0.869    0.900  ...    10.0      12.0      40.0   1.13
mu         0.203   0.024   0.169    0.248  ...     3.0       4.0      18.0   1.79
mus        0.210   0.028   0.161    0.258  ...    44.0      42.0      43.0   1.04
gamma      0.331   0.049   0.262    0.431  ...    35.0      37.0      49.0   1.03
Is_begin  10.521   6.457   0.162   22.433  ...    25.0      22.0      21.0   1.11
Ia_begin  46.394  26.021   3.887   92.029  ...     8.0       8.0      15.0   1.22
E_begin   27.995  20.405   2.077   62.982  ...    38.0      41.0      59.0   1.00

[8 rows x 11 columns]