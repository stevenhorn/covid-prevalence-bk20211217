0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1515.88    41.88
p_loo       47.19        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      593   93.5%
 (0.5, 0.7]   (ok)         33    5.2%
   (0.7, 1]   (bad)         7    1.1%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.159  0.007   0.150    0.171  ...   131.0      87.0      38.0   1.01
pu        0.703  0.003   0.700    0.709  ...    39.0      17.0      58.0   1.08
mu        0.095  0.013   0.072    0.115  ...    51.0      47.0      57.0   1.03
mus       0.170  0.029   0.117    0.215  ...    72.0      71.0      40.0   1.02
gamma     0.236  0.039   0.175    0.297  ...    52.0      55.0      74.0   1.03
Is_begin  0.516  0.560   0.001    1.372  ...    95.0     137.0      69.0   1.00
Ia_begin  0.781  0.836   0.010    2.586  ...   108.0      70.0      59.0   1.01
E_begin   0.398  0.456   0.006    1.454  ...    60.0      49.0      60.0   1.00

[8 rows x 11 columns]