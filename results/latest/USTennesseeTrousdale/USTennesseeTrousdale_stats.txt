0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2148.99    86.74
p_loo       49.36        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      596   94.0%
 (0.5, 0.7]   (ok)         29    4.6%
   (0.7, 1]   (bad)         4    0.6%
   (1, Inf)   (very bad)    5    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.163  0.012   0.150    0.184  ...    78.0     124.0      48.0   1.02
pu        0.704  0.003   0.700    0.711  ...   152.0     112.0      91.0   1.00
mu        0.054  0.010   0.038    0.070  ...    70.0     118.0      43.0   1.01
mus       0.277  0.035   0.206    0.336  ...    57.0      60.0      60.0   1.00
gamma     0.569  0.082   0.433    0.703  ...    80.0      82.0      60.0   1.00
Is_begin  0.252  0.278   0.004    0.856  ...    75.0      82.0      93.0   0.99
Ia_begin  0.285  0.288   0.002    0.925  ...    43.0      61.0      25.0   1.06
E_begin   0.166  0.247   0.001    0.582  ...    84.0      55.0      60.0   1.01

[8 rows x 11 columns]