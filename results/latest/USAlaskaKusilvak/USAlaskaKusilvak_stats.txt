0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1827.38    57.39
p_loo       56.95        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      591   93.2%
 (0.5, 0.7]   (ok)         29    4.6%
   (0.7, 1]   (bad)         9    1.4%
   (1, Inf)   (very bad)    5    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.158  0.008   0.150    0.176  ...    36.0      25.0      31.0   1.04
pu        0.703  0.003   0.700    0.710  ...   122.0      92.0      83.0   1.01
mu        0.104  0.020   0.076    0.142  ...    41.0      48.0      52.0   1.05
mus       0.214  0.040   0.160    0.299  ...    24.0      17.0      40.0   1.10
gamma     0.316  0.055   0.214    0.397  ...    54.0      57.0      58.0   1.02
Is_begin  0.228  0.265   0.001    0.748  ...    21.0      15.0      30.0   1.10
Ia_begin  0.293  0.349   0.007    0.836  ...    49.0      38.0      49.0   1.02
E_begin   0.129  0.143   0.003    0.316  ...    79.0      52.0      30.0   1.01

[8 rows x 11 columns]