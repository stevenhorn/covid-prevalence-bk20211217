0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -1712.21    43.09
p_loo       37.06        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      609   96.4%
 (0.5, 0.7]   (ok)         19    3.0%
   (0.7, 1]   (bad)         3    0.5%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.173  0.021   0.150    0.215  ...    86.0      76.0      60.0   1.03
pu        0.709  0.007   0.700    0.722  ...    95.0      92.0      96.0   1.02
mu        0.118  0.018   0.085    0.146  ...    25.0      24.0      60.0   1.08
mus       0.189  0.036   0.124    0.242  ...    39.0      40.0      40.0   1.01
gamma     0.256  0.053   0.180    0.362  ...    26.0      48.0      35.0   1.11
Is_begin  0.630  0.675   0.009    1.851  ...   118.0      76.0      53.0   1.02
Ia_begin  1.311  1.300   0.019    3.575  ...   113.0     111.0      60.0   1.01
E_begin   0.405  0.522   0.014    1.217  ...    58.0      69.0      65.0   1.03

[8 rows x 11 columns]