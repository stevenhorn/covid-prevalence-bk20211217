0 Divergences 
Failed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -2205.23    43.47
p_loo       39.10        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      584   92.4%
 (0.5, 0.7]   (ok)         39    6.2%
   (0.7, 1]   (bad)         6    0.9%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.168  0.014   0.150    0.190  ...   103.0     106.0      75.0   1.00
pu        0.706  0.006   0.700    0.718  ...    90.0      38.0      29.0   1.02
mu        0.119  0.021   0.071    0.148  ...     5.0       6.0      14.0   1.35
mus       0.193  0.042   0.132    0.273  ...    13.0      14.0      59.0   1.12
gamma     0.195  0.031   0.138    0.241  ...    42.0      39.0      84.0   1.07
Is_begin  0.563  0.539   0.001    1.679  ...    55.0      35.0      14.0   1.02
Ia_begin  1.349  1.135   0.010    3.460  ...    33.0      20.0      20.0   1.09
E_begin   0.470  0.505   0.004    1.714  ...    88.0      82.0      93.0   0.99

[8 rows x 11 columns]