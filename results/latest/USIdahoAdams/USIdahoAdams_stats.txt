0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo  -930.85    44.01
p_loo       52.51        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      588   92.7%
 (0.5, 0.7]   (ok)         35    5.5%
   (0.7, 1]   (bad)         9    1.4%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.255  0.048   0.183    0.346  ...    60.0      57.0      44.0   1.03
pu        0.745  0.019   0.710    0.782  ...    67.0      70.0      58.0   1.02
mu        0.160  0.022   0.125    0.200  ...    73.0      71.0      93.0   0.99
mus       0.248  0.034   0.192    0.320  ...   133.0     102.0      70.0   1.03
gamma     0.311  0.047   0.234    0.372  ...    67.0      77.0      54.0   1.01
Is_begin  0.594  0.512   0.001    1.643  ...    67.0      54.0      59.0   1.02
Ia_begin  1.345  1.290   0.039    3.571  ...   100.0      75.0      86.0   1.00
E_begin   0.605  0.749   0.003    1.911  ...    56.0      74.0      77.0   0.99

[8 rows x 11 columns]