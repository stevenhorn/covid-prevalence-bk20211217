0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2752.07    42.73
p_loo       51.44        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      595   93.8%
 (0.5, 0.7]   (ok)         31    4.9%
   (0.7, 1]   (bad)         6    0.9%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.161  0.008   0.150    0.175  ...   127.0     110.0      72.0   1.00
pu        0.703  0.003   0.700    0.710  ...   126.0      75.0      88.0   1.01
mu        0.100  0.016   0.071    0.128  ...    10.0      11.0      87.0   1.17
mus       0.187  0.030   0.136    0.247  ...    98.0     109.0      97.0   1.01
gamma     0.289  0.047   0.217    0.371  ...    41.0      44.0      49.0   1.06
Is_begin  0.771  0.684   0.018    2.080  ...   130.0     152.0      74.0   1.00
Ia_begin  1.208  1.106   0.007    3.222  ...   106.0     152.0      18.0   1.02
E_begin   0.422  0.371   0.011    1.165  ...   116.0      90.0      88.0   1.00

[8 rows x 11 columns]