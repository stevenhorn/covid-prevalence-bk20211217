0 Divergences 
Passed validation 
2021-12-09 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 635 log-likelihood matrix

         Estimate       SE
elpd_loo -2962.29    84.28
p_loo       66.36        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      594   93.5%
 (0.5, 0.7]   (ok)         33    5.2%
   (0.7, 1]   (bad)         5    0.8%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.160  0.011   0.150    0.181  ...   119.0      75.0      60.0   0.98
pu        0.704  0.004   0.700    0.712  ...   152.0     152.0      58.0   1.02
mu        0.095  0.010   0.074    0.113  ...    23.0      21.0      24.0   1.08
mus       0.307  0.037   0.244    0.375  ...    74.0      73.0      54.0   1.02
gamma     0.548  0.083   0.398    0.689  ...   116.0      81.0      81.0   1.03
Is_begin  1.001  0.931   0.040    2.810  ...    68.0      30.0      60.0   1.05
Ia_begin  1.501  1.639   0.001    4.007  ...    72.0      33.0      43.0   1.06
E_begin   0.526  0.581   0.002    1.610  ...    34.0      21.0      80.0   1.08

[8 rows x 11 columns]