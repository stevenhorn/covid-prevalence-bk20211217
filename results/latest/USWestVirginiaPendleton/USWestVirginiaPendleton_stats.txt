0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -1179.80    43.87
p_loo       48.10        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      577   91.3%
 (0.5, 0.7]   (ok)         44    7.0%
   (0.7, 1]   (bad)         8    1.3%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.175  0.022   0.151    0.213  ...    96.0     152.0      72.0   1.03
pu        0.707  0.005   0.700    0.715  ...    73.0      58.0      40.0   1.01
mu        0.161  0.020   0.133    0.208  ...   109.0     129.0      56.0   0.99
mus       0.247  0.033   0.188    0.309  ...   113.0     115.0      91.0   1.06
gamma     0.363  0.049   0.276    0.448  ...    56.0      56.0      96.0   1.02
Is_begin  0.559  0.609   0.006    1.759  ...    87.0      54.0      39.0   1.01
Ia_begin  1.248  1.296   0.017    3.765  ...   115.0      93.0      60.0   1.00
E_begin   0.441  0.583   0.003    1.687  ...   109.0      52.0      60.0   1.03

[8 rows x 11 columns]