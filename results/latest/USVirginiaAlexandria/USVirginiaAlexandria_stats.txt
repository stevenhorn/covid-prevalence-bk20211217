0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -2465.24    30.00
p_loo       37.78        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      586   92.7%
 (0.5, 0.7]   (ok)         41    6.5%
   (0.7, 1]   (bad)         4    0.6%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.242  0.049   0.160    0.326  ...   107.0     104.0      57.0   1.06
pu        0.754  0.033   0.700    0.804  ...    39.0      38.0      40.0   1.06
mu        0.136  0.021   0.098    0.171  ...    33.0      32.0      49.0   1.08
mus       0.168  0.031   0.104    0.221  ...   102.0     139.0      97.0   1.01
gamma     0.254  0.037   0.189    0.327  ...    80.0      82.0      60.0   1.02
Is_begin  0.689  0.585   0.016    1.852  ...    61.0      50.0      60.0   1.02
Ia_begin  1.682  1.145   0.087    3.742  ...    78.0      52.0      42.0   1.00
E_begin   1.698  1.479   0.030    4.771  ...    62.0      56.0      60.0   1.01

[8 rows x 11 columns]