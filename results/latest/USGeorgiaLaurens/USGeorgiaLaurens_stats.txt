0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2285.29    71.92
p_loo       51.24        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      584   92.1%
 (0.5, 0.7]   (ok)         43    6.8%
   (0.7, 1]   (bad)         5    0.8%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.159  0.009   0.150    0.179  ...   152.0     152.0      99.0   1.00
pu        0.703  0.003   0.700    0.712  ...   152.0     152.0      76.0   1.02
mu        0.095  0.011   0.076    0.116  ...    55.0      59.0      58.0   1.04
mus       0.239  0.041   0.182    0.339  ...    30.0      48.0      24.0   1.03
gamma     0.435  0.075   0.276    0.571  ...    72.0     109.0      39.0   1.04
Is_begin  0.792  0.691   0.005    2.094  ...    83.0      76.0      96.0   0.98
Ia_begin  0.558  0.503   0.004    1.398  ...    64.0      77.0      29.0   1.06
E_begin   0.406  0.606   0.014    1.010  ...    93.0     113.0      58.0   1.00

[8 rows x 11 columns]