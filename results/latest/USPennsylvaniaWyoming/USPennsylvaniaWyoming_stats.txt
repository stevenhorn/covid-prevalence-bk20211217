0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1360.91    29.81
p_loo       43.01        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      586   92.4%
 (0.5, 0.7]   (ok)         38    6.0%
   (0.7, 1]   (bad)         8    1.3%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.225  0.045   0.154    0.294  ...    93.0     108.0      60.0   1.02
pu        0.722  0.015   0.700    0.749  ...    75.0      78.0      59.0   0.99
mu        0.144  0.021   0.102    0.175  ...    29.0      27.0      60.0   1.05
mus       0.209  0.029   0.168    0.274  ...   152.0     149.0      53.0   1.02
gamma     0.292  0.046   0.210    0.375  ...   116.0     119.0     102.0   1.00
Is_begin  0.653  0.687   0.005    2.383  ...   126.0     109.0      43.0   0.99
Ia_begin  1.535  1.494   0.034    4.259  ...   117.0      86.0      91.0   1.01
E_begin   0.649  0.635   0.007    1.885  ...    68.0      63.0      41.0   0.98

[8 rows x 11 columns]