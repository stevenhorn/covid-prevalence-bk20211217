0 Divergences 
Passed validation 
2021-12-09 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 635 log-likelihood matrix

         Estimate       SE
elpd_loo -1287.40    73.34
p_loo       53.63        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      579   91.2%
 (0.5, 0.7]   (ok)         47    7.4%
   (0.7, 1]   (bad)         8    1.3%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.186  0.032   0.150    0.250  ...   152.0     152.0      74.0   1.02
pu        0.712  0.009   0.700    0.728  ...   109.0     100.0     100.0   1.02
mu        0.150  0.020   0.122    0.185  ...    13.0      11.0      59.0   1.16
mus       0.281  0.037   0.229    0.363  ...   104.0     137.0      79.0   1.00
gamma     0.461  0.060   0.348    0.553  ...   152.0     152.0      95.0   1.01
Is_begin  0.650  0.658   0.001    1.982  ...   122.0      54.0      32.0   1.01
Ia_begin  0.987  0.937   0.010    2.826  ...   115.0      94.0      60.0   0.99
E_begin   0.479  0.620   0.008    1.497  ...    74.0      78.0      86.0   1.01

[8 rows x 11 columns]