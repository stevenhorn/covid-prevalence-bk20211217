0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2650.24    44.20
p_loo       49.55        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      595   93.8%
 (0.5, 0.7]   (ok)         28    4.4%
   (0.7, 1]   (bad)         6    0.9%
   (1, Inf)   (very bad)    5    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.172  0.015   0.152    0.202  ...   152.0     152.0      60.0   1.00
pu        0.706  0.006   0.700    0.716  ...    75.0      64.0      18.0   1.00
mu        0.111  0.019   0.081    0.144  ...    58.0      59.0      53.0   1.03
mus       0.155  0.032   0.107    0.212  ...    46.0      59.0      38.0   1.04
gamma     0.204  0.034   0.154    0.270  ...    27.0      29.0      60.0   1.05
Is_begin  0.516  0.458   0.017    1.465  ...    86.0      77.0      54.0   1.00
Ia_begin  0.881  0.889   0.010    2.393  ...    55.0      44.0      60.0   1.06
E_begin   0.387  0.365   0.016    1.083  ...    60.0      60.0      59.0   1.05

[8 rows x 11 columns]