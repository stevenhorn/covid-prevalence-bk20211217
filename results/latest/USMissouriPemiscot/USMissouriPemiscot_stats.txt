0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1676.31    76.70
p_loo       65.42        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      582   91.8%
 (0.5, 0.7]   (ok)         36    5.7%
   (0.7, 1]   (bad)        12    1.9%
   (1, Inf)   (very bad)    4    0.6%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.180  0.023   0.151    0.228  ...   102.0      92.0      58.0   1.01
pu        0.709  0.007   0.700    0.725  ...    48.0      43.0      22.0   1.03
mu        0.121  0.013   0.098    0.148  ...    11.0      11.0      39.0   1.16
mus       0.262  0.036   0.182    0.317  ...    86.0      81.0      59.0   0.99
gamma     0.460  0.063   0.355    0.571  ...   126.0     110.0      88.0   1.00
Is_begin  0.642  0.566   0.019    1.596  ...   110.0     107.0      91.0   0.99
Ia_begin  1.017  0.970   0.025    2.545  ...    91.0     112.0      83.0   1.02
E_begin   0.442  0.490   0.003    1.445  ...   110.0      99.0      53.0   0.98

[8 rows x 11 columns]