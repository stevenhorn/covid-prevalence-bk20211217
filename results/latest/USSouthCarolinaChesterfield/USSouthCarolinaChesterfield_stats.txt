0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2229.83    36.14
p_loo       33.80        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      601   94.8%
 (0.5, 0.7]   (ok)         30    4.7%
   (0.7, 1]   (bad)         3    0.5%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.202  0.038   0.152    0.274  ...    11.0       8.0      18.0   1.20
pu        0.718  0.013   0.700    0.739  ...    12.0       9.0      18.0   1.19
mu        0.102  0.017   0.066    0.131  ...    25.0      25.0      21.0   1.09
mus       0.149  0.033   0.086    0.200  ...   135.0     132.0      87.0   1.01
gamma     0.179  0.034   0.125    0.234  ...    58.0      64.0      60.0   1.01
Is_begin  0.614  0.627   0.002    1.743  ...   120.0     152.0      93.0   0.99
Ia_begin  1.119  1.011   0.013    3.238  ...    57.0      47.0     100.0   1.04
E_begin   0.553  0.567   0.009    1.642  ...   111.0      92.0      80.0   1.03

[8 rows x 11 columns]