1 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2495.07    35.41
p_loo       44.21        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      576   90.9%
 (0.5, 0.7]   (ok)         49    7.7%
   (0.7, 1]   (bad)         9    1.4%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.182  0.021   0.153    0.224  ...    64.0      55.0      44.0   1.01
pu        0.713  0.009   0.702    0.730  ...    21.0      34.0      60.0   1.07
mu        0.115  0.015   0.089    0.138  ...     9.0      10.0      38.0   1.17
mus       0.174  0.032   0.121    0.254  ...    89.0     102.0      81.0   1.00
gamma     0.262  0.036   0.198    0.328  ...   148.0     152.0      77.0   1.01
Is_begin  0.814  0.511   0.031    1.738  ...    72.0      66.0      56.0   1.02
Ia_begin  1.804  1.355   0.042    4.237  ...    60.0      52.0      44.0   1.03
E_begin   1.608  1.536   0.011    4.059  ...    38.0      37.0      59.0   1.07

[8 rows x 11 columns]