0 Divergences 
Passed validation 
2021-12-16 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 643 log-likelihood matrix

         Estimate       SE
elpd_loo  -503.31    50.77
p_loo       76.31        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      585   91.0%
 (0.5, 0.7]   (ok)         42    6.5%
   (0.7, 1]   (bad)        12    1.9%
   (1, Inf)   (very bad)    4    0.6%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.243  0.057   0.153    0.329  ...   109.0     104.0      56.0   1.04
pu        0.733  0.025   0.700    0.784  ...    54.0      55.0      91.0   1.10
mu        0.161  0.028   0.120    0.214  ...    29.0      22.0      95.0   1.07
mus       0.294  0.046   0.211    0.379  ...    13.0      11.0      61.0   1.14
gamma     0.316  0.053   0.229    0.423  ...    13.0      11.0      42.0   1.16
Is_begin  0.687  0.637   0.019    1.805  ...   137.0      63.0      59.0   1.01
Ia_begin  0.737  0.633   0.017    1.824  ...   126.0     101.0      59.0   1.02
E_begin   0.378  0.418   0.010    1.140  ...   105.0      86.0      96.0   0.99

[8 rows x 11 columns]