0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1342.72    39.16
p_loo       42.27        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      596   94.0%
 (0.5, 0.7]   (ok)         31    4.9%
   (0.7, 1]   (bad)         6    0.9%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.190  0.033   0.150    0.257  ...    66.0      95.0      74.0   1.02
pu        0.713  0.011   0.700    0.734  ...    64.0      42.0      57.0   1.03
mu        0.126  0.018   0.099    0.161  ...    30.0      36.0      48.0   1.02
mus       0.170  0.029   0.121    0.222  ...    97.0      99.0      61.0   0.99
gamma     0.237  0.047   0.163    0.312  ...   152.0     146.0      70.0   1.00
Is_begin  0.275  0.268   0.007    0.863  ...    75.0      70.0      60.0   1.02
Ia_begin  0.406  0.417   0.005    1.292  ...    86.0      65.0      53.0   1.00
E_begin   0.210  0.356   0.001    0.745  ...    98.0      68.0      49.0   1.02

[8 rows x 11 columns]