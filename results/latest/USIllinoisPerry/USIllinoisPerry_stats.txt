0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1923.27    47.10
p_loo       39.82        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      590   93.1%
 (0.5, 0.7]   (ok)         37    5.8%
   (0.7, 1]   (bad)         6    0.9%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.162  0.010   0.150    0.184  ...   109.0      84.0      41.0   1.02
pu        0.703  0.002   0.700    0.707  ...    69.0      54.0      59.0   1.02
mu        0.107  0.013   0.087    0.135  ...    21.0      24.0      43.0   1.10
mus       0.177  0.027   0.127    0.219  ...    63.0      66.0      22.0   1.01
gamma     0.288  0.044   0.207    0.361  ...    50.0      53.0      60.0   0.99
Is_begin  0.502  0.576   0.002    1.618  ...    20.0      33.0      15.0   1.11
Ia_begin  0.794  1.155   0.062    2.464  ...    41.0      21.0      46.0   1.07
E_begin   0.278  0.333   0.001    1.089  ...    30.0      38.0      43.0   1.08

[8 rows x 11 columns]