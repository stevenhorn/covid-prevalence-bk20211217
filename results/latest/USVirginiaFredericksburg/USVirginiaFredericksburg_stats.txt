0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -1748.53    29.17
p_loo       34.64        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      590   93.4%
 (0.5, 0.7]   (ok)         35    5.5%
   (0.7, 1]   (bad)         5    0.8%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.256  0.051   0.171    0.347  ...    74.0      73.0      33.0   1.11
pu        0.768  0.025   0.729    0.811  ...     8.0       8.0      54.0   1.23
mu        0.149  0.020   0.116    0.187  ...    13.0      13.0      60.0   1.11
mus       0.171  0.030   0.126    0.233  ...    69.0      88.0      55.0   1.11
gamma     0.225  0.036   0.170    0.281  ...    50.0      50.0      92.0   1.01
Is_begin  0.647  0.641   0.022    1.724  ...    80.0      63.0      54.0   1.01
Ia_begin  1.412  1.132   0.047    3.657  ...    75.0      74.0      59.0   0.99
E_begin   0.627  0.713   0.005    1.949  ...    63.0      37.0      69.0   1.02

[8 rows x 11 columns]