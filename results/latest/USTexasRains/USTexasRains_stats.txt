0 Divergences 
Failed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -1694.30    65.03
p_loo       57.21        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      599   94.8%
 (0.5, 0.7]   (ok)         21    3.3%
   (0.7, 1]   (bad)         9    1.4%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.219  0.044   0.151    0.289  ...    28.0      31.0      59.0   1.11
pu        0.725  0.016   0.702    0.756  ...    88.0      78.0      60.0   1.00
mu        0.128  0.017   0.094    0.155  ...    61.0      63.0      74.0   1.00
mus       0.189  0.034   0.137    0.249  ...    26.0      28.0      24.0   1.01
gamma     0.274  0.040   0.204    0.346  ...    15.0      15.0      80.0   1.12
Is_begin  0.473  0.495   0.000    1.554  ...    26.0      12.0      20.0   1.13
Ia_begin  0.814  0.763   0.015    1.973  ...    30.0      33.0      40.0   1.06
E_begin   0.337  0.297   0.008    1.018  ...    54.0      44.0      59.0   1.05

[8 rows x 11 columns]