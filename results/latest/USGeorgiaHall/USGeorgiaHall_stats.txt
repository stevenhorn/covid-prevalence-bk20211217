0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -3105.54    46.46
p_loo       47.64        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      581   91.6%
 (0.5, 0.7]   (ok)         46    7.3%
   (0.7, 1]   (bad)         5    0.8%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.157  0.008   0.150    0.171  ...    59.0      62.0      59.0   1.02
pu        0.703  0.003   0.700    0.707  ...    80.0      85.0      43.0   1.02
mu        0.069  0.011   0.048    0.091  ...    15.0      15.0      40.0   1.11
mus       0.174  0.032   0.122    0.224  ...    68.0      77.0      38.0   1.01
gamma     0.264  0.038   0.197    0.322  ...    66.0      66.0      55.0   1.03
Is_begin  1.030  1.196   0.017    3.426  ...    74.0      54.0      59.0   1.04
Ia_begin  2.124  2.307   0.041    6.544  ...    14.0      19.0      22.0   1.10
E_begin   1.734  2.317   0.028    5.611  ...    66.0      17.0      14.0   1.10

[8 rows x 11 columns]