0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1601.60    35.32
p_loo       39.93        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      581   91.6%
 (0.5, 0.7]   (ok)         39    6.2%
   (0.7, 1]   (bad)        12    1.9%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.180  0.024   0.151    0.229  ...   129.0      98.0      56.0   1.00
pu        0.710  0.008   0.701    0.725  ...   124.0     118.0      58.0   0.99
mu        0.136  0.020   0.101    0.169  ...    24.0      22.0      93.0   1.08
mus       0.191  0.031   0.129    0.247  ...   152.0     152.0      69.0   1.00
gamma     0.311  0.052   0.226    0.399  ...   152.0     152.0      84.0   1.04
Is_begin  0.409  0.401   0.010    1.293  ...   105.0      84.0      58.0   1.03
Ia_begin  0.668  0.714   0.019    2.115  ...    47.0      45.0      74.0   1.04
E_begin   0.333  0.445   0.000    0.931  ...    36.0      30.0      61.0   1.09

[8 rows x 11 columns]