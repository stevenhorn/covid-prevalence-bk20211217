0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo  -825.70    59.89
p_loo       64.84        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      576   90.9%
 (0.5, 0.7]   (ok)         37    5.8%
   (0.7, 1]   (bad)        17    2.7%
   (1, Inf)   (very bad)    4    0.6%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.234  0.052   0.156    0.334  ...    46.0      39.0      22.0   1.13
pu        0.741  0.025   0.703    0.779  ...    51.0      52.0      61.0   1.02
mu        0.137  0.027   0.092    0.178  ...    22.0      19.0      50.0   1.10
mus       0.187  0.036   0.137    0.266  ...    82.0      97.0      69.0   1.00
gamma     0.219  0.046   0.154    0.295  ...    76.0      58.0      40.0   1.05
Is_begin  0.361  0.411   0.001    1.263  ...    58.0      35.0      43.0   1.09
Ia_begin  0.595  0.797   0.005    2.247  ...    37.0      47.0      60.0   1.06
E_begin   0.276  0.308   0.001    0.915  ...    43.0      46.0      40.0   1.08

[8 rows x 11 columns]