0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -2206.17    62.57
p_loo       62.84        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      584   92.4%
 (0.5, 0.7]   (ok)         38    6.0%
   (0.7, 1]   (bad)         6    0.9%
   (1, Inf)   (very bad)    4    0.6%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.226  0.047   0.153    0.298  ...    36.0      39.0      60.0   1.07
pu        0.727  0.019   0.702    0.757  ...    20.0      31.0      45.0   1.07
mu        0.139  0.015   0.111    0.163  ...     7.0       7.0      59.0   1.26
mus       0.220  0.035   0.147    0.277  ...    64.0      65.0      60.0   1.00
gamma     0.330  0.063   0.232    0.444  ...    11.0      12.0      45.0   1.12
Is_begin  0.738  0.796   0.004    2.384  ...   117.0      67.0      43.0   1.02
Ia_begin  1.522  1.328   0.011    3.049  ...    89.0      54.0      60.0   1.00
E_begin   0.606  0.462   0.007    1.408  ...    57.0      40.0      36.0   1.07

[8 rows x 11 columns]