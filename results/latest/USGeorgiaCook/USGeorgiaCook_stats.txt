0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1722.46    56.04
p_loo       47.28        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      597   94.2%
 (0.5, 0.7]   (ok)         29    4.6%
   (0.7, 1]   (bad)         7    1.1%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.156  0.005   0.150    0.164  ...   143.0     108.0      60.0   1.02
pu        0.702  0.002   0.700    0.707  ...   152.0     144.0      46.0   0.98
mu        0.089  0.011   0.070    0.109  ...    39.0      38.0      39.0   1.02
mus       0.173  0.024   0.138    0.224  ...   105.0     101.0      59.0   1.01
gamma     0.346  0.045   0.260    0.439  ...    68.0      65.0      40.0   1.07
Is_begin  0.494  0.577   0.002    1.569  ...    68.0      28.0      93.0   1.07
Ia_begin  0.785  0.994   0.025    3.380  ...    74.0      35.0      80.0   1.05
E_begin   0.322  0.449   0.002    1.141  ...    53.0      39.0      14.0   1.02

[8 rows x 11 columns]