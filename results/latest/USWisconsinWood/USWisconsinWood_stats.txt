0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -2403.96    41.67
p_loo       35.46        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      600   94.9%
 (0.5, 0.7]   (ok)         23    3.6%
   (0.7, 1]   (bad)         8    1.3%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.171  0.017   0.152    0.205  ...    64.0      34.0     100.0   1.04
pu        0.706  0.006   0.700    0.719  ...    53.0      81.0      31.0   1.01
mu        0.110  0.016   0.084    0.139  ...    14.0      19.0      57.0   1.10
mus       0.161  0.030   0.118    0.221  ...    53.0      37.0      48.0   1.03
gamma     0.224  0.043   0.146    0.289  ...    39.0      38.0      43.0   1.06
Is_begin  0.621  0.526   0.013    1.488  ...    63.0      63.0      43.0   1.01
Ia_begin  0.938  0.843   0.014    2.876  ...    23.0      16.0      38.0   1.11
E_begin   0.467  0.506   0.008    1.299  ...   100.0      76.0      40.0   1.01

[8 rows x 11 columns]