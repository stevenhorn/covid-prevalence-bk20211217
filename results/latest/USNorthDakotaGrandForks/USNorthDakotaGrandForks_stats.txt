0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2459.57    73.10
p_loo       55.37        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      578   91.2%
 (0.5, 0.7]   (ok)         49    7.7%
   (0.7, 1]   (bad)         5    0.8%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.156  0.006   0.150    0.168  ...   128.0      67.0      36.0   1.00
pu        0.702  0.002   0.700    0.708  ...    84.0     108.0      93.0   1.01
mu        0.100  0.011   0.083    0.118  ...    47.0      49.0      41.0   1.01
mus       0.215  0.032   0.172    0.279  ...   141.0     136.0      84.0   1.08
gamma     0.407  0.050   0.324    0.491  ...    95.0     100.0      95.0   1.07
Is_begin  0.734  0.731   0.024    2.088  ...   104.0     100.0      60.0   0.99
Ia_begin  1.255  1.398   0.053    4.134  ...   104.0     125.0      44.0   1.00
E_begin   0.620  0.778   0.004    2.058  ...    67.0      89.0      59.0   1.04

[8 rows x 11 columns]