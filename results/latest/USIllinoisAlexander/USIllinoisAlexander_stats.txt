0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1058.58    29.41
p_loo       35.50        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      592   93.4%
 (0.5, 0.7]   (ok)         38    6.0%
   (0.7, 1]   (bad)         4    0.6%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.239  0.048   0.170    0.317  ...    22.0      24.0      31.0   1.06
pu        0.744  0.019   0.707    0.769  ...    22.0      21.0      59.0   1.07
mu        0.131  0.024   0.091    0.169  ...    25.0      26.0      60.0   1.05
mus       0.178  0.033   0.125    0.233  ...   129.0     118.0      93.0   0.99
gamma     0.218  0.040   0.157    0.308  ...    44.0      42.0      60.0   1.02
Is_begin  0.522  0.570   0.006    1.438  ...    87.0      53.0      43.0   1.01
Ia_begin  0.946  1.134   0.026    3.213  ...    90.0      42.0      55.0   1.03
E_begin   0.404  0.402   0.014    1.191  ...    38.0      36.0      52.0   1.03

[8 rows x 11 columns]