0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1387.22    46.13
p_loo       47.99        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      581   91.6%
 (0.5, 0.7]   (ok)         43    6.8%
   (0.7, 1]   (bad)         9    1.4%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.233  0.048   0.164    0.320  ...    63.0     152.0      74.0   1.06
pu        0.730  0.018   0.700    0.757  ...    64.0      71.0      59.0   1.02
mu        0.125  0.017   0.098    0.155  ...    35.0      35.0      93.0   1.05
mus       0.209  0.032   0.162    0.272  ...   107.0      95.0      78.0   1.02
gamma     0.347  0.047   0.277    0.435  ...   115.0     115.0      96.0   0.99
Is_begin  0.374  0.362   0.022    0.987  ...    52.0      40.0      60.0   1.00
Ia_begin  0.666  0.779   0.000    2.520  ...    80.0      63.0      24.0   1.13
E_begin   0.280  0.374   0.003    1.159  ...    77.0      47.0      60.0   1.01

[8 rows x 11 columns]