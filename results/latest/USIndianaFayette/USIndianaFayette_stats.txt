0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1772.41    44.98
p_loo       41.21        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      590   93.1%
 (0.5, 0.7]   (ok)         33    5.2%
   (0.7, 1]   (bad)         8    1.3%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.156  0.005   0.150    0.166  ...   108.0      66.0      54.0   1.07
pu        0.702  0.002   0.700    0.708  ...    81.0      83.0      29.0   1.06
mu        0.078  0.011   0.060    0.102  ...    69.0      72.0     100.0   1.03
mus       0.163  0.026   0.116    0.210  ...   152.0     152.0      88.0   0.99
gamma     0.375  0.043   0.305    0.454  ...    17.0      16.0      40.0   1.11
Is_begin  0.631  0.502   0.001    1.389  ...    92.0      73.0      43.0   0.99
Ia_begin  0.982  1.248   0.000    4.191  ...    46.0      30.0      14.0   1.07
E_begin   0.328  0.383   0.006    1.019  ...    85.0      73.0      40.0   1.02

[8 rows x 11 columns]