0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1283.63    39.36
p_loo       45.66        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      600   94.6%
 (0.5, 0.7]   (ok)         26    4.1%
   (0.7, 1]   (bad)         8    1.3%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.187  0.032   0.150    0.247  ...    80.0      75.0      58.0   1.02
pu        0.711  0.008   0.700    0.727  ...    78.0      73.0      60.0   1.03
mu        0.141  0.021   0.097    0.174  ...    40.0      36.0      45.0   1.04
mus       0.217  0.038   0.146    0.291  ...   152.0     152.0      73.0   1.00
gamma     0.292  0.036   0.226    0.344  ...    83.0      81.0      99.0   0.99
Is_begin  0.379  0.431   0.008    1.012  ...   108.0      79.0      40.0   1.01
Ia_begin  0.614  0.897   0.002    1.866  ...    97.0      85.0      54.0   1.01
E_begin   0.326  0.676   0.000    1.075  ...    92.0      95.0      45.0   1.02

[8 rows x 11 columns]