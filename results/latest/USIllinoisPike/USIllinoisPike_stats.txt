0 Divergences 
Passed validation 
2021-12-09 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 635 log-likelihood matrix

         Estimate       SE
elpd_loo -1609.61    34.47
p_loo       33.47        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      593   93.4%
 (0.5, 0.7]   (ok)         35    5.5%
   (0.7, 1]   (bad)         7    1.1%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.159  0.009   0.150    0.172  ...   152.0     152.0      59.0   0.99
pu        0.702  0.002   0.700    0.708  ...    88.0      58.0      60.0   1.00
mu        0.079  0.013   0.054    0.104  ...    32.0      25.0      16.0   1.14
mus       0.153  0.026   0.107    0.189  ...   152.0     152.0      93.0   1.03
gamma     0.271  0.046   0.199    0.356  ...   152.0     152.0     100.0   1.01
Is_begin  0.395  0.490   0.001    1.375  ...   100.0      75.0      96.0   1.01
Ia_begin  0.481  0.588   0.003    2.006  ...    68.0      75.0      60.0   1.02
E_begin   0.192  0.179   0.009    0.547  ...    74.0      45.0      44.0   1.06

[8 rows x 11 columns]