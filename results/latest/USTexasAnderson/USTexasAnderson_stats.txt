0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -2974.32    86.96
p_loo       90.12        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      589   93.2%
 (0.5, 0.7]   (ok)         29    4.6%
   (0.7, 1]   (bad)         8    1.3%
   (1, Inf)   (very bad)    6    0.9%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.163  0.011   0.150    0.187  ...   152.0     107.0      43.0   1.00
pu        0.704  0.005   0.700    0.715  ...   152.0      71.0      40.0   1.03
mu        0.136  0.012   0.113    0.155  ...    14.0      14.0      60.0   1.11
mus       0.294  0.033   0.233    0.353  ...    45.0      45.0      57.0   1.00
gamma     0.724  0.103   0.562    0.937  ...    60.0      62.0      93.0   1.02
Is_begin  0.745  0.692   0.025    2.309  ...    87.0      61.0      88.0   0.99
Ia_begin  0.977  1.326   0.004    2.894  ...    32.0      16.0      24.0   1.12
E_begin   0.548  0.516   0.012    1.648  ...    90.0      73.0      95.0   1.01

[8 rows x 11 columns]