4 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 633 log-likelihood matrix

         Estimate       SE
elpd_loo -4890.43    39.25
p_loo       51.89        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      568   89.7%
 (0.5, 0.7]   (ok)         55    8.7%
   (0.7, 1]   (bad)        10    1.6%
   (1, Inf)   (very bad)    0    0.0%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.241   0.048   0.165    0.309  ...     5.0       6.0      32.0   1.31
pu         0.833   0.012   0.812    0.849  ...     4.0       4.0      38.0   1.55
mu         0.138   0.015   0.118    0.171  ...     4.0       4.0      53.0   1.50
mus        0.159   0.021   0.116    0.192  ...     5.0       5.0      33.0   1.48
gamma      0.327   0.027   0.274    0.369  ...    44.0      43.0      40.0   1.05
Is_begin   7.810   6.567   0.035   18.439  ...     4.0       4.0      24.0   1.75
Ia_begin  28.362   8.534  15.672   45.385  ...    17.0      19.0      47.0   1.10
E_begin   61.449  30.813  11.989  114.959  ...    15.0      15.0      15.0   1.11

[8 rows x 11 columns]