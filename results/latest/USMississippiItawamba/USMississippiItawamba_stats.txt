0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1867.57    35.58
p_loo       34.55        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      595   93.8%
 (0.5, 0.7]   (ok)         33    5.2%
   (0.7, 1]   (bad)         6    0.9%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.156  0.006   0.150    0.165  ...   115.0     105.0      43.0   0.98
pu        0.702  0.002   0.700    0.706  ...   127.0     116.0      91.0   0.98
mu        0.077  0.014   0.051    0.100  ...    39.0      40.0      40.0   1.05
mus       0.151  0.027   0.111    0.208  ...   150.0     152.0      74.0   1.02
gamma     0.260  0.038   0.192    0.326  ...    78.0      85.0      88.0   1.06
Is_begin  0.525  0.494   0.004    1.454  ...   125.0      94.0      30.0   0.99
Ia_begin  0.740  1.018   0.008    3.019  ...    81.0      83.0      60.0   1.00
E_begin   0.339  0.481   0.001    1.007  ...    59.0      79.0      49.0   1.01

[8 rows x 11 columns]