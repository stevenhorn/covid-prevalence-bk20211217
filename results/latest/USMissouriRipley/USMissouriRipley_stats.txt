0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1678.69    80.62
p_loo       74.15        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      585   92.3%
 (0.5, 0.7]   (ok)         37    5.8%
   (0.7, 1]   (bad)         9    1.4%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.166  0.016   0.150    0.197  ...    90.0      73.0      34.0   1.03
pu        0.705  0.005   0.700    0.714  ...   105.0      95.0      88.0   0.99
mu        0.115  0.012   0.093    0.134  ...     7.0       8.0      86.0   1.22
mus       0.309  0.032   0.254    0.366  ...    84.0      92.0      49.0   1.01
gamma     0.499  0.085   0.347    0.645  ...    91.0     142.0      91.0   1.01
Is_begin  0.879  0.848   0.000    2.179  ...    88.0      73.0      60.0   1.01
Ia_begin  1.200  1.352   0.014    3.281  ...   100.0     115.0      58.0   1.00
E_begin   0.457  0.539   0.007    1.515  ...   105.0      74.0      56.0   1.02

[8 rows x 11 columns]