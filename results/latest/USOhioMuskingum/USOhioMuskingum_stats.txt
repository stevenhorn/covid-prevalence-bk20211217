2 Divergences 
Passed validation 
2021-12-09 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 635 log-likelihood matrix

         Estimate       SE
elpd_loo -2285.83    42.93
p_loo       49.37        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      563   88.7%
 (0.5, 0.7]   (ok)         53    8.3%
   (0.7, 1]   (bad)        18    2.8%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.155  0.005   0.150    0.164  ...    78.0      73.0      57.0   1.03
pu        0.702  0.003   0.700    0.708  ...   111.0     100.0      31.0   1.01
mu        0.071  0.019   0.046    0.109  ...     7.0       8.0      14.0   1.23
mus       0.168  0.031   0.109    0.207  ...   110.0     150.0      83.0   1.00
gamma     0.207  0.057   0.063    0.276  ...     6.0       8.0      14.0   1.23
Is_begin  0.625  0.667   0.001    1.883  ...   118.0     135.0      60.0   0.99
Ia_begin  0.876  0.828   0.003    2.320  ...    37.0      45.0      84.0   1.05
E_begin   0.377  0.412   0.002    1.300  ...    77.0      80.0      80.0   1.01

[8 rows x 11 columns]