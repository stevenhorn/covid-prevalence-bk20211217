0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1359.72    42.16
p_loo       44.11        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      582   91.8%
 (0.5, 0.7]   (ok)         40    6.3%
   (0.7, 1]   (bad)        11    1.7%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.175  0.021   0.150    0.219  ...    98.0     101.0      30.0   1.03
pu        0.708  0.006   0.700    0.719  ...    79.0      59.0      30.0   1.02
mu        0.118  0.020   0.088    0.159  ...    16.0      23.0      21.0   1.10
mus       0.196  0.036   0.135    0.269  ...   132.0     152.0      42.0   1.04
gamma     0.269  0.047   0.192    0.359  ...    43.0      50.0      33.0   1.03
Is_begin  0.336  0.433   0.005    0.971  ...   101.0      63.0      60.0   1.02
Ia_begin  0.517  0.636   0.007    1.670  ...    71.0      77.0      58.0   0.99
E_begin   0.247  0.222   0.010    0.745  ...    66.0      60.0      43.0   0.98

[8 rows x 11 columns]