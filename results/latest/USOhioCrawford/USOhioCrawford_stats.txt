0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1743.45    37.40
p_loo       33.73        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      591   93.2%
 (0.5, 0.7]   (ok)         39    6.2%
   (0.7, 1]   (bad)         4    0.6%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.158  0.009   0.150    0.178  ...   123.0      96.0      57.0   0.99
pu        0.703  0.002   0.700    0.707  ...    89.0      80.0      87.0   1.01
mu        0.077  0.012   0.059    0.103  ...    42.0      39.0      40.0   1.04
mus       0.150  0.022   0.104    0.190  ...   124.0     131.0      97.0   1.02
gamma     0.255  0.040   0.191    0.332  ...   152.0     137.0      79.0   1.01
Is_begin  0.551  0.614   0.001    1.892  ...    87.0      44.0      96.0   1.03
Ia_begin  1.037  1.067   0.000    3.152  ...    94.0      59.0      17.0   1.02
E_begin   0.473  0.395   0.019    1.166  ...    54.0      35.0      17.0   1.04

[8 rows x 11 columns]