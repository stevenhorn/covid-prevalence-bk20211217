0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2008.84    68.88
p_loo       52.90        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      584   92.1%
 (0.5, 0.7]   (ok)         34    5.4%
   (0.7, 1]   (bad)        14    2.2%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.168  0.016   0.150    0.194  ...   123.0     152.0      60.0   1.02
pu        0.705  0.006   0.700    0.718  ...    46.0      22.0      20.0   1.07
mu        0.107  0.016   0.081    0.136  ...    11.0      10.0      25.0   1.18
mus       0.184  0.029   0.140    0.243  ...   152.0     152.0     100.0   1.09
gamma     0.278  0.047   0.203    0.361  ...   152.0     152.0      57.0   1.00
Is_begin  0.858  0.917   0.007    2.712  ...   125.0     152.0      22.0   1.01
Ia_begin  1.557  1.551   0.054    4.635  ...   132.0     111.0      59.0   1.00
E_begin   0.612  0.751   0.016    2.173  ...   108.0      91.0      61.0   1.02

[8 rows x 11 columns]