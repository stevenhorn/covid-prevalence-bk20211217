0 Divergences 
Passed validation 
2021-11-21 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 617 log-likelihood matrix

         Estimate       SE
elpd_loo -3619.85   112.40
p_loo      110.91        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      562   91.1%
 (0.5, 0.7]   (ok)         43    7.0%
   (0.7, 1]   (bad)        10    1.6%
   (1, Inf)   (very bad)    2    0.3%

              mean        sd   hdi_3%  ...  ess_bulk  ess_tail  r_hat
pa           0.243     0.066    0.157  ...      64.0      60.0   1.01
pu           0.778     0.049    0.708  ...       6.0      14.0   1.32
mu           0.117     0.022    0.089  ...       7.0      45.0   1.25
mus          0.522     0.149    0.305  ...      15.0      38.0   1.10
gamma        0.678     0.175    0.395  ...      15.0      65.0   1.09
Is_begin  1135.093   680.822  140.091  ...      59.0      59.0   1.00
Ia_begin  1034.099  1042.583   10.817  ...      11.0      27.0   1.16
E_begin   1015.726  1230.015   11.403  ...      20.0      33.0   1.08

[8 rows x 11 columns]