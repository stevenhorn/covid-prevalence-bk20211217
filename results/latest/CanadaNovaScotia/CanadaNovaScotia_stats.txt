0 Divergences 
Passed validation 
2021-12-15 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 642 log-likelihood matrix

         Estimate       SE
elpd_loo -2062.69    44.02
p_loo       53.40        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      595   92.7%
 (0.5, 0.7]   (ok)         34    5.3%
   (0.7, 1]   (bad)         8    1.2%
   (1, Inf)   (very bad)    5    0.8%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.269   0.052   0.176    0.343  ...    63.0      60.0      43.0   1.05
pu         0.887   0.013   0.860    0.900  ...    71.0      71.0      39.0   1.01
mu         0.195   0.015   0.174    0.227  ...    12.0      12.0      40.0   1.14
mus        0.234   0.034   0.181    0.299  ...   152.0     152.0      93.0   0.98
gamma      0.480   0.059   0.390    0.610  ...    74.0      74.0      59.0   1.04
Is_begin   7.323   6.021   0.154   16.528  ...    94.0      61.0      41.0   1.02
Ia_begin  45.719  25.129   5.495   95.630  ...    76.0      68.0      40.0   1.01
E_begin   33.472  20.363   1.492   71.494  ...    66.0      80.0      60.0   1.05

[8 rows x 11 columns]