0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1850.10    34.10
p_loo       45.45        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      544   85.8%
 (0.5, 0.7]   (ok)         84   13.2%
   (0.7, 1]   (bad)         5    0.8%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.230  0.045   0.153    0.297  ...    97.0      95.0      60.0   1.01
pu        0.731  0.019   0.704    0.770  ...    60.0      62.0      60.0   1.01
mu        0.151  0.019   0.114    0.181  ...    29.0      35.0      60.0   1.05
mus       0.201  0.035   0.127    0.266  ...    48.0      48.0      34.0   1.01
gamma     0.266  0.044   0.192    0.337  ...   120.0     126.0      93.0   1.00
Is_begin  1.325  0.901   0.029    3.069  ...    78.0      56.0      32.0   1.01
Ia_begin  0.708  0.722   0.004    1.923  ...    44.0      31.0      32.0   1.05
E_begin   0.830  0.672   0.021    2.188  ...    87.0      80.0     100.0   0.99

[8 rows x 11 columns]