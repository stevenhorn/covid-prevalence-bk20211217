0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1839.72    43.51
p_loo       44.48        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      594   93.7%
 (0.5, 0.7]   (ok)         34    5.4%
   (0.7, 1]   (bad)         5    0.8%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.164  0.012   0.151    0.179  ...    88.0      60.0      57.0   1.06
pu        0.705  0.005   0.700    0.716  ...    49.0      86.0      60.0   1.07
mu        0.112  0.019   0.085    0.160  ...     8.0       9.0      46.0   1.20
mus       0.178  0.032   0.135    0.236  ...    51.0      47.0      54.0   1.05
gamma     0.255  0.038   0.185    0.323  ...    48.0      59.0      59.0   1.03
Is_begin  0.531  0.578   0.004    1.754  ...    87.0      58.0      19.0   1.01
Ia_begin  0.982  1.162   0.004    2.933  ...    82.0      61.0      60.0   0.99
E_begin   0.476  0.721   0.007    2.162  ...    76.0      66.0      38.0   1.03

[8 rows x 11 columns]