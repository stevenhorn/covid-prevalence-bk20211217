0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2847.16   104.10
p_loo       68.33        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      597   94.2%
 (0.5, 0.7]   (ok)         32    5.0%
   (0.7, 1]   (bad)         2    0.3%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.158  0.008   0.150    0.173  ...   140.0      73.0      57.0   1.01
pu        0.702  0.003   0.700    0.708  ...    87.0      72.0      91.0   1.03
mu        0.117  0.010   0.096    0.135  ...     9.0       9.0      40.0   1.22
mus       0.297  0.037   0.230    0.372  ...    38.0      54.0      31.0   1.02
gamma     0.776  0.092   0.606    0.929  ...    79.0      78.0      97.0   1.03
Is_begin  0.532  0.597   0.002    1.658  ...    69.0      50.0      40.0   0.99
Ia_begin  0.438  0.540   0.010    1.495  ...    85.0      40.0      59.0   1.03
E_begin   0.201  0.244   0.001    0.639  ...    47.0      35.0      15.0   1.00

[8 rows x 11 columns]