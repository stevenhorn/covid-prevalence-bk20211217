0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -3632.09    46.72
p_loo       42.29        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      577   91.3%
 (0.5, 0.7]   (ok)         49    7.8%
   (0.7, 1]   (bad)         3    0.5%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.205  0.031   0.155    0.260  ...    61.0      63.0      96.0   1.03
pu        0.717  0.010   0.700    0.736  ...    57.0      49.0      40.0   1.05
mu        0.140  0.018   0.112    0.175  ...    13.0      15.0      14.0   1.08
mus       0.192  0.033   0.129    0.245  ...   122.0     135.0      52.0   1.11
gamma     0.233  0.046   0.149    0.319  ...    28.0      38.0      46.0   1.06
Is_begin  1.974  1.616   0.093    4.603  ...    74.0      59.0      74.0   1.02
Ia_begin  2.793  2.917   0.070    8.693  ...    87.0      66.0      60.0   1.03
E_begin   1.749  2.108   0.042    5.694  ...    87.0      23.0      65.0   1.08

[8 rows x 11 columns]