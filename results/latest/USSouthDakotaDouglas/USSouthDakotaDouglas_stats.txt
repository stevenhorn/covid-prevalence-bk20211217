0 Divergences 
Failed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo  -923.10    37.70
p_loo       50.65        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      591   93.5%
 (0.5, 0.7]   (ok)         37    5.9%
   (0.7, 1]   (bad)         4    0.6%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.218  0.045   0.153    0.312  ...    69.0      85.0      40.0   1.06
pu        0.722  0.015   0.700    0.748  ...    73.0      64.0      56.0   1.01
mu        0.127  0.026   0.085    0.178  ...    66.0      64.0      75.0   1.00
mus       0.216  0.044   0.148    0.295  ...    86.0      81.0      75.0   1.00
gamma     0.253  0.051   0.164    0.349  ...    81.0      75.0      57.0   1.01
Is_begin  0.427  0.393   0.007    1.043  ...   125.0     152.0      91.0   0.99
Ia_begin  0.562  0.598   0.003    1.828  ...    71.0      75.0      43.0   1.05
E_begin   0.269  0.314   0.007    0.803  ...    69.0      73.0      59.0   1.00

[8 rows x 11 columns]