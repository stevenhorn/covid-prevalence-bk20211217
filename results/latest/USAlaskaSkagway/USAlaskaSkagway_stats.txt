0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo  -323.39    68.01
p_loo       59.78        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      584   92.1%
 (0.5, 0.7]   (ok)         32    5.0%
   (0.7, 1]   (bad)        14    2.2%
   (1, Inf)   (very bad)    4    0.6%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.170  0.014   0.150    0.195  ...    55.0      38.0      20.0   1.06
pu        0.708  0.006   0.700    0.722  ...    71.0      78.0      57.0   0.99
mu        0.129  0.024   0.081    0.172  ...    56.0      57.0      38.0   1.01
mus       0.345  0.046   0.263    0.433  ...   152.0     152.0      66.0   1.00
gamma     0.374  0.048   0.279    0.463  ...   106.0      91.0      84.0   1.01
Is_begin  0.347  0.347   0.004    1.123  ...   103.0     152.0      93.0   0.99
Ia_begin  0.343  0.404   0.008    1.119  ...    86.0     107.0      74.0   1.00
E_begin   0.176  0.206   0.005    0.495  ...   102.0      81.0     102.0   1.00

[8 rows x 11 columns]