0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -3097.77    40.56
p_loo       44.62        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      604   95.3%
 (0.5, 0.7]   (ok)         22    3.5%
   (0.7, 1]   (bad)         5    0.8%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.177  0.023   0.150    0.223  ...   152.0     121.0      79.0   0.99
pu        0.709  0.007   0.701    0.723  ...   113.0     105.0      87.0   0.99
mu        0.118  0.015   0.099    0.150  ...    29.0      31.0      40.0   1.03
mus       0.156  0.020   0.124    0.190  ...    72.0      78.0      88.0   1.01
gamma     0.283  0.039   0.216    0.353  ...   128.0     145.0      61.0   1.01
Is_begin  1.119  0.868   0.158    2.687  ...    81.0      61.0      39.0   1.03
Ia_begin  2.513  2.307   0.003    6.582  ...    55.0      49.0      58.0   1.06
E_begin   1.426  1.296   0.008    3.706  ...    81.0      68.0      88.0   1.02

[8 rows x 11 columns]