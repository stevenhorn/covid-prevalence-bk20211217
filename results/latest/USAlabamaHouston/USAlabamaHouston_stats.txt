0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2658.91    69.42
p_loo       60.67        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      601   94.8%
 (0.5, 0.7]   (ok)         23    3.6%
   (0.7, 1]   (bad)         6    0.9%
   (1, Inf)   (very bad)    4    0.6%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.177  0.022   0.150    0.223  ...    70.0      60.0      60.0   0.99
pu        0.709  0.007   0.700    0.723  ...    61.0      61.0      39.0   1.02
mu        0.088  0.018   0.061    0.122  ...     8.0       8.0      31.0   1.23
mus       0.183  0.033   0.122    0.231  ...   132.0     152.0      40.0   1.08
gamma     0.264  0.045   0.193    0.356  ...   117.0     109.0      97.0   0.99
Is_begin  0.625  0.593   0.018    1.981  ...   119.0      73.0      59.0   1.01
Ia_begin  1.003  1.076   0.023    3.110  ...    66.0      60.0     100.0   1.02
E_begin   0.455  0.458   0.004    1.211  ...    67.0      32.0      49.0   1.05

[8 rows x 11 columns]