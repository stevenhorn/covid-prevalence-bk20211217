0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2121.85   114.32
p_loo       76.57        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      579   91.3%
 (0.5, 0.7]   (ok)         44    6.9%
   (0.7, 1]   (bad)         9    1.4%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.157  0.006   0.150    0.169  ...   152.0     152.0      93.0   1.01
pu        0.702  0.002   0.700    0.707  ...    56.0      44.0      60.0   1.05
mu        0.097  0.012   0.077    0.118  ...    10.0      10.0      59.0   1.18
mus       0.296  0.040   0.229    0.360  ...   125.0     102.0      44.0   1.01
gamma     0.766  0.093   0.606    0.951  ...    37.0      31.0      61.0   1.05
Is_begin  0.854  0.955   0.001    2.956  ...   122.0      78.0      43.0   0.98
Ia_begin  1.335  1.289   0.036    4.233  ...    72.0      68.0      26.0   1.01
E_begin   0.460  0.517   0.005    1.182  ...    74.0      50.0      91.0   1.03

[8 rows x 11 columns]