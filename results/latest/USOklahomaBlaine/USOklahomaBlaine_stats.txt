0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1702.75    47.10
p_loo       50.84        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      606   95.6%
 (0.5, 0.7]   (ok)         20    3.2%
   (0.7, 1]   (bad)         6    0.9%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.163  0.011   0.151    0.184  ...    91.0      58.0      34.0   1.02
pu        0.705  0.005   0.700    0.713  ...   152.0     131.0      58.0   1.02
mu        0.095  0.016   0.064    0.121  ...    48.0      60.0      27.0   1.06
mus       0.170  0.028   0.126    0.217  ...   152.0     152.0      38.0   1.05
gamma     0.246  0.043   0.188    0.350  ...   152.0     152.0      49.0   1.00
Is_begin  0.301  0.414   0.003    1.024  ...    90.0      94.0      72.0   0.98
Ia_begin  0.393  0.434   0.005    1.172  ...    74.0      62.0     100.0   1.01
E_begin   0.180  0.222   0.003    0.639  ...    92.0      87.0     100.0   0.99

[8 rows x 11 columns]