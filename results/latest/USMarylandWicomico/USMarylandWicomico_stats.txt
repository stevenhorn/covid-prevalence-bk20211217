0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2159.34    49.11
p_loo       48.22        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      578   91.2%
 (0.5, 0.7]   (ok)         42    6.6%
   (0.7, 1]   (bad)        13    2.1%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.228  0.052   0.151    0.313  ...    84.0     105.0      59.0   1.02
pu        0.729  0.021   0.700    0.770  ...    55.0      47.0      43.0   1.05
mu        0.142  0.019   0.107    0.175  ...    11.0      11.0      33.0   1.15
mus       0.200  0.029   0.146    0.250  ...   105.0     103.0      73.0   0.99
gamma     0.296  0.043   0.231    0.368  ...   118.0     100.0      88.0   1.00
Is_begin  0.606  0.523   0.047    1.562  ...    67.0      66.0      51.0   1.02
Ia_begin  0.456  0.342   0.012    0.992  ...    27.0      46.0      43.0   1.03
E_begin   0.395  0.365   0.021    1.306  ...    79.0      68.0      45.0   1.04

[8 rows x 11 columns]