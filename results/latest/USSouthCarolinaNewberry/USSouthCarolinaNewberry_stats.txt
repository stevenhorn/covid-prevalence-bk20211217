0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -2291.23    41.02
p_loo       47.48        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      584   92.4%
 (0.5, 0.7]   (ok)         37    5.9%
   (0.7, 1]   (bad)         8    1.3%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.169  0.015   0.151    0.193  ...   143.0     152.0     100.0   1.04
pu        0.706  0.005   0.700    0.716  ...   146.0     122.0      91.0   1.00
mu        0.114  0.019   0.088    0.156  ...    30.0      26.0      58.0   1.02
mus       0.174  0.026   0.134    0.221  ...    84.0     109.0      58.0   0.98
gamma     0.255  0.035   0.186    0.313  ...    77.0      96.0      46.0   1.02
Is_begin  0.610  0.589   0.008    1.879  ...    52.0      86.0      39.0   1.01
Ia_begin  1.252  1.319   0.007    3.820  ...    70.0      82.0      59.0   1.00
E_begin   0.537  0.756   0.000    2.183  ...    73.0      53.0      37.0   1.04

[8 rows x 11 columns]