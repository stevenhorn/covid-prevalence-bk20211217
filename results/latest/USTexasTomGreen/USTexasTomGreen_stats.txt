0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -2670.14    44.28
p_loo       35.64        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      591   93.5%
 (0.5, 0.7]   (ok)         35    5.5%
   (0.7, 1]   (bad)         5    0.8%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.156  0.005   0.150    0.167  ...    92.0      86.0      56.0   1.01
pu        0.701  0.001   0.700    0.704  ...   114.0      46.0      30.0   1.01
mu        0.070  0.010   0.056    0.091  ...    41.0      41.0      59.0   1.02
mus       0.154  0.024   0.117    0.193  ...    91.0     100.0      59.0   1.06
gamma     0.305  0.049   0.211    0.361  ...   135.0     107.0      88.0   1.01
Is_begin  0.430  0.663   0.001    1.246  ...    84.0      67.0      43.0   1.02
Ia_begin  0.742  0.927   0.018    2.442  ...    40.0      82.0      74.0   1.01
E_begin   0.302  0.330   0.000    0.846  ...   105.0      66.0      38.0   1.05

[8 rows x 11 columns]