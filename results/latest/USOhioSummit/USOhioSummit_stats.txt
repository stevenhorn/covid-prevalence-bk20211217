0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -3043.60    58.85
p_loo       45.23        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      569   89.7%
 (0.5, 0.7]   (ok)         48    7.6%
   (0.7, 1]   (bad)        14    2.2%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.214  0.041   0.156    0.277  ...   105.0      87.0      40.0   1.00
pu        0.727  0.016   0.704    0.756  ...   115.0     126.0      65.0   1.00
mu        0.111  0.016   0.083    0.139  ...    29.0      31.0      57.0   1.06
mus       0.187  0.041   0.126    0.258  ...    54.0      89.0      59.0   1.03
gamma     0.254  0.037   0.191    0.315  ...   105.0     107.0      43.0   1.03
Is_begin  2.469  1.939   0.098    6.147  ...    98.0     147.0      48.0   0.99
Ia_begin  6.654  4.062   0.432   13.304  ...    90.0      83.0     100.0   1.03
E_begin   5.302  4.246   0.172   13.778  ...    92.0      66.0      46.0   1.02

[8 rows x 11 columns]