0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -1357.75    35.05
p_loo       35.29        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      595   94.1%
 (0.5, 0.7]   (ok)         30    4.7%
   (0.7, 1]   (bad)         6    0.9%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.167  0.016   0.151    0.203  ...    71.0      61.0      40.0   1.00
pu        0.706  0.005   0.700    0.715  ...    99.0      80.0      59.0   1.03
mu        0.122  0.021   0.090    0.166  ...    32.0      34.0      38.0   1.05
mus       0.186  0.032   0.126    0.232  ...   136.0     152.0      61.0   1.00
gamma     0.205  0.032   0.149    0.263  ...    75.0      83.0      59.0   1.08
Is_begin  0.667  0.544   0.043    1.947  ...   102.0      87.0      45.0   1.01
Ia_begin  1.339  1.133   0.070    3.453  ...    68.0      68.0      59.0   1.03
E_begin   0.450  0.523   0.006    1.472  ...    94.0      59.0      60.0   1.07

[8 rows x 11 columns]