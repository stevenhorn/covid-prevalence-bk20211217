0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1130.49    52.06
p_loo       53.70        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      596   94.0%
 (0.5, 0.7]   (ok)         26    4.1%
   (0.7, 1]   (bad)        10    1.6%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.161  0.010   0.151    0.178  ...   152.0     106.0      60.0   0.99
pu        0.703  0.002   0.700    0.707  ...   119.0      78.0     100.0   0.99
mu        0.113  0.019   0.078    0.142  ...    78.0      75.0      91.0   1.00
mus       0.194  0.034   0.126    0.245  ...    37.0      32.0      65.0   1.04
gamma     0.234  0.053   0.153    0.335  ...    29.0      19.0      49.0   1.09
Is_begin  0.516  0.500   0.041    1.846  ...    35.0      43.0      40.0   1.18
Ia_begin  0.812  0.871   0.000    2.715  ...    67.0      32.0      17.0   1.04
E_begin   0.291  0.292   0.004    0.840  ...    67.0      52.0      43.0   1.01

[8 rows x 11 columns]