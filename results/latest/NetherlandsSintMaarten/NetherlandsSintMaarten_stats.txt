0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 633 log-likelihood matrix

         Estimate       SE
elpd_loo -1905.23    30.89
p_loo       37.85        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      592   93.5%
 (0.5, 0.7]   (ok)         36    5.7%
   (0.7, 1]   (bad)         5    0.8%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.238  0.046   0.170    0.330  ...   101.0      92.0      84.0   1.06
pu        0.744  0.025   0.701    0.784  ...     8.0       8.0      49.0   1.23
mu        0.155  0.017   0.128    0.184  ...    42.0      42.0      80.0   1.04
mus       0.205  0.038   0.160    0.281  ...    87.0      97.0      86.0   1.03
gamma     0.316  0.048   0.220    0.389  ...   129.0     116.0      48.0   1.06
Is_begin  1.156  1.029   0.012    3.201  ...   152.0      92.0      60.0   1.01
Ia_begin  2.542  2.104   0.012    6.012  ...    47.0      25.0      56.0   1.07
E_begin   1.254  1.037   0.006    2.927  ...    81.0      66.0      57.0   1.02

[8 rows x 11 columns]