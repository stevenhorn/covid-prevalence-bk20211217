0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2251.12    44.93
p_loo       57.28        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      588   92.7%
 (0.5, 0.7]   (ok)         36    5.7%
   (0.7, 1]   (bad)         8    1.3%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.163  0.012   0.150    0.184  ...   152.0     152.0      45.0   0.99
pu        0.704  0.004   0.700    0.712  ...   152.0     138.0      80.0   1.00
mu        0.114  0.015   0.090    0.143  ...    55.0      64.0      61.0   1.02
mus       0.184  0.028   0.123    0.226  ...    89.0      84.0      96.0   1.01
gamma     0.267  0.053   0.195    0.405  ...   152.0     152.0      51.0   1.00
Is_begin  0.654  0.599   0.018    1.658  ...   120.0     116.0     100.0   0.99
Ia_begin  0.909  1.034   0.023    2.778  ...    78.0      92.0      60.0   0.99
E_begin   0.418  0.449   0.004    1.246  ...    38.0     126.0      40.0   1.01

[8 rows x 11 columns]