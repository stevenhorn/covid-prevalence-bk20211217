5 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2568.74    42.38
p_loo       39.26        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      603   95.1%
 (0.5, 0.7]   (ok)         26    4.1%
   (0.7, 1]   (bad)         5    0.8%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.166  0.013   0.150    0.189  ...   136.0      99.0      69.0   1.00
pu        0.706  0.005   0.700    0.713  ...    78.0      62.0      59.0   1.05
mu        0.107  0.017   0.082    0.148  ...     6.0       6.0      60.0   1.32
mus       0.148  0.025   0.105    0.195  ...    98.0      86.0      66.0   1.02
gamma     0.204  0.034   0.144    0.268  ...    61.0      88.0      61.0   1.05
Is_begin  0.373  0.460   0.005    1.361  ...    88.0      81.0      59.0   1.00
Ia_begin  0.752  1.119   0.001    2.734  ...    59.0      52.0      56.0   0.99
E_begin   0.357  0.499   0.001    1.224  ...    35.0      54.0      48.0   1.00

[8 rows x 11 columns]