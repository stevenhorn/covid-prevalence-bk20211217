31 Divergences 
Passed validation 
2021-12-15 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 642 log-likelihood matrix

         Estimate       SE
elpd_loo -4164.14    88.74
p_loo      213.03        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      577   89.9%
 (0.5, 0.7]   (ok)         40    6.2%
   (0.7, 1]   (bad)        19    3.0%
   (1, Inf)   (very bad)    6    0.9%

              mean       sd   hdi_3%  ...  ess_bulk  ess_tail  r_hat
pa           0.263    0.054    0.154  ...      43.0      59.0   1.03
pu           0.801    0.024    0.764  ...      16.0      16.0   1.12
mu           0.225    0.088    0.141  ...       3.0      40.0   1.92
mus          0.220    0.068    0.150  ...       3.0      24.0   1.94
gamma        0.383    0.060    0.290  ...       8.0      48.0   1.19
Is_begin    83.712   24.685   36.356  ...      33.0      33.0   1.08
Ia_begin   235.657   48.752  178.255  ...       8.0      81.0   1.23
E_begin   1432.555  809.962  411.472  ...       3.0      19.0   2.01

[8 rows x 11 columns]