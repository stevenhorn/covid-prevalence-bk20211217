0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -1868.93    62.22
p_loo       54.35        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      586   92.7%
 (0.5, 0.7]   (ok)         37    5.9%
   (0.7, 1]   (bad)         7    1.1%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.187  0.034   0.151    0.253  ...    84.0     120.0      59.0   0.99
pu        0.708  0.007   0.700    0.720  ...    13.0      13.0      84.0   1.16
mu        0.127  0.022   0.093    0.171  ...     8.0       7.0      59.0   1.28
mus       0.227  0.029   0.183    0.283  ...   139.0     134.0      79.0   0.99
gamma     0.354  0.058   0.246    0.449  ...   111.0     146.0      59.0   1.00
Is_begin  0.705  0.670   0.001    1.947  ...   108.0      81.0      19.0   1.06
Ia_begin  1.159  1.218   0.023    3.392  ...    77.0      65.0      31.0   1.00
E_begin   0.568  0.683   0.002    1.659  ...    54.0      20.0      56.0   1.08

[8 rows x 11 columns]