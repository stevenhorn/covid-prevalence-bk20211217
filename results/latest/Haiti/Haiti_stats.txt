0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 633 log-likelihood matrix

         Estimate       SE
elpd_loo -3089.80    34.70
p_loo       41.30        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      593   93.7%
 (0.5, 0.7]   (ok)         32    5.1%
   (0.7, 1]   (bad)         6    0.9%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.276  0.047   0.190    0.347  ...   142.0     142.0      31.0   1.01
pu        0.881  0.018   0.840    0.899  ...   152.0     152.0      96.0   0.99
mu        0.160  0.022   0.120    0.200  ...     5.0       5.0      38.0   1.49
mus       0.162  0.024   0.121    0.204  ...   152.0     152.0      88.0   0.99
gamma     0.259  0.044   0.194    0.341  ...    52.0      59.0      60.0   1.01
Is_begin  0.744  0.686   0.018    1.993  ...    78.0      80.0      56.0   0.98
Ia_begin  0.703  0.604   0.018    2.015  ...   152.0     152.0      96.0   1.02
E_begin   0.550  0.656   0.009    1.655  ...    54.0     117.0      48.0   1.17

[8 rows x 11 columns]