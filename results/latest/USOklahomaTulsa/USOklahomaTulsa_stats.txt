13 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -4093.22    43.27
p_loo       40.86        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      586   92.4%
 (0.5, 0.7]   (ok)         36    5.7%
   (0.7, 1]   (bad)        10    1.6%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.158  0.009   0.150    0.182  ...    37.0       8.0      22.0   1.23
pu        0.704  0.005   0.700    0.716  ...    46.0      43.0      15.0   1.01
mu        0.090  0.016   0.065    0.121  ...    12.0      13.0      51.0   1.12
mus       0.146  0.031   0.095    0.201  ...    50.0      77.0      80.0   1.03
gamma     0.190  0.036   0.136    0.254  ...    79.0      94.0      49.0   1.04
Is_begin  0.715  0.675   0.002    1.977  ...    91.0      52.0      66.0   1.02
Ia_begin  0.510  0.439   0.004    1.399  ...    71.0      54.0      14.0   0.99
E_begin   0.457  0.509   0.001    1.248  ...    78.0      34.0      42.0   1.02

[8 rows x 11 columns]