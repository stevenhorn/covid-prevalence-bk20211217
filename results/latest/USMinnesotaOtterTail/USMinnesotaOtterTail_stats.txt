1 Divergences 
Passed validation 
2021-12-09 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 635 log-likelihood matrix

         Estimate       SE
elpd_loo -2176.05    37.08
p_loo       32.80        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      595   93.7%
 (0.5, 0.7]   (ok)         36    5.7%
   (0.7, 1]   (bad)         4    0.6%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.186  0.024   0.154    0.231  ...    71.0      70.0     100.0   1.06
pu        0.710  0.008   0.700    0.725  ...    59.0      35.0      31.0   1.02
mu        0.127  0.020   0.084    0.157  ...    20.0      21.0      19.0   1.07
mus       0.181  0.028   0.129    0.234  ...   112.0     105.0      58.0   1.01
gamma     0.232  0.041   0.151    0.286  ...   152.0     152.0      88.0   1.02
Is_begin  0.586  0.497   0.018    1.610  ...   114.0      91.0      69.0   1.04
Ia_begin  1.247  1.269   0.016    3.936  ...    80.0      75.0      60.0   1.01
E_begin   0.463  0.631   0.002    1.603  ...    98.0      73.0      39.0   1.02

[8 rows x 11 columns]