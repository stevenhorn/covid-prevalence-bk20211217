0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1227.34    49.14
p_loo       57.12        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      587   92.6%
 (0.5, 0.7]   (ok)         36    5.7%
   (0.7, 1]   (bad)         6    0.9%
   (1, Inf)   (very bad)    5    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.240  0.052   0.150    0.337  ...   131.0     134.0      48.0   1.02
pu        0.736  0.021   0.703    0.771  ...    45.0     100.0      74.0   1.03
mu        0.127  0.022   0.086    0.161  ...    17.0      17.0      38.0   1.09
mus       0.219  0.040   0.152    0.275  ...   134.0     152.0      99.0   0.99
gamma     0.306  0.048   0.221    0.377  ...   121.0     112.0      86.0   1.01
Is_begin  0.431  0.500   0.000    1.514  ...    89.0      50.0      39.0   1.05
Ia_begin  0.886  0.858   0.006    2.500  ...   101.0      75.0      39.0   1.05
E_begin   0.369  0.455   0.002    0.892  ...    85.0      63.0      60.0   1.01

[8 rows x 11 columns]