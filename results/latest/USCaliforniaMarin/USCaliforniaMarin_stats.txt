1 Divergences 
Passed validation 
2021-12-09 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 635 log-likelihood matrix

         Estimate       SE
elpd_loo -2684.17    45.64
p_loo       51.96        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      573   90.2%
 (0.5, 0.7]   (ok)         46    7.2%
   (0.7, 1]   (bad)        15    2.4%
   (1, Inf)   (very bad)    1    0.2%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.241   0.053   0.158    0.335  ...    67.0      70.0      40.0   1.06
pu         0.770   0.035   0.711    0.830  ...    30.0      30.0      60.0   1.08
mu         0.137   0.028   0.096    0.198  ...     8.0       9.0      22.0   1.20
mus        0.217   0.033   0.158    0.286  ...    73.0      96.0      83.0   1.00
gamma      0.302   0.055   0.208    0.410  ...   152.0     152.0      49.0   1.10
Is_begin   9.618   5.110   2.466   19.887  ...   128.0     122.0      83.0   1.00
Ia_begin  20.874  13.283   0.520   39.421  ...    20.0      15.0      22.0   1.12
E_begin   19.260  14.925   0.758   48.174  ...    16.0      29.0      53.0   1.09

[8 rows x 11 columns]