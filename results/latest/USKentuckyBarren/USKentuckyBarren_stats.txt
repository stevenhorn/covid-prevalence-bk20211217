0 Divergences 
Failed validation 
2021-12-09 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 635 log-likelihood matrix

         Estimate       SE
elpd_loo -2250.16    43.79
p_loo       43.64        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      596   93.9%
 (0.5, 0.7]   (ok)         31    4.9%
   (0.7, 1]   (bad)         8    1.3%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.158  0.007   0.150    0.173  ...   100.0      68.0      59.0   0.99
pu        0.702  0.002   0.700    0.707  ...   133.0      79.0      88.0   1.02
mu        0.079  0.010   0.059    0.091  ...    39.0      40.0      59.0   1.03
mus       0.176  0.024   0.136    0.230  ...    75.0      78.0      56.0   1.00
gamma     0.272  0.043   0.184    0.336  ...    72.0      70.0      93.0   0.99
Is_begin  0.467  0.420   0.020    1.186  ...    84.0      61.0      40.0   1.02
Ia_begin  0.741  0.808   0.010    1.882  ...    82.0      61.0      43.0   1.02
E_begin   0.318  0.355   0.006    1.003  ...   133.0     111.0      65.0   1.01

[8 rows x 11 columns]