0 Divergences 
Passed validation 
2021-12-16 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 643 log-likelihood matrix

         Estimate       SE
elpd_loo -2414.64    60.49
p_loo       71.25        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      577   89.7%
 (0.5, 0.7]   (ok)         51    7.9%
   (0.7, 1]   (bad)        11    1.7%
   (1, Inf)   (very bad)    4    0.6%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.253   0.050   0.156    0.335  ...   117.0     135.0      86.0   1.05
pu         0.808   0.050   0.721    0.887  ...     7.0      10.0      46.0   1.19
mu         0.179   0.033   0.114    0.232  ...     4.0       4.0      15.0   1.58
mus        0.232   0.042   0.151    0.310  ...   104.0     112.0      81.0   1.03
gamma      0.352   0.051   0.259    0.436  ...   108.0     101.0      57.0   1.01
Is_begin   5.588   3.755   0.566   13.004  ...    95.0      75.0      78.0   1.02
Ia_begin  17.489  16.694   0.409   56.571  ...    36.0      53.0      54.0   1.02
E_begin    7.440   6.147   0.033   16.972  ...    98.0      94.0      69.0   1.00

[8 rows x 11 columns]