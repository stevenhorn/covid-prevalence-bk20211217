0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1147.23    45.79
p_loo       54.20        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      588   92.7%
 (0.5, 0.7]   (ok)         32    5.0%
   (0.7, 1]   (bad)        13    2.1%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.189  0.032   0.150    0.256  ...    42.0      30.0      40.0   1.04
pu        0.712  0.009   0.701    0.732  ...    61.0      26.0      43.0   1.07
mu        0.196  0.041   0.132    0.266  ...     4.0       5.0      29.0   1.49
mus       0.240  0.040   0.189    0.324  ...     7.0       7.0      16.0   1.24
gamma     0.325  0.063   0.215    0.430  ...    29.0      29.0      40.0   1.08
Is_begin  0.157  0.196   0.000    0.683  ...    31.0      36.0      60.0   1.01
Ia_begin  0.264  0.327   0.003    0.812  ...    56.0      17.0      76.0   1.11
E_begin   0.147  0.204   0.001    0.498  ...    60.0      25.0      57.0   1.07

[8 rows x 11 columns]