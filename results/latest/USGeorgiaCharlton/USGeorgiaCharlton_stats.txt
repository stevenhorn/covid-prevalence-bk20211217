0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1735.51    64.35
p_loo       65.01        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      598   94.3%
 (0.5, 0.7]   (ok)         29    4.6%
   (0.7, 1]   (bad)         6    0.9%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.183  0.028   0.151    0.237  ...   152.0     152.0      95.0   1.00
pu        0.708  0.008   0.700    0.722  ...   143.0      70.0      20.0   1.02
mu        0.157  0.019   0.123    0.185  ...    34.0      37.0      20.0   1.06
mus       0.254  0.035   0.200    0.337  ...    80.0      91.0      93.0   0.99
gamma     0.409  0.069   0.309    0.525  ...   144.0     125.0     100.0   1.00
Is_begin  0.694  0.646   0.043    2.187  ...   102.0     132.0      74.0   1.03
Ia_begin  0.822  1.014   0.017    2.696  ...    21.0      22.0      59.0   1.09
E_begin   0.387  0.397   0.009    1.209  ...    60.0      67.0      87.0   1.06

[8 rows x 11 columns]