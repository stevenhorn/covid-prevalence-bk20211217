0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 633 log-likelihood matrix

         Estimate       SE
elpd_loo -4343.59    50.92
p_loo       40.21        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      555   87.7%
 (0.5, 0.7]   (ok)         69   10.9%
   (0.7, 1]   (bad)         6    0.9%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.290  0.048   0.196    0.348  ...    17.0      19.0      31.0   1.10
pu        0.886  0.017   0.849    0.900  ...    29.0      30.0      49.0   1.01
mu        0.109  0.026   0.071    0.161  ...     4.0       4.0      21.0   1.72
mus       0.164  0.032   0.110    0.226  ...   106.0      96.0      54.0   1.00
gamma     0.177  0.031   0.119    0.225  ...    60.0      53.0      60.0   1.01
Is_begin  1.588  1.258   0.022    4.557  ...    91.0      94.0      54.0   1.07
Ia_begin  2.838  2.703   0.033    9.506  ...    76.0      36.0      16.0   1.05
E_begin   2.150  2.193   0.063    5.623  ...    71.0      43.0      88.0   1.02

[8 rows x 11 columns]