0 Divergences 
Passed validation 
2021-12-09 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 635 log-likelihood matrix

         Estimate       SE
elpd_loo -1635.61    40.40
p_loo       35.26        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      594   93.5%
 (0.5, 0.7]   (ok)         29    4.6%
   (0.7, 1]   (bad)        12    1.9%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.166  0.012   0.150    0.188  ...    84.0      68.0      54.0   0.99
pu        0.705  0.005   0.700    0.714  ...   137.0     142.0      20.0   1.01
mu        0.102  0.014   0.075    0.125  ...    35.0      37.0      35.0   1.06
mus       0.157  0.029   0.111    0.204  ...    81.0      75.0      59.0   1.05
gamma     0.199  0.034   0.136    0.257  ...   152.0     152.0      74.0   0.99
Is_begin  0.481  0.468   0.026    1.440  ...   107.0      99.0      77.0   1.00
Ia_begin  0.859  0.918   0.011    2.819  ...    67.0      66.0      56.0   1.03
E_begin   0.345  0.468   0.007    1.274  ...    51.0      79.0      44.0   1.03

[8 rows x 11 columns]