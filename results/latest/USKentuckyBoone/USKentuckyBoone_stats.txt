0 Divergences 
Passed validation 
2021-12-09 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 635 log-likelihood matrix

         Estimate       SE
elpd_loo -2758.63    34.25
p_loo       32.78        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      573   90.2%
 (0.5, 0.7]   (ok)         52    8.2%
   (0.7, 1]   (bad)         9    1.4%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.177  0.025   0.150    0.226  ...   145.0     103.0      58.0   0.98
pu        0.709  0.007   0.700    0.723  ...    94.0      83.0      59.0   1.04
mu        0.094  0.015   0.074    0.129  ...    48.0      40.0      59.0   1.04
mus       0.151  0.026   0.109    0.203  ...   152.0     152.0      69.0   1.00
gamma     0.202  0.033   0.142    0.259  ...   116.0     113.0     100.0   1.01
Is_begin  0.558  0.631   0.002    2.178  ...   124.0      38.0      52.0   1.03
Ia_begin  1.354  1.261   0.050    4.014  ...    88.0     137.0      45.0   1.00
E_begin   0.700  0.673   0.003    2.011  ...    94.0     107.0      21.0   1.03

[8 rows x 11 columns]