0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2057.58    53.54
p_loo       55.58        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      576   90.9%
 (0.5, 0.7]   (ok)         43    6.8%
   (0.7, 1]   (bad)        11    1.7%
   (1, Inf)   (very bad)    4    0.6%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.164  0.012   0.151    0.187  ...   152.0     152.0      80.0   0.99
pu        0.704  0.004   0.700    0.713  ...   152.0     152.0      48.0   1.00
mu        0.095  0.016   0.069    0.127  ...    77.0      70.0      59.0   1.02
mus       0.188  0.034   0.127    0.248  ...   128.0     119.0      48.0   1.01
gamma     0.295  0.039   0.227    0.371  ...    59.0      53.0      81.0   1.02
Is_begin  0.596  0.659   0.000    1.639  ...    59.0     105.0      59.0   1.00
Ia_begin  0.913  0.939   0.004    3.046  ...    66.0      59.0      56.0   1.02
E_begin   0.482  0.776   0.000    1.506  ...    76.0      61.0      56.0   1.02

[8 rows x 11 columns]