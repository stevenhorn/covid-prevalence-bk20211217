0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2820.59    40.23
p_loo       51.24        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      588   92.7%
 (0.5, 0.7]   (ok)         39    6.2%
   (0.7, 1]   (bad)         5    0.8%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.159  0.009   0.150    0.175  ...   131.0     152.0      38.0   0.99
pu        0.703  0.003   0.700    0.708  ...    82.0      68.0      87.0   1.07
mu        0.098  0.013   0.076    0.124  ...    16.0      14.0      24.0   1.12
mus       0.170  0.031   0.124    0.235  ...    58.0      62.0      58.0   0.99
gamma     0.288  0.036   0.230    0.369  ...    90.0      86.0      88.0   1.00
Is_begin  1.570  1.242   0.019    3.562  ...    62.0      39.0      37.0   1.04
Ia_begin  3.610  3.061   0.133    8.329  ...   104.0      83.0      97.0   1.02
E_begin   2.419  2.620   0.047    7.441  ...    65.0      70.0      60.0   1.00

[8 rows x 11 columns]