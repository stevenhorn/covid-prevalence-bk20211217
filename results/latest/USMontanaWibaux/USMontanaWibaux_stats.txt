0 Divergences 
Failed validation 
2021-12-09 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 635 log-likelihood matrix

         Estimate       SE
elpd_loo  -601.84    55.52
p_loo       69.24        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      583   91.8%
 (0.5, 0.7]   (ok)         40    6.3%
   (0.7, 1]   (bad)         9    1.4%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.260  0.060   0.156    0.342  ...    42.0      52.0      72.0   1.04
pu        0.765  0.029   0.704    0.816  ...    18.0      20.0      40.0   1.10
mu        0.163  0.033   0.105    0.222  ...    46.0      49.0      75.0   1.01
mus       0.284  0.056   0.204    0.384  ...    38.0      34.0      44.0   1.00
gamma     0.352  0.076   0.204    0.481  ...    69.0      67.0      37.0   0.99
Is_begin  0.469  0.506   0.025    1.367  ...    77.0      60.0      86.0   1.01
Ia_begin  0.574  0.665   0.007    1.673  ...    68.0     108.0      46.0   1.02
E_begin   0.255  0.294   0.003    0.812  ...    44.0      46.0      40.0   1.06

[8 rows x 11 columns]