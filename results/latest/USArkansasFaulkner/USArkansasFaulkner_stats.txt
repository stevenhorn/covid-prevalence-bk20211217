0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2452.00    43.64
p_loo       46.54        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      590   93.1%
 (0.5, 0.7]   (ok)         33    5.2%
   (0.7, 1]   (bad)         8    1.3%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.163  0.010   0.150    0.181  ...   111.0      85.0      57.0   1.00
pu        0.703  0.003   0.700    0.709  ...   140.0     152.0      59.0   0.99
mu        0.080  0.011   0.061    0.098  ...    48.0      48.0      60.0   1.02
mus       0.178  0.036   0.111    0.246  ...   107.0      87.0      43.0   1.06
gamma     0.267  0.047   0.195    0.375  ...   116.0     150.0      68.0   1.00
Is_begin  1.071  0.822   0.011    2.691  ...   138.0     128.0      60.0   0.99
Ia_begin  2.049  1.737   0.035    4.903  ...    67.0      59.0      60.0   1.02
E_begin   0.935  0.933   0.031    3.010  ...    41.0      29.0      95.0   1.04

[8 rows x 11 columns]