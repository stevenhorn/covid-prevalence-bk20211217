0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1675.00    48.10
p_loo       45.00        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      578   91.2%
 (0.5, 0.7]   (ok)         39    6.2%
   (0.7, 1]   (bad)        15    2.4%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.250  0.053   0.154    0.325  ...   152.0     152.0      60.0   0.98
pu        0.735  0.022   0.700    0.774  ...   102.0      68.0      17.0   1.02
mu        0.146  0.022   0.109    0.185  ...     9.0       8.0      40.0   1.20
mus       0.198  0.036   0.140    0.267  ...    57.0      64.0      88.0   1.02
gamma     0.252  0.039   0.196    0.346  ...    97.0      91.0      69.0   1.00
Is_begin  0.745  0.735   0.016    2.106  ...   134.0      84.0      60.0   1.00
Ia_begin  1.561  1.548   0.012    4.709  ...   112.0     136.0      52.0   1.00
E_begin   0.730  0.818   0.003    2.382  ...   117.0     119.0      96.0   0.99

[8 rows x 11 columns]