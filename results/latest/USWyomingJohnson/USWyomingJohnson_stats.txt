0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -1535.37    87.54
p_loo       70.86        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      594   94.0%
 (0.5, 0.7]   (ok)         32    5.1%
   (0.7, 1]   (bad)         4    0.6%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.168  0.018   0.150    0.202  ...   125.0     105.0      38.0   0.99
pu        0.706  0.004   0.700    0.713  ...    72.0     105.0      32.0   1.09
mu        0.142  0.017   0.114    0.169  ...    69.0      68.0      19.0   1.03
mus       0.308  0.039   0.237    0.377  ...    73.0      69.0      80.0   1.02
gamma     0.507  0.078   0.361    0.622  ...   115.0     112.0      60.0   1.03
Is_begin  0.727  0.742   0.009    2.318  ...    97.0      72.0      43.0   1.03
Ia_begin  1.358  1.406   0.017    3.997  ...    59.0      96.0      48.0   1.00
E_begin   0.480  0.475   0.007    1.471  ...    68.0      64.0      30.0   1.01

[8 rows x 11 columns]