0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1361.36    35.30
p_loo       41.81        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      572   90.2%
 (0.5, 0.7]   (ok)         49    7.7%
   (0.7, 1]   (bad)        13    2.1%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.204  0.036   0.150    0.263  ...    65.0      62.0      56.0   1.04
pu        0.717  0.012   0.700    0.738  ...    22.0      20.0      43.0   1.09
mu        0.141  0.021   0.111    0.181  ...    46.0      45.0      45.0   1.01
mus       0.197  0.032   0.141    0.256  ...    85.0      92.0      93.0   1.00
gamma     0.248  0.044   0.185    0.342  ...   135.0     133.0      37.0   1.01
Is_begin  0.616  0.593   0.001    1.648  ...   107.0      67.0      48.0   1.01
Ia_begin  1.147  1.531   0.005    3.553  ...   100.0      82.0      80.0   0.99
E_begin   0.476  0.508   0.000    1.611  ...    73.0      71.0      60.0   1.03

[8 rows x 11 columns]