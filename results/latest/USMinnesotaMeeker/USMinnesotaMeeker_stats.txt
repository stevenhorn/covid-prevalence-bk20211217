0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1807.44    35.38
p_loo       36.22        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      590   93.1%
 (0.5, 0.7]   (ok)         36    5.7%
   (0.7, 1]   (bad)         7    1.1%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.168  0.016   0.150    0.186  ...    36.0      40.0      42.0   1.01
pu        0.707  0.006   0.700    0.718  ...   104.0      55.0      46.0   1.03
mu        0.129  0.018   0.100    0.161  ...    26.0      27.0      43.0   1.06
mus       0.186  0.024   0.152    0.237  ...   100.0     101.0      61.0   1.00
gamma     0.262  0.041   0.189    0.335  ...    41.0      45.0      49.0   1.04
Is_begin  0.562  0.558   0.011    1.856  ...    98.0      74.0      92.0   1.04
Ia_begin  0.943  0.910   0.005    2.789  ...   122.0      89.0      60.0   1.00
E_begin   0.472  0.659   0.002    1.803  ...    95.0      56.0      77.0   0.98

[8 rows x 11 columns]