0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -1579.82    45.55
p_loo       49.00        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      588   93.0%
 (0.5, 0.7]   (ok)         32    5.1%
   (0.7, 1]   (bad)        10    1.6%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.216  0.041   0.151    0.293  ...    52.0      28.0      22.0   1.07
pu        0.723  0.014   0.701    0.750  ...    44.0      41.0      60.0   1.01
mu        0.134  0.022   0.104    0.181  ...    40.0      39.0      60.0   1.01
mus       0.202  0.038   0.136    0.267  ...    47.0      55.0      72.0   1.04
gamma     0.236  0.050   0.157    0.333  ...    64.0      78.0      59.0   1.00
Is_begin  0.461  0.449   0.008    1.171  ...    68.0      47.0      38.0   1.01
Ia_begin  0.877  0.906   0.008    2.569  ...    97.0      83.0      72.0   1.01
E_begin   0.350  0.387   0.001    0.873  ...   101.0      45.0      22.0   1.02

[8 rows x 11 columns]