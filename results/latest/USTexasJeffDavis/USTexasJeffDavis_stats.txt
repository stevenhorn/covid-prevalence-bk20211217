0 Divergences 
Failed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo  -806.38    59.14
p_loo       61.00        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      581   91.9%
 (0.5, 0.7]   (ok)         39    6.2%
   (0.7, 1]   (bad)         8    1.3%
   (1, Inf)   (very bad)    4    0.6%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.246  0.045   0.163    0.318  ...   106.0     112.0      69.0   1.00
pu        0.835  0.017   0.804    0.866  ...    85.0      75.0      60.0   1.03
mu        0.117  0.023   0.080    0.165  ...    65.0      73.0     100.0   1.03
mus       0.207  0.037   0.136    0.280  ...   121.0     120.0     100.0   0.99
gamma     0.264  0.043   0.200    0.364  ...   143.0     135.0      59.0   0.99
Is_begin  0.410  0.554   0.001    1.480  ...   108.0     121.0      60.0   1.03
Ia_begin  0.574  0.906   0.005    2.100  ...    92.0      87.0      69.0   1.01
E_begin   0.247  0.540   0.002    0.669  ...    90.0      76.0      96.0   1.04

[8 rows x 11 columns]