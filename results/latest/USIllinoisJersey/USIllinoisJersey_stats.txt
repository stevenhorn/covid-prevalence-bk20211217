0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1863.82    68.47
p_loo       51.21        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      596   94.0%
 (0.5, 0.7]   (ok)         32    5.0%
   (0.7, 1]   (bad)         4    0.6%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.165  0.013   0.151    0.189  ...   121.0     111.0      49.0   1.01
pu        0.705  0.003   0.701    0.711  ...   100.0      74.0      59.0   1.03
mu        0.119  0.015   0.090    0.145  ...    27.0      26.0      60.0   1.09
mus       0.201  0.029   0.154    0.251  ...    24.0      25.0      93.0   1.06
gamma     0.381  0.060   0.262    0.480  ...    73.0      75.0      83.0   1.00
Is_begin  0.568  0.482   0.010    1.302  ...    76.0      70.0      97.0   1.00
Ia_begin  0.983  1.001   0.000    3.040  ...    90.0      73.0      54.0   1.02
E_begin   0.434  0.516   0.007    1.247  ...    65.0      21.0      45.0   1.08

[8 rows x 11 columns]