0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1100.13    52.84
p_loo       63.80        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      587   92.6%
 (0.5, 0.7]   (ok)         31    4.9%
   (0.7, 1]   (bad)        14    2.2%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.163  0.013   0.150    0.187  ...   107.0      81.0      79.0   1.03
pu        0.705  0.005   0.700    0.713  ...   152.0     126.0      57.0   1.03
mu        0.117  0.019   0.093    0.159  ...    32.0      31.0      57.0   1.05
mus       0.218  0.030   0.165    0.270  ...   152.0     152.0      86.0   1.03
gamma     0.284  0.048   0.209    0.371  ...    15.0      16.0      59.0   1.11
Is_begin  0.325  0.442   0.004    1.141  ...    96.0      95.0      60.0   0.98
Ia_begin  0.386  0.438   0.002    1.396  ...    17.0      12.0      60.0   1.13
E_begin   0.204  0.193   0.001    0.555  ...    92.0      57.0      84.0   1.02

[8 rows x 11 columns]