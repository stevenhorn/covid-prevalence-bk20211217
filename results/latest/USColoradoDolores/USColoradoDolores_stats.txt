0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo  -686.81    41.53
p_loo       57.31        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      598   94.3%
 (0.5, 0.7]   (ok)         30    4.7%
   (0.7, 1]   (bad)         4    0.6%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.218  0.049   0.151    0.304  ...    83.0      78.0      40.0   0.99
pu        0.728  0.022   0.701    0.768  ...    38.0      48.0      38.0   1.04
mu        0.181  0.029   0.117    0.223  ...    55.0      53.0      60.0   1.02
mus       0.260  0.037   0.203    0.313  ...    62.0      77.0      97.0   1.03
gamma     0.318  0.045   0.255    0.416  ...   109.0      97.0      37.0   1.03
Is_begin  0.453  0.497   0.011    1.596  ...    97.0      73.0      93.0   1.01
Ia_begin  0.648  0.741   0.007    2.006  ...    73.0      71.0      81.0   1.00
E_begin   0.264  0.325   0.004    0.777  ...    84.0      63.0      61.0   1.03

[8 rows x 11 columns]