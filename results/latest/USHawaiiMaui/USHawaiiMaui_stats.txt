0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2073.10    45.96
p_loo       56.05        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      566   89.3%
 (0.5, 0.7]   (ok)         55    8.7%
   (0.7, 1]   (bad)        12    1.9%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.258  0.051   0.175    0.342  ...   152.0     152.0      78.0   1.12
pu        0.812  0.029   0.744    0.851  ...    34.0      41.0      80.0   1.04
mu        0.152  0.021   0.113    0.192  ...    18.0      16.0      56.0   1.09
mus       0.222  0.035   0.166    0.292  ...   152.0     152.0      99.0   0.99
gamma     0.327  0.059   0.246    0.464  ...   152.0     152.0      96.0   1.01
Is_begin  0.841  0.675   0.015    2.005  ...    58.0      50.0      58.0   1.02
Ia_begin  1.769  1.340   0.064    4.055  ...   126.0     152.0      72.0   0.99
E_begin   1.249  1.053   0.037    3.663  ...    85.0      63.0      85.0   1.00

[8 rows x 11 columns]