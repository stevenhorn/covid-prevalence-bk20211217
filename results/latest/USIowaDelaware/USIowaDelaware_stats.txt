0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1924.67    42.14
p_loo       45.38        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      612   96.5%
 (0.5, 0.7]   (ok)         16    2.5%
   (0.7, 1]   (bad)         4    0.6%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.157  0.007   0.150    0.167  ...   111.0      85.0      91.0   1.00
pu        0.702  0.002   0.700    0.707  ...   130.0     102.0     100.0   1.02
mu        0.089  0.011   0.071    0.110  ...    43.0      41.0      38.0   1.04
mus       0.158  0.030   0.096    0.210  ...    88.0     104.0      60.0   1.00
gamma     0.209  0.035   0.154    0.270  ...    88.0      83.0      96.0   1.00
Is_begin  0.660  0.528   0.008    1.482  ...    84.0      67.0      40.0   1.03
Ia_begin  0.929  0.862   0.006    2.359  ...    87.0      99.0      60.0   1.01
E_begin   0.372  0.409   0.002    1.207  ...    51.0      34.0      24.0   1.04

[8 rows x 11 columns]