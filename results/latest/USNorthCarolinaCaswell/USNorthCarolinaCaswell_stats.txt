0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1873.60    44.60
p_loo       47.81        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      595   93.8%
 (0.5, 0.7]   (ok)         30    4.7%
   (0.7, 1]   (bad)         6    0.9%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.200  0.037   0.151    0.279  ...   135.0     152.0      60.0   1.00
pu        0.717  0.014   0.700    0.744  ...    83.0      84.0      59.0   1.02
mu        0.116  0.017   0.089    0.153  ...    34.0      32.0      60.0   1.01
mus       0.173  0.031   0.121    0.230  ...    51.0      49.0      60.0   1.05
gamma     0.242  0.047   0.154    0.305  ...    62.0      50.0      65.0   1.04
Is_begin  0.605  0.447   0.019    1.371  ...   127.0     119.0     100.0   1.01
Ia_begin  1.054  1.091   0.021    2.549  ...   103.0     125.0      69.0   0.99
E_begin   0.466  0.513   0.025    1.270  ...   104.0     107.0      60.0   1.00

[8 rows x 11 columns]