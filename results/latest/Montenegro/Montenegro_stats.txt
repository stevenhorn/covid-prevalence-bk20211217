0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 633 log-likelihood matrix

         Estimate       SE
elpd_loo -3440.55    68.03
p_loo      145.82        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      564   89.1%
 (0.5, 0.7]   (ok)         48    7.6%
   (0.7, 1]   (bad)        15    2.4%
   (1, Inf)   (very bad)    6    0.9%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.152  0.002   0.150    0.156  ...    59.0      62.0      40.0   1.08
pu        0.701  0.001   0.700    0.702  ...    73.0      84.0      54.0   1.01
mu        0.056  0.020   0.033    0.087  ...     4.0       4.0      18.0   1.57
mus       0.247  0.048   0.180    0.343  ...    76.0      74.0      61.0   1.01
gamma     0.305  0.162   0.082    0.554  ...     4.0       5.0      35.0   1.48
Is_begin  2.577  1.914   0.087    6.404  ...   152.0     152.0     100.0   0.99
Ia_begin  3.885  3.119   0.249    9.343  ...    76.0      62.0      60.0   1.02
E_begin   2.020  2.217   0.000    7.586  ...    60.0      34.0      22.0   1.06

[8 rows x 11 columns]