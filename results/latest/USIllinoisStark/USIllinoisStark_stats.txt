0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1126.01    42.90
p_loo       51.06        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      582   91.8%
 (0.5, 0.7]   (ok)         37    5.8%
   (0.7, 1]   (bad)        13    2.1%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.169  0.019   0.150    0.208  ...   124.0      45.0      24.0   1.04
pu        0.706  0.006   0.700    0.719  ...   152.0     127.0      86.0   0.99
mu        0.126  0.021   0.092    0.151  ...    44.0      66.0      91.0   1.03
mus       0.235  0.041   0.158    0.302  ...    70.0      80.0      86.0   0.99
gamma     0.312  0.051   0.227    0.397  ...   146.0     150.0      61.0   1.11
Is_begin  0.546  0.589   0.007    1.721  ...    86.0      59.0      59.0   1.00
Ia_begin  0.801  0.814   0.011    1.919  ...   115.0      77.0      88.0   0.99
E_begin   0.344  0.369   0.003    1.177  ...    71.0      46.0      22.0   1.02

[8 rows x 11 columns]