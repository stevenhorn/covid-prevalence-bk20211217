0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2035.15    40.31
p_loo       42.83        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      589   92.9%
 (0.5, 0.7]   (ok)         31    4.9%
   (0.7, 1]   (bad)        12    1.9%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.249  0.049   0.164    0.334  ...    33.0      33.0      32.0   1.02
pu        0.732  0.021   0.700    0.764  ...    58.0      46.0      30.0   1.01
mu        0.099  0.019   0.069    0.142  ...    25.0      24.0      24.0   1.07
mus       0.145  0.027   0.090    0.193  ...   152.0     152.0      91.0   1.05
gamma     0.202  0.040   0.143    0.272  ...    17.0      18.0      57.0   1.10
Is_begin  0.703  0.614   0.009    1.882  ...    71.0      73.0      58.0   1.01
Ia_begin  1.578  1.500   0.014    4.360  ...    61.0      72.0      58.0   1.02
E_begin   0.861  1.096   0.001    2.522  ...    59.0      66.0      57.0   1.02

[8 rows x 11 columns]