0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1719.15    39.93
p_loo       41.68        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      584   92.1%
 (0.5, 0.7]   (ok)         43    6.8%
   (0.7, 1]   (bad)         6    0.9%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.154  0.004   0.150    0.163  ...   152.0     137.0      40.0   0.99
pu        0.701  0.001   0.700    0.704  ...   121.0      46.0      22.0   1.02
mu        0.077  0.013   0.054    0.098  ...    41.0      49.0      35.0   1.00
mus       0.164  0.028   0.112    0.216  ...   112.0     130.0      61.0   1.01
gamma     0.247  0.037   0.189    0.316  ...   102.0     100.0      80.0   0.98
Is_begin  0.443  0.454   0.002    1.336  ...    75.0     152.0      60.0   1.03
Ia_begin  0.654  0.832   0.000    1.979  ...    53.0      58.0      20.0   1.02
E_begin   0.249  0.320   0.002    0.919  ...   123.0      61.0      55.0   1.01

[8 rows x 11 columns]