0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo  -649.13    42.83
p_loo       43.42        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      599   94.5%
 (0.5, 0.7]   (ok)         22    3.5%
   (0.7, 1]   (bad)        12    1.9%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.251  0.044   0.176    0.334  ...    26.0      30.0      48.0   1.03
pu        0.886  0.009   0.869    0.899  ...    27.0      19.0      83.0   1.08
mu        0.126  0.019   0.090    0.158  ...    38.0      40.0      59.0   1.00
mus       0.187  0.031   0.137    0.242  ...   152.0     130.0      57.0   1.01
gamma     0.243  0.038   0.172    0.308  ...    55.0      56.0      96.0   0.99
Is_begin  0.573  0.663   0.002    1.985  ...    96.0      29.0      40.0   1.06
Ia_begin  0.938  0.971   0.004    3.248  ...    66.0      43.0      22.0   1.03
E_begin   0.456  0.507   0.003    1.501  ...    86.0      42.0      36.0   1.06

[8 rows x 11 columns]