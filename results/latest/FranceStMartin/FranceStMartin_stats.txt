13 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 633 log-likelihood matrix

         Estimate       SE
elpd_loo -2489.26    48.84
p_loo       64.83        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      594   93.8%
 (0.5, 0.7]   (ok)         31    4.9%
   (0.7, 1]   (bad)         7    1.1%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.191  0.032   0.152    0.258  ...    73.0      43.0      39.0   1.04
pu        0.712  0.010   0.700    0.729  ...    73.0      42.0      36.0   1.04
mu        0.132  0.030   0.082    0.179  ...     4.0       4.0      14.0   1.54
mus       0.178  0.029   0.133    0.232  ...    58.0      59.0      96.0   1.04
gamma     0.223  0.032   0.166    0.275  ...    38.0      43.0      39.0   1.04
Is_begin  0.831  0.774   0.058    2.378  ...    37.0      26.0      59.0   1.03
Ia_begin  0.776  0.614   0.027    1.899  ...    19.0      25.0      16.0   1.10
E_begin   0.312  0.280   0.017    0.805  ...    35.0      37.0      48.0   1.05

[8 rows x 11 columns]