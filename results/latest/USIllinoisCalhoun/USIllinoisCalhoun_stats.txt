0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1205.42    42.30
p_loo       50.14        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      590   93.1%
 (0.5, 0.7]   (ok)         34    5.4%
   (0.7, 1]   (bad)         7    1.1%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.170  0.015   0.150    0.203  ...    95.0      79.0      43.0   1.00
pu        0.708  0.006   0.701    0.722  ...   152.0     152.0      72.0   1.06
mu        0.108  0.018   0.080    0.140  ...    33.0      24.0      39.0   1.08
mus       0.189  0.033   0.126    0.244  ...   106.0      95.0      96.0   1.02
gamma     0.271  0.050   0.170    0.347  ...    46.0      49.0      60.0   1.03
Is_begin  0.306  0.387   0.002    1.035  ...    37.0      14.0      60.0   1.11
Ia_begin  0.559  0.897   0.003    1.781  ...    75.0      29.0      59.0   1.05
E_begin   0.244  0.304   0.001    0.811  ...   114.0      19.0      48.0   1.08

[8 rows x 11 columns]