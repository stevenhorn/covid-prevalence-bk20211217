0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2239.54    53.55
p_loo       49.66        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      589   92.9%
 (0.5, 0.7]   (ok)         33    5.2%
   (0.7, 1]   (bad)        10    1.6%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.248  0.047   0.160    0.327  ...   115.0     126.0      91.0   0.99
pu        0.732  0.019   0.703    0.761  ...    87.0      71.0      60.0   1.03
mu        0.137  0.016   0.111    0.171  ...    60.0      58.0      60.0   1.02
mus       0.190  0.033   0.136    0.265  ...   132.0     146.0     100.0   1.02
gamma     0.235  0.040   0.177    0.316  ...   152.0     144.0      88.0   0.99
Is_begin  0.894  0.793   0.012    2.304  ...    94.0      64.0      15.0   1.03
Ia_begin  1.884  1.790   0.116    5.349  ...   108.0      97.0      91.0   1.02
E_begin   0.799  1.101   0.034    2.381  ...    95.0      87.0      76.0   1.00

[8 rows x 11 columns]