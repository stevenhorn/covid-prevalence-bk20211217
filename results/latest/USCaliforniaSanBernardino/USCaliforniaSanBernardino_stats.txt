0 Divergences 
Failed validation 
2021-12-09 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 635 log-likelihood matrix

         Estimate       SE
elpd_loo -4327.20    42.33
p_loo       44.00        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      593   93.4%
 (0.5, 0.7]   (ok)         36    5.7%
   (0.7, 1]   (bad)         5    0.8%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.170  0.017   0.151    0.201  ...    61.0      69.0      44.0   1.03
pu        0.706  0.005   0.700    0.714  ...   152.0     121.0      57.0   1.00
mu        0.090  0.013   0.067    0.114  ...     6.0       7.0      22.0   1.27
mus       0.168  0.026   0.121    0.203  ...   152.0     152.0      79.0   0.98
gamma     0.309  0.051   0.215    0.397  ...   140.0     150.0      69.0   0.99
Is_begin  1.627  1.326   0.125    4.349  ...    99.0      83.0      88.0   1.04
Ia_begin  3.867  2.891   0.170    9.799  ...    45.0     109.0      80.0   1.01
E_begin   3.119  2.712   0.069    8.991  ...    83.0      49.0      15.0   0.99

[8 rows x 11 columns]