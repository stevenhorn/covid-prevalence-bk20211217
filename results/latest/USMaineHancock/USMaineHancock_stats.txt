0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1749.15    50.72
p_loo       46.94        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      600   94.6%
 (0.5, 0.7]   (ok)         25    3.9%
   (0.7, 1]   (bad)         7    1.1%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.246  0.053   0.150    0.329  ...   105.0      97.0      40.0   1.02
pu        0.798  0.023   0.762    0.842  ...    85.0      99.0      55.0   1.02
mu        0.133  0.022   0.099    0.185  ...    12.0      12.0      33.0   1.13
mus       0.172  0.030   0.124    0.223  ...    87.0     113.0      91.0   1.02
gamma     0.214  0.037   0.162    0.289  ...    90.0      95.0      60.0   1.01
Is_begin  0.912  0.839   0.028    2.599  ...   131.0     129.0      93.0   1.02
Ia_begin  2.118  2.028   0.104    5.467  ...   111.0     143.0      60.0   1.00
E_begin   0.640  0.650   0.001    2.172  ...    57.0      46.0      88.0   1.02

[8 rows x 11 columns]