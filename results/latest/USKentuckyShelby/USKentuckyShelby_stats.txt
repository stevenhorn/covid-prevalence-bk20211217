0 Divergences 
Passed validation 
2021-12-09 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 635 log-likelihood matrix

         Estimate       SE
elpd_loo -2305.52    44.47
p_loo       46.19        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      588   92.6%
 (0.5, 0.7]   (ok)         38    6.0%
   (0.7, 1]   (bad)         6    0.9%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.196  0.034   0.151    0.251  ...    61.0      56.0      87.0   1.01
pu        0.716  0.012   0.700    0.737  ...    58.0      55.0      58.0   1.00
mu        0.126  0.017   0.101    0.161  ...    10.0      12.0      14.0   1.11
mus       0.169  0.027   0.128    0.214  ...    95.0      90.0      69.0   0.99
gamma     0.239  0.039   0.161    0.303  ...    53.0      49.0      95.0   1.04
Is_begin  0.521  0.487   0.018    1.464  ...   115.0      89.0      83.0   1.00
Ia_begin  0.851  1.342   0.003    4.480  ...    98.0      65.0      18.0   1.00
E_begin   0.403  0.552   0.000    1.649  ...    60.0      39.0      59.0   1.02

[8 rows x 11 columns]