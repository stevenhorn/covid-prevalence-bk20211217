0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1875.60    75.33
p_loo       56.77        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      581   91.6%
 (0.5, 0.7]   (ok)         41    6.5%
   (0.7, 1]   (bad)        11    1.7%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.157  0.007   0.150    0.168  ...   152.0     152.0      48.0   0.99
pu        0.703  0.004   0.700    0.711  ...   152.0     152.0      60.0   1.03
mu        0.081  0.011   0.062    0.103  ...    32.0      36.0      33.0   1.02
mus       0.224  0.034   0.171    0.293  ...   152.0     152.0     100.0   1.01
gamma     0.321  0.053   0.235    0.408  ...    91.0      88.0      60.0   1.00
Is_begin  0.635  0.601   0.016    1.858  ...   139.0     122.0      84.0   1.00
Ia_begin  0.736  0.905   0.011    2.125  ...    88.0     101.0      45.0   1.00
E_begin   0.334  0.439   0.001    0.983  ...    92.0      91.0      40.0   1.00

[8 rows x 11 columns]