0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2035.80    49.67
p_loo       47.40        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      597   94.2%
 (0.5, 0.7]   (ok)         32    5.0%
   (0.7, 1]   (bad)         3    0.5%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.168  0.014   0.150    0.194  ...    76.0      71.0      86.0   1.00
pu        0.705  0.005   0.700    0.715  ...   100.0      84.0      91.0   1.07
mu        0.091  0.012   0.071    0.115  ...    36.0      35.0      60.0   1.02
mus       0.172  0.029   0.109    0.224  ...   139.0     131.0      92.0   1.02
gamma     0.277  0.047   0.202    0.364  ...   114.0     140.0      76.0   0.99
Is_begin  0.595  0.590   0.002    1.856  ...   149.0     101.0      44.0   1.01
Ia_begin  0.905  0.837   0.022    2.608  ...   118.0      96.0      64.0   1.00
E_begin   0.451  0.474   0.002    1.615  ...    81.0      66.0      40.0   0.99

[8 rows x 11 columns]