0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1226.75    36.57
p_loo       40.99        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      596   94.0%
 (0.5, 0.7]   (ok)         32    5.0%
   (0.7, 1]   (bad)         4    0.6%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.203  0.035   0.152    0.265  ...    99.0      98.0      91.0   1.09
pu        0.715  0.012   0.700    0.736  ...    99.0      77.0     100.0   1.01
mu        0.131  0.022   0.091    0.172  ...    22.0      23.0      20.0   1.08
mus       0.198  0.036   0.151    0.286  ...    68.0      64.0      60.0   1.05
gamma     0.245  0.041   0.183    0.334  ...    59.0      59.0      59.0   1.05
Is_begin  0.538  0.595   0.004    1.673  ...    45.0      98.0      61.0   1.06
Ia_begin  0.899  0.974   0.014    2.940  ...    70.0      83.0      79.0   1.01
E_begin   0.372  0.366   0.002    1.103  ...    74.0      65.0      97.0   1.00

[8 rows x 11 columns]