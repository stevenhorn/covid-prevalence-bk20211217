0 Divergences 
Failed validation 
2021-12-09 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 635 log-likelihood matrix

         Estimate       SE
elpd_loo  -802.61    56.94
p_loo       67.33        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      589   92.8%
 (0.5, 0.7]   (ok)         33    5.2%
   (0.7, 1]   (bad)         7    1.1%
   (1, Inf)   (very bad)    6    0.9%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.196  0.031   0.152    0.247  ...   103.0      94.0      88.0   1.01
pu        0.716  0.013   0.700    0.741  ...   152.0     152.0      49.0   1.00
mu        0.145  0.021   0.102    0.178  ...    19.0      19.0      48.0   1.09
mus       0.240  0.034   0.178    0.290  ...   114.0     152.0      61.0   1.03
gamma     0.332  0.049   0.260    0.443  ...   112.0     106.0      56.0   1.03
Is_begin  0.323  0.384   0.001    0.963  ...    86.0      64.0      59.0   1.00
Ia_begin  0.375  0.471   0.003    1.483  ...    42.0      55.0      53.0   1.00
E_begin   0.207  0.351   0.001    0.579  ...    74.0      51.0      88.0   1.00

[8 rows x 11 columns]