0 Divergences 
Failed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo  -670.52    37.40
p_loo       55.38        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      573   90.7%
 (0.5, 0.7]   (ok)         48    7.6%
   (0.7, 1]   (bad)        11    1.7%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.271  0.059   0.169    0.348  ...    39.0      39.0      91.0   1.04
pu        0.762  0.028   0.709    0.807  ...    20.0      23.0      22.0   1.10
mu        0.138  0.026   0.093    0.186  ...    23.0      22.0      67.0   1.06
mus       0.200  0.032   0.142    0.253  ...    79.0      84.0      75.0   1.04
gamma     0.242  0.044   0.157    0.313  ...   152.0     152.0      79.0   1.00
Is_begin  0.266  0.373   0.006    0.840  ...    70.0      30.0      43.0   1.07
Ia_begin  0.516  0.656   0.002    1.698  ...    76.0      68.0      57.0   1.07
E_begin   0.195  0.320   0.001    0.739  ...    57.0     118.0      59.0   0.99

[8 rows x 11 columns]