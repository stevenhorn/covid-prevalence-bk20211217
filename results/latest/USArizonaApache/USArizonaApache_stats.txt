0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2714.43    67.07
p_loo       54.68        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      595   93.8%
 (0.5, 0.7]   (ok)         27    4.3%
   (0.7, 1]   (bad)         8    1.3%
   (1, Inf)   (very bad)    4    0.6%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.156  0.004   0.150    0.165  ...   114.0      92.0      83.0   1.00
pu        0.702  0.003   0.700    0.706  ...   152.0     152.0      59.0   1.02
mu        0.071  0.010   0.057    0.094  ...    77.0      74.0      60.0   0.99
mus       0.173  0.036   0.113    0.246  ...   152.0     152.0      88.0   1.01
gamma     0.259  0.042   0.186    0.348  ...    67.0      69.0      59.0   1.01
Is_begin  0.693  0.699   0.000    2.069  ...    80.0      58.0      25.0   1.04
Ia_begin  0.565  0.534   0.002    1.435  ...   111.0      95.0      96.0   1.00
E_begin   0.440  0.431   0.001    1.133  ...   116.0      74.0      60.0   1.01

[8 rows x 11 columns]