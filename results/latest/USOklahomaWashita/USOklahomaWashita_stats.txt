0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1715.91    45.76
p_loo       45.92        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      605   95.4%
 (0.5, 0.7]   (ok)         23    3.6%
   (0.7, 1]   (bad)         5    0.8%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.172  0.019   0.150    0.209  ...    98.0      84.0      59.0   1.01
pu        0.706  0.006   0.700    0.718  ...   152.0     117.0      22.0   1.00
mu        0.104  0.015   0.080    0.135  ...    41.0      41.0      21.0   1.09
mus       0.157  0.028   0.118    0.217  ...   152.0     152.0      74.0   1.03
gamma     0.208  0.041   0.121    0.278  ...    71.0      87.0      60.0   1.02
Is_begin  0.536  0.564   0.011    1.547  ...   119.0      89.0     100.0   0.99
Ia_begin  0.978  0.985   0.012    2.462  ...    77.0      50.0      59.0   1.01
E_begin   0.410  0.428   0.005    1.324  ...    74.0      69.0      58.0   1.05

[8 rows x 11 columns]