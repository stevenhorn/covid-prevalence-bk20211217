0 Divergences 
Passed validation 
2021-12-09 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 635 log-likelihood matrix

         Estimate       SE
elpd_loo -2404.70    40.67
p_loo       44.07        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      610   96.1%
 (0.5, 0.7]   (ok)         19    3.0%
   (0.7, 1]   (bad)         6    0.9%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.159  0.009   0.150    0.177  ...    33.0      16.0      15.0   1.12
pu        0.703  0.005   0.700    0.709  ...    13.0      10.0      59.0   1.16
mu        0.131  0.023   0.100    0.180  ...     4.0       4.0      14.0   1.60
mus       0.174  0.026   0.123    0.213  ...     7.0       8.0      22.0   1.27
gamma     0.279  0.064   0.188    0.394  ...     5.0       5.0      16.0   1.46
Is_begin  1.015  0.886   0.017    2.572  ...     7.0       6.0      48.0   1.37
Ia_begin  2.898  1.730   0.472    5.935  ...    55.0      43.0      39.0   1.09
E_begin   2.056  1.461   0.030    4.718  ...     6.0       5.0      38.0   1.45

[8 rows x 11 columns]