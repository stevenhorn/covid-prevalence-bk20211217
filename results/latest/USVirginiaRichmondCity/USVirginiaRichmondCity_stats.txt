0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -2861.14    41.66
p_loo       42.89        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      573   90.7%
 (0.5, 0.7]   (ok)         48    7.6%
   (0.7, 1]   (bad)        10    1.6%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.229  0.054   0.160    0.339  ...    72.0      81.0      23.0   1.06
pu        0.753  0.026   0.705    0.792  ...    34.0      35.0      60.0   1.06
mu        0.124  0.021   0.083    0.158  ...     8.0       8.0      38.0   1.19
mus       0.169  0.027   0.123    0.219  ...    62.0      64.0      60.0   1.03
gamma     0.228  0.047   0.157    0.315  ...    74.0      80.0      93.0   1.02
Is_begin  0.716  0.590   0.010    1.617  ...    81.0      60.0      81.0   1.03
Ia_begin  2.288  1.557   0.095    5.017  ...    71.0      65.0      60.0   1.05
E_begin   1.657  1.378   0.015    3.946  ...    97.0      62.0      19.0   1.05

[8 rows x 11 columns]