0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo  -824.88    65.04
p_loo       71.83        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      583   92.0%
 (0.5, 0.7]   (ok)         40    6.3%
   (0.7, 1]   (bad)         5    0.8%
   (1, Inf)   (very bad)    6    0.9%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.163  0.010   0.151    0.178  ...    91.0      81.0      60.0   1.02
pu        0.704  0.004   0.700    0.712  ...   131.0     149.0      86.0   1.00
mu        0.130  0.022   0.089    0.167  ...    88.0      80.0      76.0   1.00
mus       0.235  0.035   0.175    0.288  ...   116.0     101.0      88.0   1.01
gamma     0.270  0.042   0.195    0.342  ...   137.0     152.0      59.0   1.02
Is_begin  0.155  0.158   0.005    0.510  ...    79.0      84.0      59.0   1.06
Ia_begin  0.293  0.387   0.003    1.046  ...    24.0      62.0      43.0   1.08
E_begin   0.115  0.122   0.001    0.361  ...    76.0      69.0      38.0   1.08

[8 rows x 11 columns]