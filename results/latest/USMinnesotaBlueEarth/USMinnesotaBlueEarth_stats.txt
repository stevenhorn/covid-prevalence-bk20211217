0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2403.73    38.21
p_loo       38.95        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      594   93.7%
 (0.5, 0.7]   (ok)         27    4.3%
   (0.7, 1]   (bad)         9    1.4%
   (1, Inf)   (very bad)    4    0.6%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.171  0.015   0.153    0.199  ...   152.0     152.0      72.0   1.02
pu        0.707  0.006   0.700    0.718  ...   107.0      83.0      57.0   1.01
mu        0.130  0.015   0.108    0.163  ...    34.0      33.0      60.0   1.03
mus       0.188  0.027   0.138    0.228  ...    76.0      70.0      59.0   1.01
gamma     0.270  0.043   0.208    0.361  ...    92.0     130.0      75.0   0.99
Is_begin  0.740  0.765   0.002    1.989  ...    90.0      96.0      60.0   0.99
Ia_begin  0.509  0.469   0.026    1.370  ...   110.0     111.0      75.0   1.02
E_begin   0.420  0.571   0.005    1.137  ...    45.0      85.0      60.0   1.03

[8 rows x 11 columns]