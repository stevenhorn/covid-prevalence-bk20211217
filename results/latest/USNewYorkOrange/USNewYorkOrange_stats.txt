0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -3137.58    53.13
p_loo       41.75        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      571   90.1%
 (0.5, 0.7]   (ok)         51    8.0%
   (0.7, 1]   (bad)        10    1.6%
   (1, Inf)   (very bad)    2    0.3%

             mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa          0.167   0.015   0.150    0.194  ...   123.0      90.0      39.0   1.00
pu          0.705   0.004   0.700    0.714  ...    55.0      44.0      38.0   1.02
mu          0.113   0.013   0.091    0.143  ...    17.0      15.0      25.0   1.12
mus         0.174   0.027   0.135    0.226  ...   101.0     110.0      99.0   0.98
gamma       0.356   0.052   0.284    0.466  ...   116.0     108.0      93.0   1.03
Is_begin   45.231  26.708   3.582   96.478  ...    77.0      65.0      99.0   0.99
Ia_begin  127.561  56.322  37.742  240.255  ...    85.0      65.0      24.0   1.11
E_begin   143.854  97.818   1.073  295.372  ...   142.0      93.0      33.0   1.01

[8 rows x 11 columns]