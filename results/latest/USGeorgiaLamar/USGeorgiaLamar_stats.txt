0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1788.00    61.67
p_loo       52.52        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      597   94.2%
 (0.5, 0.7]   (ok)         31    4.9%
   (0.7, 1]   (bad)         5    0.8%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.190  0.029   0.151    0.245  ...   119.0     133.0      53.0   1.04
pu        0.711  0.009   0.701    0.730  ...    83.0      91.0      40.0   1.04
mu        0.118  0.018   0.090    0.160  ...    67.0      63.0      40.0   1.03
mus       0.205  0.032   0.156    0.265  ...   131.0     127.0      93.0   1.01
gamma     0.386  0.070   0.255    0.489  ...    94.0     147.0      59.0   1.01
Is_begin  0.958  0.922   0.032    2.474  ...   152.0     152.0      72.0   0.99
Ia_begin  1.677  1.828   0.008    4.991  ...    98.0     152.0      57.0   0.98
E_begin   0.584  0.574   0.003    1.549  ...   119.0     145.0      46.0   0.99

[8 rows x 11 columns]