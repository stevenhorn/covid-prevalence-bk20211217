0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 633 log-likelihood matrix

         Estimate       SE
elpd_loo -2894.79    57.37
p_loo       66.05        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      563   88.9%
 (0.5, 0.7]   (ok)         52    8.2%
   (0.7, 1]   (bad)        16    2.5%
   (1, Inf)   (very bad)    2    0.3%

            mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.236  0.059   0.151    0.335  ...    24.0      36.0      17.0   1.17
pu         0.779  0.049   0.706    0.865  ...    56.0      53.0      60.0   1.05
mu         0.157  0.022   0.128    0.213  ...     3.0       3.0      15.0   1.97
mus        0.324  0.057   0.224    0.416  ...    21.0      24.0      40.0   1.08
gamma      0.380  0.068   0.279    0.490  ...    73.0      62.0      84.0   1.02
Is_begin   7.160  5.083   0.196   16.113  ...    88.0      68.0      60.0   1.07
Ia_begin  17.831  9.209   2.402   32.702  ...   139.0     126.0      97.0   1.00
E_begin   12.189  9.377   1.219   30.956  ...    97.0      94.0      88.0   1.04

[8 rows x 11 columns]