0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1981.17    58.71
p_loo       46.68        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      596   94.0%
 (0.5, 0.7]   (ok)         29    4.6%
   (0.7, 1]   (bad)         7    1.1%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.156  0.006   0.150    0.168  ...    96.0     152.0      60.0   1.07
pu        0.702  0.002   0.700    0.705  ...    90.0      37.0      39.0   1.04
mu        0.075  0.010   0.060    0.095  ...    71.0      73.0      97.0   1.05
mus       0.180  0.031   0.132    0.234  ...    92.0     111.0      88.0   0.99
gamma     0.224  0.050   0.153    0.312  ...    68.0      32.0      55.0   1.05
Is_begin  0.254  0.385   0.006    1.207  ...    47.0      60.0      59.0   1.02
Ia_begin  0.286  0.319   0.002    1.014  ...    73.0      67.0      93.0   1.00
E_begin   0.136  0.155   0.001    0.487  ...    62.0      59.0      97.0   0.99

[8 rows x 11 columns]