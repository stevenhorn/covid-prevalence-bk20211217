0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1427.93    38.78
p_loo       48.67        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      589   92.9%
 (0.5, 0.7]   (ok)         29    4.6%
   (0.7, 1]   (bad)        15    2.4%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.206  0.040   0.155    0.284  ...    90.0      98.0      95.0   1.05
pu        0.717  0.013   0.700    0.738  ...    66.0      55.0      36.0   1.02
mu        0.136  0.022   0.097    0.174  ...    54.0      55.0      59.0   1.04
mus       0.211  0.039   0.140    0.283  ...   131.0     134.0      97.0   0.99
gamma     0.263  0.041   0.204    0.359  ...    83.0      88.0      59.0   1.00
Is_begin  0.617  0.642   0.003    1.676  ...   116.0     104.0      18.0   1.01
Ia_begin  1.258  1.344   0.054    4.642  ...   121.0     146.0      88.0   1.00
E_begin   0.585  0.756   0.021    2.512  ...   124.0      91.0      81.0   1.01

[8 rows x 11 columns]