0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1997.81    51.21
p_loo       55.28        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      595   93.8%
 (0.5, 0.7]   (ok)         31    4.9%
   (0.7, 1]   (bad)         8    1.3%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.162  0.011   0.150    0.182  ...   126.0     113.0      47.0   1.07
pu        0.703  0.003   0.700    0.709  ...   152.0     125.0      96.0   1.01
mu        0.123  0.020   0.085    0.159  ...    13.0      14.0      36.0   1.11
mus       0.211  0.033   0.157    0.267  ...    80.0      87.0      96.0   1.00
gamma     0.296  0.056   0.176    0.389  ...    48.0      45.0      42.0   1.03
Is_begin  0.649  0.633   0.027    1.795  ...    63.0      48.0      60.0   1.04
Ia_begin  1.050  1.218   0.000    3.030  ...   101.0      54.0      60.0   1.03
E_begin   0.511  0.674   0.000    1.995  ...    64.0      22.0      22.0   1.07

[8 rows x 11 columns]