0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2165.58    38.05
p_loo       37.45        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      585   92.3%
 (0.5, 0.7]   (ok)         43    6.8%
   (0.7, 1]   (bad)         5    0.8%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.169  0.014   0.150    0.194  ...   152.0     152.0      60.0   0.99
pu        0.706  0.006   0.700    0.717  ...   152.0     141.0      38.0   1.03
mu        0.107  0.014   0.087    0.134  ...    67.0      65.0      91.0   1.01
mus       0.160  0.026   0.118    0.214  ...    79.0      78.0      87.0   1.04
gamma     0.254  0.040   0.180    0.325  ...   152.0     152.0      49.0   0.99
Is_begin  0.294  0.414   0.000    1.010  ...    85.0      61.0      56.0   1.00
Ia_begin  0.474  0.481   0.003    1.468  ...    84.0      56.0      48.0   1.03
E_begin   0.222  0.220   0.002    0.619  ...    66.0      38.0      30.0   1.03

[8 rows x 11 columns]