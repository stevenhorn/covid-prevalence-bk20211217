0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1368.10    34.70
p_loo       42.82        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      584   92.1%
 (0.5, 0.7]   (ok)         39    6.2%
   (0.7, 1]   (bad)        10    1.6%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.169  0.016   0.150    0.200  ...   152.0     152.0      56.0   1.03
pu        0.706  0.005   0.700    0.715  ...   103.0      80.0      59.0   1.00
mu        0.098  0.014   0.079    0.125  ...    29.0      28.0      53.0   1.05
mus       0.185  0.033   0.124    0.231  ...    70.0      66.0      88.0   0.99
gamma     0.282  0.054   0.194    0.384  ...    65.0      59.0      57.0   1.06
Is_begin  0.366  0.466   0.011    1.209  ...    15.0      35.0      25.0   1.05
Ia_begin  0.501  0.629   0.002    1.595  ...    51.0      44.0      40.0   1.05
E_begin   0.227  0.294   0.001    0.733  ...    70.0      52.0      72.0   1.05

[8 rows x 11 columns]