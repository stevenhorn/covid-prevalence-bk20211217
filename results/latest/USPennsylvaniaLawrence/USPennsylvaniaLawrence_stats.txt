0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2005.65    33.88
p_loo       42.80        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      578   91.2%
 (0.5, 0.7]   (ok)         42    6.6%
   (0.7, 1]   (bad)        14    2.2%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.175  0.017   0.153    0.211  ...    76.0      82.0      86.0   1.00
pu        0.707  0.005   0.700    0.716  ...    49.0      47.0      21.0   1.00
mu        0.129  0.015   0.108    0.161  ...    28.0      28.0      40.0   1.13
mus       0.191  0.029   0.146    0.245  ...    56.0      56.0      93.0   1.03
gamma     0.282  0.050   0.203    0.374  ...    52.0      46.0      35.0   1.05
Is_begin  1.221  1.008   0.007    3.148  ...   112.0      47.0      57.0   1.07
Ia_begin  2.474  2.542   0.036    7.697  ...    55.0      47.0      53.0   1.06
E_begin   1.085  1.052   0.043    2.893  ...    30.0      33.0      42.0   1.03

[8 rows x 11 columns]