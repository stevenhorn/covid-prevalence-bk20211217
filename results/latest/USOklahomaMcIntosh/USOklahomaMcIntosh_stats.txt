0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1998.24    41.22
p_loo       42.70        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      601   94.8%
 (0.5, 0.7]   (ok)         25    3.9%
   (0.7, 1]   (bad)         7    1.1%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.183  0.029   0.151    0.245  ...    66.0      54.0      60.0   1.03
pu        0.712  0.010   0.700    0.731  ...    62.0      47.0      22.0   0.99
mu        0.129  0.023   0.095    0.174  ...    17.0      19.0      20.0   1.07
mus       0.167  0.030   0.114    0.214  ...    58.0      57.0      40.0   0.99
gamma     0.169  0.024   0.122    0.207  ...    37.0      35.0      32.0   1.04
Is_begin  0.567  0.554   0.000    1.626  ...    77.0      48.0      40.0   1.02
Ia_begin  1.006  1.108   0.008    2.696  ...    89.0      65.0      60.0   1.02
E_begin   0.451  0.514   0.011    1.473  ...    84.0      37.0      42.0   1.05

[8 rows x 11 columns]