0 Divergences 
Failed validation 
2021-12-09 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 635 log-likelihood matrix

         Estimate       SE
elpd_loo -1517.87    49.13
p_loo       60.51        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      601   94.6%
 (0.5, 0.7]   (ok)         26    4.1%
   (0.7, 1]   (bad)         3    0.5%
   (1, Inf)   (very bad)    5    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.161  0.013   0.150    0.180  ...   152.0     126.0      38.0   1.02
pu        0.704  0.003   0.700    0.710  ...    20.0      16.0      40.0   1.10
mu        0.094  0.014   0.072    0.120  ...    47.0      50.0      38.0   1.02
mus       0.178  0.040   0.112    0.251  ...   119.0     152.0      44.0   1.03
gamma     0.226  0.034   0.170    0.289  ...   133.0     152.0      93.0   1.05
Is_begin  0.506  0.394   0.003    1.175  ...    76.0      61.0      14.0   1.01
Ia_begin  0.869  1.182   0.002    2.842  ...   103.0     143.0      53.0   1.02
E_begin   0.318  0.273   0.002    0.813  ...    76.0      72.0      55.0   1.01

[8 rows x 11 columns]