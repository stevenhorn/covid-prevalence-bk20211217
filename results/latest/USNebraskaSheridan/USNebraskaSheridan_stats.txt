0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1408.56    93.96
p_loo       59.92        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      603   95.1%
 (0.5, 0.7]   (ok)         23    3.6%
   (0.7, 1]   (bad)         7    1.1%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.166  0.015   0.150    0.193  ...    59.0      53.0      52.0   1.01
pu        0.705  0.004   0.700    0.713  ...    80.0      60.0      56.0   1.04
mu        0.084  0.012   0.064    0.109  ...    15.0      13.0      49.0   1.13
mus       0.262  0.041   0.188    0.317  ...    52.0      59.0      53.0   1.00
gamma     0.600  0.109   0.404    0.769  ...    48.0      62.0      65.0   1.02
Is_begin  0.139  0.187   0.002    0.499  ...    29.0      16.0      59.0   1.09
Ia_begin  0.233  0.541   0.001    0.911  ...    46.0      18.0      40.0   1.14
E_begin   0.094  0.136   0.000    0.341  ...    26.0      19.0      37.0   1.08

[8 rows x 11 columns]