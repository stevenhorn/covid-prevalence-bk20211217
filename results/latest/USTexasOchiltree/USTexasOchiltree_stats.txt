0 Divergences 
Failed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -1709.69    53.50
p_loo       55.33        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      592   93.7%
 (0.5, 0.7]   (ok)         29    4.6%
   (0.7, 1]   (bad)         9    1.4%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.176  0.023   0.151    0.229  ...   127.0     152.0      60.0   1.01
pu        0.708  0.007   0.700    0.723  ...   118.0     135.0      60.0   1.01
mu        0.101  0.015   0.076    0.123  ...   102.0     103.0      60.0   1.03
mus       0.173  0.032   0.123    0.219  ...    84.0     121.0      60.0   0.99
gamma     0.245  0.042   0.172    0.320  ...    82.0      81.0      72.0   1.01
Is_begin  0.489  0.565   0.002    1.607  ...   104.0      92.0      40.0   1.02
Ia_begin  0.718  0.870   0.010    2.220  ...    92.0      72.0      60.0   1.00
E_begin   0.365  0.518   0.003    1.147  ...   105.0      95.0      59.0   1.02

[8 rows x 11 columns]