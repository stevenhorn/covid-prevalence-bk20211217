0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo  1109.41   144.77
p_loo      115.72        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      620   98.1%
 (0.5, 0.7]   (ok)          3    0.5%
   (0.7, 1]   (bad)         3    0.5%
   (1, Inf)   (very bad)    6    0.9%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.294  0.040   0.216    0.349  ...    81.0      36.0     100.0   1.04
pu        0.893  0.010   0.868    0.900  ...    38.0      26.0      41.0   1.06
mu        0.178  0.038   0.098    0.240  ...    58.0      71.0      77.0   1.03
mus       0.466  0.060   0.369    0.584  ...    38.0      36.0      40.0   1.03
gamma     0.498  0.054   0.411    0.593  ...    87.0      93.0      54.0   1.02
Is_begin  0.523  0.483   0.005    1.434  ...    49.0      38.0      53.0   1.01
Ia_begin  0.632  0.595   0.002    1.655  ...    56.0      54.0      79.0   1.03
E_begin   0.231  0.233   0.001    0.668  ...    52.0      29.0      22.0   1.04

[8 rows x 11 columns]