0 Divergences 
Passed validation 
2021-12-09 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 635 log-likelihood matrix

         Estimate       SE
elpd_loo -1597.93    45.26
p_loo       46.72        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      593   93.4%
 (0.5, 0.7]   (ok)         34    5.4%
   (0.7, 1]   (bad)         7    1.1%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.159  0.011   0.150    0.180  ...   152.0     152.0      54.0   0.99
pu        0.703  0.003   0.700    0.708  ...   152.0     134.0      60.0   0.98
mu        0.098  0.017   0.069    0.127  ...    57.0      54.0      54.0   1.01
mus       0.200  0.038   0.138    0.250  ...    91.0      78.0      45.0   1.02
gamma     0.257  0.044   0.186    0.335  ...   135.0     138.0      49.0   1.03
Is_begin  0.370  0.375   0.002    1.058  ...    80.0      66.0      34.0   1.01
Ia_begin  0.521  0.705   0.002    1.965  ...   117.0     152.0      60.0   1.00
E_begin   0.222  0.316   0.001    0.736  ...    85.0      73.0      99.0   1.00

[8 rows x 11 columns]