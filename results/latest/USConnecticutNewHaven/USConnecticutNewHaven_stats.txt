0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -3853.18    38.87
p_loo       39.09        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      579   91.3%
 (0.5, 0.7]   (ok)         46    7.3%
   (0.7, 1]   (bad)         7    1.1%
   (1, Inf)   (very bad)    2    0.3%

            mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.162  0.011   0.150    0.182  ...   152.0     152.0      86.0   1.06
pu         0.704  0.003   0.700    0.711  ...   152.0     152.0      74.0   1.00
mu         0.109  0.013   0.082    0.131  ...    19.0      15.0      56.0   1.10
mus        0.150  0.016   0.124    0.185  ...   102.0      97.0      88.0   1.03
gamma      0.374  0.057   0.283    0.478  ...   152.0     152.0      83.0   1.03
Is_begin   4.598  3.636   0.051   11.198  ...   101.0      63.0      39.0   1.03
Ia_begin  11.883  5.655   2.125   22.287  ...    64.0      62.0      43.0   1.07
E_begin   14.643  9.397   2.380   34.999  ...   109.0      92.0      47.0   1.04

[8 rows x 11 columns]