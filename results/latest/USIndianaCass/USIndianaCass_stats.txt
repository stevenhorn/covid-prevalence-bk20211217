0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2249.15    60.28
p_loo       53.41        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      603   95.1%
 (0.5, 0.7]   (ok)         24    3.8%
   (0.7, 1]   (bad)         6    0.9%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.155  0.005   0.150    0.162  ...    16.0       9.0      24.0   1.20
pu        0.702  0.002   0.700    0.706  ...    24.0      11.0      42.0   1.13
mu        0.096  0.010   0.082    0.119  ...    43.0      45.0      32.0   1.03
mus       0.303  0.035   0.247    0.357  ...    22.0      21.0      54.0   1.09
gamma     0.617  0.081   0.481    0.752  ...    11.0      11.0      66.0   1.15
Is_begin  0.413  0.401   0.005    1.038  ...    91.0      73.0      65.0   1.03
Ia_begin  0.326  0.338   0.001    0.931  ...    97.0      73.0      40.0   1.02
E_begin   0.166  0.153   0.000    0.460  ...    64.0      57.0      33.0   1.01

[8 rows x 11 columns]