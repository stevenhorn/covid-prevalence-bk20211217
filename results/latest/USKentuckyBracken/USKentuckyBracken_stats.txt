0 Divergences 
Passed validation 
2021-12-09 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 635 log-likelihood matrix

         Estimate       SE
elpd_loo -1343.16    41.46
p_loo       46.73        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      597   94.0%
 (0.5, 0.7]   (ok)         35    5.5%
   (0.7, 1]   (bad)         3    0.5%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.169  0.015   0.151    0.204  ...    66.0      47.0      48.0   1.03
pu        0.707  0.007   0.700    0.720  ...    88.0      76.0      52.0   1.04
mu        0.125  0.016   0.099    0.153  ...    43.0      41.0      61.0   1.05
mus       0.195  0.031   0.138    0.245  ...   106.0     104.0      59.0   1.04
gamma     0.269  0.047   0.181    0.348  ...    82.0      90.0      74.0   1.01
Is_begin  0.708  0.831   0.000    2.386  ...   130.0      98.0      24.0   1.02
Ia_begin  1.015  1.078   0.015    2.554  ...    50.0      69.0      58.0   1.00
E_begin   0.397  0.442   0.005    1.286  ...   118.0      67.0      43.0   1.00

[8 rows x 11 columns]