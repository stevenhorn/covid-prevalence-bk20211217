0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo  -803.95    44.31
p_loo       55.59        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      584   92.1%
 (0.5, 0.7]   (ok)         40    6.3%
   (0.7, 1]   (bad)         7    1.1%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.256  0.052   0.178    0.346  ...    64.0      69.0      88.0   1.08
pu        0.805  0.021   0.770    0.841  ...    79.0      80.0      96.0   1.00
mu        0.137  0.025   0.093    0.181  ...    22.0      24.0      55.0   1.06
mus       0.205  0.037   0.142    0.276  ...    67.0      71.0      51.0   1.03
gamma     0.247  0.050   0.168    0.352  ...    54.0      72.0      60.0   1.04
Is_begin  0.385  0.405   0.008    1.370  ...    84.0      61.0      59.0   0.99
Ia_begin  0.460  0.543   0.000    1.570  ...    68.0      53.0      43.0   1.00
E_begin   0.250  0.260   0.009    0.640  ...    59.0      52.0      79.0   1.00

[8 rows x 11 columns]