0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1853.67    48.99
p_loo       43.35        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      593   93.5%
 (0.5, 0.7]   (ok)         29    4.6%
   (0.7, 1]   (bad)        11    1.7%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.155  0.004   0.150    0.161  ...   141.0      81.0      60.0   1.02
pu        0.702  0.001   0.700    0.704  ...   152.0     152.0     100.0   0.99
mu        0.085  0.012   0.065    0.107  ...    38.0      40.0      40.0   1.03
mus       0.196  0.033   0.150    0.255  ...    50.0      52.0      60.0   1.03
gamma     0.324  0.050   0.246    0.434  ...    62.0      57.0      83.0   1.03
Is_begin  0.466  0.636   0.003    1.945  ...    95.0      64.0      60.0   1.00
Ia_begin  0.593  0.616   0.002    1.643  ...    65.0      42.0      79.0   1.02
E_begin   0.242  0.302   0.000    0.762  ...    94.0      35.0      17.0   1.01

[8 rows x 11 columns]