0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -3108.36    49.87
p_loo       52.75        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      587   92.6%
 (0.5, 0.7]   (ok)         37    5.8%
   (0.7, 1]   (bad)         9    1.4%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.255  0.044   0.169    0.317  ...    48.0      53.0      20.0   1.09
pu        0.758  0.024   0.720    0.797  ...    27.0      30.0      37.0   1.06
mu        0.136  0.021   0.097    0.165  ...    20.0      15.0      75.0   1.09
mus       0.196  0.032   0.145    0.252  ...   114.0     117.0      73.0   1.03
gamma     0.248  0.060   0.171    0.374  ...    56.0      65.0      60.0   1.02
Is_begin  1.375  1.290   0.055    3.819  ...    41.0      72.0      53.0   1.03
Ia_begin  0.693  0.474   0.008    1.548  ...    69.0      54.0      38.0   1.00
E_begin   0.838  0.918   0.028    2.570  ...   107.0     114.0      99.0   1.00

[8 rows x 11 columns]