0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2974.48    45.88
p_loo       92.39        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      530   83.6%
 (0.5, 0.7]   (ok)         53    8.4%
   (0.7, 1]   (bad)        18    2.8%
   (1, Inf)   (very bad)   33    5.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.167  0.007   0.154    0.182  ...    29.0      27.0      26.0   1.42
pu        0.704  0.003   0.700    0.710  ...     9.0       8.0      11.0   1.84
mu        0.232  0.011   0.205    0.252  ...     5.0       5.0      11.0   1.46
mus       0.228  0.020   0.197    0.249  ...     3.0       3.0      10.0   2.29
gamma     0.142  0.011   0.115    0.157  ...     4.0       4.0      23.0   1.92
Is_begin  1.902  0.928   0.535    3.694  ...    24.0      24.0      60.0   1.60
Ia_begin  2.661  2.575   0.390    8.755  ...     6.0       5.0      22.0   1.84
E_begin   1.491  1.888   0.371    6.722  ...     6.0       7.0      43.0   1.72

[8 rows x 11 columns]