0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1672.74    46.35
p_loo       44.98        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      599   94.5%
 (0.5, 0.7]   (ok)         26    4.1%
   (0.7, 1]   (bad)         6    0.9%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.159  0.009   0.150    0.174  ...   140.0      46.0      22.0   1.06
pu        0.703  0.003   0.700    0.708  ...   152.0      95.0      60.0   0.99
mu        0.107  0.019   0.075    0.143  ...    72.0      60.0      84.0   1.00
mus       0.180  0.031   0.129    0.237  ...   152.0     152.0      79.0   1.00
gamma     0.271  0.047   0.192    0.363  ...    89.0     109.0      60.0   1.02
Is_begin  0.270  0.345   0.001    1.090  ...   116.0      86.0      36.0   1.01
Ia_begin  0.425  0.548   0.005    1.461  ...    97.0      76.0      53.0   0.99
E_begin   0.208  0.291   0.001    0.712  ...    27.0      58.0      21.0   1.05

[8 rows x 11 columns]