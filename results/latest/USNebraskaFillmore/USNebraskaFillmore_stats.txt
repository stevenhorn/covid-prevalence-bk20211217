0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1302.15   117.64
p_loo       68.34        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      572   90.2%
 (0.5, 0.7]   (ok)         54    8.5%
   (0.7, 1]   (bad)         7    1.1%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.161  0.011   0.150    0.186  ...   152.0     152.0      93.0   1.01
pu        0.704  0.004   0.700    0.712  ...   123.0      96.0      97.0   0.99
mu        0.096  0.013   0.074    0.118  ...    33.0      35.0      60.0   1.02
mus       0.314  0.038   0.261    0.389  ...    69.0      68.0      39.0   1.01
gamma     0.849  0.074   0.712    0.962  ...    85.0      90.0      93.0   1.02
Is_begin  0.366  0.488   0.000    1.235  ...    36.0      31.0      22.0   1.07
Ia_begin  0.472  0.973   0.010    1.664  ...    55.0     104.0      73.0   1.03
E_begin   0.177  0.303   0.004    0.662  ...    59.0      21.0      33.0   1.08

[8 rows x 11 columns]