0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -3317.49    34.21
p_loo       34.15        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      585   92.3%
 (0.5, 0.7]   (ok)         42    6.6%
   (0.7, 1]   (bad)         7    1.1%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.234  0.060   0.152    0.337  ...    45.0      56.0      24.0   1.03
pu        0.725  0.019   0.701    0.759  ...    51.0      45.0      23.0   1.04
mu        0.110  0.020   0.069    0.140  ...     9.0       9.0      35.0   1.18
mus       0.151  0.026   0.111    0.203  ...   152.0     136.0      72.0   1.02
gamma     0.169  0.032   0.124    0.237  ...    87.0      81.0      60.0   1.00
Is_begin  0.870  0.811   0.009    2.302  ...    67.0      68.0      59.0   0.99
Ia_begin  0.571  0.445   0.004    1.230  ...    35.0      25.0      39.0   1.07
E_begin   0.516  0.474   0.005    1.279  ...    48.0      45.0      60.0   1.02

[8 rows x 11 columns]