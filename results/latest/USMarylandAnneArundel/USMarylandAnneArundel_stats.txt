0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2849.57    37.96
p_loo       44.45        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      565   89.1%
 (0.5, 0.7]   (ok)         62    9.8%
   (0.7, 1]   (bad)         7    1.1%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.238  0.053   0.164    0.332  ...    58.0      65.0      60.0   1.01
pu        0.732  0.025   0.701    0.776  ...    39.0      24.0      43.0   1.06
mu        0.135  0.018   0.109    0.171  ...    40.0      33.0      31.0   1.02
mus       0.202  0.035   0.128    0.265  ...    73.0      76.0      57.0   1.00
gamma     0.323  0.050   0.238    0.414  ...   135.0     146.0      91.0   1.06
Is_begin  3.652  2.178   0.359    7.132  ...    61.0      68.0      74.0   1.02
Ia_begin  8.513  4.964   1.150   16.078  ...   104.0      91.0      59.0   1.03
E_begin   9.884  6.419   0.748   19.989  ...    52.0      40.0      22.0   1.06

[8 rows x 11 columns]