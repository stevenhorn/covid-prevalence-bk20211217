0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2010.39    60.57
p_loo       50.43        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      595   93.8%
 (0.5, 0.7]   (ok)         27    4.3%
   (0.7, 1]   (bad)         9    1.4%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.171  0.019   0.150    0.198  ...   123.0      90.0      60.0   1.01
pu        0.707  0.006   0.700    0.717  ...   117.0     119.0      60.0   1.12
mu        0.088  0.013   0.071    0.120  ...    33.0      22.0      57.0   1.09
mus       0.190  0.037   0.125    0.266  ...   139.0     152.0      69.0   1.01
gamma     0.323  0.058   0.204    0.411  ...    67.0      65.0      91.0   1.05
Is_begin  0.551  0.443   0.016    1.174  ...    63.0      62.0      58.0   1.10
Ia_begin  1.327  1.313   0.058    4.018  ...   113.0      80.0      87.0   1.01
E_begin   0.575  0.694   0.007    1.853  ...    40.0      81.0      54.0   1.04

[8 rows x 11 columns]