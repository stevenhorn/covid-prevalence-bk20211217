0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2734.43    79.88
p_loo       50.62        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      594   93.7%
 (0.5, 0.7]   (ok)         29    4.6%
   (0.7, 1]   (bad)         9    1.4%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.161  0.012   0.150    0.181  ...    70.0      47.0      56.0   1.04
pu        0.703  0.003   0.700    0.709  ...    48.0      14.0      22.0   1.11
mu        0.118  0.010   0.100    0.134  ...     8.0       8.0      60.0   1.21
mus       0.297  0.038   0.245    0.374  ...    56.0      57.0      60.0   1.00
gamma     0.642  0.107   0.490    0.882  ...    64.0      49.0      45.0   1.04
Is_begin  0.646  0.773   0.026    1.414  ...    31.0      11.0      40.0   1.15
Ia_begin  1.984  2.653   0.084    8.284  ...    38.0      26.0      43.0   1.07
E_begin   0.855  1.232   0.002    3.857  ...    21.0       8.0      24.0   1.23

[8 rows x 11 columns]