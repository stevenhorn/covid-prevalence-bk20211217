0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -2586.25    49.81
p_loo       48.84        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      598   94.6%
 (0.5, 0.7]   (ok)         24    3.8%
   (0.7, 1]   (bad)         6    0.9%
   (1, Inf)   (very bad)    4    0.6%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.163  0.013   0.150    0.181  ...   152.0     152.0      79.0   0.98
pu        0.704  0.003   0.700    0.711  ...   133.0      85.0      55.0   1.07
mu        0.091  0.015   0.068    0.121  ...     7.0       7.0      57.0   1.22
mus       0.184  0.026   0.140    0.231  ...   137.0     152.0      95.0   1.03
gamma     0.249  0.042   0.178    0.311  ...    19.0      23.0      32.0   1.08
Is_begin  0.655  0.618   0.010    1.795  ...   102.0      81.0      86.0   0.99
Ia_begin  0.536  0.419   0.004    1.206  ...   130.0     152.0      80.0   1.02
E_begin   0.292  0.310   0.000    0.796  ...   113.0      60.0      33.0   1.01

[8 rows x 11 columns]