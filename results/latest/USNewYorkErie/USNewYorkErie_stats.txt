0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -3251.62    34.09
p_loo       42.84        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      583   92.0%
 (0.5, 0.7]   (ok)         41    6.5%
   (0.7, 1]   (bad)        10    1.6%
   (1, Inf)   (very bad)    0    0.0%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.191   0.022   0.154    0.227  ...    85.0      87.0      56.0   1.08
pu         0.711   0.008   0.701    0.726  ...    89.0      82.0      57.0   0.99
mu         0.135   0.013   0.113    0.160  ...    35.0      31.0      22.0   1.03
mus        0.181   0.026   0.136    0.223  ...    84.0      86.0      60.0   1.00
gamma      0.321   0.049   0.236    0.399  ...    40.0      31.0      91.0   1.06
Is_begin  19.016   8.255   2.635   31.273  ...    91.0     103.0      96.0   1.00
Ia_begin  33.939  17.553   5.394   65.345  ...    11.0      11.0      24.0   1.15
E_begin   42.941  26.857   0.547   92.710  ...    31.0      29.0      43.0   1.04

[8 rows x 11 columns]