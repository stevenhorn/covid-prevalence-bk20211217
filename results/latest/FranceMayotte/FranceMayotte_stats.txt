0 Divergences 
Failed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 633 log-likelihood matrix

         Estimate       SE
elpd_loo -3103.87    48.34
p_loo       58.31        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      588   92.9%
 (0.5, 0.7]   (ok)         35    5.5%
   (0.7, 1]   (bad)         9    1.4%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.258  0.059   0.172    0.349  ...    79.0      79.0      40.0   1.07
pu        0.778  0.031   0.720    0.828  ...    19.0      21.0      29.0   1.07
mu        0.145  0.023   0.113    0.193  ...     7.0       7.0      45.0   1.25
mus       0.180  0.034   0.126    0.239  ...    77.0      74.0      26.0   1.03
gamma     0.240  0.043   0.175    0.326  ...    55.0      47.0      59.0   1.03
Is_begin  0.567  0.447   0.007    1.260  ...   118.0      92.0      96.0   1.01
Ia_begin  1.709  1.673   0.020    5.888  ...    44.0      21.0      60.0   1.06
E_begin   1.003  1.087   0.004    3.343  ...    38.0      24.0      15.0   1.04

[8 rows x 11 columns]