0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1837.95    88.03
p_loo       66.85        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      599   94.5%
 (0.5, 0.7]   (ok)         27    4.3%
   (0.7, 1]   (bad)         6    0.9%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.158  0.010   0.150    0.175  ...   152.0      98.0      17.0   1.01
pu        0.703  0.002   0.700    0.706  ...   143.0     112.0      55.0   0.99
mu        0.100  0.011   0.082    0.120  ...    73.0      67.0      59.0   1.00
mus       0.280  0.034   0.217    0.336  ...    59.0      53.0      42.0   1.03
gamma     0.539  0.079   0.403    0.671  ...   119.0      58.0      56.0   1.04
Is_begin  0.664  0.570   0.032    1.567  ...   109.0      88.0      80.0   1.01
Ia_begin  0.878  1.079   0.013    3.179  ...   104.0     101.0      69.0   1.10
E_begin   0.376  0.435   0.005    1.149  ...    86.0      79.0      67.0   1.03

[8 rows x 11 columns]