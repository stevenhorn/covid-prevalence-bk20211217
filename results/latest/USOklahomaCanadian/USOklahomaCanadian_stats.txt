0 Divergences 
Passed validation 
2021-12-09 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 635 log-likelihood matrix

         Estimate       SE
elpd_loo -3167.55    47.16
p_loo       49.46        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      587   92.4%
 (0.5, 0.7]   (ok)         32    5.0%
   (0.7, 1]   (bad)        13    2.0%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.161  0.009   0.150    0.179  ...   142.0      99.0      60.0   0.99
pu        0.704  0.004   0.700    0.712  ...   152.0     152.0      76.0   0.98
mu        0.100  0.015   0.073    0.128  ...    17.0      16.0      22.0   1.12
mus       0.162  0.027   0.117    0.212  ...    98.0      93.0      59.0   1.01
gamma     0.260  0.041   0.188    0.326  ...   152.0     152.0      40.0   1.03
Is_begin  0.773  0.675   0.001    2.160  ...    83.0      68.0      58.0   1.03
Ia_begin  0.632  0.475   0.002    1.574  ...    81.0      61.0      40.0   1.02
E_begin   0.425  0.389   0.002    1.145  ...    80.0      64.0      40.0   1.03

[8 rows x 11 columns]