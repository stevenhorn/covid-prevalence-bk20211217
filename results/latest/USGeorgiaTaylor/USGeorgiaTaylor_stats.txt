0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1409.14    81.65
p_loo       73.81        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      602   95.0%
 (0.5, 0.7]   (ok)         23    3.6%
   (0.7, 1]   (bad)         4    0.6%
   (1, Inf)   (very bad)    5    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.175  0.019   0.150    0.218  ...   130.0     112.0      58.0   0.99
pu        0.709  0.008   0.700    0.720  ...   152.0     152.0      36.0   1.12
mu        0.113  0.017   0.084    0.147  ...    50.0      50.0      52.0   1.00
mus       0.238  0.031   0.190    0.289  ...   121.0     135.0      77.0   0.99
gamma     0.427  0.075   0.264    0.540  ...    86.0      77.0      60.0   0.99
Is_begin  0.709  0.668   0.027    2.337  ...   130.0      97.0      43.0   1.02
Ia_begin  0.993  1.009   0.005    2.982  ...    68.0      54.0      60.0   1.02
E_begin   0.426  0.574   0.004    1.408  ...    62.0      67.0      59.0   1.02

[8 rows x 11 columns]