3 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2124.58    27.94
p_loo       42.97        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      571   90.1%
 (0.5, 0.7]   (ok)         50    7.9%
   (0.7, 1]   (bad)        13    2.1%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.223  0.056   0.154    0.336  ...    16.0      62.0      57.0   1.08
pu        0.729  0.023   0.700    0.773  ...    23.0      24.0      59.0   1.08
mu        0.121  0.022   0.092    0.163  ...     4.0       4.0      19.0   1.66
mus       0.203  0.036   0.144    0.269  ...    70.0      93.0      57.0   1.03
gamma     0.288  0.054   0.195    0.386  ...   117.0     116.0      60.0   1.01
Is_begin  4.644  2.492   0.782    8.747  ...    83.0      69.0      80.0   1.03
Ia_begin  8.215  4.913   2.517   18.550  ...    47.0      35.0      39.0   1.04
E_begin   9.352  6.827   0.724   22.867  ...    30.0      17.0      41.0   1.11

[8 rows x 11 columns]