0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -1118.78    40.25
p_loo       44.03        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      573   90.7%
 (0.5, 0.7]   (ok)         54    8.5%
   (0.7, 1]   (bad)         5    0.8%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.234  0.044   0.163    0.309  ...    44.0      48.0      86.0   1.01
pu        0.727  0.019   0.703    0.761  ...    45.0      53.0      56.0   1.03
mu        0.156  0.025   0.120    0.214  ...    32.0      31.0      49.0   1.04
mus       0.218  0.038   0.153    0.270  ...    93.0     125.0      76.0   1.04
gamma     0.339  0.055   0.248    0.437  ...    54.0      73.0      24.0   1.07
Is_begin  0.495  0.478   0.009    1.388  ...    50.0      46.0      67.0   1.04
Ia_begin  0.988  0.946   0.015    2.992  ...    90.0      94.0      60.0   1.01
E_begin   0.455  0.493   0.009    1.183  ...    20.0      17.0      88.0   1.10

[8 rows x 11 columns]