0 Divergences 
Passed validation 
2021-12-09 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 635 log-likelihood matrix

         Estimate       SE
elpd_loo -2769.61    39.42
p_loo       40.69        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      597   94.0%
 (0.5, 0.7]   (ok)         33    5.2%
   (0.7, 1]   (bad)         4    0.6%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.179  0.025   0.151    0.223  ...    77.0      68.0      96.0   1.01
pu        0.712  0.008   0.701    0.725  ...   152.0     152.0      35.0   1.08
mu        0.143  0.015   0.123    0.173  ...    62.0      60.0      60.0   0.99
mus       0.173  0.020   0.142    0.213  ...    80.0      74.0      55.0   1.01
gamma     0.318  0.043   0.253    0.399  ...    86.0      81.0      87.0   1.06
Is_begin  0.918  0.510   0.053    1.803  ...   138.0     118.0      60.0   0.99
Ia_begin  1.562  1.209   0.048    3.835  ...   121.0      88.0      60.0   0.99
E_begin   1.975  2.129   0.034    5.295  ...   103.0      76.0      54.0   0.99

[8 rows x 11 columns]