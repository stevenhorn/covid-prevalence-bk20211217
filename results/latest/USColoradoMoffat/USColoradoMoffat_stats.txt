0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1485.53    34.96
p_loo       39.63        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      580   91.5%
 (0.5, 0.7]   (ok)         42    6.6%
   (0.7, 1]   (bad)        11    1.7%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.177  0.020   0.150    0.216  ...   152.0     152.0      53.0   0.99
pu        0.708  0.007   0.700    0.722  ...   134.0     137.0     100.0   0.99
mu        0.112  0.015   0.087    0.141  ...    59.0      58.0      60.0   1.01
mus       0.187  0.035   0.131    0.254  ...   104.0     108.0      58.0   0.98
gamma     0.254  0.045   0.178    0.323  ...   152.0     152.0      79.0   0.98
Is_begin  0.844  0.729   0.033    2.139  ...   123.0     107.0      58.0   1.03
Ia_begin  1.243  1.301   0.020    3.412  ...    65.0      74.0      60.0   1.02
E_begin   0.756  1.252   0.029    2.730  ...    95.0     117.0      42.0   0.99

[8 rows x 11 columns]