0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -2764.53    75.11
p_loo       72.89        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      595   94.1%
 (0.5, 0.7]   (ok)         30    4.7%
   (0.7, 1]   (bad)         2    0.3%
   (1, Inf)   (very bad)    5    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.161  0.010   0.150    0.174  ...   133.0      73.0      57.0   1.02
pu        0.704  0.003   0.700    0.710  ...    73.0      62.0      31.0   0.99
mu        0.095  0.012   0.077    0.117  ...    17.0      16.0      44.0   1.11
mus       0.222  0.043   0.150    0.312  ...    67.0      66.0      58.0   0.99
gamma     0.425  0.065   0.309    0.541  ...    43.0      44.0      32.0   1.00
Is_begin  0.664  0.498   0.018    1.344  ...    71.0      65.0      91.0   1.00
Ia_begin  1.226  1.210   0.020    4.152  ...    62.0      73.0      59.0   0.98
E_begin   0.554  0.584   0.008    1.785  ...    78.0      68.0     100.0   1.04

[8 rows x 11 columns]