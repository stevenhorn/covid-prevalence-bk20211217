0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2263.21    39.23
p_loo       41.42        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      571   90.1%
 (0.5, 0.7]   (ok)         50    7.9%
   (0.7, 1]   (bad)        11    1.7%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.247  0.050   0.159    0.318  ...    97.0     105.0      60.0   1.09
pu        0.774  0.021   0.743    0.816  ...    26.0      28.0      26.0   1.09
mu        0.149  0.018   0.108    0.176  ...    47.0      48.0      53.0   1.04
mus       0.180  0.031   0.120    0.226  ...    75.0      47.0      56.0   1.04
gamma     0.248  0.036   0.191    0.320  ...   136.0     140.0     100.0   1.02
Is_begin  1.137  0.945   0.015    2.750  ...    91.0      69.0      83.0   1.00
Ia_begin  3.102  2.668   0.008    7.596  ...   118.0      97.0     100.0   0.99
E_begin   1.403  1.301   0.026    3.606  ...   130.0     119.0      96.0   1.00

[8 rows x 11 columns]