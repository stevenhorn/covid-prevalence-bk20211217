0 Divergences 
Passed validation 
2021-12-16 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 637 log-likelihood matrix

         Estimate       SE
elpd_loo -1268.68    37.43
p_loo       59.86        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      574   90.1%
 (0.5, 0.7]   (ok)         54    8.5%
   (0.7, 1]   (bad)         7    1.1%
   (1, Inf)   (very bad)    2    0.3%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.217   0.048   0.151    0.305  ...    63.0      62.0      31.0   1.01
pu         0.726   0.022   0.700    0.770  ...    90.0      83.0      49.0   1.01
mu         0.260   0.027   0.212    0.307  ...    36.0      34.0      53.0   1.10
mus        0.359   0.034   0.300    0.420  ...   115.0     129.0      96.0   0.99
gamma      0.612   0.086   0.475    0.767  ...    72.0      79.0      57.0   1.03
Is_begin   6.334   4.653   0.288   16.164  ...    77.0      91.0      40.0   1.01
Ia_begin  17.315  16.567   0.526   55.158  ...    27.0      35.0      60.0   1.07
E_begin    7.212   5.951   0.218   18.316  ...    96.0      87.0      83.0   0.99

[8 rows x 11 columns]