0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1799.68    41.94
p_loo       43.63        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      590   93.1%
 (0.5, 0.7]   (ok)         38    6.0%
   (0.7, 1]   (bad)         4    0.6%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.197  0.035   0.151    0.257  ...    53.0      54.0      59.0   1.01
pu        0.716  0.015   0.700    0.746  ...    66.0      57.0      57.0   1.05
mu        0.148  0.025   0.096    0.185  ...     5.0       5.0      40.0   1.41
mus       0.190  0.029   0.140    0.253  ...    75.0      73.0      59.0   1.01
gamma     0.264  0.045   0.198    0.341  ...   108.0     119.0      93.0   1.00
Is_begin  0.643  0.574   0.006    1.715  ...    60.0      14.0      59.0   1.13
Ia_begin  1.496  1.595   0.012    4.556  ...    81.0      74.0      96.0   1.01
E_begin   0.565  0.850   0.006    1.458  ...    97.0      66.0      59.0   1.05

[8 rows x 11 columns]