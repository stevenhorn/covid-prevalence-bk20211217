0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -1591.88    34.65
p_loo       37.16        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      601   95.1%
 (0.5, 0.7]   (ok)         22    3.5%
   (0.7, 1]   (bad)         8    1.3%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.251  0.055   0.167    0.346  ...   152.0     152.0      68.0   1.06
pu        0.811  0.019   0.774    0.839  ...   152.0     152.0      99.0   0.98
mu        0.118  0.024   0.076    0.160  ...    44.0      38.0      17.0   1.04
mus       0.170  0.039   0.105    0.235  ...    77.0     101.0      74.0   1.03
gamma     0.207  0.045   0.142    0.296  ...   136.0     152.0      91.0   1.00
Is_begin  0.764  0.903   0.003    2.718  ...   103.0      79.0      45.0   1.01
Ia_begin  1.807  1.493   0.041    4.754  ...   109.0     125.0      59.0   0.99
E_begin   0.713  0.705   0.015    1.923  ...   116.0     152.0      96.0   1.00

[8 rows x 11 columns]