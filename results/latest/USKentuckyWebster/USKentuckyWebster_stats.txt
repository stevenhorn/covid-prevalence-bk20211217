0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1657.18    54.65
p_loo       46.98        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      586   92.4%
 (0.5, 0.7]   (ok)         35    5.5%
   (0.7, 1]   (bad)        10    1.6%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.163  0.011   0.151    0.184  ...    72.0     105.0      60.0   1.05
pu        0.704  0.003   0.700    0.709  ...   152.0     124.0      43.0   1.02
mu        0.156  0.017   0.124    0.183  ...    51.0      58.0     100.0   1.02
mus       0.218  0.034   0.158    0.276  ...    36.0      30.0      43.0   1.05
gamma     0.249  0.049   0.170    0.337  ...    38.0      49.0      59.0   1.01
Is_begin  0.562  0.481   0.003    1.401  ...    18.0      12.0      27.0   1.14
Ia_begin  1.201  1.301   0.056    4.397  ...    58.0      59.0      40.0   1.02
E_begin   0.440  0.561   0.004    1.634  ...    72.0      49.0      43.0   1.02

[8 rows x 11 columns]