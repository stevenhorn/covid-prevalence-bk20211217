0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2364.67    40.58
p_loo       45.63        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      592   93.4%
 (0.5, 0.7]   (ok)         33    5.2%
   (0.7, 1]   (bad)         7    1.1%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.164  0.012   0.150    0.189  ...   152.0     152.0      42.0   1.01
pu        0.704  0.004   0.700    0.711  ...   152.0     152.0      76.0   1.02
mu        0.108  0.017   0.069    0.131  ...    29.0      30.0      39.0   1.07
mus       0.172  0.024   0.135    0.216  ...   102.0      90.0      39.0   0.99
gamma     0.274  0.045   0.206    0.369  ...    77.0      75.0      56.0   1.05
Is_begin  1.022  0.960   0.015    3.098  ...    63.0      21.0      95.0   1.10
Ia_begin  0.760  0.570   0.006    1.877  ...   116.0     101.0      58.0   1.00
E_begin   0.772  0.833   0.022    2.167  ...   120.0     101.0      83.0   1.01

[8 rows x 11 columns]