18 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -1427.73    37.05
p_loo       42.97        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      581   91.9%
 (0.5, 0.7]   (ok)         41    6.5%
   (0.7, 1]   (bad)         8    1.3%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.178  0.026   0.150    0.227  ...    27.0      28.0      19.0   1.07
pu        0.709  0.008   0.700    0.724  ...    32.0      44.0      14.0   1.11
mu        0.149  0.030   0.100    0.202  ...    30.0      29.0      33.0   1.02
mus       0.220  0.036   0.167    0.296  ...    46.0      53.0      32.0   1.11
gamma     0.319  0.058   0.211    0.421  ...    43.0      47.0      38.0   1.04
Is_begin  0.562  0.771   0.001    2.447  ...    27.0      30.0      14.0   1.10
Ia_begin  0.972  1.304   0.023    3.173  ...    32.0      25.0      38.0   1.10
E_begin   0.312  0.347   0.006    0.982  ...    67.0      46.0      59.0   1.02

[8 rows x 11 columns]