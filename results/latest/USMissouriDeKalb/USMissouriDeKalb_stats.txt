0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1651.90    92.68
p_loo       64.09        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      588   92.7%
 (0.5, 0.7]   (ok)         37    5.8%
   (0.7, 1]   (bad)         6    0.9%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.168  0.016   0.150    0.200  ...   152.0     152.0      91.0   1.00
pu        0.706  0.006   0.700    0.718  ...   152.0     152.0      99.0   1.02
mu        0.118  0.013   0.093    0.140  ...    26.0      27.0      56.0   1.05
mus       0.290  0.038   0.237    0.367  ...   128.0     152.0      97.0   0.99
gamma     0.604  0.071   0.492    0.726  ...   152.0     152.0     100.0   1.03
Is_begin  0.759  0.903   0.001    2.605  ...   102.0      78.0      57.0   1.01
Ia_begin  1.119  1.227   0.006    3.529  ...    90.0      69.0      30.0   0.99
E_begin   0.519  0.587   0.005    1.966  ...    83.0      77.0      59.0   0.99

[8 rows x 11 columns]