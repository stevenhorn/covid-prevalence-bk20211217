0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -2565.05    54.24
p_loo       36.93        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      608   96.2%
 (0.5, 0.7]   (ok)         15    2.4%
   (0.7, 1]   (bad)         7    1.1%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.158  0.008   0.151    0.173  ...    16.0      16.0      14.0   1.07
pu        0.702  0.002   0.700    0.706  ...    32.0      19.0      43.0   1.11
mu        0.170  0.013   0.151    0.198  ...     4.0       4.0      15.0   1.52
mus       0.197  0.030   0.148    0.240  ...     3.0       3.0      15.0   2.08
gamma     0.188  0.014   0.161    0.213  ...     5.0       5.0      14.0   1.46
Is_begin  0.744  0.940   0.027    2.511  ...     6.0       6.0      25.0   1.33
Ia_begin  2.250  1.290   0.660    4.769  ...     9.0       6.0      30.0   1.38
E_begin   0.947  0.683   0.247    2.096  ...    27.0      21.0      40.0   1.08

[8 rows x 11 columns]