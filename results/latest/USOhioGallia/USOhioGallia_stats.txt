0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1652.03    39.42
p_loo       39.84        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      571   90.1%
 (0.5, 0.7]   (ok)         52    8.2%
   (0.7, 1]   (bad)        11    1.7%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.166  0.016   0.150    0.196  ...   104.0      69.0      30.0   1.05
pu        0.706  0.006   0.700    0.718  ...    74.0     152.0      60.0   1.02
mu        0.096  0.015   0.070    0.121  ...    65.0      67.0      59.0   1.03
mus       0.160  0.028   0.107    0.202  ...   150.0     152.0      38.0   1.00
gamma     0.205  0.033   0.144    0.267  ...    84.0      88.0      77.0   1.00
Is_begin  0.642  0.641   0.000    1.637  ...    84.0      62.0      31.0   1.01
Ia_begin  1.308  1.479   0.009    3.835  ...    98.0      69.0      60.0   1.04
E_begin   0.470  0.447   0.002    1.237  ...    51.0      69.0      58.0   1.01

[8 rows x 11 columns]