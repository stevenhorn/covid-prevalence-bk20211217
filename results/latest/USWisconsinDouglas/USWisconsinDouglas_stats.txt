0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -2068.90    37.24
p_loo       37.48        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      577   91.3%
 (0.5, 0.7]   (ok)         44    7.0%
   (0.7, 1]   (bad)         9    1.4%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.192  0.034   0.151    0.258  ...    58.0      45.0      54.0   1.04
pu        0.716  0.010   0.700    0.731  ...    23.0      22.0      31.0   1.07
mu        0.131  0.016   0.108    0.167  ...    22.0      17.0      66.0   1.07
mus       0.180  0.023   0.127    0.219  ...    82.0      81.0      32.0   1.02
gamma     0.271  0.050   0.169    0.343  ...    66.0      68.0      60.0   0.99
Is_begin  0.842  0.579   0.034    1.930  ...   113.0      91.0      57.0   1.01
Ia_begin  1.387  1.516   0.025    3.444  ...    23.0      10.0      38.0   1.19
E_begin   0.503  0.579   0.012    1.554  ...    70.0      41.0      61.0   1.05

[8 rows x 11 columns]