0 Divergences 
Passed validation 
2021-12-09 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 635 log-likelihood matrix

         Estimate       SE
elpd_loo -2078.51    49.23
p_loo       49.68        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      593   93.4%
 (0.5, 0.7]   (ok)         36    5.7%
   (0.7, 1]   (bad)         4    0.6%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.158  0.007   0.150    0.171  ...   152.0     152.0      91.0   1.00
pu        0.703  0.003   0.700    0.710  ...   152.0     152.0      56.0   0.99
mu        0.071  0.012   0.047    0.092  ...    10.0      11.0      24.0   1.30
mus       0.201  0.036   0.123    0.252  ...    80.0     100.0      42.0   1.03
gamma     0.297  0.049   0.221    0.380  ...   127.0     128.0      97.0   0.98
Is_begin  0.836  0.746   0.026    2.225  ...   102.0      75.0      59.0   1.00
Ia_begin  1.202  1.376   0.021    3.993  ...   124.0     135.0      62.0   1.01
E_begin   0.448  0.391   0.003    1.101  ...    77.0      65.0      52.0   1.02

[8 rows x 11 columns]