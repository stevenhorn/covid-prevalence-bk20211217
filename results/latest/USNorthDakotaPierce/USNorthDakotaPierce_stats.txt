0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1066.81    51.76
p_loo       50.33        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      580   91.5%
 (0.5, 0.7]   (ok)         40    6.3%
   (0.7, 1]   (bad)        12    1.9%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.203  0.032   0.154    0.257  ...    29.0      25.0      49.0   1.05
pu        0.718  0.012   0.701    0.734  ...    39.0      42.0      54.0   1.01
mu        0.131  0.020   0.097    0.169  ...    55.0      47.0      55.0   1.02
mus       0.212  0.034   0.153    0.278  ...    44.0      55.0      48.0   1.15
gamma     0.279  0.050   0.211    0.376  ...    50.0      45.0      58.0   1.00
Is_begin  0.705  0.790   0.009    1.838  ...   100.0      85.0      93.0   1.00
Ia_begin  1.042  1.019   0.021    2.796  ...    88.0      72.0      32.0   1.07
E_begin   0.411  0.395   0.004    1.238  ...    72.0      60.0      37.0   1.01

[8 rows x 11 columns]