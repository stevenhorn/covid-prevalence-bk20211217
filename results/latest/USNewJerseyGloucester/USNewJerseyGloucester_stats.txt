16 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2708.25    36.55
p_loo       48.45        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      567   89.4%
 (0.5, 0.7]   (ok)         50    7.9%
   (0.7, 1]   (bad)        15    2.4%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.175  0.017   0.151    0.197  ...    18.0      20.0      60.0   1.10
pu        0.705  0.006   0.700    0.716  ...    20.0       6.0      16.0   1.41
mu        0.108  0.011   0.090    0.133  ...     9.0       7.0      40.0   1.26
mus       0.184  0.033   0.137    0.249  ...     9.0      10.0      14.0   1.16
gamma     0.299  0.050   0.221    0.411  ...    98.0      93.0      60.0   1.00
Is_begin  1.052  0.599   0.133    1.971  ...    62.0      54.0      76.0   1.03
Ia_begin  3.139  1.769   0.594    5.676  ...    45.0      38.0      93.0   1.01
E_begin   3.237  3.545   0.210    9.236  ...    65.0      40.0      80.0   1.02

[8 rows x 11 columns]