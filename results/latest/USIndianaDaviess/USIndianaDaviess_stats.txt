0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1845.73    40.64
p_loo       46.09        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      582   91.8%
 (0.5, 0.7]   (ok)         45    7.1%
   (0.7, 1]   (bad)         6    0.9%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.175  0.024   0.151    0.220  ...   106.0      51.0      31.0   1.02
pu        0.708  0.008   0.700    0.725  ...   152.0     130.0      69.0   1.00
mu        0.125  0.019   0.091    0.163  ...    26.0      41.0      60.0   1.08
mus       0.188  0.023   0.143    0.233  ...   115.0     112.0      96.0   0.99
gamma     0.296  0.051   0.228    0.412  ...    37.0      50.0      24.0   1.02
Is_begin  0.548  0.504   0.049    1.628  ...   117.0     152.0      93.0   1.00
Ia_begin  1.094  1.156   0.004    3.135  ...    96.0     101.0      92.0   1.08
E_begin   0.503  0.619   0.001    1.667  ...    97.0      85.0      60.0   1.00

[8 rows x 11 columns]