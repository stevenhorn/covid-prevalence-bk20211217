0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -2107.76    38.48
p_loo       39.49        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      600   94.9%
 (0.5, 0.7]   (ok)         24    3.8%
   (0.7, 1]   (bad)         7    1.1%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.203  0.033   0.151    0.273  ...    71.0      76.0      59.0   1.02
pu        0.716  0.010   0.700    0.734  ...    41.0      31.0      27.0   1.09
mu        0.118  0.020   0.087    0.152  ...    35.0      31.0      58.0   1.00
mus       0.168  0.025   0.127    0.209  ...    68.0      68.0      83.0   1.02
gamma     0.208  0.043   0.128    0.290  ...    84.0     100.0      18.0   1.04
Is_begin  0.508  0.502   0.010    1.657  ...   106.0      80.0      93.0   1.00
Ia_begin  1.075  1.120   0.009    2.943  ...    43.0      43.0      56.0   1.03
E_begin   0.426  0.490   0.001    1.371  ...    46.0      21.0      87.0   1.07

[8 rows x 11 columns]