0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 633 log-likelihood matrix

         Estimate       SE
elpd_loo -4750.13    57.44
p_loo       70.35        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      575   90.8%
 (0.5, 0.7]   (ok)         41    6.5%
   (0.7, 1]   (bad)        16    2.5%
   (1, Inf)   (very bad)    1    0.2%

             mean       sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa          0.180    0.026   0.150    0.231  ...     9.0      11.0      22.0   1.18
pu          0.728    0.024   0.701    0.764  ...     3.0       3.0      25.0   2.07
mu          0.120    0.026   0.084    0.164  ...     3.0       3.0      15.0   1.93
mus         0.227    0.038   0.153    0.286  ...     4.0       4.0      53.0   1.55
gamma       0.518    0.058   0.416    0.621  ...    47.0      37.0      91.0   1.07
Is_begin  137.097   73.227  18.569  242.287  ...    45.0      33.0      20.0   1.08
Ia_begin  291.502  154.857  70.026  556.939  ...    37.0      25.0      59.0   1.06
E_begin   317.775  166.525  58.293  639.761  ...    49.0      30.0      57.0   1.06

[8 rows x 11 columns]