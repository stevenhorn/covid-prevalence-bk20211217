0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1717.21    37.54
p_loo       35.37        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      600   94.6%
 (0.5, 0.7]   (ok)         30    4.7%
   (0.7, 1]   (bad)         3    0.5%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.161  0.009   0.150    0.176  ...    94.0      85.0      26.0   1.04
pu        0.704  0.004   0.700    0.713  ...   152.0     107.0      36.0   1.00
mu        0.093  0.017   0.066    0.128  ...    80.0      79.0      60.0   1.01
mus       0.170  0.025   0.135    0.226  ...    92.0      94.0      72.0   1.04
gamma     0.260  0.050   0.167    0.339  ...   123.0     108.0      43.0   0.99
Is_begin  0.182  0.189   0.001    0.559  ...    68.0      61.0      93.0   0.99
Ia_begin  0.324  0.377   0.001    1.148  ...    91.0      80.0      59.0   1.01
E_begin   0.152  0.132   0.000    0.425  ...    55.0      41.0      58.0   1.03

[8 rows x 11 columns]