0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2492.91    38.74
p_loo       38.60        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      572   90.2%
 (0.5, 0.7]   (ok)         52    8.2%
   (0.7, 1]   (bad)         9    1.4%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.183  0.026   0.150    0.235  ...    41.0      33.0      15.0   1.05
pu        0.714  0.012   0.700    0.735  ...    56.0      56.0      60.0   1.04
mu        0.128  0.020   0.097    0.168  ...    13.0      14.0      88.0   1.10
mus       0.168  0.026   0.123    0.223  ...    15.0      16.0      49.0   1.10
gamma     0.220  0.040   0.155    0.284  ...    38.0      26.0      60.0   1.06
Is_begin  0.860  0.919   0.023    3.159  ...    90.0      65.0      93.0   1.03
Ia_begin  1.638  1.480   0.067    4.758  ...   119.0     107.0     100.0   1.00
E_begin   0.604  0.769   0.007    2.512  ...    70.0      64.0      74.0   1.06

[8 rows x 11 columns]