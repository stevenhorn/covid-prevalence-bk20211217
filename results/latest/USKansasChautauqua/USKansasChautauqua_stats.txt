0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1114.10    41.44
p_loo       47.52        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      590   93.1%
 (0.5, 0.7]   (ok)         36    5.7%
   (0.7, 1]   (bad)         6    0.9%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.162  0.010   0.150    0.180  ...    84.0      73.0      59.0   1.01
pu        0.704  0.004   0.700    0.708  ...   122.0      51.0      40.0   1.02
mu        0.124  0.021   0.087    0.160  ...    36.0      34.0      80.0   1.03
mus       0.198  0.033   0.146    0.261  ...    79.0      88.0      59.0   1.04
gamma     0.228  0.044   0.155    0.316  ...    44.0      46.0      59.0   1.01
Is_begin  0.648  0.631   0.006    1.699  ...    46.0      23.0      38.0   1.07
Ia_begin  0.979  1.091   0.012    3.510  ...    16.0      23.0      38.0   1.09
E_begin   0.406  0.509   0.001    1.395  ...    87.0      17.0      42.0   1.10

[8 rows x 11 columns]