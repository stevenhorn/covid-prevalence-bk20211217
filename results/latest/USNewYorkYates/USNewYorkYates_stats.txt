0 Divergences 
Passed validation 
2021-12-09 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 635 log-likelihood matrix

         Estimate       SE
elpd_loo -1237.79    36.02
p_loo       46.65        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      569   89.6%
 (0.5, 0.7]   (ok)         53    8.3%
   (0.7, 1]   (bad)        11    1.7%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.254  0.049   0.172    0.334  ...   152.0     152.0      40.0   1.04
pu        0.803  0.025   0.753    0.838  ...    76.0      75.0      93.0   1.01
mu        0.177  0.022   0.133    0.216  ...    31.0      35.0      97.0   1.04
mus       0.232  0.036   0.177    0.308  ...   150.0      90.0      24.0   1.07
gamma     0.319  0.047   0.232    0.384  ...    68.0      80.0      57.0   1.03
Is_begin  0.720  0.607   0.025    2.040  ...    59.0     123.0      60.0   1.04
Ia_begin  1.655  1.389   0.107    4.518  ...   116.0     124.0      60.0   1.00
E_begin   0.791  0.716   0.026    2.105  ...   140.0     144.0      50.0   0.99

[8 rows x 11 columns]