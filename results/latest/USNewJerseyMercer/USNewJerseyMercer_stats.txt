0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2674.92    31.90
p_loo       41.99        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      565   89.1%
 (0.5, 0.7]   (ok)         57    9.0%
   (0.7, 1]   (bad)        12    1.9%
   (1, Inf)   (very bad)    0    0.0%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.226   0.046   0.155    0.289  ...    84.0      86.0      96.0   1.00
pu         0.728   0.018   0.701    0.756  ...    29.0      28.0      37.0   1.08
mu         0.118   0.017   0.093    0.149  ...    22.0      21.0      40.0   1.07
mus        0.176   0.032   0.124    0.241  ...   101.0     105.0      79.0   1.01
gamma      0.291   0.046   0.212    0.369  ...   119.0     114.0      93.0   1.02
Is_begin   8.741   4.412   2.349   17.589  ...   121.0      93.0      56.0   1.07
Ia_begin  22.796   8.805   7.668   40.070  ...    71.0      82.0      96.0   1.02
E_begin   26.563  17.600   1.152   60.333  ...    44.0      33.0      24.0   1.04

[8 rows x 11 columns]