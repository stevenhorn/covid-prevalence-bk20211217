21 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -2136.77    53.96
p_loo       46.37        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      585   92.6%
 (0.5, 0.7]   (ok)         38    6.0%
   (0.7, 1]   (bad)         9    1.4%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.156  0.007   0.150    0.169  ...    44.0      19.0      59.0   1.08
pu        0.703  0.002   0.700    0.706  ...    34.0      37.0      32.0   1.01
mu        0.090  0.013   0.064    0.114  ...     7.0       7.0      22.0   1.24
mus       0.231  0.030   0.179    0.293  ...    34.0      36.0      31.0   1.03
gamma     0.312  0.050   0.235    0.400  ...    67.0      60.0      24.0   1.00
Is_begin  0.846  0.600   0.067    2.034  ...    45.0      36.0      16.0   1.18
Ia_begin  0.598  0.521   0.017    1.607  ...    20.0      15.0      49.0   1.14
E_begin   0.361  0.324   0.004    0.873  ...    28.0      29.0      60.0   1.04

[8 rows x 11 columns]