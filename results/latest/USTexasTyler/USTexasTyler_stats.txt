0 Divergences 
Failed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -2170.10    92.46
p_loo       70.99        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      603   95.4%
 (0.5, 0.7]   (ok)         21    3.3%
   (0.7, 1]   (bad)         4    0.6%
   (1, Inf)   (very bad)    4    0.6%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.161  0.010   0.151    0.184  ...    18.0      13.0      40.0   1.20
pu        0.704  0.003   0.700    0.709  ...    28.0      27.0      56.0   1.05
mu        0.153  0.013   0.129    0.170  ...    10.0      11.0      51.0   1.14
mus       0.289  0.033   0.237    0.350  ...    11.0      14.0      15.0   1.10
gamma     0.617  0.082   0.459    0.736  ...     8.0       8.0      53.0   1.24
Is_begin  0.441  0.528   0.024    1.354  ...    12.0       5.0      24.0   1.42
Ia_begin  0.933  0.729   0.066    2.095  ...     9.0       7.0      40.0   1.25
E_begin   0.445  0.351   0.077    1.028  ...    27.0      35.0      31.0   1.00

[8 rows x 11 columns]