0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 633 log-likelihood matrix

         Estimate       SE
elpd_loo -2700.20    45.29
p_loo       48.50        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      576   91.0%
 (0.5, 0.7]   (ok)         45    7.1%
   (0.7, 1]   (bad)        12    1.9%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.266  0.055   0.152    0.337  ...   152.0     152.0      77.0   1.11
pu        0.872  0.024   0.818    0.899  ...    61.0      68.0      60.0   1.03
mu        0.175  0.021   0.148    0.212  ...     7.0       8.0      36.0   1.21
mus       0.222  0.029   0.180    0.275  ...   107.0     146.0      60.0   1.03
gamma     0.376  0.055   0.283    0.496  ...    46.0      41.0      59.0   1.08
Is_begin  0.790  0.793   0.024    2.571  ...   108.0     124.0      60.0   1.06
Ia_begin  1.507  1.464   0.040    4.621  ...    88.0      65.0      60.0   1.05
E_begin   0.694  0.888   0.021    2.341  ...    91.0      91.0      79.0   1.00

[8 rows x 11 columns]