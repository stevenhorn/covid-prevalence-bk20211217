0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 633 log-likelihood matrix

         Estimate       SE
elpd_loo  -819.28    39.80
p_loo       63.16        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      575   90.8%
 (0.5, 0.7]   (ok)         44    7.0%
   (0.7, 1]   (bad)        12    1.9%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.267  0.049   0.185    0.348  ...    50.0      49.0      40.0   1.01
pu        0.853  0.026   0.811    0.897  ...    15.0      13.0      40.0   1.14
mu        0.189  0.012   0.167    0.209  ...    59.0      62.0      58.0   1.01
mus       0.453  0.050   0.375    0.530  ...    62.0      68.0      93.0   0.99
gamma     0.923  0.042   0.841    0.981  ...   106.0     106.0      60.0   1.01
Is_begin  1.310  1.129   0.110    3.514  ...    94.0      53.0      32.0   1.00
Ia_begin  2.454  2.313   0.090    6.856  ...    43.0      21.0      58.0   1.08
E_begin   1.340  1.317   0.019    4.226  ...    21.0      30.0      57.0   1.06

[8 rows x 11 columns]