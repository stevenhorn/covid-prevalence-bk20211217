3 Divergences 
Passed validation 
2021-12-15 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 642 log-likelihood matrix

         Estimate       SE
elpd_loo -3209.86    41.62
p_loo       29.91        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      587   91.4%
 (0.5, 0.7]   (ok)         49    7.6%
   (0.7, 1]   (bad)         6    0.9%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.237  0.050   0.154    0.326  ...    52.0      54.0      60.0   0.98
pu        0.886  0.010   0.868    0.900  ...    29.0      23.0      21.0   1.09
mu        0.186  0.022   0.142    0.223  ...     6.0       6.0      18.0   1.37
mus       0.191  0.032   0.148    0.253  ...   144.0     152.0      95.0   1.01
gamma     0.236  0.038   0.169    0.297  ...   125.0     117.0      40.0   1.06
Is_begin  2.960  1.780   0.343    6.174  ...   124.0     112.0      59.0   0.99
Ia_begin  8.573  4.572   0.306   16.839  ...    99.0     104.0      96.0   1.02
E_begin   9.030  8.004   0.210   23.696  ...    85.0      68.0      55.0   1.00

[8 rows x 11 columns]