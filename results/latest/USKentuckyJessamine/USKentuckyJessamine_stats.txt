0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2230.85    42.61
p_loo       38.26        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      590   93.1%
 (0.5, 0.7]   (ok)         38    6.0%
   (0.7, 1]   (bad)         4    0.6%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.176  0.019   0.151    0.212  ...   104.0     109.0      59.0   1.01
pu        0.708  0.009   0.700    0.724  ...    86.0     152.0      54.0   1.07
mu        0.118  0.020   0.083    0.151  ...     7.0       7.0      48.0   1.29
mus       0.184  0.027   0.140    0.235  ...   107.0     105.0      97.0   1.00
gamma     0.266  0.047   0.195    0.346  ...   139.0     119.0      57.0   1.00
Is_begin  0.714  0.772   0.001    1.734  ...    96.0      67.0      60.0   1.01
Ia_begin  1.171  1.113   0.007    3.219  ...   101.0      91.0      76.0   0.98
E_begin   0.463  0.514   0.015    1.257  ...    57.0      68.0      72.0   1.00

[8 rows x 11 columns]