0 Divergences 
Passed validation 
2021-12-09 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 635 log-likelihood matrix

         Estimate       SE
elpd_loo -2215.49    39.35
p_loo       40.18        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      584   92.0%
 (0.5, 0.7]   (ok)         43    6.8%
   (0.7, 1]   (bad)         6    0.9%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.222  0.039   0.155    0.293  ...    89.0     100.0      54.0   1.01
pu        0.721  0.016   0.700    0.753  ...   108.0     102.0      59.0   1.13
mu        0.148  0.022   0.111    0.177  ...    45.0      40.0      91.0   1.02
mus       0.187  0.035   0.139    0.256  ...    90.0     113.0      60.0   0.99
gamma     0.251  0.047   0.168    0.331  ...    83.0      85.0      60.0   0.99
Is_begin  0.694  0.827   0.011    2.721  ...    54.0      76.0      56.0   1.02
Ia_begin  1.316  1.354   0.024    3.693  ...    93.0      96.0      60.0   1.04
E_begin   0.539  0.703   0.001    1.806  ...    89.0      59.0      36.0   1.05

[8 rows x 11 columns]