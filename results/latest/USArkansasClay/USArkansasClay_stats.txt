0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1505.84    39.64
p_loo       44.01        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      589   92.9%
 (0.5, 0.7]   (ok)         37    5.8%
   (0.7, 1]   (bad)         8    1.3%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.160  0.009   0.150    0.175  ...   152.0      91.0      60.0   1.01
pu        0.703  0.003   0.700    0.709  ...   152.0     132.0      60.0   1.03
mu        0.092  0.015   0.062    0.114  ...     9.0       9.0      65.0   1.20
mus       0.176  0.035   0.117    0.241  ...    30.0      32.0      75.0   1.05
gamma     0.217  0.035   0.154    0.280  ...    20.0      21.0      38.0   1.08
Is_begin  0.385  0.472   0.002    1.302  ...    98.0      69.0      74.0   1.03
Ia_begin  0.615  0.688   0.001    2.112  ...    69.0      65.0      53.0   1.02
E_begin   0.288  0.357   0.008    0.789  ...    81.0      22.0      93.0   1.07

[8 rows x 11 columns]