0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1878.43    38.21
p_loo       36.14        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      584   92.1%
 (0.5, 0.7]   (ok)         38    6.0%
   (0.7, 1]   (bad)        12    1.9%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.239  0.048   0.153    0.311  ...   110.0      94.0      39.0   1.03
pu        0.731  0.017   0.700    0.764  ...    69.0      75.0      60.0   1.00
mu        0.152  0.027   0.110    0.199  ...     6.0       7.0      14.0   1.25
mus       0.183  0.028   0.130    0.222  ...    69.0      76.0      61.0   1.00
gamma     0.228  0.042   0.155    0.306  ...    25.0      27.0      36.0   1.07
Is_begin  0.799  0.840   0.001    2.728  ...   132.0      83.0      60.0   0.98
Ia_begin  1.485  1.248   0.033    4.004  ...    78.0      70.0      60.0   0.99
E_begin   0.717  0.824   0.006    2.394  ...    77.0      60.0      60.0   1.01

[8 rows x 11 columns]