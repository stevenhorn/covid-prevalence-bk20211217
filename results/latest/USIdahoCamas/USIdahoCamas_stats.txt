0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo  -363.87    60.57
p_loo       75.52        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      584   92.1%
 (0.5, 0.7]   (ok)         37    5.8%
   (0.7, 1]   (bad)        10    1.6%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.238  0.043   0.158    0.309  ...    29.0      29.0      87.0   1.04
pu        0.774  0.034   0.723    0.847  ...     3.0       4.0      27.0   1.85
mu        0.205  0.040   0.136    0.289  ...    38.0      40.0      81.0   1.00
mus       0.316  0.040   0.244    0.388  ...    19.0      19.0      21.0   1.09
gamma     0.392  0.061   0.303    0.530  ...    54.0      50.0      36.0   1.01
Is_begin  0.799  0.635   0.019    1.934  ...    74.0      70.0      97.0   1.01
Ia_begin  1.051  1.354   0.013    3.897  ...    52.0      76.0      40.0   1.04
E_begin   0.447  0.416   0.003    1.311  ...    71.0      32.0      51.0   1.02

[8 rows x 11 columns]