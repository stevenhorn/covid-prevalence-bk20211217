0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2379.38    36.12
p_loo       39.29        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      578   91.2%
 (0.5, 0.7]   (ok)         39    6.2%
   (0.7, 1]   (bad)        17    2.7%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.250  0.048   0.174    0.348  ...   144.0     135.0      56.0   1.02
pu        0.787  0.029   0.732    0.830  ...    39.0      42.0      33.0   1.01
mu        0.132  0.022   0.101    0.183  ...    31.0      32.0      56.0   1.04
mus       0.172  0.028   0.126    0.227  ...    53.0      55.0      93.0   1.01
gamma     0.232  0.037   0.157    0.282  ...   120.0     127.0      47.0   1.01
Is_begin  0.851  0.722   0.000    2.040  ...   140.0     115.0      60.0   0.98
Ia_begin  2.087  1.800   0.024    5.355  ...   122.0     117.0      79.0   1.00
E_begin   0.807  1.017   0.005    2.686  ...    89.0      76.0      58.0   1.01

[8 rows x 11 columns]