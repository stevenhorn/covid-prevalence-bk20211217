0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1246.26    98.23
p_loo       69.02        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      587   92.6%
 (0.5, 0.7]   (ok)         34    5.4%
   (0.7, 1]   (bad)        10    1.6%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.164  0.013   0.151    0.187  ...    82.0      71.0      93.0   0.99
pu        0.705  0.005   0.700    0.714  ...    94.0      47.0      59.0   1.05
mu        0.127  0.019   0.091    0.159  ...    12.0      11.0      54.0   1.16
mus       0.331  0.038   0.269    0.411  ...    68.0      67.0      57.0   0.99
gamma     0.654  0.114   0.509    0.893  ...    67.0      63.0      60.0   1.00
Is_begin  0.608  0.847   0.000    2.485  ...   105.0      35.0      43.0   1.02
Ia_begin  0.992  0.921   0.019    2.336  ...    78.0      62.0      60.0   1.01
E_begin   0.430  0.488   0.005    1.372  ...    80.0      69.0      54.0   1.00

[8 rows x 11 columns]