0 Divergences 
Passed validation 
2021-12-09 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 635 log-likelihood matrix

         Estimate       SE
elpd_loo -1944.09    33.93
p_loo       42.88        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      582   91.7%
 (0.5, 0.7]   (ok)         45    7.1%
   (0.7, 1]   (bad)         7    1.1%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.196  0.033   0.152    0.258  ...    85.0      87.0      60.0   1.03
pu        0.711  0.010   0.701    0.732  ...    82.0      80.0      48.0   1.05
mu        0.145  0.019   0.108    0.176  ...    31.0      26.0      37.0   1.06
mus       0.207  0.030   0.168    0.282  ...    67.0      67.0      53.0   1.01
gamma     0.312  0.050   0.218    0.396  ...   152.0     152.0      60.0   0.98
Is_begin  0.467  0.467   0.002    1.237  ...    70.0      36.0      31.0   1.04
Ia_begin  0.796  0.994   0.023    2.871  ...    98.0      68.0      76.0   1.04
E_begin   0.367  0.477   0.007    1.089  ...   111.0      71.0     100.0   1.00

[8 rows x 11 columns]