0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 633 log-likelihood matrix

         Estimate       SE
elpd_loo -1418.70    38.34
p_loo       49.88        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      577   91.2%
 (0.5, 0.7]   (ok)         51    8.1%
   (0.7, 1]   (bad)         3    0.5%
   (1, Inf)   (very bad)    2    0.3%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.202   0.035   0.153    0.270  ...    59.0      79.0      60.0   1.06
pu         0.720   0.017   0.700    0.752  ...   142.0     152.0      74.0   1.01
mu         0.168   0.021   0.133    0.206  ...     5.0       5.0      19.0   1.43
mus        0.259   0.035   0.200    0.331  ...     9.0       9.0      50.0   1.19
gamma      0.337   0.062   0.240    0.460  ...    63.0      55.0      51.0   1.04
Is_begin  10.877   7.011   0.489   23.726  ...    23.0      19.0      40.0   1.09
Ia_begin  26.470  14.508   1.980   54.616  ...    63.0      56.0      40.0   0.99
E_begin   23.523  16.378   0.016   48.063  ...    29.0      25.0      14.0   1.03

[8 rows x 11 columns]