0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -1370.39    81.45
p_loo       87.47        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      598   94.6%
 (0.5, 0.7]   (ok)         24    3.8%
   (0.7, 1]   (bad)         3    0.5%
   (1, Inf)   (very bad)    7    1.1%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.216  0.048   0.151    0.305  ...    89.0      74.0      58.0   1.04
pu        0.725  0.019   0.701    0.763  ...    86.0      74.0      60.0   1.00
mu        0.142  0.027   0.100    0.188  ...    52.0      58.0      38.0   1.00
mus       0.244  0.037   0.187    0.318  ...    64.0      75.0      60.0   1.01
gamma     0.320  0.041   0.252    0.388  ...    40.0      34.0      75.0   1.04
Is_begin  0.328  0.401   0.004    1.332  ...    81.0      79.0      75.0   1.01
Ia_begin  0.521  0.790   0.004    1.955  ...    86.0      91.0      52.0   1.07
E_begin   0.243  0.346   0.001    0.842  ...   105.0      90.0      88.0   1.01

[8 rows x 11 columns]