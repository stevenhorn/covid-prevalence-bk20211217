0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1047.23    37.03
p_loo       45.61        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      576   90.9%
 (0.5, 0.7]   (ok)         42    6.6%
   (0.7, 1]   (bad)        13    2.1%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.225  0.041   0.152    0.293  ...    69.0      66.0      14.0   1.02
pu        0.724  0.018   0.701    0.755  ...    56.0      51.0      60.0   1.00
mu        0.147  0.025   0.093    0.194  ...    24.0      34.0      45.0   1.05
mus       0.213  0.034   0.160    0.272  ...   117.0      74.0      60.0   1.02
gamma     0.269  0.046   0.192    0.338  ...    97.0     128.0      60.0   1.02
Is_begin  0.555  0.613   0.004    1.783  ...   133.0      94.0      25.0   0.99
Ia_begin  1.012  1.150   0.013    3.578  ...   101.0      48.0      22.0   1.03
E_begin   0.393  0.405   0.013    1.283  ...    56.0      59.0      59.0   1.04

[8 rows x 11 columns]