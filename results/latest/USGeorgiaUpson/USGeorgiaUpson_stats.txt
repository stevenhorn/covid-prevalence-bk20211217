0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2102.92    89.94
p_loo       60.42        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      601   94.8%
 (0.5, 0.7]   (ok)         23    3.6%
   (0.7, 1]   (bad)         8    1.3%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.155  0.005   0.150    0.164  ...   126.0     103.0      39.0   1.03
pu        0.702  0.002   0.700    0.705  ...   152.0     152.0      44.0   1.02
mu        0.088  0.009   0.073    0.104  ...    29.0      29.0      51.0   1.03
mus       0.310  0.037   0.252    0.372  ...    18.0      15.0      57.0   1.10
gamma     0.759  0.090   0.634    0.929  ...    55.0      78.0      56.0   1.02
Is_begin  0.835  0.743   0.027    2.068  ...    78.0      77.0      61.0   1.05
Ia_begin  1.125  1.598   0.016    3.523  ...    56.0      64.0      60.0   1.04
E_begin   0.641  0.833   0.009    2.245  ...    46.0      59.0      60.0   1.04

[8 rows x 11 columns]