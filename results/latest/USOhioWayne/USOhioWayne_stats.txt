0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2197.87    48.86
p_loo       46.21        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      576   90.9%
 (0.5, 0.7]   (ok)         47    7.4%
   (0.7, 1]   (bad)         9    1.4%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.201  0.034   0.150    0.262  ...   101.0      93.0      57.0   1.01
pu        0.714  0.012   0.700    0.735  ...    93.0      80.0      58.0   1.01
mu        0.129  0.018   0.105    0.171  ...    36.0      39.0      46.0   1.06
mus       0.197  0.040   0.132    0.264  ...    98.0     138.0      60.0   0.98
gamma     0.267  0.049   0.194    0.335  ...   110.0     108.0      59.0   1.02
Is_begin  1.040  0.814   0.000    2.757  ...   113.0      76.0      19.0   1.02
Ia_begin  2.297  2.054   0.036    5.619  ...   120.0     116.0      60.0   1.01
E_begin   1.266  1.024   0.091    3.143  ...   101.0      95.0      59.0   1.01

[8 rows x 11 columns]