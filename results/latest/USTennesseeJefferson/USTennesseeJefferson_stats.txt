0 Divergences 
Failed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -2412.07    52.51
p_loo       47.58        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      598   94.6%
 (0.5, 0.7]   (ok)         24    3.8%
   (0.7, 1]   (bad)         7    1.1%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.173  0.017   0.151    0.202  ...    91.0      73.0      40.0   1.01
pu        0.708  0.007   0.700    0.719  ...   115.0      95.0      48.0   1.01
mu        0.100  0.016   0.076    0.136  ...    33.0      32.0      60.0   1.02
mus       0.174  0.026   0.125    0.220  ...   125.0     152.0      95.0   1.04
gamma     0.231  0.036   0.175    0.296  ...    83.0      93.0      91.0   0.99
Is_begin  0.569  0.523   0.007    1.649  ...   152.0     126.0      55.0   0.99
Ia_begin  0.948  1.288   0.016    2.452  ...    97.0     115.0      43.0   1.02
E_begin   0.393  0.366   0.001    1.047  ...   125.0     110.0      93.0   1.03

[8 rows x 11 columns]