0 Divergences 
Passed validation 
2021-12-09 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 635 log-likelihood matrix

         Estimate       SE
elpd_loo -2032.68    43.39
p_loo       35.56        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      607   95.6%
 (0.5, 0.7]   (ok)         25    3.9%
   (0.7, 1]   (bad)         2    0.3%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.156  0.006   0.150    0.169  ...    23.0      25.0      40.0   1.06
pu        0.703  0.003   0.700    0.709  ...    64.0      53.0      54.0   1.03
mu        0.097  0.011   0.080    0.124  ...     5.0       5.0      15.0   1.37
mus       0.160  0.028   0.125    0.221  ...    25.0      26.0      59.0   1.06
gamma     0.285  0.044   0.209    0.369  ...    17.0      13.0      42.0   1.14
Is_begin  0.599  0.564   0.009    1.554  ...    28.0      11.0      20.0   1.17
Ia_begin  0.969  0.833   0.055    2.599  ...    58.0      54.0      59.0   0.99
E_begin   0.480  0.496   0.014    1.471  ...    49.0      49.0      32.0   1.03

[8 rows x 11 columns]