0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1458.42    53.46
p_loo       49.60        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      592   93.4%
 (0.5, 0.7]   (ok)         32    5.0%
   (0.7, 1]   (bad)         8    1.3%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.173  0.022   0.150    0.217  ...    21.0      11.0      40.0   1.18
pu        0.708  0.009   0.700    0.723  ...     8.0       4.0      23.0   1.53
mu        0.198  0.049   0.127    0.272  ...     3.0       4.0      40.0   1.79
mus       0.244  0.041   0.169    0.314  ...     5.0       5.0      29.0   1.45
gamma     0.241  0.068   0.165    0.376  ...     4.0       4.0      36.0   1.79
Is_begin  0.371  0.369   0.018    0.940  ...    32.0      27.0      34.0   1.12
Ia_begin  1.323  1.163   0.001    2.798  ...    14.0       8.0      35.0   1.23
E_begin   0.499  0.390   0.005    1.314  ...     9.0       8.0      39.0   1.20

[8 rows x 11 columns]