0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -1416.32    61.68
p_loo       55.99        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      591   93.5%
 (0.5, 0.7]   (ok)         34    5.4%
   (0.7, 1]   (bad)         4    0.6%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.261  0.054   0.166    0.347  ...    40.0      36.0      40.0   1.02
pu        0.809  0.019   0.779    0.843  ...    35.0      41.0      44.0   1.04
mu        0.141  0.023   0.098    0.181  ...    25.0      38.0      56.0   1.06
mus       0.196  0.040   0.127    0.262  ...    21.0      24.0      59.0   1.06
gamma     0.267  0.065   0.166    0.379  ...    13.0      12.0      59.0   1.16
Is_begin  0.632  0.622   0.010    1.752  ...    68.0     152.0      88.0   1.00
Ia_begin  1.327  1.214   0.016    4.195  ...    27.0      24.0      59.0   1.05
E_begin   0.707  0.926   0.019    2.026  ...    58.0      79.0      49.0   1.03

[8 rows x 11 columns]