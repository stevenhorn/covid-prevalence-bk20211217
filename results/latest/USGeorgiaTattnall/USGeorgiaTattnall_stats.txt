0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1857.30    58.48
p_loo       50.75        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      595   93.8%
 (0.5, 0.7]   (ok)         27    4.3%
   (0.7, 1]   (bad)         9    1.4%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.243  0.042   0.170    0.309  ...    66.0      73.0      65.0   1.01
pu        0.718  0.016   0.700    0.745  ...    18.0      12.0      49.0   1.16
mu        0.114  0.014   0.092    0.139  ...    24.0      29.0      88.0   1.02
mus       0.166  0.021   0.134    0.208  ...    22.0      23.0      49.0   1.09
gamma     0.253  0.029   0.200    0.298  ...    25.0      32.0      36.0   1.04
Is_begin  0.490  0.441   0.010    1.412  ...    28.0      14.0      23.0   1.13
Ia_begin  0.988  0.998   0.006    3.075  ...    52.0      23.0      40.0   1.02
E_begin   0.354  0.368   0.004    1.011  ...    46.0      33.0      55.0   1.02

[8 rows x 11 columns]