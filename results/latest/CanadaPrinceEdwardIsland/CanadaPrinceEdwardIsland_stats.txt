0 Divergences 
Passed validation 
2021-12-15 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 642 log-likelihood matrix

         Estimate       SE
elpd_loo -1209.56    51.81
p_loo       66.28        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      605   94.2%
 (0.5, 0.7]   (ok)         25    3.9%
   (0.7, 1]   (bad)         8    1.2%
   (1, Inf)   (very bad)    4    0.6%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.278  0.038   0.211    0.342  ...    35.0      38.0      59.0   1.03
pu        0.892  0.009   0.876    0.900  ...    47.0      29.0      33.0   1.01
mu        0.287  0.021   0.254    0.326  ...     5.0       5.0      54.0   1.41
mus       0.268  0.049   0.173    0.345  ...     5.0       6.0      32.0   1.34
gamma     0.304  0.051   0.213    0.401  ...    20.0      14.0      60.0   1.12
Is_begin  2.168  1.093   0.315    3.993  ...     8.0       8.0      22.0   1.23
Ia_begin  0.786  0.689   0.048    2.112  ...    80.0      48.0      60.0   1.02
E_begin   1.888  1.544   0.087    4.950  ...    10.0      10.0      59.0   1.17

[8 rows x 11 columns]