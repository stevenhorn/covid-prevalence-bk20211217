0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1984.93    36.40
p_loo       37.76        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      594   93.7%
 (0.5, 0.7]   (ok)         30    4.7%
   (0.7, 1]   (bad)         9    1.4%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.221  0.039   0.159    0.284  ...   123.0     120.0      81.0   1.03
pu        0.724  0.012   0.702    0.742  ...    65.0      71.0      95.0   1.03
mu        0.114  0.020   0.072    0.145  ...    22.0      23.0      22.0   1.17
mus       0.157  0.030   0.110    0.216  ...    95.0      89.0      55.0   1.03
gamma     0.185  0.033   0.136    0.246  ...    42.0      39.0      59.0   1.02
Is_begin  0.530  0.585   0.007    1.628  ...    89.0      81.0     100.0   1.00
Ia_begin  1.029  1.125   0.008    3.024  ...    59.0      51.0      96.0   1.02
E_begin   0.437  0.652   0.002    1.996  ...    79.0      62.0      40.0   1.02

[8 rows x 11 columns]