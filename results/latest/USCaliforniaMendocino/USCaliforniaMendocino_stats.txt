11 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2240.58    39.38
p_loo       43.16        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      584   92.1%
 (0.5, 0.7]   (ok)         42    6.6%
   (0.7, 1]   (bad)         6    0.9%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.250  0.053   0.164    0.335  ...    76.0      96.0      38.0   1.01
pu        0.795  0.025   0.750    0.832  ...    43.0      42.0      49.0   1.06
mu        0.135  0.021   0.097    0.164  ...     9.0      10.0      35.0   1.20
mus       0.177  0.034   0.125    0.234  ...    75.0      78.0      60.0   1.05
gamma     0.201  0.034   0.146    0.275  ...    88.0      89.0      48.0   1.01
Is_begin  0.835  0.710   0.067    2.333  ...    94.0      87.0      56.0   1.04
Ia_begin  1.875  1.573   0.088    4.775  ...    73.0     100.0      60.0   1.03
E_begin   0.739  0.901   0.006    2.614  ...    76.0      65.0      79.0   1.00

[8 rows x 11 columns]