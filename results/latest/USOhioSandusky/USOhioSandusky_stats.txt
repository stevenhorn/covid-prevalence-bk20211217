0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2060.54    68.83
p_loo       52.67        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      579   91.3%
 (0.5, 0.7]   (ok)         44    6.9%
   (0.7, 1]   (bad)        10    1.6%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.172  0.020   0.151    0.206  ...   152.0     101.0      43.0   1.05
pu        0.707  0.006   0.700    0.720  ...    42.0      53.0      44.0   0.99
mu        0.118  0.015   0.091    0.143  ...    20.0      23.0      40.0   1.07
mus       0.217  0.035   0.155    0.286  ...    86.0      76.0      69.0   1.00
gamma     0.294  0.045   0.207    0.362  ...    88.0      82.0      79.0   1.01
Is_begin  0.702  0.645   0.006    1.896  ...   108.0      99.0      43.0   1.01
Ia_begin  1.174  1.386   0.045    3.581  ...    59.0      61.0      40.0   1.05
E_begin   0.544  0.639   0.001    1.493  ...    97.0      72.0      61.0   1.08

[8 rows x 11 columns]