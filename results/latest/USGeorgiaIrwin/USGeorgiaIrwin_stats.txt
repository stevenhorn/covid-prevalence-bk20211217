0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1414.63    55.96
p_loo       56.89        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      591   93.2%
 (0.5, 0.7]   (ok)         36    5.7%
   (0.7, 1]   (bad)         5    0.8%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.189  0.034   0.150    0.254  ...    74.0      59.0      22.0   1.01
pu        0.712  0.010   0.701    0.732  ...   116.0      96.0      95.0   0.99
mu        0.139  0.017   0.106    0.169  ...    62.0      61.0      40.0   1.01
mus       0.222  0.033   0.163    0.275  ...   131.0     142.0      60.0   0.98
gamma     0.350  0.053   0.247    0.429  ...   152.0     152.0      59.0   0.98
Is_begin  0.843  0.829   0.015    2.412  ...   129.0     120.0      81.0   1.02
Ia_begin  1.199  1.201   0.014    3.647  ...    97.0      28.0      47.0   1.06
E_begin   0.618  0.850   0.005    2.419  ...    84.0      81.0      53.0   1.00

[8 rows x 11 columns]