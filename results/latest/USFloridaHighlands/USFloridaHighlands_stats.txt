0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2849.42    42.94
p_loo       33.80        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      596   94.0%
 (0.5, 0.7]   (ok)         31    4.9%
   (0.7, 1]   (bad)         4    0.6%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.167  0.014   0.150    0.195  ...   113.0      60.0      57.0   1.01
pu        0.707  0.005   0.700    0.717  ...   152.0     141.0      87.0   1.01
mu        0.139  0.024   0.080    0.177  ...     8.0       7.0      59.0   1.25
mus       0.173  0.032   0.116    0.224  ...    22.0      21.0      55.0   1.08
gamma     0.188  0.026   0.137    0.234  ...    29.0      28.0      40.0   1.09
Is_begin  0.674  0.812   0.003    2.601  ...    97.0      55.0      74.0   1.03
Ia_begin  1.358  1.241   0.048    3.952  ...    21.0      20.0      59.0   1.11
E_begin   0.567  0.558   0.002    1.511  ...    16.0       7.0      53.0   1.27

[8 rows x 11 columns]