0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1982.24   102.73
p_loo       63.80        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      583   92.0%
 (0.5, 0.7]   (ok)         44    6.9%
   (0.7, 1]   (bad)         5    0.8%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.158  0.007   0.150    0.168  ...    97.0     152.0      51.0   1.04
pu        0.703  0.003   0.700    0.709  ...   152.0     152.0      61.0   1.12
mu        0.101  0.011   0.080    0.120  ...   102.0     113.0      57.0   1.02
mus       0.316  0.033   0.259    0.372  ...   152.0     152.0      46.0   0.99
gamma     0.713  0.104   0.550    0.925  ...   152.0     152.0      47.0   1.00
Is_begin  0.599  0.425   0.090    1.474  ...    68.0     152.0      51.0   1.09
Ia_begin  0.860  0.840   0.001    2.593  ...   121.0     126.0      77.0   0.99
E_begin   0.396  0.410   0.001    1.169  ...   119.0      88.0      23.0   1.01

[8 rows x 11 columns]