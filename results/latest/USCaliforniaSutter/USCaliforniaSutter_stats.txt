0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2388.69    40.51
p_loo       44.19        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      584   92.1%
 (0.5, 0.7]   (ok)         41    6.5%
   (0.7, 1]   (bad)         8    1.3%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.235  0.053   0.154    0.337  ...    26.0      33.0      39.0   1.04
pu        0.723  0.018   0.700    0.758  ...    29.0      27.0      16.0   1.05
mu        0.138  0.020   0.099    0.173  ...     9.0       9.0      34.0   1.19
mus       0.160  0.028   0.116    0.215  ...    82.0      81.0     100.0   1.01
gamma     0.218  0.033   0.148    0.261  ...    74.0      68.0      74.0   0.99
Is_begin  0.508  0.424   0.005    1.336  ...    79.0      60.0      59.0   1.05
Ia_begin  1.393  1.363   0.028    4.104  ...    95.0      65.0      40.0   0.99
E_begin   0.490  0.646   0.002    1.908  ...    79.0      32.0      49.0   1.07

[8 rows x 11 columns]