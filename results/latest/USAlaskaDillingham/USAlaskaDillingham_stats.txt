0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1328.83    69.83
p_loo       64.07        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      594   93.7%
 (0.5, 0.7]   (ok)         30    4.7%
   (0.7, 1]   (bad)         6    0.9%
   (1, Inf)   (very bad)    4    0.6%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.158  0.007   0.150    0.171  ...   100.0      66.0      59.0   1.01
pu        0.704  0.004   0.700    0.712  ...    71.0      64.0      40.0   1.02
mu        0.097  0.016   0.075    0.132  ...    74.0      66.0      88.0   1.00
mus       0.252  0.046   0.182    0.340  ...    66.0      57.0      39.0   1.04
gamma     0.297  0.055   0.221    0.430  ...    61.0      57.0      87.0   1.04
Is_begin  0.463  0.557   0.003    1.657  ...    64.0      82.0      60.0   1.01
Ia_begin  0.545  0.870   0.001    2.303  ...    92.0      31.0      27.0   1.07
E_begin   0.200  0.343   0.001    0.974  ...    28.0      45.0      59.0   1.05

[8 rows x 11 columns]