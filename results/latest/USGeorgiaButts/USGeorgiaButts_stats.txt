0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2089.70    59.38
p_loo       58.71        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      581   91.6%
 (0.5, 0.7]   (ok)         38    6.0%
   (0.7, 1]   (bad)        11    1.7%
   (1, Inf)   (very bad)    4    0.6%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.168  0.017   0.150    0.201  ...    69.0      25.0      38.0   1.08
pu        0.706  0.006   0.700    0.718  ...    99.0      55.0      59.0   1.03
mu        0.104  0.013   0.085    0.127  ...    10.0      10.0      80.0   1.18
mus       0.194  0.037   0.144    0.264  ...    34.0      24.0      40.0   1.08
gamma     0.366  0.049   0.302    0.454  ...    68.0      77.0      96.0   1.02
Is_begin  0.800  0.793   0.041    2.167  ...    61.0      51.0      54.0   1.03
Ia_begin  0.982  0.950   0.002    3.036  ...    82.0      89.0      59.0   1.03
E_begin   0.627  0.848   0.007    2.623  ...    60.0      11.0      59.0   1.14

[8 rows x 11 columns]