0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2394.45    35.19
p_loo       33.08        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      608   95.9%
 (0.5, 0.7]   (ok)         22    3.5%
   (0.7, 1]   (bad)         4    0.6%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.167  0.016   0.150    0.195  ...   112.0     152.0      45.0   0.99
pu        0.705  0.005   0.700    0.715  ...   152.0     125.0      54.0   0.99
mu        0.103  0.020   0.067    0.134  ...    16.0      13.0      54.0   1.10
mus       0.176  0.035   0.124    0.235  ...    88.0     113.0      60.0   1.01
gamma     0.237  0.037   0.182    0.306  ...    89.0      84.0      60.0   1.01
Is_begin  0.859  0.718   0.005    2.358  ...    48.0      59.0      49.0   1.03
Ia_begin  0.494  0.465   0.027    1.453  ...    94.0      65.0      97.0   1.00
E_begin   0.380  0.371   0.004    1.079  ...    95.0      50.0      60.0   1.01

[8 rows x 11 columns]