0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -2548.21    47.22
p_loo       48.55        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      595   94.1%
 (0.5, 0.7]   (ok)         29    4.6%
   (0.7, 1]   (bad)         8    1.3%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.167  0.013   0.150    0.191  ...   127.0      90.0      60.0   0.99
pu        0.706  0.005   0.700    0.716  ...   152.0     152.0      53.0   0.99
mu        0.111  0.017   0.085    0.142  ...     5.0       6.0      57.0   1.39
mus       0.179  0.029   0.134    0.241  ...   152.0     144.0      86.0   1.01
gamma     0.255  0.044   0.181    0.336  ...   152.0     152.0      92.0   1.06
Is_begin  0.787  0.764   0.057    2.293  ...   104.0      95.0      93.0   1.05
Ia_begin  0.539  0.456   0.003    1.424  ...    79.0      54.0      40.0   1.00
E_begin   0.402  0.460   0.001    1.268  ...   104.0      42.0      60.0   1.01

[8 rows x 11 columns]