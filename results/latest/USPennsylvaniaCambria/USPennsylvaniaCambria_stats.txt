7 Divergences 
Passed validation 
2021-12-09 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 635 log-likelihood matrix

         Estimate       SE
elpd_loo -2316.12    37.70
p_loo       40.66        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      580   91.3%
 (0.5, 0.7]   (ok)         46    7.2%
   (0.7, 1]   (bad)         9    1.4%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.156  0.005   0.150    0.164  ...    75.0      58.0      59.0   1.04
pu        0.702  0.002   0.700    0.707  ...   149.0      73.0      22.0   1.01
mu        0.094  0.010   0.075    0.111  ...     6.0       7.0      53.0   1.27
mus       0.167  0.025   0.121    0.212  ...    27.0      27.0      57.0   1.06
gamma     0.251  0.036   0.188    0.319  ...   114.0     107.0      60.0   0.99
Is_begin  0.628  0.555   0.010    1.550  ...    58.0      56.0      40.0   0.98
Ia_begin  0.834  0.974   0.013    3.295  ...    33.0      48.0      48.0   1.01
E_begin   0.493  0.732   0.003    1.394  ...    69.0      10.0      36.0   1.18

[8 rows x 11 columns]