0 Divergences 
Passed validation 
2021-12-09 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 635 log-likelihood matrix

         Estimate       SE
elpd_loo -2247.51    35.38
p_loo       33.19        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      601   94.6%
 (0.5, 0.7]   (ok)         30    4.7%
   (0.7, 1]   (bad)         3    0.5%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.183  0.024   0.151    0.227  ...   120.0     113.0      85.0   1.00
pu        0.709  0.008   0.700    0.723  ...    75.0      61.0      59.0   1.05
mu        0.115  0.017   0.087    0.144  ...     7.0       7.0      60.0   1.25
mus       0.163  0.027   0.123    0.223  ...    85.0      86.0      60.0   1.00
gamma     0.204  0.034   0.148    0.272  ...    77.0      68.0      91.0   0.99
Is_begin  0.538  0.652   0.002    2.012  ...    39.0      29.0      59.0   1.07
Ia_begin  1.309  1.561   0.003    3.971  ...    60.0      46.0      43.0   1.00
E_begin   0.471  0.676   0.001    1.518  ...    84.0      56.0      60.0   1.03

[8 rows x 11 columns]