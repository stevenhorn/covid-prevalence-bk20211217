0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1638.60    72.24
p_loo       77.83        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      594   93.7%
 (0.5, 0.7]   (ok)         29    4.6%
   (0.7, 1]   (bad)         5    0.8%
   (1, Inf)   (very bad)    6    0.9%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.157  0.007   0.150    0.172  ...   152.0     128.0      14.0   1.09
pu        0.703  0.002   0.700    0.707  ...   152.0     112.0      91.0   1.01
mu        0.116  0.033   0.078    0.179  ...     4.0       4.0      24.0   1.66
mus       0.207  0.034   0.144    0.274  ...   108.0     105.0     102.0   0.98
gamma     0.239  0.065   0.115    0.352  ...     8.0       7.0      21.0   1.24
Is_begin  0.384  0.404   0.005    1.295  ...    59.0      16.0      38.0   1.08
Ia_begin  0.314  0.376   0.000    1.171  ...    43.0      21.0      40.0   1.05
E_begin   0.209  0.239   0.005    0.552  ...    83.0      64.0      77.0   1.00

[8 rows x 11 columns]