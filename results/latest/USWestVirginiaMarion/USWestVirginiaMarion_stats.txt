0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -1969.69    42.02
p_loo       43.29        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      595   94.1%
 (0.5, 0.7]   (ok)         31    4.9%
   (0.7, 1]   (bad)         6    0.9%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.186  0.026   0.153    0.237  ...   152.0     152.0      58.0   1.01
pu        0.711  0.009   0.700    0.729  ...    65.0      64.0      60.0   1.00
mu        0.128  0.021   0.092    0.163  ...    37.0      33.0      39.0   1.04
mus       0.187  0.036   0.125    0.259  ...    77.0     152.0      41.0   1.01
gamma     0.245  0.039   0.181    0.309  ...   113.0     120.0      72.0   0.98
Is_begin  0.837  0.661   0.068    2.077  ...    84.0     134.0      59.0   0.99
Ia_begin  1.561  1.523   0.057    4.514  ...    78.0      71.0      96.0   1.01
E_begin   0.581  0.602   0.004    1.737  ...    75.0      25.0      42.0   1.07

[8 rows x 11 columns]