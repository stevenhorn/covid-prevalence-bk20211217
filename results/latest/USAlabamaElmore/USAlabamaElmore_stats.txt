0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2518.64    43.40
p_loo       45.12        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      584   92.1%
 (0.5, 0.7]   (ok)         40    6.3%
   (0.7, 1]   (bad)         9    1.4%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.167  0.013   0.151    0.189  ...    94.0      75.0      88.0   0.99
pu        0.705  0.005   0.700    0.718  ...   115.0      45.0      34.0   1.06
mu        0.084  0.014   0.056    0.106  ...     6.0       6.0      24.0   1.41
mus       0.158  0.028   0.105    0.207  ...   112.0      95.0      37.0   1.02
gamma     0.202  0.033   0.154    0.273  ...   112.0     104.0      35.0   1.00
Is_begin  0.471  0.361   0.008    1.222  ...   152.0     108.0      40.0   0.99
Ia_begin  0.825  0.691   0.020    2.110  ...    87.0     113.0      60.0   0.99
E_begin   0.452  0.432   0.021    1.371  ...    72.0      88.0     100.0   1.02

[8 rows x 11 columns]