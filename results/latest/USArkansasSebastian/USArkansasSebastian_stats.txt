0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2580.92    49.51
p_loo       82.51        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      584   92.1%
 (0.5, 0.7]   (ok)         37    5.8%
   (0.7, 1]   (bad)        10    1.6%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.161  0.010   0.151    0.180  ...   147.0     117.0      80.0   1.00
pu        0.703  0.003   0.700    0.708  ...    50.0      17.0      95.0   1.11
mu        0.102  0.030   0.058    0.144  ...     3.0       4.0      53.0   1.81
mus       0.212  0.050   0.128    0.299  ...     6.0       7.0      35.0   1.31
gamma     0.201  0.041   0.138    0.277  ...     4.0       4.0      49.0   1.62
Is_begin  0.616  0.633   0.003    1.993  ...    89.0      55.0      24.0   1.03
Ia_begin  1.034  1.232   0.006    3.686  ...    57.0      39.0      91.0   1.04
E_begin   0.421  0.574   0.012    1.753  ...    20.0      18.0      83.0   1.10

[8 rows x 11 columns]