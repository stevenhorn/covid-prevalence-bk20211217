0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1869.77    48.10
p_loo       44.81        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      595   93.8%
 (0.5, 0.7]   (ok)         35    5.5%
   (0.7, 1]   (bad)         4    0.6%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.172  0.017   0.151    0.206  ...    64.0      59.0      36.0   1.04
pu        0.707  0.007   0.700    0.717  ...   152.0     112.0      58.0   1.00
mu        0.099  0.013   0.074    0.122  ...    11.0      12.0      18.0   1.19
mus       0.183  0.031   0.123    0.233  ...   151.0     152.0      33.0   0.99
gamma     0.297  0.052   0.199    0.391  ...   107.0     111.0      75.0   1.00
Is_begin  0.398  0.461   0.003    1.510  ...    81.0      48.0      60.0   1.02
Ia_begin  0.704  0.740   0.001    1.660  ...    43.0      36.0      40.0   1.11
E_begin   0.306  0.415   0.000    0.942  ...    94.0      45.0      53.0   1.01

[8 rows x 11 columns]