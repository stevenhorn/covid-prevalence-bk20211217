0 Divergences 
Failed validation 
2021-12-09 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 635 log-likelihood matrix

         Estimate       SE
elpd_loo -2076.94    76.95
p_loo       60.31        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      592   93.2%
 (0.5, 0.7]   (ok)         31    4.9%
   (0.7, 1]   (bad)        10    1.6%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.155  0.004   0.150    0.165  ...   132.0      48.0      40.0   1.03
pu        0.702  0.001   0.700    0.704  ...   150.0     101.0      39.0   1.04
mu        0.075  0.010   0.054    0.092  ...    99.0     100.0      65.0   1.00
mus       0.215  0.026   0.168    0.255  ...   126.0     152.0      59.0   0.99
gamma     0.380  0.063   0.285    0.503  ...    24.0     101.0      50.0   1.12
Is_begin  0.594  0.519   0.009    1.696  ...   117.0     127.0      93.0   1.01
Ia_begin  0.865  1.065   0.025    2.376  ...   109.0     110.0      96.0   1.00
E_begin   0.434  0.456   0.010    1.142  ...   108.0     117.0      59.0   1.02

[8 rows x 11 columns]