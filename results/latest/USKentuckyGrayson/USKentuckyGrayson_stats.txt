0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2007.00    37.40
p_loo       36.91        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      607   95.7%
 (0.5, 0.7]   (ok)         19    3.0%
   (0.7, 1]   (bad)         8    1.3%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.162  0.014   0.150    0.185  ...   145.0     113.0      19.0   1.08
pu        0.704  0.004   0.700    0.713  ...   141.0      90.0      60.0   1.02
mu        0.094  0.016   0.067    0.120  ...    41.0      38.0      93.0   1.01
mus       0.155  0.028   0.100    0.205  ...   140.0     147.0      83.0   1.06
gamma     0.221  0.042   0.152    0.289  ...    86.0     152.0      60.0   1.00
Is_begin  0.587  0.531   0.029    1.523  ...    95.0      61.0      38.0   1.03
Ia_begin  1.208  1.260   0.006    4.046  ...    86.0      47.0      56.0   1.05
E_begin   0.464  0.481   0.001    1.259  ...    22.0      19.0      54.0   1.10

[8 rows x 11 columns]