0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1471.50    95.84
p_loo       70.76        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      594   93.7%
 (0.5, 0.7]   (ok)         33    5.2%
   (0.7, 1]   (bad)         5    0.8%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.168  0.017   0.150    0.196  ...   152.0     152.0      74.0   1.05
pu        0.705  0.005   0.700    0.714  ...   108.0      84.0      37.0   1.02
mu        0.126  0.015   0.100    0.152  ...    16.0      14.0      59.0   1.12
mus       0.313  0.040   0.253    0.385  ...    90.0      86.0      84.0   0.99
gamma     0.738  0.087   0.595    0.875  ...    71.0      66.0      40.0   1.07
Is_begin  0.440  0.465   0.011    1.249  ...    89.0      95.0     100.0   0.99
Ia_begin  0.546  0.804   0.003    2.076  ...    84.0     130.0      73.0   1.01
E_begin   0.219  0.329   0.002    1.061  ...   101.0      91.0      99.0   1.01

[8 rows x 11 columns]