0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2159.49    38.16
p_loo       43.89        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      591   93.2%
 (0.5, 0.7]   (ok)         32    5.0%
   (0.7, 1]   (bad)         7    1.1%
   (1, Inf)   (very bad)    4    0.6%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.185  0.025   0.151    0.232  ...    53.0      40.0      24.0   1.05
pu        0.716  0.011   0.702    0.738  ...    60.0      69.0      43.0   1.03
mu        0.109  0.016   0.080    0.137  ...    27.0      29.0      41.0   1.01
mus       0.150  0.031   0.108    0.200  ...    23.0      18.0      30.0   1.09
gamma     0.195  0.038   0.136    0.264  ...    13.0      15.0      58.0   1.12
Is_begin  0.312  0.363   0.001    1.109  ...    29.0       8.0      43.0   1.22
Ia_begin  0.670  0.628   0.009    2.143  ...   106.0      63.0      33.0   0.99
E_begin   0.293  0.263   0.001    0.858  ...    75.0      44.0      45.0   1.03

[8 rows x 11 columns]