0 Divergences 
Passed validation 
2021-11-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 604 log-likelihood matrix

         Estimate       SE
elpd_loo   611.76   151.90
p_loo       99.39        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      589   97.5%
 (0.5, 0.7]   (ok)         10    1.7%
   (0.7, 1]   (bad)         1    0.2%
   (1, Inf)   (very bad)    4    0.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.250  0.057   0.166    0.329  ...   105.0      99.0      53.0   1.02
pu        0.779  0.029   0.723    0.821  ...    76.0      79.0      44.0   1.01
mu        0.133  0.028   0.082    0.190  ...   143.0     137.0      72.0   1.03
mus       0.396  0.044   0.306    0.466  ...    90.0      86.0      53.0   1.00
gamma     0.423  0.048   0.342    0.518  ...    64.0      60.0      33.0   1.05
Is_begin  0.171  0.213   0.003    0.489  ...    52.0      17.0      38.0   1.09
Ia_begin  0.135  0.185   0.002    0.403  ...    50.0      45.0      60.0   1.02
E_begin   0.082  0.108   0.001    0.320  ...    73.0      54.0      96.0   1.01

[8 rows x 11 columns]