0 Divergences 
Passed validation 
2021-12-16 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 643 log-likelihood matrix

         Estimate       SE
elpd_loo -2707.29    34.10
p_loo       37.20        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      584   90.8%
 (0.5, 0.7]   (ok)         44    6.8%
   (0.7, 1]   (bad)        15    2.3%
   (1, Inf)   (very bad)    0    0.0%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.235   0.060   0.151    0.342  ...    10.0      15.0      59.0   1.13
pu         0.817   0.051   0.730    0.898  ...     6.0       6.0      36.0   1.35
mu         0.158   0.016   0.135    0.190  ...    27.0      26.0      40.0   1.05
mus        0.179   0.024   0.130    0.218  ...    85.0      89.0      99.0   0.99
gamma      0.279   0.051   0.204    0.372  ...   109.0     141.0      97.0   1.00
Is_begin   6.585   5.084   0.276   15.959  ...   124.0      84.0      60.0   1.01
Ia_begin  32.032  20.062   1.858   68.125  ...    71.0      86.0      67.0   1.01
E_begin   17.530  14.979   0.412   44.505  ...    57.0      48.0      88.0   1.03

[8 rows x 11 columns]