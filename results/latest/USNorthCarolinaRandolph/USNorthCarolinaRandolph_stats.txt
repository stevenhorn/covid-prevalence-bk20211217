0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2842.50    37.80
p_loo       35.18        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      588   92.7%
 (0.5, 0.7]   (ok)         41    6.5%
   (0.7, 1]   (bad)         5    0.8%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.197  0.036   0.150    0.277  ...    52.0      57.0      38.0   1.05
pu        0.717  0.013   0.701    0.742  ...    72.0      73.0      27.0   1.04
mu        0.103  0.017   0.077    0.131  ...    34.0      31.0      40.0   1.05
mus       0.148  0.031   0.100    0.215  ...    57.0      62.0      99.0   1.02
gamma     0.189  0.032   0.139    0.260  ...    69.0      64.0      16.0   1.01
Is_begin  0.503  0.498   0.006    1.357  ...    70.0      50.0      79.0   1.00
Ia_begin  1.243  1.217   0.003    3.340  ...    79.0      81.0      56.0   0.98
E_begin   0.595  0.666   0.001    1.894  ...   116.0      60.0      22.0   0.99

[8 rows x 11 columns]