0 Divergences 
Passed validation 
2021-12-16 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 643 log-likelihood matrix

         Estimate       SE
elpd_loo -3678.91    43.64
p_loo       41.63        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      584   90.8%
 (0.5, 0.7]   (ok)         50    7.8%
   (0.7, 1]   (bad)         9    1.4%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.231  0.055   0.155    0.323  ...    58.0      96.0      59.0   1.11
pu        0.732  0.021   0.700    0.769  ...   152.0     152.0      49.0   1.09
mu        0.164  0.020   0.115    0.192  ...    35.0      36.0      37.0   1.03
mus       0.192  0.028   0.134    0.238  ...   152.0     152.0      96.0   1.02
gamma     0.328  0.057   0.240    0.435  ...    74.0      80.0      70.0   1.01
Is_begin  2.445  2.222   0.034    6.584  ...    85.0      19.0      59.0   1.08
Ia_begin  3.319  2.549   0.030    7.919  ...   152.0     143.0      60.0   0.99
E_begin   2.072  2.083   0.008    6.274  ...    83.0      26.0      33.0   1.05

[8 rows x 11 columns]