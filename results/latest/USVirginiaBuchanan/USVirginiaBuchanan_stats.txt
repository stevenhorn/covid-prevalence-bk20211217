0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -1781.53    44.66
p_loo       47.96        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      592   93.7%
 (0.5, 0.7]   (ok)         32    5.1%
   (0.7, 1]   (bad)         5    0.8%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.205  0.040   0.152    0.275  ...    82.0      58.0      58.0   1.03
pu        0.718  0.011   0.702    0.735  ...   106.0      90.0      69.0   1.03
mu        0.123  0.022   0.087    0.167  ...    32.0      34.0      86.0   1.03
mus       0.185  0.035   0.127    0.245  ...   123.0     133.0      93.0   1.00
gamma     0.222  0.052   0.141    0.325  ...   130.0     132.0      93.0   1.04
Is_begin  0.647  0.648   0.005    2.114  ...    24.0      24.0      60.0   1.07
Ia_begin  1.393  1.398   0.007    4.158  ...   100.0      91.0      56.0   1.01
E_begin   0.609  0.682   0.002    1.908  ...   106.0     100.0      24.0   1.03

[8 rows x 11 columns]