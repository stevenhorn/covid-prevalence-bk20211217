0 Divergences 
Failed validation 
2021-12-09 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 635 log-likelihood matrix

         Estimate       SE
elpd_loo -1647.10    95.84
p_loo       51.38        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      605   95.3%
 (0.5, 0.7]   (ok)         26    4.1%
   (0.7, 1]   (bad)         3    0.5%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.173  0.015   0.152    0.202  ...    88.0      62.0      60.0   1.02
pu        0.709  0.008   0.700    0.725  ...    49.0      30.0      39.0   1.05
mu        0.092  0.013   0.069    0.113  ...    61.0      66.0      60.0   1.05
mus       0.307  0.043   0.231    0.391  ...    46.0      40.0      15.0   1.01
gamma     0.755  0.103   0.580    0.947  ...    47.0      47.0      18.0   1.00
Is_begin  0.489  0.569   0.001    1.866  ...    61.0      48.0      40.0   1.03
Ia_begin  0.611  0.711   0.001    2.076  ...    85.0      62.0      88.0   1.01
E_begin   0.308  0.445   0.000    0.852  ...    50.0      33.0      56.0   1.02

[8 rows x 11 columns]