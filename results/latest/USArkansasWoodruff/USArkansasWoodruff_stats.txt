0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1155.55    42.54
p_loo       50.43        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      592   93.4%
 (0.5, 0.7]   (ok)         36    5.7%
   (0.7, 1]   (bad)         5    0.8%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.186  0.024   0.152    0.226  ...    65.0      60.0      49.0   1.03
pu        0.714  0.010   0.700    0.731  ...    43.0      41.0      60.0   1.00
mu        0.124  0.020   0.092    0.167  ...    44.0      45.0      37.0   1.00
mus       0.202  0.039   0.138    0.283  ...    51.0      44.0      38.0   1.03
gamma     0.271  0.051   0.192    0.374  ...    23.0      26.0      61.0   1.08
Is_begin  0.537  0.610   0.008    1.783  ...   102.0     101.0      74.0   1.01
Ia_begin  0.846  0.847   0.001    2.972  ...    16.0      15.0      59.0   1.12
E_begin   0.340  0.326   0.008    0.972  ...    47.0      57.0      46.0   1.01

[8 rows x 11 columns]