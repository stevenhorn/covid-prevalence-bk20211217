0 Divergences 
Passed validation 
2021-12-09 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 635 log-likelihood matrix

         Estimate       SE
elpd_loo -2380.95    44.57
p_loo       40.70        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      575   90.6%
 (0.5, 0.7]   (ok)         49    7.7%
   (0.7, 1]   (bad)        10    1.6%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.192  0.030   0.152    0.243  ...    76.0      85.0      59.0   1.07
pu        0.714  0.011   0.700    0.736  ...    51.0      39.0      43.0   1.05
mu        0.118  0.014   0.094    0.143  ...    13.0      13.0      48.0   1.12
mus       0.162  0.025   0.115    0.208  ...    95.0     101.0      57.0   1.03
gamma     0.217  0.032   0.170    0.265  ...    38.0      68.0      93.0   1.07
Is_begin  0.632  0.518   0.030    1.574  ...    91.0      81.0      59.0   1.00
Ia_begin  1.319  1.646   0.005    4.218  ...    88.0      81.0      79.0   0.99
E_begin   0.537  0.683   0.001    1.675  ...    93.0      42.0      59.0   1.01

[8 rows x 11 columns]