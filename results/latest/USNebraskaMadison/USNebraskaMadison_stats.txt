0 Divergences 
Failed validation 
2021-12-09 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 635 log-likelihood matrix

         Estimate       SE
elpd_loo -2403.87    96.53
p_loo       59.90        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      604   95.1%
 (0.5, 0.7]   (ok)         26    4.1%
   (0.7, 1]   (bad)         3    0.5%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.168  0.018   0.150    0.204  ...   151.0     152.0      47.0   0.99
pu        0.705  0.005   0.700    0.716  ...   125.0      84.0      69.0   1.06
mu        0.086  0.011   0.064    0.108  ...    31.0      30.0      59.0   1.00
mus       0.249  0.029   0.199    0.299  ...   102.0      92.0      67.0   1.02
gamma     0.660  0.111   0.459    0.843  ...   152.0     152.0      74.0   1.01
Is_begin  0.525  0.609   0.000    1.523  ...   103.0      99.0      60.0   1.00
Ia_begin  0.615  0.784   0.013    1.908  ...    80.0     118.0      38.0   0.99
E_begin   0.376  0.454   0.002    1.252  ...   151.0      31.0      38.0   1.07

[8 rows x 11 columns]