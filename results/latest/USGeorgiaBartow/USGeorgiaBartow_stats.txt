0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2686.63    55.61
p_loo       42.64        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      589   92.9%
 (0.5, 0.7]   (ok)         39    6.2%
   (0.7, 1]   (bad)         4    0.6%
   (1, Inf)   (very bad)    2    0.3%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.157   0.006   0.150    0.169  ...   152.0     152.0      75.0   1.04
pu         0.702   0.002   0.700    0.707  ...    95.0      64.0      62.0   1.02
mu         0.076   0.010   0.060    0.099  ...    44.0      48.0      30.0   1.00
mus        0.191   0.031   0.136    0.244  ...   152.0     152.0      77.0   0.99
gamma      0.250   0.036   0.189    0.307  ...   152.0     152.0      86.0   1.05
Is_begin  12.576   8.357   1.050   29.133  ...   152.0     152.0      83.0   0.98
Ia_begin  19.228  12.916   2.219   40.650  ...   127.0     112.0      60.0   1.00
E_begin    9.014   8.015   0.105   21.292  ...    80.0      40.0      69.0   1.05

[8 rows x 11 columns]