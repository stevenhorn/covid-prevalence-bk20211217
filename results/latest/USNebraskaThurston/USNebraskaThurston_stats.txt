0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1563.58    90.78
p_loo       79.49        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      587   92.6%
 (0.5, 0.7]   (ok)         32    5.0%
   (0.7, 1]   (bad)        12    1.9%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.166  0.015   0.151    0.202  ...    19.0      17.0      33.0   1.10
pu        0.706  0.006   0.700    0.718  ...    77.0     114.0      59.0   1.05
mu        0.128  0.021   0.095    0.166  ...    21.0      24.0      59.0   1.07
mus       0.279  0.030   0.227    0.325  ...     7.0       7.0      38.0   1.24
gamma     0.634  0.104   0.477    0.841  ...    21.0      19.0      42.0   1.08
Is_begin  0.361  0.445   0.002    0.966  ...    96.0      47.0      59.0   1.04
Ia_begin  0.711  1.058   0.003    2.360  ...    73.0      69.0      39.0   1.04
E_begin   0.313  0.389   0.004    0.874  ...    99.0     128.0      96.0   0.99

[8 rows x 11 columns]