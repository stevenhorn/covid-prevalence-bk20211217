0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -2645.32    73.92
p_loo       61.54        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      584   92.4%
 (0.5, 0.7]   (ok)         41    6.5%
   (0.7, 1]   (bad)         2    0.3%
   (1, Inf)   (very bad)    5    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.206  0.034   0.150    0.281  ...    81.0      77.0      53.0   1.05
pu        0.715  0.010   0.702    0.738  ...    95.0     105.0      88.0   0.99
mu        0.147  0.017   0.117    0.179  ...     6.0       6.0      27.0   1.34
mus       0.316  0.044   0.249    0.415  ...    62.0      80.0      59.0   1.21
gamma     0.555  0.076   0.416    0.705  ...    67.0      65.0      93.0   0.99
Is_begin  0.796  0.751   0.001    2.461  ...    99.0      66.0      22.0   1.00
Ia_begin  1.298  1.578   0.042    3.712  ...    48.0      48.0      43.0   1.01
E_begin   0.532  0.584   0.044    1.504  ...    68.0      63.0     100.0   1.05

[8 rows x 11 columns]