0 Divergences 
Failed validation 
2021-12-09 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 635 log-likelihood matrix

         Estimate       SE
elpd_loo -1569.80    64.30
p_loo       51.56        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      599   94.3%
 (0.5, 0.7]   (ok)         24    3.8%
   (0.7, 1]   (bad)         8    1.3%
   (1, Inf)   (very bad)    4    0.6%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.157  0.006   0.150    0.170  ...   106.0      55.0      43.0   1.03
pu        0.702  0.002   0.700    0.705  ...    37.0      32.0      14.0   1.01
mu        0.104  0.016   0.078    0.136  ...    19.0      23.0      60.0   1.07
mus       0.170  0.027   0.131    0.224  ...    83.0      95.0      60.0   1.07
gamma     0.274  0.043   0.206    0.359  ...    52.0      50.0      22.0   1.07
Is_begin  0.524  0.571   0.006    1.304  ...    66.0      46.0      60.0   1.05
Ia_begin  0.829  0.882   0.006    2.287  ...    59.0      40.0      35.0   1.03
E_begin   0.346  0.402   0.001    0.965  ...    53.0      29.0      39.0   1.04

[8 rows x 11 columns]