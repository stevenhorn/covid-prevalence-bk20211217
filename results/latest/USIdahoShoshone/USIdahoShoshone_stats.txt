0 Divergences 
Passed validation 
2021-12-09 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 635 log-likelihood matrix

         Estimate       SE
elpd_loo -1490.72    40.44
p_loo       45.65        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      588   92.6%
 (0.5, 0.7]   (ok)         37    5.8%
   (0.7, 1]   (bad)         8    1.3%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.156  0.005   0.150    0.166  ...    76.0      55.0      65.0   1.02
pu        0.702  0.002   0.700    0.706  ...   151.0     100.0      40.0   1.01
mu        0.093  0.011   0.072    0.109  ...    31.0      31.0      39.0   1.04
mus       0.205  0.039   0.138    0.279  ...    78.0      88.0      59.0   1.01
gamma     0.280  0.052   0.206    0.377  ...    36.0      52.0      59.0   1.06
Is_begin  0.121  0.172   0.000    0.454  ...     7.0       7.0      57.0   1.29
Ia_begin  0.180  0.287   0.001    0.632  ...     9.0       9.0      17.0   1.21
E_begin   0.106  0.155   0.001    0.460  ...    10.0      10.0      20.0   1.18

[8 rows x 11 columns]