0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2485.18    44.04
p_loo       46.55        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      585   92.3%
 (0.5, 0.7]   (ok)         37    5.8%
   (0.7, 1]   (bad)         8    1.3%
   (1, Inf)   (very bad)    4    0.6%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.175  0.023   0.151    0.215  ...   152.0     152.0      96.0   1.04
pu        0.707  0.006   0.700    0.717  ...   152.0     115.0      60.0   0.99
mu        0.105  0.015   0.076    0.130  ...    47.0      46.0      43.0   1.04
mus       0.161  0.028   0.099    0.206  ...   152.0     152.0      83.0   1.04
gamma     0.229  0.032   0.175    0.287  ...    75.0      83.0      97.0   1.00
Is_begin  0.713  0.685   0.004    2.132  ...   117.0      65.0      59.0   1.01
Ia_begin  0.607  0.426   0.012    1.485  ...    76.0      90.0      40.0   1.03
E_begin   0.471  0.450   0.016    1.254  ...    73.0     112.0      58.0   1.00

[8 rows x 11 columns]