0 Divergences 
Failed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -1066.00    77.96
p_loo       69.33        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      584   92.4%
 (0.5, 0.7]   (ok)         39    6.2%
   (0.7, 1]   (bad)         7    1.1%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.168  0.013   0.152    0.191  ...   115.0      87.0     100.0   1.02
pu        0.705  0.004   0.700    0.712  ...    76.0      48.0      18.0   1.03
mu        0.168  0.035   0.114    0.222  ...    13.0      10.0      21.0   1.17
mus       0.345  0.056   0.238    0.446  ...    99.0      99.0      52.0   1.02
gamma     0.444  0.061   0.358    0.583  ...    65.0      67.0      57.0   1.03
Is_begin  0.601  0.693   0.017    1.925  ...   104.0      80.0      59.0   1.02
Ia_begin  0.941  0.768   0.027    2.491  ...    64.0      45.0      49.0   1.03
E_begin   0.435  0.421   0.005    1.325  ...    56.0      63.0      59.0   1.01

[8 rows x 11 columns]