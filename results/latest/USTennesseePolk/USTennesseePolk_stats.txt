0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -1833.63    52.39
p_loo       48.57        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      588   93.0%
 (0.5, 0.7]   (ok)         31    4.9%
   (0.7, 1]   (bad)        12    1.9%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.183  0.027   0.152    0.222  ...   123.0      85.0      60.0   1.02
pu        0.711  0.008   0.700    0.726  ...    27.0      35.0      58.0   1.02
mu        0.121  0.018   0.097    0.156  ...    44.0      44.0      56.0   1.04
mus       0.182  0.030   0.124    0.231  ...    61.0      52.0      73.0   1.04
gamma     0.281  0.037   0.212    0.331  ...    52.0      63.0      40.0   0.99
Is_begin  0.537  0.542   0.003    1.643  ...   115.0      70.0      40.0   1.02
Ia_begin  0.897  0.949   0.003    2.213  ...    71.0      50.0      54.0   1.01
E_begin   0.408  0.441   0.008    1.341  ...    51.0      38.0      56.0   1.03

[8 rows x 11 columns]