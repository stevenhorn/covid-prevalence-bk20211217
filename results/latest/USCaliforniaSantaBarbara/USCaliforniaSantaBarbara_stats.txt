0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -3155.29    40.48
p_loo       48.79        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      573   90.4%
 (0.5, 0.7]   (ok)         49    7.7%
   (0.7, 1]   (bad)        11    1.7%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.225  0.045   0.152    0.291  ...   148.0     152.0      59.0   1.00
pu        0.725  0.017   0.702    0.753  ...    91.0      80.0      42.0   1.03
mu        0.135  0.016   0.109    0.165  ...    26.0      25.0      48.0   1.01
mus       0.209  0.030   0.162    0.269  ...    68.0      71.0      69.0   1.02
gamma     0.323  0.050   0.232    0.395  ...   125.0     151.0      88.0   0.99
Is_begin  1.522  1.182   0.011    3.529  ...    72.0      47.0      22.0   1.01
Ia_begin  4.310  3.125   0.272   12.223  ...    68.0      52.0      43.0   1.14
E_begin   3.510  3.502   0.065    9.694  ...    59.0      60.0      54.0   1.05

[8 rows x 11 columns]