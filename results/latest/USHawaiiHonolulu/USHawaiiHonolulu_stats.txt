29 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2866.98    48.98
p_loo       66.06        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      501   79.0%
 (0.5, 0.7]   (ok)         64   10.1%
   (0.7, 1]   (bad)        41    6.5%
   (1, Inf)   (very bad)   28    4.4%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.282   0.048   0.183    0.347  ...     9.0       8.0      39.0   1.26
pu         0.843   0.016   0.827    0.883  ...     6.0       7.0      14.0   1.32
mu         0.172   0.015   0.140    0.194  ...    18.0      19.0      22.0   1.22
mus        0.216   0.055   0.101    0.313  ...    13.0      21.0      14.0   1.07
gamma      0.371   0.057   0.284    0.461  ...    57.0      57.0      91.0   1.02
Is_begin   9.450   5.534   1.584   19.304  ...    35.0      32.0      42.0   1.03
Ia_begin  22.981   9.631   7.152   37.273  ...    18.0      14.0      14.0   1.15
E_begin   35.333  19.257   4.673   72.534  ...    11.0      12.0      44.0   1.13

[8 rows x 11 columns]