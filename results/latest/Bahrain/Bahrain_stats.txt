40 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 633 log-likelihood matrix

         Estimate       SE
elpd_loo -3386.73    34.01
p_loo       39.57        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      502   79.3%
 (0.5, 0.7]   (ok)         99   15.6%
   (0.7, 1]   (bad)        24    3.8%
   (1, Inf)   (very bad)    8    1.3%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.241   0.042   0.165    0.276  ...     3.0       4.0      52.0   1.85
pu         0.715   0.009   0.704    0.734  ...     6.0      12.0      95.0   1.85
mu         0.110   0.009   0.092    0.123  ...     5.0       5.0      25.0   1.93
mus        0.246   0.028   0.202    0.312  ...    15.0      14.0      16.0   2.18
gamma      0.499   0.143   0.291    0.639  ...     3.0       3.0      22.0   2.01
Is_begin  27.713   9.273   6.833   42.469  ...    17.0      27.0      22.0   1.75
Ia_begin  50.622  19.024   9.435   84.756  ...    42.0      46.0      57.0   1.69
E_begin   40.116  20.310   1.276   89.118  ...    87.0     121.0      64.0   1.99

[8 rows x 11 columns]