0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1664.99    40.29
p_loo       34.49        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      589   92.9%
 (0.5, 0.7]   (ok)         35    5.5%
   (0.7, 1]   (bad)         9    1.4%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.169  0.017   0.150    0.196  ...    70.0      29.0      45.0   1.06
pu        0.708  0.007   0.700    0.720  ...   137.0     115.0      55.0   1.01
mu        0.107  0.013   0.085    0.130  ...    51.0      50.0      18.0   1.03
mus       0.168  0.024   0.123    0.208  ...    64.0      67.0      91.0   1.01
gamma     0.213  0.043   0.146    0.275  ...    74.0      64.0      40.0   1.05
Is_begin  0.464  0.536   0.001    1.627  ...    86.0      16.0      33.0   1.10
Ia_begin  0.636  0.656   0.000    2.091  ...    54.0      34.0      22.0   1.03
E_begin   0.355  0.391   0.000    1.175  ...    42.0      20.0      22.0   1.08

[8 rows x 11 columns]