0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1829.47    85.04
p_loo       58.01        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      585   92.3%
 (0.5, 0.7]   (ok)         31    4.9%
   (0.7, 1]   (bad)        15    2.4%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.162  0.009   0.150    0.177  ...   152.0     152.0      79.0   0.98
pu        0.704  0.003   0.700    0.710  ...   152.0      99.0      60.0   1.01
mu        0.093  0.008   0.078    0.108  ...    52.0      55.0      97.0   1.03
mus       0.335  0.042   0.262    0.403  ...    88.0      80.0      33.0   1.03
gamma     0.667  0.111   0.488    0.845  ...   152.0     152.0      96.0   1.01
Is_begin  0.700  0.754   0.004    2.387  ...    98.0      50.0      60.0   1.00
Ia_begin  0.949  0.947   0.017    2.415  ...   115.0     116.0      61.0   1.05
E_begin   0.432  0.572   0.003    1.589  ...    96.0      64.0      60.0   0.99

[8 rows x 11 columns]