0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1095.76    44.61
p_loo       48.04        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      568   89.6%
 (0.5, 0.7]   (ok)         48    7.6%
   (0.7, 1]   (bad)        15    2.4%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.170  0.017   0.151    0.205  ...    98.0      90.0      57.0   1.02
pu        0.707  0.006   0.700    0.719  ...    55.0     152.0      18.0   1.08
mu        0.115  0.019   0.080    0.147  ...    37.0      40.0      69.0   1.05
mus       0.205  0.033   0.139    0.254  ...    85.0      86.0     100.0   1.00
gamma     0.288  0.053   0.208    0.390  ...    48.0      45.0      53.0   1.06
Is_begin  0.228  0.304   0.002    0.897  ...    91.0      47.0      96.0   1.04
Ia_begin  0.295  0.371   0.006    1.060  ...    99.0      80.0      60.0   1.00
E_begin   0.129  0.166   0.002    0.452  ...    80.0      59.0      60.0   1.03

[8 rows x 11 columns]