0 Divergences 
Failed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 633 log-likelihood matrix

         Estimate       SE
elpd_loo -2076.51    78.14
p_loo       35.92        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      615   97.2%
 (0.5, 0.7]   (ok)         12    1.9%
   (0.7, 1]   (bad)         6    0.9%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.165  0.003   0.160    0.170  ...     3.0       4.0      38.0   1.78
pu        0.704  0.003   0.701    0.709  ...     3.0       3.0      33.0   2.21
mu        0.214  0.016   0.194    0.235  ...     3.0       3.0      14.0   2.11
mus       0.181  0.012   0.158    0.194  ...     3.0       3.0      28.0   2.11
gamma     0.213  0.009   0.197    0.227  ...     3.0       4.0      20.0   1.68
Is_begin  1.216  0.445   0.641    1.933  ...     3.0       3.0      24.0   2.08
Ia_begin  0.796  0.585   0.129    1.653  ...     3.0       3.0      14.0   2.24
E_begin   0.526  0.167   0.325    0.872  ...     4.0       4.0      16.0   1.72

[8 rows x 11 columns]