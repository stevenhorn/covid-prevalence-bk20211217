0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2218.39    30.76
p_loo       39.27        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      571   90.1%
 (0.5, 0.7]   (ok)         55    8.7%
   (0.7, 1]   (bad)         8    1.3%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.174  0.017   0.151    0.210  ...    62.0      58.0      59.0   1.03
pu        0.707  0.005   0.700    0.717  ...   120.0     112.0      56.0   1.04
mu        0.105  0.011   0.086    0.126  ...    14.0      14.0      60.0   1.13
mus       0.183  0.028   0.132    0.233  ...   103.0     129.0      60.0   0.99
gamma     0.317  0.043   0.232    0.393  ...   102.0      99.0      45.0   1.00
Is_begin  0.874  0.635   0.015    2.098  ...    66.0      53.0      57.0   1.01
Ia_begin  3.819  2.155   0.503    7.891  ...    69.0      82.0      59.0   1.02
E_begin   4.278  3.652   0.034   12.011  ...    68.0      57.0      56.0   0.99

[8 rows x 11 columns]