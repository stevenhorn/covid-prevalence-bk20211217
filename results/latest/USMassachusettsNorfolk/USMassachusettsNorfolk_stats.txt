0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -3265.11    36.64
p_loo       34.33        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      590   93.1%
 (0.5, 0.7]   (ok)         39    6.2%
   (0.7, 1]   (bad)         5    0.8%
   (1, Inf)   (very bad)    0    0.0%

             mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa          0.209   0.042   0.150    0.283  ...    53.0      55.0      81.0   1.03
pu          0.720   0.017   0.700    0.754  ...    47.0      43.0      49.0   0.99
mu          0.144   0.022   0.118    0.193  ...    11.0      13.0      38.0   1.19
mus         0.183   0.029   0.144    0.247  ...    54.0      89.0      60.0   0.99
gamma       0.315   0.050   0.228    0.407  ...    29.0      27.0      65.0   1.06
Is_begin   15.779   9.627   1.904   30.646  ...    45.0      36.0      29.0   1.03
Ia_begin   49.233  16.574  13.459   72.705  ...    37.0      35.0      38.0   1.01
E_begin   113.856  46.125  31.105  178.138  ...    78.0      71.0      60.0   1.04

[8 rows x 11 columns]