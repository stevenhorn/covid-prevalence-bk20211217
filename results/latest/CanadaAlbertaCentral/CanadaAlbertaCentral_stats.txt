0 Divergences 
Passed validation 
2021-12-16 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 643 log-likelihood matrix

         Estimate       SE
elpd_loo -3071.47    48.84
p_loo       48.24        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      592   92.1%
 (0.5, 0.7]   (ok)         42    6.5%
   (0.7, 1]   (bad)         7    1.1%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.250  0.054   0.165    0.339  ...    15.0      14.0      56.0   1.11
pu        0.788  0.030   0.735    0.835  ...    19.0      19.0      40.0   1.05
mu        0.175  0.021   0.140    0.214  ...     7.0       8.0      69.0   1.20
mus       0.202  0.036   0.146    0.274  ...    84.0      77.0      60.0   1.00
gamma     0.356  0.060   0.258    0.462  ...    60.0      81.0      76.0   1.02
Is_begin  1.642  1.817   0.006    5.433  ...    54.0      18.0      42.0   1.10
Ia_begin  1.932  2.341   0.005    6.141  ...    66.0      10.0      24.0   1.17
E_begin   0.944  1.015   0.007    2.764  ...    34.0      19.0      56.0   1.08

[8 rows x 11 columns]