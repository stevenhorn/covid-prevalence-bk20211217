0 Divergences 
Passed validation 
2021-12-09 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 635 log-likelihood matrix

         Estimate       SE
elpd_loo -1188.84    38.51
p_loo       47.17        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      585   92.1%
 (0.5, 0.7]   (ok)         38    6.0%
   (0.7, 1]   (bad)        12    1.9%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.229  0.051   0.153    0.327  ...    62.0      68.0      57.0   1.04
pu        0.742  0.019   0.705    0.769  ...    51.0      45.0      32.0   1.05
mu        0.140  0.029   0.089    0.188  ...    19.0      23.0      47.0   1.07
mus       0.192  0.035   0.116    0.243  ...    28.0      32.0      60.0   1.05
gamma     0.228  0.046   0.141    0.316  ...    78.0      68.0      20.0   1.07
Is_begin  0.375  0.476   0.016    1.257  ...    65.0      82.0      60.0   1.01
Ia_begin  0.578  0.819   0.003    1.764  ...    86.0      71.0      59.0   0.99
E_begin   0.246  0.258   0.007    0.685  ...    79.0      50.0      60.0   1.02

[8 rows x 11 columns]