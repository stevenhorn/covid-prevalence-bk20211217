0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1306.60    34.79
p_loo       42.54        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      586   92.4%
 (0.5, 0.7]   (ok)         36    5.7%
   (0.7, 1]   (bad)        10    1.6%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.212  0.043   0.151    0.285  ...    63.0      58.0      81.0   1.04
pu        0.720  0.016   0.700    0.752  ...   123.0      85.0      14.0   1.07
mu        0.160  0.026   0.118    0.210  ...    16.0      19.0      60.0   1.08
mus       0.212  0.033   0.163    0.278  ...    47.0      54.0     100.0   1.04
gamma     0.319  0.049   0.230    0.415  ...    79.0      93.0      60.0   1.02
Is_begin  0.672  0.631   0.008    1.962  ...    40.0      78.0      40.0   1.07
Ia_begin  1.269  1.080   0.008    3.294  ...    84.0      72.0      60.0   0.99
E_begin   0.556  0.633   0.001    1.789  ...    67.0      73.0      40.0   0.99

[8 rows x 11 columns]