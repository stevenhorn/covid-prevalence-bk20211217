0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2288.08    33.44
p_loo       45.67        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      586   92.4%
 (0.5, 0.7]   (ok)         33    5.2%
   (0.7, 1]   (bad)        15    2.4%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.257  0.054   0.160    0.334  ...    57.0      71.0      87.0   1.05
pu        0.752  0.037   0.704    0.815  ...     8.0       8.0      20.0   1.21
mu        0.115  0.020   0.078    0.147  ...    29.0      28.0      50.0   1.11
mus       0.166  0.028   0.120    0.215  ...    52.0      54.0      40.0   1.05
gamma     0.239  0.039   0.179    0.302  ...   152.0     135.0      74.0   1.01
Is_begin  1.238  1.043   0.036    3.225  ...    75.0      46.0      34.0   1.03
Ia_begin  0.703  0.518   0.014    1.664  ...   109.0      79.0      60.0   1.05
E_begin   1.024  1.222   0.031    3.322  ...   118.0     111.0      46.0   1.01

[8 rows x 11 columns]