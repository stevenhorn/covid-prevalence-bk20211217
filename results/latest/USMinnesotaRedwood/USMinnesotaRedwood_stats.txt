0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1639.65    50.23
p_loo       52.70        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      590   93.1%
 (0.5, 0.7]   (ok)         38    6.0%
   (0.7, 1]   (bad)         5    0.8%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.166  0.012   0.151    0.188  ...    60.0      50.0      55.0   1.00
pu        0.708  0.008   0.700    0.722  ...    76.0      13.0      22.0   1.14
mu        0.128  0.014   0.104    0.151  ...     9.0       8.0      60.0   1.23
mus       0.197  0.029   0.146    0.245  ...    42.0      41.0      59.0   1.05
gamma     0.280  0.048   0.201    0.370  ...    31.0      29.0      43.0   1.06
Is_begin  0.562  0.629   0.002    1.697  ...    57.0      30.0      74.0   1.04
Ia_begin  0.771  0.831   0.029    2.710  ...    23.0      13.0      73.0   1.12
E_begin   0.371  0.508   0.022    1.359  ...    42.0      17.0      83.0   1.08

[8 rows x 11 columns]