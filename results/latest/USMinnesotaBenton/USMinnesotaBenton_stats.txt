0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2220.35    41.51
p_loo       37.82        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      586   92.4%
 (0.5, 0.7]   (ok)         40    6.3%
   (0.7, 1]   (bad)         7    1.1%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.155  0.005   0.150    0.162  ...    84.0      73.0      59.0   1.04
pu        0.702  0.002   0.700    0.706  ...    67.0      55.0      38.0   1.08
mu        0.091  0.012   0.072    0.111  ...    29.0      28.0      60.0   1.03
mus       0.156  0.026   0.110    0.201  ...    54.0      62.0      56.0   1.02
gamma     0.249  0.034   0.185    0.305  ...    21.0      19.0      92.0   1.12
Is_begin  0.534  0.562   0.008    1.433  ...    92.0      73.0      31.0   1.01
Ia_begin  1.012  1.373   0.005    4.292  ...   108.0      75.0     100.0   1.01
E_begin   0.475  0.630   0.011    1.621  ...    95.0      75.0      88.0   1.02

[8 rows x 11 columns]