0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -3549.42    45.98
p_loo       46.79        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      591   93.5%
 (0.5, 0.7]   (ok)         36    5.7%
   (0.7, 1]   (bad)         4    0.6%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.193  0.029   0.156    0.247  ...    57.0      52.0      60.0   1.04
pu        0.713  0.010   0.701    0.733  ...    33.0      32.0      38.0   1.02
mu        0.113  0.020   0.073    0.145  ...     6.0       6.0      20.0   1.33
mus       0.154  0.035   0.083    0.219  ...   110.0     121.0      60.0   1.01
gamma     0.198  0.036   0.135    0.261  ...    24.0      22.0      16.0   1.06
Is_begin  1.471  1.085   0.019    3.741  ...    98.0     105.0      85.0   1.02
Ia_begin  2.799  2.390   0.084    7.697  ...    80.0      54.0      60.0   1.02
E_begin   1.528  1.455   0.061    4.465  ...    85.0      96.0      66.0   1.02

[8 rows x 11 columns]