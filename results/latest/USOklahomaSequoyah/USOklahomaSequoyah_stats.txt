0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2480.72    47.42
p_loo       48.86        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      606   95.6%
 (0.5, 0.7]   (ok)         20    3.2%
   (0.7, 1]   (bad)         7    1.1%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.161  0.013   0.150    0.187  ...   108.0      82.0      60.0   0.99
pu        0.705  0.004   0.700    0.712  ...   152.0     152.0      49.0   1.04
mu        0.093  0.016   0.065    0.123  ...    16.0      16.0      70.0   1.09
mus       0.150  0.025   0.109    0.193  ...   152.0     152.0     100.0   0.99
gamma     0.212  0.041   0.144    0.282  ...    60.0      52.0      60.0   1.00
Is_begin  0.504  0.545   0.000    1.690  ...   110.0      59.0      54.0   1.03
Ia_begin  0.852  0.836   0.007    2.586  ...    86.0      61.0      57.0   1.01
E_begin   0.337  0.346   0.008    1.013  ...    84.0      61.0      61.0   1.02

[8 rows x 11 columns]