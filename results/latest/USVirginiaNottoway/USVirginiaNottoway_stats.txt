1 Divergences 
Failed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -1741.04    43.50
p_loo       42.90        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      597   94.5%
 (0.5, 0.7]   (ok)         28    4.4%
   (0.7, 1]   (bad)         5    0.8%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.228  0.053   0.153    0.326  ...    45.0      57.0     100.0   1.07
pu        0.734  0.020   0.702    0.767  ...    42.0      46.0      33.0   1.03
mu        0.102  0.018   0.069    0.132  ...    17.0      16.0      38.0   1.11
mus       0.180  0.029   0.134    0.227  ...    61.0      63.0      60.0   1.00
gamma     0.244  0.046   0.160    0.322  ...    29.0      23.0      59.0   1.09
Is_begin  0.580  0.548   0.006    1.804  ...    66.0      53.0      40.0   1.01
Ia_begin  0.921  0.992   0.002    3.184  ...    97.0      45.0      60.0   1.03
E_begin   0.322  0.388   0.005    0.830  ...    27.0      10.0      49.0   1.18

[8 rows x 11 columns]