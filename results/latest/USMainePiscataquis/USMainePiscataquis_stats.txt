0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1409.14    43.08
p_loo       48.91        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      607   95.7%
 (0.5, 0.7]   (ok)         21    3.3%
   (0.7, 1]   (bad)         5    0.8%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.215  0.040   0.156    0.295  ...    43.0      53.0      88.0   1.05
pu        0.718  0.013   0.702    0.743  ...    73.0      79.0      93.0   1.04
mu        0.129  0.016   0.101    0.162  ...    51.0      52.0      60.0   1.02
mus       0.196  0.029   0.146    0.239  ...    80.0      88.0      69.0   1.00
gamma     0.280  0.044   0.209    0.367  ...    81.0      80.0      59.0   1.03
Is_begin  0.356  0.341   0.014    1.241  ...   101.0      75.0      60.0   1.00
Ia_begin  0.572  0.556   0.008    1.603  ...   135.0     112.0      56.0   1.00
E_begin   0.290  0.307   0.004    0.811  ...    97.0      89.0      40.0   1.02

[8 rows x 11 columns]