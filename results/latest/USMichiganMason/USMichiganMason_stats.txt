1 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1974.86    38.93
p_loo       34.99        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      595   93.8%
 (0.5, 0.7]   (ok)         35    5.5%
   (0.7, 1]   (bad)         4    0.6%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.170  0.017   0.151    0.209  ...   152.0     125.0      48.0   1.00
pu        0.707  0.004   0.701    0.714  ...   152.0     152.0      54.0   0.99
mu        0.137  0.014   0.114    0.163  ...    40.0      59.0     100.0   1.03
mus       0.182  0.027   0.140    0.229  ...    95.0      69.0      70.0   0.99
gamma     0.283  0.040   0.200    0.354  ...    74.0      69.0      57.0   1.01
Is_begin  0.615  0.668   0.034    2.275  ...    90.0     152.0      59.0   0.99
Ia_begin  1.269  1.222   0.061    4.042  ...   112.0      66.0      86.0   1.00
E_begin   0.496  0.483   0.004    1.329  ...    76.0      48.0      83.0   0.99

[8 rows x 11 columns]