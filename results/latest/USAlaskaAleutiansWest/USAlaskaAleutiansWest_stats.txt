0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1454.45    68.62
p_loo       67.10        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      597   94.2%
 (0.5, 0.7]   (ok)         25    3.9%
   (0.7, 1]   (bad)         5    0.8%
   (1, Inf)   (very bad)    7    1.1%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.164  0.014   0.150    0.195  ...    18.0      17.0      24.0   1.10
pu        0.703  0.003   0.700    0.710  ...    12.0       9.0      43.0   1.18
mu        0.216  0.030   0.174    0.268  ...     4.0       4.0      33.0   1.66
mus       0.355  0.038   0.278    0.413  ...    40.0      37.0      39.0   1.18
gamma     0.572  0.107   0.408    0.798  ...    15.0      17.0      40.0   1.11
Is_begin  0.299  0.498   0.004    1.567  ...    14.0       5.0      73.0   1.41
Ia_begin  0.477  0.638   0.004    1.518  ...    58.0      44.0      30.0   1.24
E_begin   0.216  0.263   0.001    0.754  ...    22.0      11.0      54.0   1.19

[8 rows x 11 columns]