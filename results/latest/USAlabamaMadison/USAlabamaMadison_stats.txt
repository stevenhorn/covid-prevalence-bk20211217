0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -3042.48    43.24
p_loo       45.49        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      580   91.5%
 (0.5, 0.7]   (ok)         42    6.6%
   (0.7, 1]   (bad)        12    1.9%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.195  0.031   0.153    0.253  ...    58.0      49.0      39.0   1.01
pu        0.718  0.013   0.700    0.739  ...    65.0      52.0      60.0   1.04
mu        0.127  0.019   0.098    0.156  ...    17.0      16.0      59.0   1.10
mus       0.203  0.037   0.138    0.266  ...    70.0      98.0      80.0   0.99
gamma     0.268  0.038   0.205    0.352  ...    75.0      63.0      26.0   1.03
Is_begin  1.680  1.189   0.083    3.875  ...   122.0      98.0      59.0   1.03
Ia_begin  3.456  2.354   0.080    7.569  ...   110.0      80.0      52.0   0.99
E_begin   2.244  2.315   0.018    7.235  ...   100.0      62.0      40.0   1.00

[8 rows x 11 columns]