0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -1993.29    65.15
p_loo       78.01        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      595   94.1%
 (0.5, 0.7]   (ok)         23    3.6%
   (0.7, 1]   (bad)         9    1.4%
   (1, Inf)   (very bad)    5    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.157  0.009   0.150    0.171  ...   152.0     141.0      15.0   1.04
pu        0.703  0.002   0.700    0.706  ...   152.0     135.0      42.0   1.00
mu        0.119  0.020   0.090    0.165  ...     6.0       6.0      14.0   1.28
mus       0.217  0.032   0.166    0.262  ...    48.0      31.0      55.0   1.06
gamma     0.336  0.062   0.245    0.448  ...    55.0      68.0      40.0   1.06
Is_begin  0.531  0.669   0.011    1.682  ...    71.0      66.0      53.0   1.01
Ia_begin  1.201  1.202   0.045    3.514  ...    60.0      79.0      60.0   1.01
E_begin   0.477  0.549   0.005    1.350  ...    33.0      33.0      22.0   1.01

[8 rows x 11 columns]