0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -2527.84    61.32
p_loo       61.99        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      602   95.3%
 (0.5, 0.7]   (ok)         26    4.1%
   (0.7, 1]   (bad)         1    0.2%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.169  0.016   0.150    0.202  ...   152.0     145.0      35.0   1.00
pu        0.707  0.005   0.700    0.718  ...   112.0      91.0      60.0   1.04
mu        0.107  0.014   0.085    0.134  ...    11.0      12.0      59.0   1.18
mus       0.210  0.033   0.158    0.265  ...    63.0      79.0      39.0   1.04
gamma     0.304  0.057   0.203    0.402  ...    63.0     135.0      88.0   1.01
Is_begin  0.749  0.673   0.014    2.174  ...   105.0      95.0      60.0   0.99
Ia_begin  0.610  0.563   0.028    1.862  ...   136.0     152.0      53.0   1.11
E_begin   0.390  0.542   0.002    1.350  ...   108.0      88.0      37.0   1.00

[8 rows x 11 columns]