0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1820.78    41.84
p_loo       39.04        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      594   93.7%
 (0.5, 0.7]   (ok)         32    5.0%
   (0.7, 1]   (bad)         6    0.9%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.158  0.009   0.150    0.177  ...   148.0      96.0      91.0   1.00
pu        0.703  0.003   0.700    0.709  ...    79.0     119.0      46.0   1.06
mu        0.094  0.010   0.077    0.111  ...    58.0      54.0      93.0   0.99
mus       0.170  0.025   0.128    0.212  ...   121.0     124.0      48.0   1.00
gamma     0.304  0.046   0.228    0.389  ...   132.0     122.0      93.0   1.01
Is_begin  1.355  1.127   0.028    3.323  ...   108.0      72.0      91.0   1.02
Ia_begin  3.806  2.481   0.164    8.728  ...    63.0      47.0      22.0   1.00
E_begin   1.835  1.794   0.027    5.534  ...    27.0      23.0      59.0   1.08

[8 rows x 11 columns]