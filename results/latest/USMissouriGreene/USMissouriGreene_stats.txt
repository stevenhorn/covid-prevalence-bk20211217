0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -3278.38   102.17
p_loo       56.33        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      597   94.2%
 (0.5, 0.7]   (ok)         31    4.9%
   (0.7, 1]   (bad)         4    0.6%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.159  0.009   0.150    0.177  ...   111.0      80.0      22.0   1.01
pu        0.703  0.003   0.700    0.709  ...   121.0     151.0      72.0   1.00
mu        0.085  0.009   0.071    0.100  ...    41.0      32.0      59.0   1.09
mus       0.340  0.041   0.286    0.414  ...    96.0     108.0      77.0   1.03
gamma     0.699  0.098   0.554    0.898  ...    56.0      64.0      60.0   1.01
Is_begin  1.026  0.823   0.001    2.657  ...    70.0      55.0      42.0   1.01
Ia_begin  1.334  1.158   0.020    3.647  ...    58.0      17.0      40.0   1.09
E_begin   0.578  0.519   0.005    1.566  ...    75.0      59.0      31.0   1.00

[8 rows x 11 columns]