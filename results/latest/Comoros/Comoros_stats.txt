0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 633 log-likelihood matrix

         Estimate       SE
elpd_loo -2017.37    44.45
p_loo       52.79        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      594   93.8%
 (0.5, 0.7]   (ok)         29    4.6%
   (0.7, 1]   (bad)         9    1.4%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.254  0.055   0.166    0.341  ...   118.0     108.0      60.0   0.99
pu        0.827  0.055   0.717    0.899  ...     4.0       5.0      15.0   1.44
mu        0.186  0.030   0.132    0.235  ...     7.0       8.0      43.0   1.22
mus       0.204  0.035   0.156    0.294  ...    63.0      81.0      38.0   1.02
gamma     0.277  0.047   0.210    0.379  ...    95.0      85.0     100.0   1.04
Is_begin  0.355  0.415   0.000    1.298  ...    78.0      59.0      56.0   1.04
Ia_begin  0.813  1.090   0.000    2.599  ...    79.0      29.0      60.0   1.07
E_begin   0.352  0.547   0.001    1.634  ...    57.0      23.0      60.0   1.07

[8 rows x 11 columns]