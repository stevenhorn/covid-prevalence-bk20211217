0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2083.91    38.27
p_loo       41.76        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      602   95.0%
 (0.5, 0.7]   (ok)         25    3.9%
   (0.7, 1]   (bad)         6    0.9%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.170  0.015   0.151    0.193  ...   152.0     146.0      61.0   0.99
pu        0.705  0.005   0.700    0.712  ...    96.0      59.0      30.0   1.01
mu        0.118  0.015   0.092    0.145  ...    70.0      67.0      80.0   1.02
mus       0.178  0.028   0.122    0.223  ...   107.0     107.0      93.0   1.00
gamma     0.265  0.042   0.199    0.352  ...   103.0      91.0      91.0   1.01
Is_begin  0.791  0.740   0.011    2.077  ...    93.0      99.0      18.0   1.00
Ia_begin  1.214  1.248   0.020    3.451  ...    45.0      73.0      49.0   1.08
E_begin   0.730  0.860   0.000    3.072  ...    80.0      63.0      60.0   1.02

[8 rows x 11 columns]