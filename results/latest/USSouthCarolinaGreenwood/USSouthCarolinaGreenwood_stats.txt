0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -2542.16    34.92
p_loo       33.18        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      600   94.9%
 (0.5, 0.7]   (ok)         24    3.8%
   (0.7, 1]   (bad)         8    1.3%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.182  0.025   0.152    0.232  ...    82.0      64.0      57.0   1.00
pu        0.712  0.011   0.700    0.734  ...    78.0      81.0      40.0   0.99
mu        0.108  0.017   0.079    0.143  ...     9.0       9.0      58.0   1.17
mus       0.148  0.028   0.103    0.201  ...    40.0      33.0      83.0   1.04
gamma     0.198  0.035   0.147    0.267  ...    60.0      58.0      21.0   1.05
Is_begin  0.627  0.653   0.003    1.867  ...    81.0      58.0      54.0   0.99
Ia_begin  0.519  0.467   0.007    1.478  ...    61.0      49.0      39.0   1.02
E_begin   0.310  0.361   0.015    0.837  ...    60.0      47.0      65.0   1.02

[8 rows x 11 columns]