0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -1632.74    60.83
p_loo       63.17        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      585   92.6%
 (0.5, 0.7]   (ok)         35    5.5%
   (0.7, 1]   (bad)         9    1.4%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.174  0.019   0.152    0.210  ...    64.0      69.0      79.0   1.03
pu        0.707  0.006   0.700    0.718  ...    94.0      47.0      24.0   1.10
mu        0.142  0.021   0.108    0.179  ...    12.0      13.0      67.0   1.12
mus       0.218  0.034   0.150    0.275  ...    68.0      63.0      25.0   1.02
gamma     0.317  0.046   0.243    0.395  ...    97.0      91.0      88.0   0.99
Is_begin  0.610  0.590   0.012    1.625  ...    83.0      71.0      59.0   1.00
Ia_begin  1.053  1.058   0.006    3.172  ...    69.0      66.0     100.0   1.02
E_begin   0.433  0.485   0.006    1.486  ...   104.0      82.0      84.0   1.00

[8 rows x 11 columns]