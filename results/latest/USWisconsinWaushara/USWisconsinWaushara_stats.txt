0 Divergences 
Failed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -1763.64    61.39
p_loo       45.94        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      583   92.2%
 (0.5, 0.7]   (ok)         37    5.9%
   (0.7, 1]   (bad)         9    1.4%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.162  0.012   0.150    0.184  ...    56.0      45.0      86.0   1.04
pu        0.704  0.003   0.700    0.709  ...   118.0      55.0      19.0   1.05
mu        0.088  0.014   0.063    0.114  ...     9.0       9.0      40.0   1.20
mus       0.236  0.044   0.157    0.309  ...   125.0     128.0      60.0   1.00
gamma     0.297  0.055   0.177    0.383  ...    21.0      23.0      20.0   1.08
Is_begin  0.405  0.536   0.001    1.394  ...    86.0      35.0      56.0   1.03
Ia_begin  0.756  0.943   0.017    2.659  ...   112.0      88.0      72.0   0.99
E_begin   0.313  0.429   0.006    1.234  ...    91.0      22.0      56.0   1.08

[8 rows x 11 columns]