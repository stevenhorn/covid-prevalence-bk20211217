0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 633 log-likelihood matrix

         Estimate       SE
elpd_loo -1514.11    71.37
p_loo       59.93        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      591   93.4%
 (0.5, 0.7]   (ok)         39    6.2%
   (0.7, 1]   (bad)         2    0.3%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.265  0.051   0.175    0.347  ...    50.0      50.0      40.0   1.01
pu        0.859  0.018   0.826    0.890  ...    34.0      38.0      54.0   1.06
mu        0.145  0.023   0.102    0.182  ...     6.0       6.0      18.0   1.36
mus       0.270  0.047   0.196    0.367  ...    51.0      55.0      75.0   1.03
gamma     0.434  0.071   0.309    0.562  ...   152.0     152.0      60.0   1.00
Is_begin  1.136  0.881   0.015    2.841  ...   128.0     152.0      95.0   1.00
Ia_begin  2.040  1.902   0.098    6.046  ...    76.0      70.0      54.0   1.00
E_begin   1.198  1.418   0.037    4.393  ...    97.0      29.0      74.0   1.05

[8 rows x 11 columns]