0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1536.75    48.44
p_loo       48.60        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      580   91.5%
 (0.5, 0.7]   (ok)         46    7.3%
   (0.7, 1]   (bad)         6    0.9%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.166  0.014   0.150    0.196  ...    89.0      80.0      60.0   0.99
pu        0.704  0.005   0.700    0.714  ...   122.0      46.0      59.0   1.05
mu        0.098  0.019   0.066    0.133  ...    27.0      26.0      83.0   1.06
mus       0.215  0.039   0.144    0.279  ...   134.0     152.0      57.0   0.99
gamma     0.294  0.047   0.215    0.372  ...    85.0      82.0      53.0   1.10
Is_begin  0.552  0.457   0.017    1.415  ...   105.0      97.0     100.0   1.01
Ia_begin  0.985  1.093   0.005    2.847  ...    62.0      68.0      38.0   1.00
E_begin   0.386  0.550   0.003    1.109  ...    35.0      49.0      30.0   1.01

[8 rows x 11 columns]