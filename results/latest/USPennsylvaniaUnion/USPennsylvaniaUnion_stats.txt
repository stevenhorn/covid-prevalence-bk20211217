0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2252.89    96.25
p_loo       72.53        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      598   94.3%
 (0.5, 0.7]   (ok)         27    4.3%
   (0.7, 1]   (bad)         6    0.9%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.156  0.006   0.150    0.170  ...   152.0     152.0      52.0   1.01
pu        0.702  0.003   0.700    0.709  ...   141.0     152.0      73.0   1.04
mu        0.085  0.010   0.066    0.102  ...     9.0       8.0      19.0   1.21
mus       0.356  0.064   0.238    0.460  ...     8.0       8.0      97.0   1.24
gamma     0.524  0.103   0.326    0.671  ...    11.0      10.0      24.0   1.19
Is_begin  0.841  0.874   0.004    2.696  ...   152.0     152.0      60.0   1.01
Ia_begin  1.017  1.379   0.004    2.964  ...    88.0     152.0      52.0   1.03
E_begin   0.424  0.420   0.004    1.128  ...   109.0     116.0      97.0   1.01

[8 rows x 11 columns]