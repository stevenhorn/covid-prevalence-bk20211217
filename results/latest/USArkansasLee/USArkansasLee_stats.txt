0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1792.82    78.81
p_loo       68.33        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      596   94.0%
 (0.5, 0.7]   (ok)         32    5.0%
   (0.7, 1]   (bad)         2    0.3%
   (1, Inf)   (very bad)    4    0.6%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.159  0.010   0.150    0.178  ...    88.0      52.0      43.0   1.04
pu        0.704  0.004   0.700    0.710  ...   152.0     142.0      39.0   1.00
mu        0.118  0.014   0.091    0.145  ...    37.0      32.0      65.0   1.06
mus       0.334  0.057   0.251    0.465  ...    18.0      20.0      19.0   1.08
gamma     0.610  0.100   0.439    0.784  ...    60.0      56.0      56.0   1.03
Is_begin  0.693  0.657   0.016    2.126  ...    74.0      87.0      60.0   0.99
Ia_begin  0.833  0.885   0.068    2.097  ...    73.0     113.0      60.0   1.09
E_begin   0.327  0.352   0.001    1.025  ...    36.0      46.0      72.0   1.05

[8 rows x 11 columns]