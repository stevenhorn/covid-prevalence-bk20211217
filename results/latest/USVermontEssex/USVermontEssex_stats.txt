0 Divergences 
Failed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -1099.68    61.68
p_loo       59.90        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      596   94.3%
 (0.5, 0.7]   (ok)         23    3.6%
   (0.7, 1]   (bad)        10    1.6%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.228  0.052   0.151    0.311  ...    27.0      22.0      59.0   1.09
pu        0.749  0.022   0.718    0.792  ...    33.0      30.0      48.0   1.06
mu        0.139  0.027   0.091    0.188  ...    97.0      82.0      75.0   1.04
mus       0.205  0.037   0.145    0.277  ...   152.0     152.0      84.0   1.01
gamma     0.242  0.049   0.147    0.338  ...   152.0     152.0      93.0   1.01
Is_begin  0.512  0.506   0.000    1.492  ...    91.0      59.0      45.0   1.03
Ia_begin  0.878  1.077   0.001    3.064  ...    82.0      97.0      60.0   0.98
E_begin   0.304  0.313   0.002    0.918  ...   129.0     142.0     100.0   0.99

[8 rows x 11 columns]