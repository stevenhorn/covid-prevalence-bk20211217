0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1400.37    38.32
p_loo       46.91        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      607   95.7%
 (0.5, 0.7]   (ok)         19    3.0%
   (0.7, 1]   (bad)         6    0.9%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.237  0.042   0.169    0.315  ...   102.0     121.0      72.0   1.09
pu        0.732  0.019   0.700    0.766  ...    49.0      72.0      69.0   1.03
mu        0.115  0.018   0.088    0.141  ...    93.0     105.0     100.0   1.03
mus       0.175  0.030   0.133    0.239  ...    88.0      90.0      58.0   1.00
gamma     0.259  0.046   0.179    0.331  ...   152.0     152.0      59.0   0.99
Is_begin  0.743  0.629   0.057    1.831  ...    88.0      95.0      61.0   1.00
Ia_begin  1.296  1.338   0.001    3.725  ...    97.0      64.0      37.0   1.01
E_begin   0.603  0.591   0.050    1.889  ...    93.0     105.0      83.0   1.01

[8 rows x 11 columns]