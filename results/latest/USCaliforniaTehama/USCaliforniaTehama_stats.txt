0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2149.20    40.27
p_loo       36.90        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      595   93.8%
 (0.5, 0.7]   (ok)         31    4.9%
   (0.7, 1]   (bad)         5    0.8%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.254  0.048   0.173    0.342  ...    42.0      47.0      58.0   1.05
pu        0.737  0.021   0.701    0.773  ...    18.0      19.0      86.0   1.09
mu        0.127  0.018   0.092    0.155  ...     8.0       8.0      16.0   1.23
mus       0.162  0.029   0.121    0.218  ...   113.0     102.0      91.0   1.06
gamma     0.231  0.038   0.181    0.308  ...    42.0      40.0      60.0   1.02
Is_begin  0.313  0.257   0.006    0.795  ...    49.0      54.0      36.0   1.03
Ia_begin  0.705  0.769   0.013    2.222  ...    67.0      54.0      60.0   1.01
E_begin   0.331  0.414   0.001    1.089  ...    85.0      63.0      72.0   1.01

[8 rows x 11 columns]