0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2683.00    36.80
p_loo       47.52        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      583   92.0%
 (0.5, 0.7]   (ok)         44    6.9%
   (0.7, 1]   (bad)         6    0.9%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.159  0.007   0.150    0.171  ...    94.0      87.0      60.0   1.04
pu        0.703  0.003   0.700    0.710  ...   152.0     152.0      60.0   1.01
mu        0.063  0.011   0.041    0.081  ...    28.0      25.0      39.0   1.06
mus       0.144  0.026   0.099    0.189  ...    17.0      22.0      91.0   1.09
gamma     0.280  0.041   0.208    0.355  ...    89.0      93.0      44.0   1.01
Is_begin  0.516  0.628   0.002    2.157  ...   103.0      69.0      31.0   1.00
Ia_begin  0.824  0.792   0.015    2.175  ...   115.0      78.0      69.0   1.00
E_begin   0.323  0.339   0.007    1.254  ...    80.0      63.0      81.0   1.02

[8 rows x 11 columns]