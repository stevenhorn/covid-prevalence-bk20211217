0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -1703.13    61.98
p_loo       60.15        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      588   93.0%
 (0.5, 0.7]   (ok)         32    5.1%
   (0.7, 1]   (bad)         8    1.3%
   (1, Inf)   (very bad)    4    0.6%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.218  0.049   0.160    0.341  ...    64.0      75.0      39.0   1.02
pu        0.727  0.022   0.701    0.771  ...    68.0      70.0      42.0   1.03
mu        0.157  0.027   0.116    0.206  ...    30.0      29.0      59.0   1.06
mus       0.241  0.040   0.166    0.314  ...    50.0      46.0      40.0   1.06
gamma     0.363  0.050   0.287    0.460  ...    74.0      69.0      56.0   1.03
Is_begin  0.577  0.519   0.004    1.701  ...    92.0      87.0      60.0   1.00
Ia_begin  1.172  1.204   0.006    3.752  ...    76.0      52.0      56.0   0.99
E_begin   0.455  0.475   0.002    1.464  ...    86.0      66.0      39.0   0.98

[8 rows x 11 columns]