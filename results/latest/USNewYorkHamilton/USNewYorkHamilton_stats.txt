0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo  -926.41    52.22
p_loo       61.76        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      582   91.8%
 (0.5, 0.7]   (ok)         38    6.0%
   (0.7, 1]   (bad)        10    1.6%
   (1, Inf)   (very bad)    4    0.6%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.226  0.048   0.154    0.301  ...    64.0      93.0     100.0   1.02
pu        0.736  0.020   0.702    0.771  ...    36.0      39.0      91.0   1.03
mu        0.168  0.030   0.113    0.225  ...    25.0      23.0      99.0   1.06
mus       0.257  0.040   0.188    0.332  ...    49.0      53.0      55.0   1.01
gamma     0.336  0.062   0.207    0.450  ...    36.0      38.0      74.0   1.06
Is_begin  0.858  0.887   0.022    2.430  ...    46.0      68.0      40.0   1.01
Ia_begin  0.719  0.690   0.035    1.966  ...   117.0     121.0      60.0   1.02
E_begin   0.527  0.574   0.001    1.900  ...   119.0      90.0      39.0   0.99

[8 rows x 11 columns]