0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1695.18    91.97
p_loo       71.78        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      589   92.9%
 (0.5, 0.7]   (ok)         34    5.4%
   (0.7, 1]   (bad)         8    1.3%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.156  0.005   0.150    0.163  ...   143.0     149.0      59.0   1.03
pu        0.702  0.002   0.700    0.706  ...   152.0     109.0      24.0   1.01
mu        0.113  0.012   0.089    0.131  ...    49.0      49.0      42.0   0.98
mus       0.284  0.035   0.233    0.361  ...    85.0      85.0      60.0   1.01
gamma     0.598  0.077   0.470    0.726  ...   118.0     114.0      99.0   0.99
Is_begin  0.349  0.496   0.002    1.300  ...    82.0      28.0      60.0   1.07
Ia_begin  0.366  0.373   0.002    1.203  ...    13.0      13.0      59.0   1.16
E_begin   0.175  0.272   0.001    0.765  ...    24.0      10.0      36.0   1.16

[8 rows x 11 columns]