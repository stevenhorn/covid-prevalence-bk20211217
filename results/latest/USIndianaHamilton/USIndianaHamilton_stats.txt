0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -3094.53    49.57
p_loo       44.43        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      578   91.2%
 (0.5, 0.7]   (ok)         43    6.8%
   (0.7, 1]   (bad)        11    1.7%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.172  0.021   0.151    0.224  ...    14.0      13.0      40.0   1.13
pu        0.708  0.007   0.700    0.721  ...    74.0      65.0      60.0   1.00
mu        0.100  0.013   0.076    0.124  ...     6.0       6.0      40.0   1.31
mus       0.172  0.026   0.125    0.215  ...   100.0      90.0      81.0   1.01
gamma     0.285  0.038   0.215    0.341  ...    97.0     120.0      96.0   0.98
Is_begin  2.046  1.450   0.037    4.812  ...   119.0      97.0      47.0   1.01
Ia_begin  5.541  3.498   0.559   13.059  ...   101.0      78.0      72.0   1.04
E_begin   4.238  3.463   0.093   10.641  ...    91.0      54.0      97.0   1.03

[8 rows x 11 columns]