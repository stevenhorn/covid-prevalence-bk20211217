0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2823.17    50.21
p_loo       45.05        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      588   92.7%
 (0.5, 0.7]   (ok)         41    6.5%
   (0.7, 1]   (bad)         5    0.8%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.162  0.012   0.150    0.190  ...   101.0      75.0      59.0   1.01
pu        0.703  0.003   0.700    0.710  ...   152.0     148.0      57.0   1.01
mu        0.077  0.011   0.061    0.100  ...    18.0      19.0      21.0   1.12
mus       0.158  0.029   0.111    0.214  ...   150.0     152.0      93.0   0.99
gamma     0.251  0.041   0.188    0.333  ...    91.0     101.0      59.0   1.04
Is_begin  0.721  0.669   0.010    1.993  ...    20.0      18.0      32.0   1.09
Ia_begin  1.258  1.101   0.008    3.582  ...    51.0      39.0      60.0   0.99
E_begin   0.607  0.587   0.009    1.856  ...    79.0      51.0      59.0   0.99

[8 rows x 11 columns]