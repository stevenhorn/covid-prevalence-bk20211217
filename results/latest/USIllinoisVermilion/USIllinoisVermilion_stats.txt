0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2557.45    44.55
p_loo       33.81        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      586   92.4%
 (0.5, 0.7]   (ok)         39    6.2%
   (0.7, 1]   (bad)         8    1.3%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.156  0.005   0.150    0.164  ...   152.0     120.0      88.0   1.00
pu        0.702  0.002   0.700    0.705  ...   104.0      84.0      59.0   1.02
mu        0.078  0.009   0.061    0.095  ...    36.0      34.0      61.0   1.08
mus       0.146  0.019   0.114    0.180  ...   130.0     143.0      91.0   1.02
gamma     0.268  0.043   0.202    0.357  ...    62.0      60.0      77.0   1.03
Is_begin  0.415  0.395   0.015    1.154  ...    85.0      64.0      80.0   1.03
Ia_begin  0.774  0.923   0.006    2.541  ...    75.0      55.0      39.0   1.07
E_begin   0.327  0.339   0.006    0.906  ...    62.0      69.0      25.0   1.03

[8 rows x 11 columns]