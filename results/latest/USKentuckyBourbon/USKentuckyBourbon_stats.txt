0 Divergences 
Failed validation 
2021-12-09 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 635 log-likelihood matrix

         Estimate       SE
elpd_loo -1795.83    41.40
p_loo       38.32        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      602   94.8%
 (0.5, 0.7]   (ok)         27    4.3%
   (0.7, 1]   (bad)         6    0.9%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.166  0.013   0.150    0.195  ...    59.0      63.0      40.0   1.11
pu        0.705  0.006   0.700    0.716  ...   132.0      64.0      60.0   1.03
mu        0.093  0.017   0.064    0.123  ...     9.0       8.0      24.0   1.25
mus       0.170  0.028   0.125    0.222  ...    78.0      84.0      50.0   1.05
gamma     0.212  0.034   0.149    0.269  ...    83.0      76.0      59.0   1.01
Is_begin  0.655  0.548   0.020    1.670  ...    83.0      50.0      24.0   1.05
Ia_begin  0.585  0.427   0.042    1.330  ...    53.0      53.0      33.0   1.00
E_begin   0.364  0.305   0.027    1.087  ...    74.0      53.0      87.0   1.05

[8 rows x 11 columns]