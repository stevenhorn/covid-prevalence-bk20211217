0 Divergences 
Passed validation 
2021-12-16 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 643 log-likelihood matrix

         Estimate       SE
elpd_loo -1103.64    51.95
p_loo       61.61        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      595   92.5%
 (0.5, 0.7]   (ok)         38    5.9%
   (0.7, 1]   (bad)         8    1.2%
   (1, Inf)   (very bad)    2    0.3%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.277   0.053   0.177    0.348  ...    85.0      86.0      77.0   1.12
pu         0.886   0.015   0.859    0.900  ...    22.0      18.0      47.0   1.08
mu         0.251   0.029   0.205    0.317  ...    21.0      23.0      15.0   1.07
mus        0.285   0.038   0.212    0.354  ...   117.0     109.0     100.0   1.00
gamma      0.482   0.074   0.332    0.592  ...    51.0      79.0      96.0   1.03
Is_begin   5.061   4.551   0.185   14.074  ...   107.0     152.0      72.0   0.98
Ia_begin  20.881  14.569   0.202   46.766  ...   126.0     115.0      58.0   1.01
E_begin    8.250   7.971   0.233   25.430  ...    46.0      61.0      60.0   1.04

[8 rows x 11 columns]