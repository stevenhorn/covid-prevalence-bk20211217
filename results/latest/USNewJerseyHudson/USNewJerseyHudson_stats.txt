0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -3299.51    44.13
p_loo       38.45        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      576   90.9%
 (0.5, 0.7]   (ok)         49    7.7%
   (0.7, 1]   (bad)         8    1.3%
   (1, Inf)   (very bad)    1    0.2%

             mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa          0.162   0.010   0.150    0.183  ...   152.0      98.0      81.0   1.00
pu          0.706   0.005   0.700    0.715  ...   104.0      98.0      60.0   1.00
mu          0.093   0.012   0.072    0.118  ...     9.0      11.0      22.0   1.21
mus         0.169   0.025   0.123    0.211  ...   111.0     126.0     100.0   1.00
gamma       0.361   0.044   0.296    0.437  ...   152.0     147.0     100.0   0.99
Is_begin   26.338  17.579   0.872   55.909  ...    54.0      45.0      58.0   0.99
Ia_begin   76.560  27.427  27.554  125.270  ...    56.0      59.0      43.0   1.01
E_begin   104.675  47.313   7.106  200.000  ...    76.0      68.0      59.0   1.04

[8 rows x 11 columns]