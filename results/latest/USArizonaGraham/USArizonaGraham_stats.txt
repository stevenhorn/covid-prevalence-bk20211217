0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2331.84    80.08
p_loo       70.87        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      601   94.8%
 (0.5, 0.7]   (ok)         24    3.8%
   (0.7, 1]   (bad)         4    0.6%
   (1, Inf)   (very bad)    5    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.155  0.007   0.150    0.163  ...   120.0      99.0      81.0   0.99
pu        0.702  0.002   0.700    0.705  ...   152.0     137.0      30.0   1.00
mu        0.101  0.011   0.080    0.119  ...    33.0      36.0      57.0   1.03
mus       0.201  0.032   0.143    0.265  ...    94.0     103.0      49.0   0.99
gamma     0.398  0.061   0.313    0.540  ...    33.0      65.0      57.0   1.10
Is_begin  0.555  0.558   0.009    1.627  ...   115.0      47.0      59.0   1.03
Ia_begin  0.896  0.886   0.032    2.542  ...   110.0      89.0      80.0   1.00
E_begin   0.340  0.331   0.003    0.974  ...    99.0      79.0      60.0   0.99

[8 rows x 11 columns]