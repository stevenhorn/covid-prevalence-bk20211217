0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1666.29    54.12
p_loo       63.33        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      590   93.1%
 (0.5, 0.7]   (ok)         40    6.3%
   (0.7, 1]   (bad)         1    0.2%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.168  0.017   0.150    0.203  ...   152.0     152.0      60.0   0.98
pu        0.706  0.006   0.700    0.718  ...   152.0     100.0      22.0   1.03
mu        0.152  0.018   0.111    0.178  ...   103.0      93.0      59.0   1.02
mus       0.244  0.034   0.186    0.296  ...    98.0     119.0      79.0   1.01
gamma     0.430  0.066   0.321    0.557  ...   152.0     152.0      54.0   1.09
Is_begin  0.652  0.702   0.004    2.080  ...    60.0     102.0      56.0   1.01
Ia_begin  1.031  1.068   0.054    3.057  ...    66.0      47.0      45.0   1.04
E_begin   0.488  0.469   0.016    1.407  ...    62.0     107.0      60.0   1.02

[8 rows x 11 columns]