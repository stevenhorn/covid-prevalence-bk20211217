0 Divergences 
Failed validation 
2021-12-09 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 635 log-likelihood matrix

         Estimate       SE
elpd_loo -2139.07    90.91
p_loo       57.32        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      603   95.0%
 (0.5, 0.7]   (ok)         24    3.8%
   (0.7, 1]   (bad)         7    1.1%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.168  0.014   0.151    0.200  ...    99.0     129.0      65.0   1.05
pu        0.705  0.004   0.700    0.714  ...    84.0      49.0      15.0   1.03
mu        0.092  0.011   0.072    0.113  ...    11.0      13.0      60.0   1.14
mus       0.224  0.031   0.170    0.281  ...   119.0     131.0      40.0   1.18
gamma     0.621  0.093   0.476    0.773  ...    79.0      76.0      28.0   1.01
Is_begin  0.377  0.432   0.001    1.371  ...    69.0     129.0      59.0   1.03
Ia_begin  0.420  0.438   0.003    1.187  ...    75.0      76.0      59.0   1.01
E_begin   0.213  0.228   0.002    0.735  ...   103.0      93.0      59.0   1.00

[8 rows x 11 columns]