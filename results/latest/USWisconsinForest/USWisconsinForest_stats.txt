0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -1371.65    39.31
p_loo       40.56        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      588   93.0%
 (0.5, 0.7]   (ok)         33    5.2%
   (0.7, 1]   (bad)        10    1.6%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.156  0.007   0.150    0.173  ...   152.0     152.0      60.0   1.00
pu        0.702  0.002   0.700    0.706  ...   132.0      83.0      54.0   1.00
mu        0.080  0.014   0.058    0.106  ...   119.0     124.0      24.0   1.13
mus       0.188  0.026   0.141    0.235  ...   150.0     152.0      96.0   0.99
gamma     0.292  0.046   0.216    0.375  ...   144.0     152.0      96.0   1.12
Is_begin  0.259  0.296   0.002    0.839  ...   102.0      84.0      43.0   0.98
Ia_begin  0.334  0.545   0.007    1.083  ...    92.0     100.0     100.0   1.02
E_begin   0.168  0.267   0.001    0.616  ...    65.0      98.0      19.0   1.00

[8 rows x 11 columns]