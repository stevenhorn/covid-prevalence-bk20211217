0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -3021.16    68.55
p_loo       51.38        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      584   92.1%
 (0.5, 0.7]   (ok)         38    6.0%
   (0.7, 1]   (bad)        10    1.6%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.173  0.017   0.152    0.205  ...    66.0      60.0      56.0   1.00
pu        0.707  0.006   0.700    0.720  ...    88.0      47.0      24.0   1.01
mu        0.079  0.010   0.063    0.096  ...    16.0      24.0      93.0   1.11
mus       0.246  0.040   0.182    0.315  ...    69.0      65.0      37.0   1.02
gamma     0.339  0.053   0.257    0.456  ...   152.0     152.0      87.0   1.05
Is_begin  0.623  0.582   0.008    1.836  ...   152.0      83.0      60.0   1.00
Ia_begin  0.891  0.808   0.061    2.344  ...    57.0      35.0      79.0   1.00
E_begin   0.506  0.458   0.002    1.379  ...   102.0      43.0      56.0   1.03

[8 rows x 11 columns]