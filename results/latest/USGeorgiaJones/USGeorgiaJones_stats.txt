0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1871.50    60.59
p_loo       59.83        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      597   94.2%
 (0.5, 0.7]   (ok)         28    4.4%
   (0.7, 1]   (bad)         6    0.9%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.250  0.053   0.160    0.335  ...    70.0      71.0      56.0   1.03
pu        0.745  0.026   0.702    0.781  ...    41.0      41.0      83.0   1.02
mu        0.123  0.020   0.090    0.163  ...    26.0      25.0      40.0   1.02
mus       0.168  0.030   0.107    0.212  ...    77.0      78.0      84.0   0.98
gamma     0.252  0.048   0.161    0.335  ...    29.0      37.0      59.0   1.06
Is_begin  0.657  0.601   0.023    1.817  ...   100.0      94.0      87.0   1.02
Ia_begin  1.182  1.222   0.013    3.704  ...   104.0      86.0      40.0   0.99
E_begin   0.524  0.585   0.005    1.700  ...   108.0      90.0      60.0   0.98

[8 rows x 11 columns]