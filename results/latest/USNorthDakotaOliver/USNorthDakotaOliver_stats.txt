0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo  -563.74    41.77
p_loo       50.29        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      572   90.2%
 (0.5, 0.7]   (ok)         53    8.4%
   (0.7, 1]   (bad)         9    1.4%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.254  0.050   0.162    0.333  ...   148.0     143.0      49.0   1.07
pu        0.783  0.023   0.743    0.820  ...   152.0     152.0      67.0   1.01
mu        0.151  0.024   0.105    0.196  ...    51.0      45.0      39.0   1.05
mus       0.237  0.035   0.179    0.298  ...    64.0      92.0      53.0   1.08
gamma     0.284  0.045   0.195    0.374  ...    90.0     109.0      37.0   0.98
Is_begin  0.566  0.626   0.007    1.514  ...   108.0     152.0      88.0   1.10
Ia_begin  1.045  1.104   0.007    3.405  ...   100.0     102.0      69.0   1.02
E_begin   0.397  0.387   0.031    1.208  ...    97.0      98.0      69.0   0.99

[8 rows x 11 columns]