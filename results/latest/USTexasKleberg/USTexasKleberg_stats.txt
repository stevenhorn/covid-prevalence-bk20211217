0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -2036.70    71.58
p_loo       66.75        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      597   94.5%
 (0.5, 0.7]   (ok)         30    4.7%
   (0.7, 1]   (bad)         2    0.3%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.168  0.013   0.152    0.192  ...    69.0      86.0      31.0   1.00
pu        0.706  0.006   0.700    0.716  ...    29.0      23.0      31.0   1.06
mu        0.119  0.013   0.093    0.141  ...    16.0      15.0      86.0   1.10
mus       0.265  0.031   0.206    0.319  ...    12.0      13.0      59.0   1.14
gamma     0.504  0.083   0.364    0.683  ...    24.0      26.0      59.0   1.06
Is_begin  0.801  0.761   0.029    2.299  ...    23.0      52.0      20.0   1.04
Ia_begin  1.155  1.444   0.028    3.767  ...    77.0      25.0      59.0   1.07
E_begin   0.433  0.435   0.014    1.335  ...    23.0      14.0      72.0   1.10

[8 rows x 11 columns]