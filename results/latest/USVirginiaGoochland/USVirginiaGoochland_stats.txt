0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -1586.84    43.89
p_loo       48.42        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      595   94.1%
 (0.5, 0.7]   (ok)         30    4.7%
   (0.7, 1]   (bad)         5    0.8%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.262  0.046   0.192    0.347  ...    43.0      43.0      54.0   1.03
pu        0.768  0.028   0.714    0.811  ...    30.0      34.0      22.0   1.02
mu        0.160  0.025   0.114    0.208  ...    30.0      29.0      46.0   1.03
mus       0.195  0.025   0.148    0.234  ...    92.0      91.0      86.0   0.99
gamma     0.260  0.053   0.174    0.342  ...    60.0      83.0      38.0   1.02
Is_begin  1.136  0.891   0.004    2.941  ...    85.0      57.0      34.0   1.03
Ia_begin  2.517  1.950   0.008    6.299  ...   106.0      74.0      21.0   0.99
E_begin   1.726  1.738   0.016    5.229  ...   144.0     140.0      57.0   0.99

[8 rows x 11 columns]