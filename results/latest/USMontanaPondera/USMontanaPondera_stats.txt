0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1170.93    38.50
p_loo       42.56        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      586   92.4%
 (0.5, 0.7]   (ok)         39    6.2%
   (0.7, 1]   (bad)         8    1.3%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.227  0.040   0.154    0.283  ...    96.0      90.0      31.0   1.02
pu        0.726  0.017   0.703    0.758  ...    43.0      48.0      60.0   1.03
mu        0.126  0.019   0.095    0.161  ...    65.0      62.0      81.0   1.01
mus       0.183  0.028   0.134    0.232  ...   152.0     152.0      91.0   0.99
gamma     0.234  0.037   0.158    0.297  ...    74.0      70.0      96.0   1.01
Is_begin  0.450  0.559   0.003    1.702  ...    14.0      12.0      45.0   1.13
Ia_begin  0.663  0.921   0.005    2.038  ...    99.0      77.0      57.0   1.02
E_begin   0.291  0.345   0.001    0.858  ...    88.0      42.0      14.0   1.03

[8 rows x 11 columns]