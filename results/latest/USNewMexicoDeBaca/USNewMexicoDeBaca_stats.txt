0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo  -842.26    55.86
p_loo       61.09        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      568   89.6%
 (0.5, 0.7]   (ok)         40    6.3%
   (0.7, 1]   (bad)        19    3.0%
   (1, Inf)   (very bad)    7    1.1%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.175  0.019   0.151    0.209  ...   123.0     107.0      29.0   1.01
pu        0.708  0.007   0.700    0.720  ...    81.0      87.0      60.0   1.09
mu        0.135  0.021   0.094    0.168  ...    32.0      46.0      14.0   1.02
mus       0.267  0.041   0.198    0.340  ...   110.0     126.0      60.0   1.01
gamma     0.307  0.042   0.236    0.389  ...    83.0      89.0      59.0   0.99
Is_begin  0.351  0.458   0.003    1.081  ...    66.0      61.0      83.0   0.98
Ia_begin  0.407  0.616   0.004    1.421  ...    90.0      39.0      39.0   1.04
E_begin   0.232  0.521   0.000    0.590  ...    91.0      68.0      43.0   1.01

[8 rows x 11 columns]