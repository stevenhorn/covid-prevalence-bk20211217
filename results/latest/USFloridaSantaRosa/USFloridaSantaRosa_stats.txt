1 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -3302.30    47.85
p_loo      111.22        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      591   93.2%
 (0.5, 0.7]   (ok)         32    5.0%
   (0.7, 1]   (bad)         9    1.4%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.177  0.028   0.151    0.226  ...    10.0      11.0      43.0   1.18
pu        0.709  0.008   0.700    0.725  ...    14.0      14.0      64.0   1.13
mu        0.153  0.018   0.116    0.181  ...    17.0      18.0      17.0   1.11
mus       0.191  0.035   0.141    0.256  ...    76.0      91.0      88.0   1.08
gamma     0.233  0.038   0.178    0.305  ...     9.0       8.0      54.0   1.23
Is_begin  0.750  0.668   0.008    1.910  ...   113.0      71.0      57.0   1.02
Ia_begin  0.619  0.602   0.005    1.795  ...   121.0      96.0      77.0   1.01
E_begin   0.387  0.438   0.006    1.226  ...    39.0     103.0      61.0   1.03

[8 rows x 11 columns]