0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -1338.61    59.86
p_loo       62.36        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      582   92.1%
 (0.5, 0.7]   (ok)         38    6.0%
   (0.7, 1]   (bad)         9    1.4%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.256  0.052   0.163    0.341  ...    90.0      93.0      40.0   1.03
pu        0.755  0.025   0.707    0.794  ...    42.0      47.0      60.0   1.03
mu        0.159  0.021   0.123    0.194  ...    78.0      73.0     100.0   0.99
mus       0.224  0.038   0.178    0.313  ...   127.0     135.0      86.0   0.99
gamma     0.288  0.045   0.219    0.389  ...   112.0     103.0      99.0   1.00
Is_begin  0.808  0.715   0.002    2.167  ...    92.0      98.0      58.0   1.00
Ia_begin  1.825  1.545   0.102    4.605  ...    89.0      80.0      43.0   1.02
E_begin   0.706  0.549   0.030    1.777  ...    97.0      57.0      91.0   1.02

[8 rows x 11 columns]