0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 633 log-likelihood matrix

         Estimate       SE
elpd_loo -3322.68    53.20
p_loo       69.59        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      567   89.6%
 (0.5, 0.7]   (ok)         50    7.9%
   (0.7, 1]   (bad)        13    2.1%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.247  0.050   0.157    0.338  ...    58.0      55.0      38.0   1.03
pu        0.792  0.056   0.702    0.875  ...    38.0      56.0      59.0   1.04
mu        0.190  0.023   0.150    0.229  ...    23.0      25.0      67.0   1.05
mus       0.253  0.032   0.204    0.310  ...    65.0      76.0      60.0   1.03
gamma     0.557  0.071   0.406    0.664  ...    91.0      91.0      80.0   1.02
Is_begin  1.036  0.867   0.026    2.544  ...   136.0     125.0      96.0   1.01
Ia_begin  2.119  2.034   0.162    6.419  ...   103.0      74.0      79.0   1.05
E_begin   1.009  1.175   0.015    3.407  ...    41.0      38.0      88.0   1.03

[8 rows x 11 columns]