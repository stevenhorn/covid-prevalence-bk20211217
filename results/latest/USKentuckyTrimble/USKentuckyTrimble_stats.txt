0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1364.25    38.24
p_loo       45.38        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      580   91.5%
 (0.5, 0.7]   (ok)         46    7.3%
   (0.7, 1]   (bad)         8    1.3%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.205  0.035   0.150    0.276  ...    10.0       9.0      22.0   1.21
pu        0.715  0.014   0.700    0.743  ...    43.0      27.0      60.0   1.08
mu        0.119  0.023   0.075    0.163  ...    27.0      29.0      59.0   1.07
mus       0.206  0.034   0.144    0.253  ...    41.0      43.0      38.0   1.06
gamma     0.258  0.049   0.182    0.341  ...    56.0      60.0      43.0   0.99
Is_begin  0.500  0.552   0.002    1.546  ...    75.0      62.0      25.0   1.04
Ia_begin  0.665  0.706   0.004    2.003  ...    93.0      62.0      15.0   1.00
E_begin   0.306  0.363   0.009    0.969  ...    74.0      60.0      76.0   1.04

[8 rows x 11 columns]