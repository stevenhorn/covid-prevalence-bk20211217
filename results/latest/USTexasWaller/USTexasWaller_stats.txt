0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -2138.10    60.98
p_loo       46.85        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      595   94.1%
 (0.5, 0.7]   (ok)         32    5.1%
   (0.7, 1]   (bad)         2    0.3%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.252  0.057   0.152    0.343  ...   152.0     152.0      53.0   1.04
pu        0.805  0.016   0.777    0.830  ...   152.0     152.0      59.0   1.00
mu        0.106  0.017   0.079    0.142  ...    19.0      22.0      18.0   1.07
mus       0.164  0.029   0.113    0.217  ...    66.0      83.0      53.0   1.02
gamma     0.199  0.033   0.151    0.262  ...    36.0      40.0      72.0   1.04
Is_begin  0.571  0.531   0.005    1.450  ...    90.0     101.0      59.0   0.99
Ia_begin  1.190  1.254   0.005    3.741  ...    99.0      95.0      58.0   1.00
E_begin   0.534  0.579   0.023    1.891  ...   114.0      82.0      99.0   1.01

[8 rows x 11 columns]