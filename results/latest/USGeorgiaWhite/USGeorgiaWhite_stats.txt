0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1908.94    59.66
p_loo       53.26        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      609   96.1%
 (0.5, 0.7]   (ok)         18    2.8%
   (0.7, 1]   (bad)         5    0.8%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.161  0.011   0.150    0.183  ...   119.0     100.0      56.0   1.09
pu        0.704  0.004   0.700    0.712  ...    97.0      76.0      55.0   1.07
mu        0.097  0.022   0.064    0.131  ...     3.0       4.0      27.0   1.76
mus       0.196  0.031   0.143    0.244  ...    82.0      75.0     102.0   1.04
gamma     0.285  0.040   0.211    0.364  ...    34.0      37.0      56.0   1.03
Is_begin  0.587  0.600   0.008    1.744  ...    85.0      61.0      72.0   1.00
Ia_begin  1.019  1.070   0.007    2.847  ...   108.0      80.0      40.0   1.00
E_begin   0.571  0.640   0.006    1.810  ...   120.0     101.0      60.0   0.99

[8 rows x 11 columns]