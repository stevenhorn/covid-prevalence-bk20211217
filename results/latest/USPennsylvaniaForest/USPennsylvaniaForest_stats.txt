0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1718.75   132.66
p_loo       59.89        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      603   95.1%
 (0.5, 0.7]   (ok)         19    3.0%
   (0.7, 1]   (bad)         8    1.3%
   (1, Inf)   (very bad)    4    0.6%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.156  0.005   0.150    0.165  ...   127.0     100.0      99.0   1.01
pu        0.702  0.002   0.700    0.705  ...   152.0     115.0      81.0   1.00
mu        0.055  0.008   0.038    0.068  ...    97.0     101.0      74.0   1.04
mus       0.314  0.034   0.254    0.371  ...   128.0     131.0      63.0   1.04
gamma     0.661  0.074   0.552    0.795  ...    82.0      96.0      60.0   1.02
Is_begin  0.431  0.468   0.006    1.326  ...    60.0      66.0      57.0   1.08
Ia_begin  0.384  0.532   0.002    1.636  ...    74.0      49.0      91.0   1.05
E_begin   0.175  0.167   0.003    0.436  ...    81.0      70.0      91.0   1.03

[8 rows x 11 columns]