0 Divergences 
Failed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -1878.19    92.11
p_loo       54.06        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      587   92.9%
 (0.5, 0.7]   (ok)         33    5.2%
   (0.7, 1]   (bad)        10    1.6%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.178  0.023   0.151    0.223  ...    98.0      89.0      59.0   1.04
pu        0.709  0.008   0.700    0.725  ...    63.0      21.0      40.0   1.08
mu        0.134  0.014   0.114    0.164  ...    31.0      37.0      61.0   1.10
mus       0.324  0.039   0.257    0.392  ...    90.0      92.0      55.0   1.04
gamma     0.599  0.102   0.428    0.767  ...   152.0     152.0      87.0   1.01
Is_begin  0.587  0.505   0.003    1.698  ...    65.0      45.0      38.0   1.00
Ia_begin  0.856  0.924   0.010    2.001  ...    83.0      78.0      88.0   0.98
E_begin   0.433  0.403   0.025    1.260  ...    76.0      68.0      43.0   1.02

[8 rows x 11 columns]