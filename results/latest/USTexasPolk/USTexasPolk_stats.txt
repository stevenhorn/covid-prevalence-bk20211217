0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -2562.71    63.68
p_loo       66.50        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      599   94.8%
 (0.5, 0.7]   (ok)         25    4.0%
   (0.7, 1]   (bad)         6    0.9%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.194  0.031   0.153    0.255  ...   152.0     152.0      87.0   0.99
pu        0.713  0.010   0.701    0.731  ...   115.0     120.0      93.0   1.00
mu        0.162  0.020   0.131    0.203  ...    47.0      45.0      60.0   1.01
mus       0.239  0.031   0.186    0.296  ...    96.0     105.0      74.0   1.03
gamma     0.439  0.057   0.354    0.554  ...    81.0      84.0      88.0   1.00
Is_begin  0.837  0.739   0.003    2.685  ...   111.0     150.0      93.0   0.99
Ia_begin  1.455  1.770   0.015    3.492  ...   111.0      33.0      96.0   1.06
E_begin   0.722  0.976   0.001    2.170  ...    97.0      46.0      60.0   1.07

[8 rows x 11 columns]