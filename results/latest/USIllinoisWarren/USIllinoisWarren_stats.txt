0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1554.45    28.91
p_loo       33.72        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      590   93.1%
 (0.5, 0.7]   (ok)         40    6.3%
   (0.7, 1]   (bad)         3    0.5%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.182  0.025   0.152    0.232  ...   116.0     145.0      45.0   1.03
pu        0.713  0.012   0.701    0.736  ...    78.0      80.0      60.0   1.02
mu        0.127  0.019   0.096    0.164  ...    11.0      12.0      80.0   1.18
mus       0.176  0.029   0.122    0.223  ...    64.0      60.0      88.0   1.05
gamma     0.243  0.042   0.184    0.316  ...   125.0     152.0      59.0   0.99
Is_begin  0.627  0.688   0.004    2.141  ...    95.0      92.0      25.0   1.00
Ia_begin  1.010  1.135   0.053    3.704  ...    97.0      95.0      60.0   0.99
E_begin   0.563  0.615   0.000    1.703  ...   109.0      72.0      60.0   0.99

[8 rows x 11 columns]