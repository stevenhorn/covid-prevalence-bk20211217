0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1928.48    50.76
p_loo       45.39        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      568   89.6%
 (0.5, 0.7]   (ok)         53    8.4%
   (0.7, 1]   (bad)         9    1.4%
   (1, Inf)   (very bad)    4    0.6%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.160  0.010   0.150    0.177  ...   106.0      73.0     100.0   1.01
pu        0.703  0.003   0.700    0.709  ...    77.0      45.0      60.0   1.04
mu        0.075  0.013   0.052    0.099  ...    20.0      20.0      15.0   1.18
mus       0.163  0.027   0.109    0.210  ...    66.0      62.0      55.0   1.00
gamma     0.242  0.053   0.165    0.346  ...    66.0      69.0      59.0   1.03
Is_begin  0.529  0.516   0.045    1.591  ...    93.0      66.0      60.0   1.04
Ia_begin  0.903  0.876   0.007    2.615  ...    41.0      46.0      60.0   1.01
E_begin   0.364  0.339   0.007    1.055  ...    71.0      62.0      59.0   1.02

[8 rows x 11 columns]