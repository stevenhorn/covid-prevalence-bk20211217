0 Divergences 
Failed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -1776.13    54.07
p_loo       40.17        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      590   93.4%
 (0.5, 0.7]   (ok)         34    5.4%
   (0.7, 1]   (bad)         7    1.1%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.172  0.017   0.151    0.200  ...    85.0      92.0      80.0   1.04
pu        0.707  0.006   0.700    0.718  ...   112.0      96.0      96.0   1.01
mu        0.104  0.015   0.079    0.132  ...    85.0      90.0      59.0   0.99
mus       0.146  0.030   0.101    0.201  ...   152.0     152.0      59.0   1.03
gamma     0.207  0.043   0.136    0.275  ...    83.0      83.0      95.0   1.02
Is_begin  0.540  0.543   0.003    1.606  ...    93.0      73.0      58.0   1.01
Ia_begin  0.991  1.255   0.011    3.093  ...    85.0      56.0      80.0   1.03
E_begin   0.418  0.554   0.003    1.407  ...    68.0      60.0      48.0   1.02

[8 rows x 11 columns]