0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -2011.55    36.11
p_loo       39.10        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      584   92.4%
 (0.5, 0.7]   (ok)         37    5.9%
   (0.7, 1]   (bad)        11    1.7%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.187  0.031   0.151    0.249  ...    87.0      95.0      60.0   1.02
pu        0.713  0.010   0.700    0.734  ...   132.0     116.0      59.0   1.02
mu        0.122  0.017   0.094    0.155  ...    28.0      29.0      21.0   1.04
mus       0.162  0.028   0.119    0.214  ...    75.0      78.0      87.0   1.02
gamma     0.233  0.049   0.137    0.327  ...    12.0      17.0      57.0   1.09
Is_begin  0.587  0.443   0.028    1.318  ...   125.0     108.0      99.0   0.99
Ia_begin  0.987  1.065   0.009    3.544  ...    51.0      36.0      60.0   1.04
E_begin   0.511  0.508   0.007    1.471  ...    90.0      92.0      79.0   0.99

[8 rows x 11 columns]