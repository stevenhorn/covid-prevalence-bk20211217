0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 633 log-likelihood matrix

         Estimate       SE
elpd_loo -3145.97    81.63
p_loo       83.69        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      580   91.6%
 (0.5, 0.7]   (ok)         34    5.4%
   (0.7, 1]   (bad)        13    2.1%
   (1, Inf)   (very bad)    6    0.9%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.250  0.051   0.162    0.333  ...    63.0      61.0      48.0   1.13
pu        0.774  0.049   0.706    0.872  ...    16.0      16.0      59.0   1.11
mu        0.203  0.015   0.182    0.237  ...    11.0      10.0      15.0   1.17
mus       0.375  0.050   0.291    0.459  ...    43.0      46.0      39.0   0.99
gamma     0.790  0.089   0.645    0.946  ...    17.0      21.0      60.0   1.08
Is_begin  0.711  0.664   0.002    1.999  ...    31.0      24.0      39.0   1.06
Ia_begin  1.049  1.302   0.003    4.428  ...    29.0      19.0      40.0   1.06
E_begin   0.502  0.643   0.001    1.870  ...    50.0      24.0      38.0   1.03

[8 rows x 11 columns]