0 Divergences 
Passed validation 
2021-12-16 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 637 log-likelihood matrix

         Estimate       SE
elpd_loo -1802.82    33.14
p_loo       38.02        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      587   92.2%
 (0.5, 0.7]   (ok)         42    6.6%
   (0.7, 1]   (bad)         7    1.1%
   (1, Inf)   (very bad)    1    0.2%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.294   0.048   0.195    0.348  ...    44.0      42.0      60.0   1.04
pu         0.886   0.012   0.865    0.899  ...    33.0      20.0     100.0   1.10
mu         0.183   0.030   0.140    0.242  ...     4.0       4.0      14.0   1.55
mus        0.199   0.030   0.155    0.268  ...   152.0     152.0      42.0   0.99
gamma      0.298   0.052   0.218    0.396  ...   100.0     122.0      68.0   1.02
Is_begin   9.612   7.583   0.247   24.946  ...    60.0      43.0      23.0   1.07
Ia_begin  79.715  40.467  13.233  151.634  ...    21.0      26.0      30.0   1.05
E_begin   31.347  23.850   0.778   85.763  ...    73.0      47.0      57.0   1.04

[8 rows x 11 columns]