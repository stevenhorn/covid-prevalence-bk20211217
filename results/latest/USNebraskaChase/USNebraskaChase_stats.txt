0 Divergences 
Passed validation 
2021-12-09 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 635 log-likelihood matrix

         Estimate       SE
elpd_loo -1154.68    71.53
p_loo       66.09        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      601   94.6%
 (0.5, 0.7]   (ok)         23    3.6%
   (0.7, 1]   (bad)         8    1.3%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.177  0.024   0.150    0.224  ...    99.0      83.0      22.0   1.00
pu        0.710  0.009   0.700    0.726  ...   152.0     152.0      60.0   1.12
mu        0.146  0.023   0.102    0.193  ...    52.0      58.0      39.0   1.01
mus       0.289  0.044   0.205    0.347  ...   152.0     152.0      86.0   0.99
gamma     0.374  0.074   0.252    0.488  ...   152.0     117.0      58.0   0.99
Is_begin  0.552  0.646   0.005    1.733  ...    99.0      39.0      93.0   1.01
Ia_begin  0.683  0.901   0.014    2.560  ...    48.0      69.0      58.0   1.04
E_begin   0.321  0.441   0.005    1.072  ...    83.0      78.0      83.0   1.00

[8 rows x 11 columns]