0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2057.99    53.39
p_loo       54.61        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      586   92.4%
 (0.5, 0.7]   (ok)         35    5.5%
   (0.7, 1]   (bad)        12    1.9%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.163  0.010   0.150    0.183  ...   116.0     152.0      45.0   1.05
pu        0.704  0.004   0.700    0.712  ...   152.0      71.0      30.0   1.03
mu        0.093  0.013   0.069    0.117  ...    16.0      13.0      38.0   1.13
mus       0.230  0.035   0.180    0.300  ...    54.0      58.0      83.0   1.03
gamma     0.358  0.050   0.280    0.453  ...   103.0      92.0      69.0   1.06
Is_begin  1.225  1.031   0.007    3.182  ...    70.0      43.0      14.0   1.02
Ia_begin  2.973  2.270   0.148    7.342  ...    22.0      21.0      60.0   1.07
E_begin   2.051  2.091   0.033    6.214  ...    57.0      23.0      40.0   1.07

[8 rows x 11 columns]