0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -2835.44    62.09
p_loo       55.97        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      590   93.4%
 (0.5, 0.7]   (ok)         37    5.9%
   (0.7, 1]   (bad)         3    0.5%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.168  0.015   0.150    0.198  ...    58.0      27.0      20.0   1.06
pu        0.707  0.007   0.700    0.721  ...   110.0      82.0      88.0   1.00
mu        0.118  0.018   0.091    0.152  ...     6.0       6.0      18.0   1.37
mus       0.218  0.036   0.170    0.291  ...     8.0      10.0      59.0   1.20
gamma     0.291  0.059   0.194    0.400  ...    71.0      70.0      99.0   0.99
Is_begin  0.947  0.847   0.017    2.872  ...    66.0      45.0      22.0   1.05
Ia_begin  2.272  2.120   0.064    5.752  ...    31.0      15.0      39.0   1.12
E_begin   0.825  0.841   0.014    2.175  ...    52.0      46.0      60.0   1.04

[8 rows x 11 columns]