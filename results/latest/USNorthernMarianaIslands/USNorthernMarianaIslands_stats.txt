0 Divergences 
Failed validation 
2021-11-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 604 log-likelihood matrix

         Estimate       SE
elpd_loo  -975.89    48.85
p_loo       52.35        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      574   95.0%
 (0.5, 0.7]   (ok)         21    3.5%
   (0.7, 1]   (bad)         7    1.2%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.268  0.057   0.150    0.340  ...    87.0      82.0      22.0   1.00
pu        0.870  0.025   0.824    0.900  ...    18.0      19.0      56.0   1.09
mu        0.156  0.030   0.112    0.216  ...    22.0      20.0      51.0   1.09
mus       0.178  0.040   0.104    0.242  ...   136.0     152.0      60.0   1.03
gamma     0.217  0.044   0.141    0.289  ...   152.0     152.0      72.0   0.99
Is_begin  1.097  1.008   0.031    3.163  ...   152.0     102.0      32.0   1.02
Ia_begin  2.845  2.218   0.092    7.176  ...   125.0     117.0      55.0   1.00
E_begin   1.440  1.308   0.031    3.864  ...    47.0      17.0      25.0   1.11

[8 rows x 11 columns]