0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2542.67    44.43
p_loo       42.34        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      577   91.0%
 (0.5, 0.7]   (ok)         50    7.9%
   (0.7, 1]   (bad)         7    1.1%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.255  0.057   0.152    0.337  ...    64.0      62.0      60.0   1.02
pu        0.794  0.025   0.742    0.836  ...    35.0      35.0      34.0   1.05
mu        0.142  0.020   0.112    0.185  ...    32.0      33.0      77.0   1.01
mus       0.183  0.029   0.137    0.236  ...    53.0      53.0      83.0   1.01
gamma     0.238  0.041   0.176    0.318  ...    90.0     119.0      48.0   1.01
Is_begin  1.038  0.919   0.011    2.874  ...   152.0     115.0      20.0   1.02
Ia_begin  1.748  1.893   0.001    4.433  ...    90.0      15.0      22.0   1.10
E_begin   0.897  1.138   0.006    2.833  ...   107.0     100.0      66.0   1.00

[8 rows x 11 columns]