0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1792.03    39.51
p_loo       37.90        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      581   91.6%
 (0.5, 0.7]   (ok)         37    5.8%
   (0.7, 1]   (bad)        14    2.2%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.162  0.012   0.150    0.186  ...    28.0      26.0      58.0   1.05
pu        0.705  0.005   0.700    0.717  ...    51.0      20.0      59.0   1.13
mu        0.157  0.022   0.124    0.200  ...     5.0       5.0      15.0   1.37
mus       0.205  0.029   0.155    0.256  ...     8.0       9.0      24.0   1.23
gamma     0.263  0.045   0.198    0.345  ...     7.0       7.0      14.0   1.25
Is_begin  0.829  0.735   0.016    2.174  ...    52.0      43.0      29.0   1.13
Ia_begin  1.951  1.612   0.117    5.289  ...    32.0      32.0      58.0   1.08
E_begin   0.853  0.750   0.046    2.135  ...    51.0      47.0      42.0   1.01

[8 rows x 11 columns]