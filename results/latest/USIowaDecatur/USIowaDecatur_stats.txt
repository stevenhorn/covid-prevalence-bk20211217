0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1395.11    46.95
p_loo       44.88        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      600   94.6%
 (0.5, 0.7]   (ok)         28    4.4%
   (0.7, 1]   (bad)         5    0.8%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.162  0.010   0.150    0.181  ...   152.0     106.0      60.0   1.01
pu        0.704  0.003   0.700    0.708  ...   103.0      75.0      68.0   1.06
mu        0.090  0.012   0.069    0.109  ...    53.0      59.0      87.0   1.01
mus       0.157  0.029   0.110    0.208  ...   128.0     152.0     100.0   1.01
gamma     0.224  0.044   0.126    0.291  ...   108.0     100.0      58.0   1.01
Is_begin  0.400  0.424   0.003    1.214  ...    93.0      65.0      40.0   1.03
Ia_begin  0.475  0.581   0.003    1.468  ...    99.0      64.0     100.0   1.03
E_begin   0.255  0.349   0.001    0.694  ...    99.0      70.0      54.0   1.00

[8 rows x 11 columns]