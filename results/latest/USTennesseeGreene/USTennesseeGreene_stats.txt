0 Divergences 
Failed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -2578.19    48.28
p_loo       39.77        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      608   96.2%
 (0.5, 0.7]   (ok)         17    2.7%
   (0.7, 1]   (bad)         6    0.9%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.159  0.009   0.150    0.173  ...   152.0     152.0      88.0   0.98
pu        0.703  0.003   0.700    0.708  ...   152.0     152.0      60.0   0.98
mu        0.086  0.013   0.064    0.107  ...    10.0      15.0      58.0   1.21
mus       0.167  0.026   0.128    0.218  ...    93.0      99.0      57.0   0.99
gamma     0.226  0.031   0.178    0.289  ...    95.0      59.0      47.0   1.05
Is_begin  0.670  0.625   0.007    1.641  ...    88.0      78.0      59.0   1.02
Ia_begin  1.139  1.270   0.004    3.853  ...    97.0      59.0      46.0   1.01
E_begin   0.420  0.394   0.009    1.060  ...    98.0      73.0      60.0   0.99

[8 rows x 11 columns]