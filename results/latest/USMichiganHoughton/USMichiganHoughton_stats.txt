0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2027.71    39.63
p_loo       23.15        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      618   97.5%
 (0.5, 0.7]   (ok)         15    2.4%
   (0.7, 1]   (bad)         1    0.2%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.155  0.005   0.150    0.163  ...     5.0       4.0      14.0   1.68
pu        0.702  0.002   0.700    0.705  ...    31.0      22.0      22.0   1.06
mu        0.164  0.014   0.136    0.180  ...     3.0       4.0      24.0   1.63
mus       0.209  0.015   0.175    0.228  ...     4.0       4.0      16.0   1.62
gamma     0.185  0.015   0.165    0.225  ...     7.0       7.0      16.0   1.24
Is_begin  0.874  0.427   0.186    1.656  ...    12.0      15.0      33.0   1.13
Ia_begin  2.242  2.319   0.279    6.778  ...     8.0       5.0      51.0   1.35
E_begin   0.854  0.960   0.019    2.351  ...     9.0       6.0      29.0   1.29

[8 rows x 11 columns]