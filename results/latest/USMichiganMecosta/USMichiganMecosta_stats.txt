40 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2144.88    43.16
p_loo       56.97        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      508   80.1%
 (0.5, 0.7]   (ok)         28    4.4%
   (0.7, 1]   (bad)        11    1.7%
   (1, Inf)   (very bad)   87   13.7%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.180  0.015   0.153    0.192  ...     4.0       4.0      14.0   2.01
pu        0.705  0.005   0.702    0.715  ...     5.0       7.0      16.0   1.92
mu        0.135  0.011   0.112    0.151  ...     9.0       9.0      16.0   1.91
mus       0.178  0.019   0.160    0.224  ...     7.0       8.0      19.0   2.08
gamma     0.241  0.029   0.196    0.285  ...     9.0      10.0      22.0   2.24
Is_begin  0.473  0.353   0.007    0.645  ...    18.0      11.0      24.0   1.93
Ia_begin  1.233  0.752   0.126    2.409  ...    20.0      15.0      40.0   1.98
E_begin   0.906  0.584   0.018    1.416  ...     4.0       4.0      22.0   2.08

[8 rows x 11 columns]