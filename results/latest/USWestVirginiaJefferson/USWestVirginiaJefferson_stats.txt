0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -2049.58    53.29
p_loo       52.60        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      581   91.9%
 (0.5, 0.7]   (ok)         43    6.8%
   (0.7, 1]   (bad)         5    0.8%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.195  0.032   0.152    0.244  ...    59.0      79.0      81.0   1.00
pu        0.715  0.011   0.701    0.731  ...    68.0      48.0      46.0   1.06
mu        0.151  0.020   0.111    0.183  ...    11.0      12.0      29.0   1.16
mus       0.206  0.030   0.163    0.281  ...   115.0     113.0      77.0   0.99
gamma     0.292  0.048   0.222    0.383  ...    83.0      82.0      60.0   1.01
Is_begin  0.860  0.716   0.049    2.222  ...    59.0      51.0      73.0   1.01
Ia_begin  0.733  0.549   0.062    1.892  ...   132.0     135.0      49.0   0.99
E_begin   0.522  0.584   0.004    1.704  ...    69.0      47.0      88.0   0.99

[8 rows x 11 columns]