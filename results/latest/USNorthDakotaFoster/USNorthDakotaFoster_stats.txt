0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1000.48    44.63
p_loo       58.45        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      581   91.6%
 (0.5, 0.7]   (ok)         43    6.8%
   (0.7, 1]   (bad)         6    0.9%
   (1, Inf)   (very bad)    4    0.6%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.177  0.022   0.151    0.223  ...    94.0      77.0      54.0   1.05
pu        0.710  0.008   0.701    0.723  ...    74.0      67.0      95.0   1.03
mu        0.117  0.021   0.080    0.152  ...    67.0      75.0      60.0   0.99
mus       0.235  0.034   0.186    0.309  ...   148.0     152.0      59.0   0.98
gamma     0.302  0.044   0.234    0.385  ...   152.0     152.0      87.0   1.00
Is_begin  0.472  0.516   0.000    1.384  ...   101.0      27.0      16.0   1.05
Ia_begin  0.532  0.730   0.005    1.818  ...    98.0      58.0      69.0   1.03
E_begin   0.271  0.371   0.002    0.941  ...    41.0      12.0     100.0   1.12

[8 rows x 11 columns]