0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1588.58    66.41
p_loo       63.36        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      599   94.5%
 (0.5, 0.7]   (ok)         28    4.4%
   (0.7, 1]   (bad)         4    0.6%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.175  0.022   0.151    0.219  ...    68.0      55.0      99.0   1.05
pu        0.705  0.006   0.700    0.718  ...    82.0      30.0      40.0   1.07
mu        0.105  0.015   0.076    0.126  ...    95.0     104.0      93.0   1.00
mus       0.276  0.053   0.184    0.358  ...    58.0      53.0      93.0   1.02
gamma     0.433  0.077   0.309    0.595  ...    49.0      56.0      83.0   1.05
Is_begin  0.265  0.374   0.001    0.953  ...    53.0      30.0      59.0   1.10
Ia_begin  0.295  0.402   0.003    1.050  ...    68.0      90.0      81.0   0.98
E_begin   0.135  0.165   0.001    0.421  ...    73.0      67.0      60.0   1.02

[8 rows x 11 columns]