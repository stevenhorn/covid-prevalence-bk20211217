0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1360.12    79.84
p_loo       55.94        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      588   92.7%
 (0.5, 0.7]   (ok)         38    6.0%
   (0.7, 1]   (bad)         4    0.6%
   (1, Inf)   (very bad)    4    0.6%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.158  0.007   0.150    0.168  ...    78.0      40.0      40.0   1.07
pu        0.703  0.003   0.700    0.709  ...   152.0     152.0      36.0   1.08
mu        0.123  0.014   0.102    0.149  ...    44.0      42.0      27.0   1.04
mus       0.300  0.035   0.241    0.353  ...    54.0      48.0      52.0   1.05
gamma     0.566  0.080   0.459    0.750  ...    54.0      56.0      91.0   1.03
Is_begin  0.791  0.712   0.029    2.168  ...    96.0      71.0      96.0   1.00
Ia_begin  1.079  1.296   0.008    3.622  ...    74.0      45.0      59.0   1.00
E_begin   0.487  0.495   0.004    1.591  ...   123.0      73.0      43.0   1.00

[8 rows x 11 columns]