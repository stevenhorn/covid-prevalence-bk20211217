0 Divergences 
Failed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -1526.59    95.53
p_loo       64.52        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      591   93.5%
 (0.5, 0.7]   (ok)         29    4.6%
   (0.7, 1]   (bad)         9    1.4%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.170  0.016   0.151    0.203  ...   116.0     100.0      43.0   1.04
pu        0.706  0.006   0.700    0.720  ...   141.0     102.0      59.0   1.01
mu        0.089  0.012   0.066    0.109  ...    33.0      35.0      72.0   1.09
mus       0.287  0.046   0.207    0.369  ...    52.0      59.0      60.0   0.98
gamma     0.396  0.065   0.297    0.518  ...    99.0     134.0      59.0   1.05
Is_begin  0.255  0.420   0.000    0.847  ...    26.0      17.0      49.0   1.10
Ia_begin  0.158  0.177   0.001    0.554  ...    15.0      10.0      56.0   1.17
E_begin   0.103  0.114   0.000    0.343  ...    12.0       7.0      20.0   1.26

[8 rows x 11 columns]