0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -1715.14    43.84
p_loo       46.81        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      587   92.9%
 (0.5, 0.7]   (ok)         33    5.2%
   (0.7, 1]   (bad)         8    1.3%
   (1, Inf)   (very bad)    4    0.6%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.217  0.043   0.151    0.296  ...    83.0      97.0      59.0   0.98
pu        0.724  0.016   0.700    0.749  ...    48.0      40.0      14.0   1.04
mu        0.129  0.025   0.090    0.174  ...    30.0      30.0      59.0   1.08
mus       0.183  0.031   0.139    0.248  ...    68.0      78.0      83.0   1.01
gamma     0.230  0.035   0.171    0.289  ...    47.0      60.0      32.0   1.04
Is_begin  0.508  0.477   0.015    1.419  ...    82.0     117.0      58.0   0.99
Ia_begin  1.168  1.037   0.003    2.953  ...   135.0     107.0      57.0   1.00
E_begin   0.512  0.577   0.010    1.810  ...    64.0      70.0      88.0   1.03

[8 rows x 11 columns]