0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1485.17    83.40
p_loo       65.48        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      586   92.4%
 (0.5, 0.7]   (ok)         39    6.2%
   (0.7, 1]   (bad)         6    0.9%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.165  0.013   0.151    0.191  ...   134.0      84.0      26.0   1.02
pu        0.704  0.005   0.700    0.714  ...    97.0     110.0      60.0   1.02
mu        0.103  0.014   0.079    0.125  ...    33.0      31.0      57.0   1.03
mus       0.269  0.042   0.194    0.342  ...    53.0      56.0      91.0   1.02
gamma     0.529  0.091   0.364    0.689  ...    40.0      43.0      43.0   1.01
Is_begin  0.557  0.611   0.002    1.786  ...   100.0      71.0      57.0   1.03
Ia_begin  0.594  0.635   0.001    1.705  ...    79.0      66.0      86.0   1.02
E_begin   0.244  0.216   0.002    0.581  ...    62.0      50.0      83.0   1.00

[8 rows x 11 columns]