0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -4786.81    40.55
p_loo       39.92        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      598   94.3%
 (0.5, 0.7]   (ok)         26    4.1%
   (0.7, 1]   (bad)         6    0.9%
   (1, Inf)   (very bad)    4    0.6%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.163   0.012   0.150    0.189  ...   121.0      88.0      60.0   1.00
pu         0.705   0.005   0.700    0.715  ...   152.0     152.0      96.0   1.00
mu         0.124   0.019   0.096    0.157  ...    22.0      24.0      59.0   1.07
mus        0.172   0.027   0.121    0.213  ...    91.0      95.0      69.0   1.02
gamma      0.258   0.041   0.196    0.331  ...   152.0     152.0      83.0   0.98
Is_begin  20.795  17.003   0.875   50.857  ...    60.0      47.0      20.0   1.03
Ia_begin  55.979  49.853   0.619  164.304  ...    32.0      28.0      40.0   1.05
E_begin   25.009  25.309   1.238   60.535  ...    69.0      30.0     100.0   1.07

[8 rows x 11 columns]