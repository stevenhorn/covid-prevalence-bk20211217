3 Divergences 
Passed validation 
2021-12-16 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 643 log-likelihood matrix

         Estimate       SE
elpd_loo -2810.20    37.40
p_loo       45.47        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      590   91.8%
 (0.5, 0.7]   (ok)         41    6.4%
   (0.7, 1]   (bad)        12    1.9%
   (1, Inf)   (very bad)    0    0.0%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.239   0.054   0.158    0.324  ...    46.0      33.0      88.0   1.09
pu         0.757   0.034   0.705    0.812  ...    34.0      31.0      33.0   1.04
mu         0.156   0.020   0.121    0.188  ...    10.0      11.0      58.0   1.12
mus        0.192   0.029   0.140    0.247  ...    71.0      69.0      40.0   1.03
gamma      0.322   0.054   0.241    0.442  ...    80.0      69.0      21.0   1.09
Is_begin   6.743   4.605   0.170   15.274  ...    49.0      84.0      25.0   1.14
Ia_begin  21.777  19.362   0.953   59.361  ...    59.0      42.0      46.0   1.00
E_begin    8.956   7.886   0.184   24.816  ...    24.0      21.0      47.0   1.09

[8 rows x 11 columns]