0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -1986.51    60.78
p_loo       50.20        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      598   94.6%
 (0.5, 0.7]   (ok)         23    3.6%
   (0.7, 1]   (bad)         9    1.4%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.169  0.018   0.150    0.203  ...    67.0      82.0      59.0   1.03
pu        0.705  0.005   0.700    0.713  ...    91.0      41.0      24.0   1.03
mu        0.124  0.015   0.092    0.146  ...    29.0      30.0      67.0   1.05
mus       0.212  0.035   0.148    0.273  ...    39.0      36.0      53.0   1.03
gamma     0.344  0.062   0.248    0.461  ...    17.0      19.0      57.0   1.09
Is_begin  0.678  0.609   0.016    1.844  ...   133.0     129.0      48.0   0.99
Ia_begin  1.086  1.066   0.065    2.889  ...   130.0     152.0      84.0   1.00
E_begin   0.428  0.417   0.005    1.231  ...    90.0      77.0      60.0   1.00

[8 rows x 11 columns]