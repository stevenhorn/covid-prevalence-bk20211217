0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -3028.49    39.47
p_loo       31.84        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      608   95.9%
 (0.5, 0.7]   (ok)         23    3.6%
   (0.7, 1]   (bad)         3    0.5%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.168  0.013   0.150    0.191  ...    86.0      84.0      91.0   1.06
pu        0.705  0.005   0.700    0.715  ...   105.0      84.0      83.0   1.02
mu        0.105  0.014   0.082    0.133  ...    21.0      25.0      52.0   1.04
mus       0.149  0.023   0.109    0.190  ...    61.0      65.0      81.0   1.02
gamma     0.225  0.029   0.173    0.272  ...    83.0      83.0      96.0   1.02
Is_begin  0.520  0.618   0.007    1.455  ...    93.0      59.0      58.0   1.01
Ia_begin  0.857  0.883   0.009    2.742  ...    76.0      68.0      60.0   1.02
E_begin   0.404  0.630   0.005    1.462  ...    90.0      47.0      86.0   1.02

[8 rows x 11 columns]