0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1814.93    49.25
p_loo       43.49        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      598   94.3%
 (0.5, 0.7]   (ok)         32    5.0%
   (0.7, 1]   (bad)         3    0.5%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.158  0.006   0.150    0.169  ...    43.0      53.0      72.0   1.03
pu        0.702  0.002   0.700    0.706  ...    52.0      45.0      60.0   1.03
mu        0.077  0.009   0.064    0.094  ...    18.0      20.0      59.0   1.13
mus       0.187  0.028   0.132    0.233  ...   111.0     111.0      72.0   0.99
gamma     0.286  0.043   0.221    0.373  ...   135.0     119.0      54.0   1.00
Is_begin  0.659  0.697   0.012    2.363  ...   142.0     117.0      83.0   1.00
Ia_begin  0.840  0.846   0.004    2.738  ...    76.0      68.0      42.0   1.01
E_begin   0.390  0.454   0.007    1.152  ...    91.0      84.0      46.0   1.02

[8 rows x 11 columns]