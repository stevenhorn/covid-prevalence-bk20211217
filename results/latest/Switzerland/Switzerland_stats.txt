0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 633 log-likelihood matrix

         Estimate       SE
elpd_loo -5126.92    49.82
p_loo       90.07        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      568   89.7%
 (0.5, 0.7]   (ok)         42    6.6%
   (0.7, 1]   (bad)        20    3.2%
   (1, Inf)   (very bad)    3    0.5%

              mean        sd    hdi_3%  ...  ess_bulk  ess_tail  r_hat
pa           0.158     0.011     0.150  ...      39.0      49.0   1.08
pu           0.704     0.004     0.700  ...      47.0      60.0   0.99
mu           0.227     0.053     0.132  ...       3.0      27.0   2.05
mus          0.222     0.037     0.164  ...       5.0      16.0   1.50
gamma        0.286     0.117     0.166  ...       4.0      22.0   1.66
Is_begin  1571.665   930.397   175.037  ...       7.0      28.0   1.28
Ia_begin  3952.424  1603.975  1374.933  ...      57.0      30.0   1.01
E_begin   4280.862  2307.038   350.535  ...      36.0      33.0   1.03

[8 rows x 11 columns]