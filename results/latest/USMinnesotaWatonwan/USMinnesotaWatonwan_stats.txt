0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1610.23    41.53
p_loo       46.79        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      595   93.8%
 (0.5, 0.7]   (ok)         30    4.7%
   (0.7, 1]   (bad)         8    1.3%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.183  0.023   0.150    0.223  ...    55.0      36.0      29.0   1.03
pu        0.716  0.011   0.702    0.738  ...    45.0      53.0      47.0   1.05
mu        0.152  0.027   0.105    0.203  ...     6.0       6.0      22.0   1.29
mus       0.213  0.033   0.158    0.276  ...    73.0      64.0      59.0   1.04
gamma     0.279  0.043   0.209    0.357  ...    47.0      48.0      22.0   1.03
Is_begin  0.568  0.557   0.007    1.637  ...    60.0      29.0      38.0   1.04
Ia_begin  1.305  1.542   0.002    3.256  ...    84.0      48.0      22.0   1.04
E_begin   0.443  0.677   0.000    1.459  ...    48.0      47.0      38.0   1.02

[8 rows x 11 columns]