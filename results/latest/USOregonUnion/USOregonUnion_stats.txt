0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1859.82    42.78
p_loo       53.45        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      594   93.7%
 (0.5, 0.7]   (ok)         33    5.2%
   (0.7, 1]   (bad)         5    0.8%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.201  0.036   0.152    0.274  ...    77.0      93.0      37.0   1.01
pu        0.717  0.013   0.700    0.741  ...    80.0      93.0      93.0   1.01
mu        0.190  0.022   0.157    0.223  ...    22.0      24.0      59.0   1.07
mus       0.292  0.042   0.222    0.371  ...    76.0      48.0      88.0   1.04
gamma     0.457  0.064   0.337    0.576  ...    88.0      73.0      49.0   1.03
Is_begin  0.997  0.773   0.028    2.402  ...    93.0      94.0      60.0   1.01
Ia_begin  1.829  1.979   0.012    5.778  ...    34.0      99.0      20.0   1.01
E_begin   0.777  0.794   0.026    2.650  ...    98.0      80.0      93.0   0.98

[8 rows x 11 columns]