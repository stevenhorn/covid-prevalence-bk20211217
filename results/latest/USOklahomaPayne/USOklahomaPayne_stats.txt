0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2728.62    45.31
p_loo       42.76        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      587   92.6%
 (0.5, 0.7]   (ok)         34    5.4%
   (0.7, 1]   (bad)        11    1.7%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.167  0.014   0.151    0.196  ...   152.0     152.0      49.0   1.04
pu        0.705  0.005   0.700    0.716  ...   152.0      87.0      40.0   1.01
mu        0.106  0.016   0.075    0.130  ...    67.0      65.0      58.0   1.02
mus       0.176  0.029   0.128    0.229  ...   115.0     112.0      91.0   1.00
gamma     0.251  0.039   0.191    0.322  ...   109.0     121.0      58.0   1.07
Is_begin  0.700  0.590   0.008    1.893  ...    54.0      69.0      59.0   1.00
Ia_begin  1.250  1.464   0.025    4.187  ...    86.0      67.0      96.0   1.01
E_begin   0.462  0.472   0.015    1.156  ...   102.0      98.0      93.0   1.00

[8 rows x 11 columns]