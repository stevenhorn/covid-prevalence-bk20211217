0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2024.94    39.44
p_loo       51.34        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      587   92.6%
 (0.5, 0.7]   (ok)         40    6.3%
   (0.7, 1]   (bad)         6    0.9%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.161  0.009   0.150    0.179  ...   119.0      93.0      59.0   1.01
pu        0.704  0.004   0.700    0.712  ...    29.0      37.0      59.0   1.04
mu        0.109  0.012   0.090    0.131  ...    19.0      18.0      35.0   1.09
mus       0.196  0.027   0.153    0.243  ...    74.0      78.0      59.0   1.03
gamma     0.312  0.047   0.237    0.401  ...   152.0     152.0      81.0   1.02
Is_begin  1.313  1.028   0.068    3.086  ...   109.0      74.0      60.0   1.03
Ia_begin  3.059  2.582   0.116    7.385  ...    92.0      63.0      59.0   1.03
E_begin   1.810  1.540   0.075    4.807  ...    66.0      62.0      71.0   1.02

[8 rows x 11 columns]