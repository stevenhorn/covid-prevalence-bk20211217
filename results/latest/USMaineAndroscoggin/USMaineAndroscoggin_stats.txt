0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2359.42    34.98
p_loo       35.48        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      581   91.6%
 (0.5, 0.7]   (ok)         45    7.1%
   (0.7, 1]   (bad)         8    1.3%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.211  0.036   0.151    0.271  ...    51.0      55.0      40.0   1.10
pu        0.725  0.015   0.702    0.752  ...    29.0      39.0      69.0   1.06
mu        0.162  0.022   0.114    0.196  ...    11.0      12.0      45.0   1.14
mus       0.195  0.030   0.157    0.263  ...    31.0      34.0      18.0   1.05
gamma     0.248  0.035   0.185    0.307  ...    84.0      75.0      54.0   1.05
Is_begin  0.804  0.679   0.025    1.889  ...    85.0      75.0      22.0   0.99
Ia_begin  0.626  0.534   0.009    1.743  ...   123.0     148.0      61.0   1.01
E_begin   0.503  0.497   0.002    1.247  ...   118.0      95.0      60.0   1.01

[8 rows x 11 columns]