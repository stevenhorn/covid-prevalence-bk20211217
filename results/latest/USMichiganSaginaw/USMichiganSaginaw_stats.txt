5 Divergences 
Passed validation 
2021-12-12 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 638 log-likelihood matrix

         Estimate       SE
elpd_loo -3119.11    41.22
p_loo       55.32        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      152   23.8%
 (0.5, 0.7]   (ok)          6    0.9%
   (0.7, 1]   (bad)         3    0.5%
   (1, Inf)   (very bad)  477   74.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.160  0.008   0.151    0.167  ...     3.0       3.0       3.0   3.16
pu        0.706  0.002   0.701    0.707  ...    11.0       6.0      15.0   2.73
mu        0.107  0.008   0.100    0.123  ...     3.0       3.0       2.0   2.67
mus       0.159  0.013   0.146    0.175  ...     4.0       5.0      15.0   2.83
gamma     0.282  0.013   0.257    0.287  ...    36.0       4.0      16.0   2.82
Is_begin  1.484  1.501   0.134    3.201  ...     3.0       3.0       2.0   3.16
Ia_begin  2.243  1.754   0.715    4.140  ...     3.0       3.0       2.0   2.64
E_begin   0.769  1.049   0.408    1.252  ...    13.0       3.0       2.0   2.73

[8 rows x 11 columns]