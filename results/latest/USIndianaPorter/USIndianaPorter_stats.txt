0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2762.87    75.52
p_loo       48.64        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      585   92.3%
 (0.5, 0.7]   (ok)         40    6.3%
   (0.7, 1]   (bad)         8    1.3%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.177  0.022   0.150    0.215  ...   152.0     105.0      80.0   1.00
pu        0.707  0.007   0.700    0.723  ...    48.0     152.0      61.0   1.06
mu        0.108  0.013   0.088    0.132  ...    39.0      41.0      86.0   1.01
mus       0.224  0.030   0.183    0.289  ...   129.0     149.0      76.0   1.00
gamma     0.384  0.060   0.296    0.505  ...    60.0      81.0      60.0   1.02
Is_begin  0.961  0.828   0.015    2.703  ...   152.0     152.0      40.0   0.99
Ia_begin  1.892  1.910   0.008    6.013  ...   151.0     117.0      60.0   1.00
E_begin   0.983  1.155   0.016    3.562  ...   113.0     117.0      85.0   1.02

[8 rows x 11 columns]