0 Divergences 
Failed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 633 log-likelihood matrix

         Estimate       SE
elpd_loo -2845.99    47.17
p_loo       40.10        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      601   94.9%
 (0.5, 0.7]   (ok)         27    4.3%
   (0.7, 1]   (bad)         4    0.6%
   (1, Inf)   (very bad)    1    0.2%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.156   0.005   0.150    0.168  ...   152.0     152.0      62.0   1.01
pu         0.702   0.002   0.700    0.706  ...   152.0     147.0      73.0   0.98
mu         0.086   0.013   0.063    0.109  ...    43.0      42.0      54.0   1.02
mus        0.179   0.020   0.143    0.212  ...   152.0     152.0      81.0   1.01
gamma      0.425   0.068   0.289    0.544  ...   152.0     152.0      91.0   1.02
Is_begin  16.530  10.485   2.287   31.381  ...   152.0     104.0      91.0   1.00
Ia_begin  32.483  20.143   1.076   70.589  ...   152.0     152.0     100.0   1.00
E_begin   16.636  10.316   0.035   33.687  ...   135.0     120.0      52.0   1.00

[8 rows x 11 columns]