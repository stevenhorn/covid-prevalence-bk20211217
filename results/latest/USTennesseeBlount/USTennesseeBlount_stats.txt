0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2992.10    63.24
p_loo       70.18        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      573   90.4%
 (0.5, 0.7]   (ok)         43    6.8%
   (0.7, 1]   (bad)        13    2.1%
   (1, Inf)   (very bad)    5    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.164  0.014   0.150    0.194  ...   152.0     152.0      43.0   1.00
pu        0.704  0.004   0.700    0.712  ...   152.0     121.0      91.0   1.00
mu        0.086  0.016   0.065    0.120  ...     8.0       9.0      14.0   1.19
mus       0.203  0.039   0.140    0.269  ...    61.0      72.0      39.0   1.04
gamma     0.268  0.046   0.192    0.367  ...    61.0      59.0      60.0   1.00
Is_begin  0.920  0.669   0.028    2.180  ...    98.0     123.0      77.0   1.03
Ia_begin  1.228  1.016   0.018    3.139  ...    37.0      44.0      44.0   1.05
E_begin   0.534  0.637   0.004    2.207  ...    27.0      50.0      19.0   1.02

[8 rows x 11 columns]