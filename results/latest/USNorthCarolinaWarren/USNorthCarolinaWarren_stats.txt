0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1754.08    45.88
p_loo       48.73        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      594   93.7%
 (0.5, 0.7]   (ok)         32    5.0%
   (0.7, 1]   (bad)         5    0.8%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.245  0.056   0.151    0.332  ...    44.0      42.0      36.0   1.03
pu        0.746  0.021   0.710    0.791  ...    34.0      32.0      93.0   1.05
mu        0.118  0.023   0.077    0.161  ...    10.0      11.0      48.0   1.18
mus       0.193  0.042   0.130    0.272  ...    68.0      70.0      60.0   1.00
gamma     0.252  0.055   0.178    0.386  ...    23.0      19.0      86.0   1.08
Is_begin  0.498  0.513   0.007    1.611  ...   117.0     101.0      58.0   0.99
Ia_begin  1.132  1.330   0.005    3.229  ...   103.0     152.0      59.0   1.00
E_begin   0.477  0.527   0.007    1.262  ...   104.0     104.0     100.0   1.01

[8 rows x 11 columns]