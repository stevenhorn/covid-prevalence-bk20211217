0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1656.40    37.09
p_loo       41.33        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      587   92.6%
 (0.5, 0.7]   (ok)         35    5.5%
   (0.7, 1]   (bad)        12    1.9%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.206  0.042   0.152    0.272  ...    91.0      76.0      50.0   1.14
pu        0.722  0.015   0.700    0.750  ...    83.0      67.0      21.0   1.00
mu        0.131  0.019   0.097    0.164  ...    12.0      12.0      40.0   1.14
mus       0.173  0.030   0.110    0.220  ...   104.0      97.0      88.0   1.01
gamma     0.218  0.034   0.155    0.265  ...    40.0      36.0      58.0   1.05
Is_begin  0.558  0.573   0.003    1.737  ...   140.0      89.0      60.0   0.99
Ia_begin  1.085  1.467   0.005    4.728  ...    66.0      57.0      36.0   1.04
E_begin   0.502  0.751   0.005    1.577  ...    54.0      57.0      57.0   1.05

[8 rows x 11 columns]