0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -2627.50    46.23
p_loo       37.17        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      583   92.2%
 (0.5, 0.7]   (ok)         44    7.0%
   (0.7, 1]   (bad)         3    0.5%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.160  0.010   0.150    0.185  ...   152.0     111.0      39.0   1.01
pu        0.703  0.002   0.700    0.707  ...    68.0      52.0      74.0   1.05
mu        0.089  0.012   0.070    0.110  ...    44.0      38.0      86.0   1.04
mus       0.148  0.024   0.111    0.198  ...    80.0      87.0      75.0   1.01
gamma     0.225  0.038   0.161    0.278  ...   130.0     127.0      88.0   0.99
Is_begin  0.750  0.778   0.017    2.502  ...    39.0      23.0      88.0   1.09
Ia_begin  0.632  0.590   0.005    1.651  ...    72.0      25.0      49.0   1.08
E_begin   0.496  0.513   0.000    1.397  ...    48.0      22.0      31.0   1.07

[8 rows x 11 columns]