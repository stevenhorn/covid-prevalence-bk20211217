0 Divergences 
Passed validation 
2021-12-09 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 635 log-likelihood matrix

         Estimate       SE
elpd_loo -2383.27    30.52
p_loo       43.91        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      578   91.0%
 (0.5, 0.7]   (ok)         48    7.6%
   (0.7, 1]   (bad)         9    1.4%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.225  0.049   0.150    0.306  ...    29.0      29.0      43.0   1.04
pu        0.719  0.017   0.700    0.751  ...    39.0      42.0      25.0   1.03
mu        0.127  0.019   0.095    0.166  ...     6.0       7.0      60.0   1.26
mus       0.202  0.033   0.142    0.248  ...    87.0      93.0      59.0   1.00
gamma     0.320  0.038   0.241    0.379  ...   106.0     102.0      88.0   0.99
Is_begin  0.939  0.625   0.040    2.083  ...   118.0      85.0      38.0   1.06
Ia_begin  2.029  1.364   0.024    4.750  ...    64.0      67.0      24.0   1.02
E_begin   2.254  2.054   0.027    6.108  ...    67.0      54.0     100.0   1.04

[8 rows x 11 columns]