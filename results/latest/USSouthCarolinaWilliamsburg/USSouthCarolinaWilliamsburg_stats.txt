0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -2035.60    34.73
p_loo       33.85        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      589   93.2%
 (0.5, 0.7]   (ok)         33    5.2%
   (0.7, 1]   (bad)        10    1.6%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.182  0.028   0.150    0.242  ...    75.0      66.0      56.0   1.00
pu        0.710  0.009   0.700    0.729  ...    57.0      87.0      51.0   1.06
mu        0.118  0.023   0.068    0.157  ...    16.0      16.0      29.0   1.07
mus       0.163  0.027   0.111    0.216  ...    85.0      87.0      56.0   1.01
gamma     0.258  0.035   0.210    0.332  ...    61.0      51.0      58.0   1.03
Is_begin  0.531  0.711   0.006    2.121  ...    55.0      70.0      39.0   1.00
Ia_begin  0.971  1.049   0.001    3.103  ...    93.0     117.0      86.0   0.98
E_begin   0.471  0.790   0.002    1.478  ...    96.0     122.0      58.0   0.99

[8 rows x 11 columns]