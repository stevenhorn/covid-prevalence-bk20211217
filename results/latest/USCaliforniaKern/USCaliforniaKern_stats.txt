0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -3902.32    39.67
p_loo       51.84        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      583   92.0%
 (0.5, 0.7]   (ok)         46    7.3%
   (0.7, 1]   (bad)         4    0.6%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.174  0.020   0.152    0.208  ...    29.0      25.0     100.0   1.06
pu        0.707  0.006   0.700    0.721  ...    68.0      34.0      43.0   1.06
mu        0.126  0.028   0.081    0.174  ...     4.0       4.0      24.0   1.69
mus       0.185  0.028   0.138    0.235  ...    11.0      12.0      54.0   1.13
gamma     0.251  0.049   0.160    0.324  ...     9.0       9.0      56.0   1.19
Is_begin  0.604  0.437   0.046    1.428  ...    71.0      48.0      59.0   1.01
Ia_begin  1.221  0.959   0.050    2.849  ...    68.0      54.0      39.0   1.03
E_begin   0.831  0.890   0.016    2.360  ...    75.0      79.0      60.0   1.02

[8 rows x 11 columns]