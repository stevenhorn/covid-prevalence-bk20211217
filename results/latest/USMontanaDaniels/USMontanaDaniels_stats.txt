0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo  -722.59    42.79
p_loo       55.87        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      596   94.0%
 (0.5, 0.7]   (ok)         30    4.7%
   (0.7, 1]   (bad)         6    0.9%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.195  0.032   0.150    0.251  ...    63.0      41.0      40.0   1.01
pu        0.713  0.010   0.700    0.728  ...    79.0      63.0      83.0   1.02
mu        0.154  0.031   0.102    0.217  ...    41.0      36.0     100.0   1.02
mus       0.238  0.031   0.193    0.302  ...    63.0      68.0      88.0   1.01
gamma     0.306  0.046   0.235    0.389  ...   110.0     103.0      93.0   1.00
Is_begin  0.413  0.427   0.004    1.405  ...    84.0      53.0      60.0   1.02
Ia_begin  0.677  0.856   0.002    2.362  ...    64.0      46.0      60.0   1.05
E_begin   0.304  0.308   0.003    0.902  ...    49.0      65.0      79.0   1.03

[8 rows x 11 columns]