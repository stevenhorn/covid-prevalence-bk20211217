0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo  -501.98    72.41
p_loo       63.53        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      578   91.2%
 (0.5, 0.7]   (ok)         42    6.6%
   (0.7, 1]   (bad)        12    1.9%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.252  0.062   0.160    0.338  ...   152.0     152.0      84.0   1.00
pu        0.826  0.024   0.781    0.862  ...    31.0      43.0      70.0   1.05
mu        0.154  0.034   0.089    0.204  ...   152.0     134.0      75.0   0.99
mus       0.240  0.044   0.156    0.306  ...    41.0      37.0     100.0   1.05
gamma     0.275  0.050   0.193    0.376  ...    72.0      93.0      81.0   1.01
Is_begin  0.590  0.548   0.003    1.633  ...   136.0     101.0      60.0   0.99
Ia_begin  1.014  1.156   0.002    3.540  ...   111.0      86.0      56.0   1.01
E_begin   0.458  0.506   0.002    1.436  ...   132.0     152.0      60.0   0.99

[8 rows x 11 columns]