0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 633 log-likelihood matrix

         Estimate       SE
elpd_loo  -509.35    69.58
p_loo       86.87        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      576   91.0%
 (0.5, 0.7]   (ok)         39    6.2%
   (0.7, 1]   (bad)        13    2.1%
   (1, Inf)   (very bad)    5    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.254  0.052   0.173    0.348  ...    84.0      94.0      24.0   1.12
pu        0.795  0.040   0.718    0.856  ...     4.0       5.0      59.0   1.47
mu        0.170  0.022   0.121    0.205  ...    32.0      33.0      35.0   1.02
mus       0.308  0.071   0.206    0.450  ...     6.0       6.0      36.0   1.32
gamma     0.352  0.071   0.237    0.464  ...     8.0       8.0      56.0   1.22
Is_begin  0.699  0.619   0.006    2.140  ...    78.0      37.0      59.0   1.05
Ia_begin  0.446  0.447   0.002    1.415  ...    37.0      16.0      38.0   1.10
E_begin   0.338  0.434   0.000    1.281  ...    76.0      36.0      31.0   0.98

[8 rows x 11 columns]