0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 633 log-likelihood matrix

         Estimate       SE
elpd_loo -3814.00    59.14
p_loo       31.59        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      597   94.3%
 (0.5, 0.7]   (ok)         26    4.1%
   (0.7, 1]   (bad)         9    1.4%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.255  0.053   0.158    0.331  ...   100.0      99.0      60.0   1.00
pu        0.792  0.019   0.751    0.824  ...    59.0      58.0      93.0   1.02
mu        0.132  0.022   0.092    0.175  ...    62.0      59.0      59.0   1.03
mus       0.178  0.033   0.129    0.247  ...    95.0     102.0      95.0   1.12
gamma     0.222  0.043   0.156    0.297  ...    52.0      94.0      72.0   1.06
Is_begin  0.505  0.464   0.003    1.351  ...    51.0      50.0      29.0   0.99
Ia_begin  1.064  0.758   0.017    2.677  ...    86.0      70.0      59.0   1.09
E_begin   0.567  0.575   0.011    1.851  ...    80.0      64.0     100.0   1.02

[8 rows x 11 columns]