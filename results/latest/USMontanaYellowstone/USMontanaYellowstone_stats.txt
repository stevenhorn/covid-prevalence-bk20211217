0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2853.34    33.78
p_loo       23.48        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      600   94.6%
 (0.5, 0.7]   (ok)         30    4.7%
   (0.7, 1]   (bad)         4    0.6%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.161  0.007   0.151    0.172  ...    74.0      68.0      60.0   1.02
pu        0.704  0.004   0.700    0.713  ...    94.0     152.0      56.0   1.01
mu        0.104  0.017   0.073    0.134  ...    12.0      13.0      32.0   1.12
mus       0.141  0.030   0.092    0.194  ...    72.0      70.0      60.0   1.03
gamma     0.146  0.032   0.116    0.244  ...     7.0       7.0      21.0   1.26
Is_begin  0.490  0.381   0.045    1.179  ...    69.0      74.0      59.0   1.02
Ia_begin  1.005  0.846   0.024    2.748  ...    62.0      51.0      25.0   1.06
E_begin   0.530  0.557   0.012    1.901  ...    73.0      65.0      59.0   1.01

[8 rows x 11 columns]