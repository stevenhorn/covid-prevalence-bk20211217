0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -1413.10    45.58
p_loo       47.97        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      588   93.0%
 (0.5, 0.7]   (ok)         36    5.7%
   (0.7, 1]   (bad)         7    1.1%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.202  0.033   0.153    0.258  ...   117.0     124.0      72.0   1.00
pu        0.717  0.014   0.701    0.746  ...   152.0     117.0      38.0   1.03
mu        0.120  0.019   0.092    0.149  ...    34.0      36.0      59.0   1.02
mus       0.181  0.029   0.134    0.229  ...   152.0     152.0      72.0   0.98
gamma     0.296  0.044   0.207    0.375  ...   138.0     140.0      29.0   1.02
Is_begin  0.579  0.548   0.007    1.668  ...    76.0      72.0      43.0   1.03
Ia_begin  1.029  1.007   0.004    3.391  ...    95.0      61.0      42.0   1.02
E_begin   0.434  0.560   0.002    1.092  ...    87.0      70.0      62.0   1.02

[8 rows x 11 columns]