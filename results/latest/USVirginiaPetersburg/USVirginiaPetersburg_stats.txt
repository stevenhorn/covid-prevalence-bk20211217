0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -2127.97    53.91
p_loo       56.01        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      572   90.5%
 (0.5, 0.7]   (ok)         48    7.6%
   (0.7, 1]   (bad)        10    1.6%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.200  0.036   0.150    0.264  ...    51.0      56.0      81.0   1.03
pu        0.718  0.011   0.702    0.737  ...    14.0      17.0      91.0   1.09
mu        0.124  0.021   0.090    0.169  ...    10.0       9.0      37.0   1.19
mus       0.180  0.027   0.134    0.231  ...    32.0      33.0      96.0   1.06
gamma     0.215  0.039   0.136    0.285  ...    62.0      62.0      59.0   1.05
Is_begin  0.607  0.535   0.022    1.476  ...    96.0      78.0      60.0   1.01
Ia_begin  0.965  1.091   0.020    2.413  ...   103.0      89.0      38.0   1.02
E_begin   0.407  0.457   0.000    1.268  ...    37.0      66.0      31.0   1.08

[8 rows x 11 columns]