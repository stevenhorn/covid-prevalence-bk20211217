0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2033.20    48.21
p_loo       49.95        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      580   91.5%
 (0.5, 0.7]   (ok)         43    6.8%
   (0.7, 1]   (bad)        10    1.6%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.160  0.009   0.150    0.178  ...   152.0     143.0      24.0   1.01
pu        0.703  0.003   0.700    0.711  ...   123.0     137.0     100.0   1.02
mu        0.095  0.012   0.076    0.117  ...    27.0      22.0      57.0   1.06
mus       0.180  0.033   0.110    0.228  ...    31.0      30.0      32.0   1.11
gamma     0.268  0.033   0.207    0.326  ...    18.0      18.0      88.0   1.09
Is_begin  0.600  0.519   0.008    1.745  ...    77.0      78.0      27.0   1.03
Ia_begin  1.053  1.240   0.011    4.192  ...   104.0      76.0      60.0   1.02
E_begin   0.384  0.430   0.016    1.114  ...    42.0      15.0      63.0   1.13

[8 rows x 11 columns]