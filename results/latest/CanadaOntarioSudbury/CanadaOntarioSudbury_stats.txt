14 Divergences 
Passed validation 
2021-12-16 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 643 log-likelihood matrix

         Estimate       SE
elpd_loo -1851.51    45.92
p_loo       49.45        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      591   91.9%
 (0.5, 0.7]   (ok)         41    6.4%
   (0.7, 1]   (bad)        10    1.6%
   (1, Inf)   (very bad)    1    0.2%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.295   0.039   0.207    0.348  ...   116.0      97.0      54.0   1.02
pu         0.889   0.009   0.872    0.899  ...   104.0      89.0      58.0   1.01
mu         0.197   0.025   0.144    0.230  ...     5.0       6.0      24.0   1.33
mus        0.195   0.037   0.134    0.260  ...    90.0     152.0      43.0   1.07
gamma      0.264   0.051   0.191    0.358  ...    67.0      42.0      59.0   1.06
Is_begin   4.114   4.188   0.029   11.050  ...    56.0      72.0      40.0   1.01
Ia_begin  13.338  13.461   0.211   37.197  ...   106.0     102.0      59.0   0.98
E_begin    5.824   9.040   0.047   13.524  ...    93.0     100.0      59.0   1.02

[8 rows x 11 columns]