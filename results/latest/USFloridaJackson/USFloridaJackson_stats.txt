2 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2667.00    47.48
p_loo       45.35        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      600   94.6%
 (0.5, 0.7]   (ok)         23    3.6%
   (0.7, 1]   (bad)        10    1.6%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.157  0.007   0.150    0.168  ...    84.0      58.0      59.0   0.98
pu        0.703  0.002   0.700    0.706  ...    96.0     106.0      93.0   0.98
mu        0.109  0.016   0.088    0.139  ...    24.0      26.0      47.0   1.06
mus       0.158  0.034   0.106    0.227  ...    45.0      41.0      60.0   1.03
gamma     0.212  0.047   0.146    0.308  ...     8.0       7.0      16.0   1.28
Is_begin  0.525  0.658   0.007    1.792  ...    61.0      56.0      87.0   1.05
Ia_begin  1.316  1.446   0.006    4.249  ...    53.0      49.0      59.0   1.03
E_begin   0.415  0.520   0.001    1.385  ...    49.0      17.0      46.0   1.10

[8 rows x 11 columns]