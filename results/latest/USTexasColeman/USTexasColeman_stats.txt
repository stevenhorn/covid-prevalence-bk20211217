0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -1501.70    60.91
p_loo       62.42        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      597   94.5%
 (0.5, 0.7]   (ok)         23    3.6%
   (0.7, 1]   (bad)         8    1.3%
   (1, Inf)   (very bad)    4    0.6%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.174  0.018   0.151    0.207  ...   131.0      72.0      97.0   1.03
pu        0.707  0.007   0.700    0.719  ...    64.0      31.0      23.0   1.04
mu        0.126  0.018   0.100    0.165  ...    92.0     108.0     100.0   1.00
mus       0.228  0.036   0.174    0.286  ...   114.0     125.0     100.0   1.00
gamma     0.321  0.049   0.248    0.422  ...    76.0      83.0      96.0   0.99
Is_begin  0.576  0.641   0.006    1.668  ...     9.0      10.0      16.0   1.20
Ia_begin  0.727  0.836   0.004    2.537  ...    67.0      58.0      57.0   1.04
E_begin   0.251  0.271   0.000    0.743  ...    48.0      19.0      22.0   1.09

[8 rows x 11 columns]