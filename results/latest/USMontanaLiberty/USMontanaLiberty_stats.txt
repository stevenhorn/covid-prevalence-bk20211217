0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo  -685.85    46.41
p_loo       60.69        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      590   93.1%
 (0.5, 0.7]   (ok)         27    4.3%
   (0.7, 1]   (bad)        15    2.4%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.253  0.061   0.159    0.349  ...    96.0      82.0      22.0   1.04
pu        0.782  0.023   0.743    0.835  ...    29.0      37.0      22.0   1.09
mu        0.151  0.029   0.108    0.202  ...    42.0      44.0      59.0   1.03
mus       0.248  0.036   0.181    0.301  ...    87.0      88.0      59.0   1.03
gamma     0.345  0.050   0.224    0.417  ...   152.0     152.0      44.0   1.00
Is_begin  0.665  0.600   0.003    1.957  ...   106.0      84.0      42.0   1.03
Ia_begin  1.114  1.185   0.009    3.348  ...    92.0      34.0      93.0   1.09
E_begin   0.484  0.680   0.000    2.188  ...   106.0      61.0      15.0   1.03

[8 rows x 11 columns]