0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1895.65    41.92
p_loo       43.22        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      588   92.7%
 (0.5, 0.7]   (ok)         37    5.8%
   (0.7, 1]   (bad)         8    1.3%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.160  0.009   0.150    0.175  ...   152.0      96.0      59.0   1.02
pu        0.703  0.003   0.700    0.710  ...    95.0      74.0      48.0   1.00
mu        0.092  0.014   0.066    0.116  ...    48.0      47.0      31.0   1.01
mus       0.179  0.029   0.134    0.236  ...    74.0      84.0      67.0   0.99
gamma     0.297  0.046   0.221    0.367  ...   116.0     116.0      84.0   1.01
Is_begin  0.532  0.475   0.011    1.588  ...    89.0      73.0      59.0   1.00
Ia_begin  0.871  1.112   0.001    2.986  ...    75.0      46.0      22.0   1.05
E_begin   0.456  0.437   0.005    1.377  ...    74.0      77.0      77.0   0.99

[8 rows x 11 columns]