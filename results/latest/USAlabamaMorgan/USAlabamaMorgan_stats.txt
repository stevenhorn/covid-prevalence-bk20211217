0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2736.96    97.52
p_loo       74.59        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      581   91.6%
 (0.5, 0.7]   (ok)         43    6.8%
   (0.7, 1]   (bad)         8    1.3%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.159  0.011   0.150    0.175  ...   152.0      97.0      14.0   1.04
pu        0.703  0.004   0.700    0.708  ...   135.0     111.0      60.0   1.01
mu        0.056  0.007   0.043    0.067  ...    43.0      46.0      49.0   1.02
mus       0.258  0.035   0.203    0.329  ...    76.0      92.0      56.0   1.06
gamma     0.373  0.056   0.275    0.498  ...    90.0      82.0      59.0   1.02
Is_begin  0.514  0.513   0.003    1.440  ...    73.0      87.0      28.0   1.02
Ia_begin  0.562  0.592   0.003    2.009  ...    53.0      49.0      40.0   1.04
E_begin   0.293  0.313   0.000    0.904  ...    91.0      60.0      38.0   1.00

[8 rows x 11 columns]