0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1997.76    73.65
p_loo       61.21        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      601   94.8%
 (0.5, 0.7]   (ok)         28    4.4%
   (0.7, 1]   (bad)         2    0.3%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.159  0.008   0.150    0.176  ...   121.0     102.0      37.0   1.03
pu        0.703  0.003   0.700    0.709  ...   119.0      84.0      38.0   1.05
mu        0.102  0.011   0.082    0.121  ...    37.0      37.0      40.0   1.04
mus       0.259  0.035   0.213    0.341  ...    29.0      21.0     100.0   1.09
gamma     0.395  0.070   0.263    0.513  ...   116.0     109.0      19.0   1.01
Is_begin  0.587  0.458   0.036    1.502  ...    38.0      38.0      60.0   1.05
Ia_begin  1.053  1.078   0.000    3.306  ...    31.0      70.0      23.0   1.03
E_begin   0.444  0.489   0.011    1.581  ...    67.0      74.0      60.0   1.00

[8 rows x 11 columns]