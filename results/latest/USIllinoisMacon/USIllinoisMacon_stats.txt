0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2555.17    40.37
p_loo       38.96        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      582   91.8%
 (0.5, 0.7]   (ok)         40    6.3%
   (0.7, 1]   (bad)         9    1.4%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.162  0.013   0.150    0.189  ...   120.0      64.0      22.0   1.04
pu        0.704  0.003   0.700    0.711  ...   147.0     152.0     100.0   0.98
mu        0.110  0.012   0.090    0.134  ...    65.0      63.0      91.0   0.99
mus       0.160  0.021   0.122    0.198  ...    98.0      99.0     100.0   0.98
gamma     0.306  0.055   0.228    0.406  ...   110.0     108.0      93.0   1.03
Is_begin  0.648  0.619   0.008    2.045  ...    83.0      96.0      72.0   0.99
Ia_begin  1.196  1.195   0.008    3.214  ...    51.0      55.0      43.0   1.01
E_begin   0.611  0.562   0.013    1.603  ...    71.0      64.0      60.0   1.00

[8 rows x 11 columns]