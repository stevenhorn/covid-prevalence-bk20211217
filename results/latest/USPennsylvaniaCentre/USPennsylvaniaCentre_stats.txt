0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2437.77    42.57
p_loo       43.46        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      578   91.2%
 (0.5, 0.7]   (ok)         46    7.3%
   (0.7, 1]   (bad)         7    1.1%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.167  0.013   0.150    0.193  ...   149.0     152.0      96.0   0.98
pu        0.705  0.004   0.700    0.714  ...    96.0      77.0     102.0   1.01
mu        0.128  0.015   0.100    0.149  ...    63.0      61.0      60.0   1.01
mus       0.196  0.027   0.150    0.249  ...   114.0     121.0      63.0   1.01
gamma     0.319  0.051   0.237    0.406  ...    81.0      74.0      59.0   1.00
Is_begin  0.923  0.926   0.008    2.485  ...   139.0      98.0      69.0   1.00
Ia_begin  2.028  1.885   0.000    6.117  ...    44.0      24.0      22.0   1.08
E_begin   0.884  0.932   0.010    3.213  ...    65.0      50.0      57.0   1.05

[8 rows x 11 columns]