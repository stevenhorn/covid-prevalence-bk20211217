0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo  -724.18    40.66
p_loo       47.64        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      591   93.2%
 (0.5, 0.7]   (ok)         32    5.0%
   (0.7, 1]   (bad)         9    1.4%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.259  0.055   0.166    0.340  ...    66.0      71.0      60.0   1.03
pu        0.772  0.026   0.721    0.809  ...    47.0      56.0      40.0   0.99
mu        0.156  0.031   0.103    0.206  ...    21.0      24.0      58.0   1.06
mus       0.224  0.031   0.175    0.286  ...    46.0      55.0      48.0   1.04
gamma     0.251  0.041   0.176    0.321  ...    76.0      77.0      48.0   1.04
Is_begin  0.565  0.565   0.028    1.516  ...   104.0      71.0      59.0   1.02
Ia_begin  1.074  0.995   0.014    3.427  ...    90.0      96.0      91.0   1.03
E_begin   0.479  0.395   0.009    1.269  ...    72.0      95.0      45.0   1.00

[8 rows x 11 columns]