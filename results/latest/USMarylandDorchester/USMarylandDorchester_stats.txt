0 Divergences 
Passed validation 
2021-12-09 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 635 log-likelihood matrix

         Estimate       SE
elpd_loo -1857.80    42.53
p_loo       47.58        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      584   92.0%
 (0.5, 0.7]   (ok)         38    6.0%
   (0.7, 1]   (bad)        11    1.7%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.183  0.024   0.151    0.225  ...    60.0      52.0      43.0   1.01
pu        0.713  0.010   0.701    0.728  ...    62.0      63.0      59.0   1.03
mu        0.129  0.016   0.101    0.162  ...    28.0      28.0      43.0   1.01
mus       0.188  0.024   0.146    0.235  ...    44.0      52.0      86.0   1.01
gamma     0.276  0.047   0.197    0.352  ...    54.0      51.0      60.0   1.00
Is_begin  0.699  0.748   0.010    2.157  ...   109.0      90.0      76.0   1.00
Ia_begin  1.103  1.398   0.003    3.167  ...    99.0      30.0      36.0   1.11
E_begin   0.541  0.506   0.026    1.587  ...    84.0      76.0      96.0   1.01

[8 rows x 11 columns]