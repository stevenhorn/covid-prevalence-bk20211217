0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1819.81    97.28
p_loo       53.98        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      598   94.3%
 (0.5, 0.7]   (ok)         26    4.1%
   (0.7, 1]   (bad)         9    1.4%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.157  0.006   0.150    0.169  ...   107.0      89.0      59.0   1.00
pu        0.703  0.002   0.700    0.707  ...   152.0     152.0      74.0   1.04
mu        0.094  0.014   0.068    0.119  ...     8.0      10.0      33.0   1.20
mus       0.269  0.033   0.205    0.324  ...    90.0      98.0      80.0   1.03
gamma     0.617  0.110   0.411    0.829  ...   152.0     152.0      48.0   1.18
Is_begin  0.489  0.500   0.008    1.552  ...   129.0     129.0      95.0   1.01
Ia_begin  0.731  0.692   0.023    2.170  ...   114.0     152.0     100.0   1.00
E_begin   0.337  0.378   0.003    1.004  ...   121.0     129.0      58.0   0.98

[8 rows x 11 columns]