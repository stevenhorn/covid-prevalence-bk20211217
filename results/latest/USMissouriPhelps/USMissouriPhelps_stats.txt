0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -2246.56    93.39
p_loo       64.96        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      594   93.7%
 (0.5, 0.7]   (ok)         30    4.7%
   (0.7, 1]   (bad)         7    1.1%
   (1, Inf)   (very bad)    3    0.5%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.161  0.010   0.150    0.182  ...   135.0     152.0      40.0   1.02
pu        0.703  0.003   0.700    0.709  ...   152.0     122.0      59.0   1.01
mu        0.089  0.010   0.071    0.106  ...   130.0     131.0      88.0   1.06
mus       0.311  0.038   0.242    0.368  ...   141.0     152.0      61.0   1.00
gamma     0.614  0.101   0.434    0.794  ...   101.0      97.0      69.0   1.03
Is_begin  0.559  0.481   0.009    1.298  ...   102.0      97.0      61.0   1.00
Ia_begin  0.552  0.645   0.004    1.845  ...   117.0      97.0      96.0   1.00
E_begin   0.274  0.328   0.004    0.893  ...    97.0      94.0      61.0   1.01

[8 rows x 11 columns]