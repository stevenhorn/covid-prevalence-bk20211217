0 Divergences 
Failed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -1279.40    74.96
p_loo       78.40        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      590   93.4%
 (0.5, 0.7]   (ok)         31    4.9%
   (0.7, 1]   (bad)         6    0.9%
   (1, Inf)   (very bad)    5    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.159  0.008   0.150    0.175  ...   104.0      76.0      88.0   1.00
pu        0.704  0.004   0.700    0.712  ...   152.0     152.0      36.0   1.00
mu        0.143  0.023   0.107    0.191  ...    48.0      66.0      54.0   1.05
mus       0.288  0.039   0.212    0.357  ...   152.0     152.0      22.0   1.10
gamma     0.397  0.058   0.315    0.517  ...   114.0     128.0      93.0   1.00
Is_begin  0.325  0.452   0.003    1.359  ...   108.0     106.0      65.0   1.02
Ia_begin  0.394  0.451   0.008    1.159  ...    81.0      87.0      60.0   1.02
E_begin   0.177  0.223   0.000    0.660  ...    79.0      47.0      43.0   1.04

[8 rows x 11 columns]