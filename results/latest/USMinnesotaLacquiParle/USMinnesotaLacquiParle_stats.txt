0 Divergences 
Passed validation 
2021-12-09 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 635 log-likelihood matrix

         Estimate       SE
elpd_loo -1255.96    42.12
p_loo       41.28        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      579   91.2%
 (0.5, 0.7]   (ok)         46    7.2%
   (0.7, 1]   (bad)         8    1.3%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.195  0.030   0.151    0.243  ...    83.0      67.0      22.0   1.08
pu        0.713  0.011   0.700    0.734  ...    67.0     106.0      60.0   1.02
mu        0.118  0.018   0.092    0.155  ...   105.0      96.0      58.0   0.99
mus       0.179  0.031   0.123    0.229  ...   152.0     152.0      80.0   1.12
gamma     0.226  0.043   0.152    0.291  ...   106.0     130.0      59.0   1.01
Is_begin  0.471  0.520   0.008    1.370  ...    45.0      31.0      60.0   1.06
Ia_begin  0.720  0.867   0.010    2.033  ...    89.0      66.0      57.0   1.01
E_begin   0.305  0.299   0.004    0.901  ...   103.0      61.0      57.0   1.00

[8 rows x 11 columns]