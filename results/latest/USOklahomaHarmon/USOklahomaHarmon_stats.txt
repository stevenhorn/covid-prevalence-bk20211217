0 Divergences 
Failed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1066.58    58.88
p_loo       76.43        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      588   92.7%
 (0.5, 0.7]   (ok)         31    4.9%
   (0.7, 1]   (bad)         8    1.3%
   (1, Inf)   (very bad)    7    1.1%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.253  0.057   0.167    0.347  ...   117.0     132.0      69.0   1.00
pu        0.746  0.026   0.702    0.787  ...    86.0      86.0      58.0   1.00
mu        0.149  0.031   0.097    0.200  ...    61.0      63.0      99.0   1.05
mus       0.245  0.041   0.177    0.320  ...    40.0      57.0      85.0   1.05
gamma     0.287  0.058   0.184    0.397  ...    93.0      90.0      60.0   1.02
Is_begin  0.471  0.586   0.001    1.704  ...    67.0     109.0      40.0   1.00
Ia_begin  0.742  1.061   0.005    2.542  ...   104.0     118.0      60.0   0.99
E_begin   0.359  0.468   0.003    1.347  ...    98.0     108.0      60.0   1.01

[8 rows x 11 columns]