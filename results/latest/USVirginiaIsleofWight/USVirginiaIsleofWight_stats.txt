0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -1860.73    34.99
p_loo       42.32        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      593   93.8%
 (0.5, 0.7]   (ok)         35    5.5%
   (0.7, 1]   (bad)         2    0.3%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.245  0.063   0.151    0.340  ...   113.0      88.0      43.0   1.03
pu        0.750  0.029   0.700    0.793  ...    39.0      41.0      39.0   1.05
mu        0.134  0.019   0.103    0.174  ...    46.0      19.0      72.0   1.09
mus       0.179  0.029   0.132    0.245  ...   152.0     152.0      81.0   0.98
gamma     0.250  0.048   0.160    0.328  ...   152.0     152.0      97.0   0.99
Is_begin  1.173  0.903   0.020    2.898  ...   138.0     141.0      60.0   1.03
Ia_begin  2.497  2.101   0.018    5.985  ...   102.0     108.0      77.0   0.99
E_begin   1.420  2.061   0.063    3.619  ...    93.0      69.0      60.0   1.00

[8 rows x 11 columns]