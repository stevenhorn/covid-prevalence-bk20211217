0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1817.05    46.01
p_loo       46.39        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      596   94.0%
 (0.5, 0.7]   (ok)         26    4.1%
   (0.7, 1]   (bad)         8    1.3%
   (1, Inf)   (very bad)    4    0.6%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.161  0.011   0.151    0.184  ...    65.0      47.0      59.0   1.03
pu        0.704  0.004   0.700    0.709  ...   102.0     104.0      91.0   0.99
mu        0.090  0.014   0.066    0.116  ...    52.0      26.0      44.0   1.08
mus       0.161  0.036   0.096    0.229  ...   152.0     152.0      79.0   0.99
gamma     0.219  0.037   0.159    0.298  ...   118.0     105.0      52.0   1.03
Is_begin  0.540  0.573   0.001    1.396  ...    84.0      41.0      61.0   1.04
Ia_begin  0.982  1.110   0.005    3.124  ...    91.0      74.0      60.0   0.99
E_begin   0.384  0.442   0.005    1.209  ...    65.0      65.0      60.0   1.05

[8 rows x 11 columns]