0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -2483.48    49.25
p_loo       45.09        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      593   93.8%
 (0.5, 0.7]   (ok)         30    4.7%
   (0.7, 1]   (bad)         8    1.3%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.170  0.016   0.150    0.200  ...    93.0      80.0      25.0   1.04
pu        0.706  0.005   0.700    0.715  ...    70.0      68.0      60.0   1.03
mu        0.106  0.016   0.080    0.135  ...    19.0      21.0      59.0   1.13
mus       0.154  0.028   0.103    0.202  ...    85.0      92.0      95.0   1.01
gamma     0.232  0.033   0.158    0.280  ...    77.0      46.0      60.0   1.05
Is_begin  1.017  0.794   0.031    2.407  ...    71.0      67.0      60.0   1.04
Ia_begin  2.521  2.274   0.003    5.853  ...   114.0      65.0      30.0   1.02
E_begin   0.993  0.801   0.053    2.388  ...    37.0      59.0      96.0   1.03

[8 rows x 11 columns]