0 Divergences 
Passed validation 
2021-12-06 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 632 log-likelihood matrix

         Estimate       SE
elpd_loo -2831.80    69.17
p_loo       46.83        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      607   96.0%
 (0.5, 0.7]   (ok)         19    3.0%
   (0.7, 1]   (bad)         4    0.6%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.182  0.007   0.172    0.194  ...     3.0       3.0      37.0   1.98
pu        0.713  0.003   0.707    0.717  ...     4.0       4.0      30.0   1.63
mu        0.211  0.019   0.187    0.234  ...     3.0       3.0      55.0   2.12
mus       0.248  0.023   0.216    0.276  ...     3.0       3.0      14.0   2.03
gamma     0.307  0.017   0.267    0.333  ...    26.0      26.0      42.0   1.04
Is_begin  1.022  0.682   0.245    2.078  ...     3.0       4.0      32.0   1.86
Ia_begin  2.121  0.676   1.052    3.538  ...     4.0       4.0      14.0   1.51
E_begin   0.695  0.196   0.264    0.944  ...     3.0       4.0      14.0   1.75

[8 rows x 11 columns]