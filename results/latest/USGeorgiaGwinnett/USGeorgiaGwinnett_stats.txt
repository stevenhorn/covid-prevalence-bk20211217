0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -3851.49    57.21
p_loo       66.70        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      588   92.7%
 (0.5, 0.7]   (ok)         38    6.0%
   (0.7, 1]   (bad)         6    0.9%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.198  0.038   0.151    0.263  ...    12.0      11.0      43.0   1.16
pu        0.718  0.015   0.700    0.746  ...     7.0       7.0      56.0   1.27
mu        0.104  0.017   0.078    0.138  ...    21.0      21.0      33.0   1.16
mus       0.193  0.036   0.126    0.257  ...    63.0      92.0      72.0   1.02
gamma     0.291  0.049   0.205    0.376  ...    42.0      48.0      73.0   1.03
Is_begin  4.035  2.921   0.073    9.459  ...   100.0      94.0      59.0   0.99
Ia_begin  9.986  6.356   0.668   20.921  ...    52.0      50.0      44.0   1.02
E_begin   7.788  7.073   0.100   20.851  ...    60.0      51.0      60.0   1.03

[8 rows x 11 columns]