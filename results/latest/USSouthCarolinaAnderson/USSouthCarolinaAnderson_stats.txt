0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -3092.98    42.87
p_loo       35.55        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      596   94.0%
 (0.5, 0.7]   (ok)         28    4.4%
   (0.7, 1]   (bad)         9    1.4%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.169  0.018   0.150    0.199  ...   152.0     152.0      93.0   0.99
pu        0.706  0.005   0.700    0.717  ...   119.0     101.0      80.0   1.01
mu        0.099  0.015   0.071    0.126  ...    57.0      52.0      88.0   1.02
mus       0.140  0.027   0.094    0.183  ...    91.0      96.0      88.0   1.01
gamma     0.226  0.036   0.157    0.281  ...   152.0     152.0     100.0   1.02
Is_begin  0.776  0.783   0.008    2.105  ...   120.0     106.0      88.0   1.04
Ia_begin  1.238  1.307   0.014    4.228  ...    48.0      87.0      58.0   1.01
E_begin   0.451  0.468   0.007    1.032  ...    79.0      66.0      93.0   0.99

[8 rows x 11 columns]