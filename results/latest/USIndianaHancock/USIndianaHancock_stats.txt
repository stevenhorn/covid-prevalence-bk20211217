0 Divergences 
Passed validation 
2021-12-09 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 635 log-likelihood matrix

         Estimate       SE
elpd_loo -2375.16    41.63
p_loo       41.50        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      586   92.3%
 (0.5, 0.7]   (ok)         41    6.5%
   (0.7, 1]   (bad)         7    1.1%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.172  0.018   0.151    0.203  ...    22.0      14.0      24.0   1.14
pu        0.705  0.005   0.700    0.715  ...    23.0      23.0      60.0   1.09
mu        0.106  0.017   0.069    0.133  ...     6.0       7.0      24.0   1.29
mus       0.156  0.028   0.119    0.228  ...    76.0      87.0      87.0   0.98
gamma     0.268  0.039   0.196    0.330  ...   101.0      97.0      95.0   1.00
Is_begin  1.191  0.985   0.066    3.141  ...    78.0      60.0      59.0   0.98
Ia_begin  3.263  2.561   0.051    7.961  ...    60.0      48.0      35.0   1.04
E_begin   1.769  1.429   0.055    4.202  ...    63.0      44.0      32.0   1.00

[8 rows x 11 columns]