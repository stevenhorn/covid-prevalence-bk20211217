0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1686.73    37.63
p_loo       39.86        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      600   94.6%
 (0.5, 0.7]   (ok)         25    3.9%
   (0.7, 1]   (bad)         8    1.3%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.179  0.024   0.152    0.230  ...   136.0     144.0      29.0   1.01
pu        0.707  0.006   0.700    0.720  ...    86.0      65.0      34.0   1.02
mu        0.115  0.018   0.090    0.157  ...    40.0      43.0      59.0   1.01
mus       0.178  0.031   0.125    0.234  ...   124.0     110.0      86.0   1.03
gamma     0.252  0.047   0.185    0.348  ...    70.0      98.0      87.0   1.02
Is_begin  0.787  0.846   0.001    2.486  ...    78.0      42.0      20.0   1.01
Ia_begin  1.314  1.294   0.027    3.977  ...    80.0      70.0      61.0   0.99
E_begin   0.622  0.693   0.003    2.086  ...   108.0      75.0      93.0   0.99

[8 rows x 11 columns]