0 Divergences 
Passed validation 
2021-12-08 date of last case count
covid_prevalence version: 0.3.0

Computed from 80 by 634 log-likelihood matrix

         Estimate       SE
elpd_loo -1990.32    40.29
p_loo       46.33        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      590   93.1%
 (0.5, 0.7]   (ok)         36    5.7%
   (0.7, 1]   (bad)         6    0.9%
   (1, Inf)   (very bad)    2    0.3%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.160  0.008   0.150    0.175  ...    70.0      68.0      58.0   1.00
pu        0.704  0.004   0.700    0.714  ...   152.0     152.0      54.0   1.00
mu        0.095  0.016   0.069    0.130  ...     6.0       6.0      24.0   1.36
mus       0.158  0.028   0.109    0.206  ...    92.0      92.0      40.0   1.05
gamma     0.272  0.053   0.193    0.373  ...    75.0      94.0      60.0   1.01
Is_begin  0.880  0.639   0.035    2.136  ...   123.0     147.0      97.0   0.99
Ia_begin  1.859  1.678   0.021    5.732  ...    74.0      81.0      56.0   1.01
E_begin   0.949  0.858   0.012    2.736  ...    70.0      67.0      60.0   1.00

[8 rows x 11 columns]